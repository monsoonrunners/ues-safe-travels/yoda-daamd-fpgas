onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib audio_blk_ram_opt

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do {wave.do}

view wave
view structure
view signals

do {audio_blk_ram.udo}

run -all

quit -force
