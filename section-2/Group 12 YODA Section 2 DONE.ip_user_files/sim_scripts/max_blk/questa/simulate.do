onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib max_blk_opt

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do {wave.do}

view wave
view structure
view signals

do {max_blk.udo}

run -all

quit -force
