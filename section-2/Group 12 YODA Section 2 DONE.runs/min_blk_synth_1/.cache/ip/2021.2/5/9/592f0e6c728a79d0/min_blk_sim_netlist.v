// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Tue May 17 16:19:15 2022
// Host        : Xronos running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ min_blk_sim_netlist.v
// Design      : min_blk
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "min_blk,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [5:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [5:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [5:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [5:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "6" *) 
  (* C_ADDRB_WIDTH = "6" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.53845 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "min_blk.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "64" *) 
  (* C_READ_DEPTH_B = "64" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "64" *) 
  (* C_WRITE_DEPTH_B = "64" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[5:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[5:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 19344)
`pragma protect data_block
WemhoqDHOg1ltjpH+QfFJ27S/tAQlx8rlOOFr+kYSpUPSpAOoiwnCpoUg8vNpRdB5/myQOcTjJZf
TGlWWSKzMY8wN29oxl/lqQXJpwSO4eNGqoUdGcvySV1dYGbay8xJAKXb7+Ae6viozCSXLKg2lNIB
YGRx7VdmFpe8lPwgYPHaAlG3emUcUzl074j+2sn1WtSd8h8fzi0zEANwSK9xKQ936MKpJ06njvAl
A7BoKGr2ketOYVWfxc6Tn11XuwaeKAP1oMhWihYwB+GEV8P8vd42QgkRglPCw47EV/IQMxO4CnU9
qGP7HkfCsf/+kJkJ2bbT3YpzZ8Z8i/OyBiQScPV08wz3x5fhtjLc5ezoRUzyh2XttSYO8R/EaMQQ
wS/l1roVhZY7o6zHzVYx42eJNomljS1ZBgQ3Fs43cVpqRvJDo7u7oy6bITMGc+FVS6FoZQm000ji
mQ5wa2E0BVNlnmV32ZfAU/PjkNwsW4b08vH35DYiZOfKZ+ugtFB1mMtZEytO10/Kp8Eh2q+VcwJe
Fj2VQ1q16bO0gyozziahlvruncmqLyyjZVfRW0RkEhz6CI3jCGUuLZBd55YMTRJauEFvs4mJto1e
CRcqH1m24tQUfFqPqrFAqyFeAPYXSvP+4NR+Ufe2IA07AzPN6TmfbG+alJW5mMz15gFEaBXVWKRl
1lsfDPjA5DGjDqNd9ABRZTTkmcELgGt18FiLGDPpRIQ4j4HTXdpfKE6ThvJfGa2LcO8DmGgvVn9u
DMixXme8fItoky/W+8tJ4xNaRAonO1LY7hIdnj0UeIx4aUb2wrTxOmbFzA4BewB4vH4gObRA1+RX
SJ697Aq/ggXh1ricyr1l0m55SS6vkVu4aThmghU6tvN82J9BLXeOlA+n5fgKh3eK9bl58TBYs4Bt
GJrX8MWpIXcjrcclJ2GBTFaT5Eso2u2ws8LkQ0ajJlqhzhzfFNznxItER6dzB4EHT2TwFEoU6fxJ
MUyTgKWrrVrdwpQXOl1GZbOZNW6cSSeqJyolOB3BnoA4mS+sdtYNMSDjE8+RKAlf8AGHIVDc0yDY
sBdIcGtyRu4OmdbJov1ugSm+A9T2G/wDs7V2IR3v5E6yaD0fX0UXP3Zep4ApYRRlv631S5RMH3sO
mTuqm9BO2ui3eh+8bSnZKFit6uV5giXW30fPfOVogNaNOFRKn3kZQSWNnr/clZXDUpVjjOybgzfq
o0mHh7hR+27MIVn+53Hq0kRbtUEf9hgb9TE5QFIWnJBVQWe0xTewLCbNhngRy1N6MHyPlGXcFqpR
20d6GXc+e18M/reGZPYGGhXQKZd3Ui81+fnOYSoy41+yVnpUa8MwLSPzP03k7hywV8C9G7kqZ48f
vNFMZdRl3BuL11DzjINumrJ8JECieKnwqmyazzJcMKuNvIpTXOZIyQYVxDIXt3Y6ZNczgXqlFtaQ
gwQEKJA8MA+FSfGcUH74kGIazM4yU4ZehDWDwxINxb+5OU44zaMx3/nNwhZ9WKkI6jRrihrwYxt9
Cxy4bQPsYGopkd1vT1FMwmYIWJsuRp3Io3k7H4tkDD1/TmEZ91sldr5dselZHjcVBFwSdP6W9QVP
9Hc2Sslp4e2wh5G4gFkssikkfyEBORKoTWKId+USRXp3HkZFuYtPUG2Nr0MyKA8P/BxrXFevqls9
its8dp/QH3KjLy54ygQBturPO+pqpKpZnzm6A7himKYa4QZy6TiuoheKXY2QO9Jr08CNVho80l04
BXbi20vAkxP7K0AAXoZ55YIdTM98f6gvpRMDW+aHp1KCbi67FBq2cZFqODcunRv+ZQIVY9VeujY6
S9SGBoBoWF52tUMY1LvQTCQjpDNaowyXMwprKvxngoChjZRcZnzIOo5bpNj3rqRW3W+wiYdjoBkb
4WEpeEawdexnDsRWVdXpxaRb9N3MBqUq5rW3pYgGewlYSC3wr2DQAtZKbux+R13QwY0UnCrItGJR
lTtFmwBmSD/jPi7PsfBpiDwuoILO54nc7SpxeLgANjlzpOBOluno8xWbuch2upfSlT7pTQophuDy
Ewei1OD24K1cEqva4aYJlkW0RfAbmh088zEd2GuV+DPmr7ObSpl05MzeWqGLhIjm+jeZM9HZNFop
zQaQpWeTmX0wwejLp7z1cAoufOXsfWd856fRw4guxDmSwJuYBNzLoAeHYhNAi0mp8/mRxj4fV+mh
nHibYJ5ZSK7hEiWk8V4T9rn19lIzVwK45fdmOCmp5hlcSfCPC/DnhIllySD0IG7h76zWt/Qqxnsu
lG5fPQA5TKjp0wXu+VWpRuFjnS+cCQvkIPhzBGnhTUVABzfl6rJpEfVbksZ2rVAG8OX/+b+t5xiM
CDuqddrNtCVaRMVKAsQDzL2rCiU8y3prE/sWtY+AcmErgxtYd8XOXSKryQKpPIPrUXoLT5D03OHj
IO5wWXoshTLNX26mFpr7r4i27O85NLUpOlnoiht7Yt6x+PN9wZg3a99omLwQl/iDV1Yem2tjKN8b
zE/l38ZxBJSqSCR3P2362XLpIjgUHakufk5hMR+UwG9XbZLIlcObZktEhVl/A1vlhW0JFPJ55QQi
gMDKGdr7O5MtV3oMWsPrnkdGlE1yPp1jrPulkwTIyJ8Px3JRKk/YjoTKmvo/Sz/ISg9Bf/JmdV64
kIOMG4iRlvUuox2TF/nzqaFR0IWxCRgc7bJt/s4XfW5FI843Lv6jwXTF3w/UDiEfqtwpHPCxydEZ
Rh6OvoKl0xSprDPVHpc4IDr7KLbNmZXc+RARIKne5T7Q4yMPc8Zn21R3jt9SS4BsN6rwwRhnWqUM
1OtBdgF+m+5V/P3x8V6OQrqa1YZd7G2Waj9tR9QDa9H8hRb2AVAT87+rxuS7r+Jy7JQk5NreVLUS
398VY/Tv1+ppAv3/wE3a8fyetOZ6l27jHIrebmHPSyVNow5Zn3SKvsrY28zFvJxg5jxg+7kFiAKu
+bSD2rKiXmBJO90Z2isWjwX6cE1FM/WlnCsijtXJuV0NRjsXaL4NF/e7pAQdfi3UUXmB3RThsB80
ywTCsVTOGhrtgGYR6Osay2txpOj1fgGn4qiUziV+Z0PGszv+RnBjjmLJ/F+YphcidfsWHfxiEthS
1O1ou+a69pwBdW78YrUDjPdRBIiJiv3D753JrEFb6YnF+ouGoRH/a/u77zXYE8BnyTzG4EZDEd0m
YCHpjT+DeOdQ9hG/i6+ljMz81cToYQFNYEOVuudDpBklsSNAFxCJDjTg0PiNdkHgExLeRnBvRvlG
psML6GZWfiIHQJWxMnT9qPdUcgh59emumoaG4GsRE03zXHM6qh/KCddS9qip4F7lFBy5O7gDzn13
apQxiDECmJymBcUtGCLckYLxf+RYR3TsszYCMVEWT0JN8iIj5nd4bEbzQVIvhcXJez0dXDW5BTo8
GwEgChLaYo/vJIrr+xSTFB2evpRdFdZeOb9whzdGXDSTtBUuRN5zh+BME7hFw667OnfHFhuK30KK
0LSI8Pvsx6lIPcNUKZwqnVX64yDkTwEmfYVgI8/yD6IQRJffptHGquOvJII47bXIoM2Jho0v/+PU
65p97W4fwpTUUXi0cZJbhqWjFt9ROOjOWghb6Q5M1kv6ivqEXKqoW5JhCZkU5DHssGKGeau9YcM4
euUyDUsi1SZIJx3YqsaukRYWo5B4PkZe1SZOlgW4lX/zAtV9abs+ovSVlF9OuZgL8SVrpvj8WIDl
Amlv/yN21L7O7N8NWZY9eQcKnZHYxXgorO74fLssZBJWSTBp40pNgo1ff+M8VxFvrRByDkofXmFU
rdPBbPrWXVJGzjDcs44cHHUXlvqa3Km/rv44+vpnQPhdRBDUHjDoMI7f+dX0ellSsvDmmgAcKoc4
5GCPPAzsDKnRQDH23c6Y3eB/qa80Yy8Cn5s6iadum+StlHYELZpxpwqm5QDfFUEDJW8xT5XvMtc9
MwHLqxHHQxdAp1eHcYs9GWb4jA3Kj9OjKV97k6skT5buhKkcMJ15CRHv9CpaT6B70tLeG3Ugrctb
uxFTVBSDo2a9OIjfzyhQqpvS7pbrtir7MSmoJagC6pxfRTP7ib+J+rbCwDJ8aY0MO0azpOkq7aeA
EB+dOto+7cMP2fJq9R5UAcl4596hUI+w5LS2t0+/at0ZCPHVJmyq7VqrkJjrgvrgD39a3GOX7Z/5
TP8dOsDX3yOScSlrL0EhVyGbe+KL/yWJwbZ6vbL2IS+lIQYimao9cD3jVQMNgbB9eB+drmiOGl5t
7dgA3HMsihp6gsY0tt7RPpV8grzK37Ej1eNdxqDIvbWS+0LE7Wd/rb9FhXb+a7trIKEd5VF1Vsez
92jRobdqq4UxK8PFydQW6MqzpV0/BnFAFm6F9Z39vAVWwu6SPJ9rBc0fk8EwhjbO9CE3PmecCdG+
FjT8gTVcII6HyDZXMtlPKuff/iaEL9IJXaH5bBJvkCwJib1TCMwZ7OVFmikp1QAJ4FuTsSrhumgW
KCCoWLAleJKuuSr8OCp9+6tYapFA694lluVXCf03fjWvidcsxsu40rMscSIUdAZIKzHr4xGkzet/
Zi8xU2bOW4XlTUshduqfX9bXvaHKAyiT+pG3j7qgIOEczQllGBU/OZZ+0Gua/rYDZ/9azOxfTQkk
X0Czfu+LZdXYRv/Q6nPPrDAWoQtTbdHt/J5SIO8nKupawe4iYNGqMu0XcQvQSQagzhr6B2MHKIuZ
zBshra4/Qo9iUfFptBTVOozDHtwLHS+nNUfXpIOyJdevECLfYR3Gk2o58Z1QZz/7UHPg9vjLsyuW
yFzpkrY8zTUOAK9GruiAG1E/JysTqS4iUbdUHSIRYtMpxYdDM43ZBzkfs2Fhs/Z/kZZyf8cd4wIA
MwrXYa7myrbOuyJ6EC/zvdmeePOmv4tgS7v+IiRlW0wiDe060qCD9JH5Iwou6+PPIO7I7tR2H2Lf
HnloGFvAokVFPzcwBWq32hEWnB3v6qq+Gy+WuAwI6zb0XTB7W9TqzEp3QcE0CYLqLgJLXMIqyht6
4jl7pCGLr0jrQD6lrNmGET3an+dkZEdRjBsGZ1ljB+TMjPvSK24Y5Y3aEgYl8i4DmzqifBdasoqM
Rs29HzFb4X4iQCRT3ff37gar2PDVhieZv87njJ1ItHoH+Rjb0HI98WmFAUklazDUAV/a3+FT493r
UsjTNrQOWTg1HxD2AYAREO529hV48IpMTOemTYtoZunwwWPhvsItJTvyXJslMtuXKD4GJyHGqOzc
nNPgJjypIh3/74tPNQDdW5GAUm9bLjSKEW00S85gFrwzTAkj+obBB0szos0zQtC74MhT3ObO6/u3
JJPKRunIIxx3BG9be7CBzP3ch6RPzVO1figW2uyrEROIGzqptoeIye2sWyBfKtTnaNDRi4th8lE5
TA/Xa5in6x2RY2K9aGDhxZX+yauY3xUmNc4M/QGB4dnzaJbLvxqZvfFj4Q0nKgNp3TMzrV4fz4fA
vZYlPQm2iWLLfsBIFxIqTRVsTGdELcM7rN2eHuUyB1ENSYOr4A40aUUscdeDoecwSV5M2xHjAX3y
IRfUS3y1phnxiUGXaxH+wMU5OqXUQ5CcrP3ckT2pRK1sixZUfFlmazTWTknj66K2a6ozQy89jBIS
x6tXe46yQ3f8D/H0B6L9yYqxAuLdN/aTRoJ+9hGSfne3HME3S8ohsyyHAgm3hD/rLQ1Q52oupx2X
PbHSXXETlRPOPZFRfyxkmVyc266Ojs/KUGHe0siMbMWQ/7fR4Dp/B5uY6qpIG8ykoKS9s+qNsdVr
Jc3Q4ZvIygqcFGmZ7rMje0Oi3PT2lVp9zt5U/NtRtWPbLhlyGCVfmnDfcpV52YLX2BONYbs8kUQo
QfTWa4TK0ioDLi7xSj+jfrm9hLkFw1xE+5uNszkKWVrActU8IDX9G6dXT40yQ1biMjqDihjFpYey
9B4joSPo33cQz4Q2otKJEcTzEpRK97yE5OBa+/u516jzTDjDcmfy/GBdoGyJ/gF+tMrQkl2XFhdM
bT+hRrpUr0DjoQThlJC35mPid3eLauakgVOo1PtetXQ7R88KOUxTqBXAA0KubOjpGJD8UMZyvxXA
//Eyb4xCth37RGr2SZYAb62TC7wSPbAh4M1nhV5Pv67QUBm6R4nGJt5QuFvru9I0fEPNinu5rUWm
Q1gPGNNwiDlf1X9m9RVFLT2qUyoDPrPsGII21jYI2AiKtVbTcGDEqyREncCMesYYqcoQcsLgeBja
FITGSZOJsdDNdcMPN5mn6KaiRUizMUVeuueqqcKfw99DkN0c59GLmz2ZBkOXtE1Z7asTfU7ABZ/w
2SG4u4+83JYZPSVQjBKw8RFRTzo5RxNixzRnpF1A4r7T9Jb4kkDbFN96Nq9U8WdPeNtqqrJJ86Le
tn6wODxMwc2gT4uvjDEdeFBybessO9UnUw1msZNSVMxC1wb//PKIBpGlhx/3a+rd+chaIkmxYLox
5VXgCrbXg1IbHbWjaZ52qyiN8MYEhU7POp8ZpA6RgfQb+E3DCrPx3+DXQ7twvZMXPeovytbGhwwT
s35Uf8HiKtEeTLszL9PR/2QnSdZjnXr2lHiDzzOGWWTL4rqq7miE6tHHJqmRRatpSoCZbnZNk68d
DiFZqlwOGi8WeAGQkxL/cG7+ESOW4J50675wIy/dRlzZnBB3BykOk7Ev4WgVsSamFtj19WDBkK5G
OPssdlVZhMNPl09fL+/VP0fX873vQPffovRpfylW3GcNg5HPX8S+tHcJsdkKx/v8+FXDAscXMaic
rxSfkhZOUDpreqPofZhH3gipo35F86ArGIqX+kxMfoR3OdtqvqhJXCYoUxQ14lZKkRnqCKZO3TtH
DVLVcpQUheCUQdxH1dx42uuCmk6khzLWMNmNhcHzMu/dI8LUVIa2bQ2vjs7m5Cv9BcA7PiqOtJIk
u/OqiXsbV8zb3v90/HiMdJzlhSWWTk12x8v1K0bsmLYEbhZCRGXgPIil0xTnOeREr0kI8/n+g1M1
rl87dVwqPcjqvzqQTFt9Styd2ThL7Je0+9Dn/Oi+2Fmjd4iOH0jiLG3+D0uOipCkdaEr5acsZoEH
Lvm4tcgbu4mlLFAasg6uiPKq7IhUHlJ6jn6Zpa7itcgYvEWkhFX8baHdqxXDtZMFmoPfJMBDuH5F
ocLmdDhVd+syskSi0SpAmGJ+3E4MJhdu54AJJx6/VEDLbx5XbQWu2ksyAGZD9h/1bwiOpsqqcQZc
bJOFFroyhZ2gWN2ZPBJ+/V4OO1Hra4VmkBWIyWTR+NtD3LxCqUvEZHEzTP65zdFetlCodonsZDRB
BZEy/U9eV2XtA2ZUVK+B9+4xrWddfdIAt0G4zru5ao2i+KcQVu7eHn9dkLx4p4hPH3DCqcY59OfM
uSlmYTncyO+PdR6WAYpoFyIvry66Sn36BZ07CSbR8DreDl454uhVDo0zp4SNujuAQ0bjwnTb7DAQ
w5s534aSh/J4CgA7J1//eD1UvQR9uIzktYXtQNUI5fleafR9cb5E0vv4OIkSn1Ti54I0c3iH9PiQ
fvX0PnUTzKbs0zQJcUJocoPJbIbncjgSY4KT/LkAH+KiW+eLJGcWoFk3dt/Dmh4ncram4hJBKlkT
QfgzW2ff1HCs1ekvW82I8qFDCioUKQ7u+y5PJLMTXcfq3nLH8a4k0JJPvV4edXYst1OpeMAZsxYe
2zRVlskEH9zFOyNAabr4TaWtl/3QcdAuiml2/mTcRvX6w+1XSCVtNMoOU+w7u43J3gi56RujPAxs
idRZKh6LBCfioJWbXZawpiNtwsqui/X3L0RzgFWPOx9Ff/zREmy+7ZpB0v/OFvB8EnBdu+nWxrxQ
x9O12wUpEv4C7EcS9mdUxeglvNCThZusCgOZOfEaz73tvSL7Los2t6lqfkND8YSbEG7P/dhVCv4y
G3pDV8uA9w/qNwmgGMBjm/gyEF+R9AgWiC9mByJnH9EAE+FjAvdKJ1rU9r4id9UEcqYiA9ezldbp
UHd/gHiPFM6KCsFIlRsFuLf7uFyvYJsFkM9W2fUSn44fL+dnmRnew5IRMVxchUlid2207F4l5Yjf
nYyDzY9Ko+421Ra0/qlRGhuFRPoujStHhEcHA3awX7CJlfimLcO6cA+hrovNdSLUzD8tqfSGmsTg
zJcYf544XQX9yVEjIIUI+JUG0N/0/5xYGVE0EBnlqqR1cJuOmosX3jd+tmEy0RoKqPZcPmZ9WdYc
cNUApfbsEaSVePhVwU+4JwWtxkfahqeZfIH9aq8hgk9OmCloFQeJWfgpkvwCFhyfYDGabtr372H0
H+/BmMs59Yqzl+rcdCN+LnJDXtAbWZynvFng3mGUWXdxlm2yFTYR6pF/RMOvb/TQndLyn+A37hHH
sZOrSoJ0FvFifXxjeLc83WdWTXrh6r5QPeZi5KXpiR8Y8Hzw8e8KDOwJS/p1U6IivLJxOdk9Wl/d
EhuB9i/cLr1S1yYcim5qtudRzK6RyPwdheAm7kbAJ7N467f1k79EFdynTzIfJ1hHHo7kHP/hqH26
1i7kEo3P4W1cdQQvPEYhaz0s328IEqDpEbTi0gqUIHEnIM/zm0HyZJaPJdUaJSI2mko1maIU9GUo
J2TED+e5zcPty/fQ89NGPBi/hPgab5qjsZZ+UrNopEIWOnucIRhYebEBOs5qIFhgJVaalA+UIL+J
vqZzUBla01lMeGevNWBN9hHEJbqpFU5OHR2GQYAO0ON9M5uZgKQqCn9A8PmNf8WxM+mRqTPlh/b4
wGUX6Qop/cm8YubBIA2r2KcD8iMaP94qwTnAiODQi4m2ReD0dwYrJCeprxcRkb71ys7ez7EJWzHv
7qJk5fTit1qW0128VEjVN9RgMQ2IyKPdrGmd6lrHaS+TXNjRg5cWx3OECuqL8oF5OBAS/dqZVb54
8qI5JQRXV2hC3KB7aOb6Dr2Dk5NdHpnTcYhWuGNycsnnLf6g1qYPrfBDWQOptHu1cWthw0Xwby+A
cjAvBnSiSIq3zUqkidU3xu13qwLXozA7rtiuilREb25uaQVFNX2ALwK/RSnZGq/pBiXUJV9UJTIN
WCIxvNkrMF6dYQZ0yXDujxfIZ1b/9bHJZg45vD65N0xe+TIqODosGWTQ08Vs4wUG72Zk+Hp4ALsb
cIh5i6i5tTV3Onw5CfZq/P1ApOhELTtWbfgXu9W1bvFB93QctYNvPq1jl9hK4g9+d0gxJ2btfoUN
WtXpj1KFNi6YvScdAdnbxB+sg0kTfiqscij7UA5XwMUVZN1izGHmWGiWCcxXHun6igP91ciHlaWi
sLlsp/ekGBrrPkO+cyyohcnmPuV+vgbgkfaVkU48fRNK51CIEygYij3ApzAgGpDGBItSWBZMlmdd
dMLHY6S6rP0gHp36lMYXLayDXfUyhbW1rGp5k6DkZOy2H2u9QQ4FRAkPsN9jZ6rKqAziD4EoEPnX
Lxxd1PQSD0aJ2+d/Oou6xaX9nEv++pBtVm7Sy/Q7zdeM6EF9iPj0TCl+sWkvcKhiq9Hf/TM9X2TE
xFx9ycWZprodwUKyCeOKfzZPRJCB55Nwb8UYnR5flXzja3hTTbSxzlCI/vxBDvMZ0vszmRKt9vl0
VhVLcOGpFa1LqJUuXzvdrBATXEJ6gbRzWEikMXigo5cfMtdNtyMPDwmsFFw4P72W7IZ0SBv9IYCJ
nG8mue3SATWsC/BisBlDjafphBX/o4b4LKk8JRPHfUDGLZ18yfzmrwiQxUaK4oyJ+PeyrVaATkCq
PnyFD/sJtkrM7UTfZBC6fN9HIobUrCFUVVGISQirwuxBFaWEItKg9JUyZCumf5yDCjOc3bXuL+FF
A0qxigWsUl3scn9BTezk2DAG25SzYTyrkJ+eojmkBTsdRCHF3Pdn9afApJEFAyemiuGkviflN4ZF
TQT1UR0UExkX5wRS5HRhko/wXcBLopj3adnuUrSz9Axc5J9TOmwJYNH+pV/hIvuIdV+KrejuMXia
xYKvK+nPhwrIFa5TH/82Lp8CJymCp4F734x2B3avgCecn5j5oDqrZygKdlk5r7HVRe8x1P65Qx0+
cCDmVFQhRVnooDJKZOIee2m8xOED8pXDn4WxOL4GriLXuCXCN34sIAshcO5VXVN8IlwJnHBlK5ya
mC5gfERjc93oKp2mlJjgiGiMNJvCBxVFi0H482TGMhMFEzFNKIW6W3tnpSFnI+LRwsDyrDw80UN8
P3gg41l1HphNzaBMOiuWpw2XE5zl+k9byJtnjxTvSkOziNHftUfe+5BpXm06K+uPpA4Uhc3ImuNy
ceu0mhzhu/TyrxC2DQ+f8+4tmNGnR16BcYMqaEQVGXmwLVctq0PPC1wATPihSWzIRpwbWzpelgZ+
38fVny5zt3ghTZP3AWSW4+XZUsXM+5RQ4J9ZWo0BFBYh0NZ0+d+azLTlg8Ny8/Xf9YddPAgGLE2p
oyVbAVSIONdOIoitVlFl9442dc2QMGzZWYErjrsq3QzuMLbGtSPg7qKo0U/9RmoVlPY4CO7s5vKG
pW1EjCFa62f4LSlKtD9G64TzYmOPby9dTAIH9ONCKRBSQJ7nV/gIrN/qYNOnHzecGsG8+v5rWh3Z
+YPYwZmQBVj5jh6hnyWMfc6n69t2NMxM6rRwQR84c7lz3lsfr+NPjJjPkaZ0FtIq2Y4gRpi4qena
c1PoSSTwMUmOg4e3ZCDoIiMcANrJJDI+0vMI04G6YaDvtU8ElqDP8qo9ABga5qo70cfWnEEQSZNk
aMCPXccJK43Z1RK4Q86rL/st2gFxTPUtvbJyTNaS7m+a9rmZzssA0371qqbo4H5i5Mf1bFZW/We+
Sk1aiRmiC0kuEksxV4jAwPbTfi/2T3hnDWHpk7dgtYCWjKQm+BUGgXz7q2iz327GQyvNgoLkikvt
/FxxKJEWhgLejwTwOgExwNeyHqTDovzQ6bF9Tezel68ts7LBcRJ2rneQGUBlg+YYFdi3T+eqlFew
yLHIlrrs6LGVOnD5O0IDR5NS/c9zaCqllhudBzqkTWSOIXQ5Ok/jc/mOLIMVGfaY0KMnvI+STK8y
rEfvptI82EhUmfnkA3xCXiBpOWJrmXTADiwZ7idm7jWg8tAnsJV6XpZdLgJ+pK1KxMgsVf/IknBh
ISaL3jRi7HzyLO87Mh+H7/Ra6+iv1tH/fIlxMv8SaT8qi9v2394yDCj8V0KKdvQoKY5PwKz8Gr8l
lZe8auV3Mv5acFEE7nTSvZXIiBQqfqGP6Yn/Ghg2e6136ynJHY31BZrHLhRlawjWL5N3d8eepZxF
D0PHfEiCXgjiVGJEulwTjBeMzrtntuY3P9WwHTv7BjRwE9bKVYioFmglhusV1KocH445/Bb5UmDJ
1ByQXQiZH7hUxJQy6PrN2eoYce8d+Pkc2YSb7p61SwMS+r77/SrxDBpcxHslMwQVx3MeZTxu7VjN
TQ6enJXYTmt8bXEuFab8JFWJ5ldx2ZHcekwKN4TGVhUtZKZ0nYlwJlRTjbDIvRdF+fqnpKGZHsyK
CBGak9ePZ80ILEm/g9fGc9r/JDTzn57DKfpgLm//c7B6yHrklIi9ppkW5qN/CqRRcf0bMI83/vRI
qxGWpp+5a59nFirVCtSy0kxN9Er0+Aarx9SErX5ymBfW01dYzvpyd/l+efMhWHiYZiOrOdZjrlAE
UZ2+K/A+px/n2rXZ13a3VRWulPdy24hbAnyIxqjd6GAM0k9wKFO1aFoIN0nQU/HsHCYVzUOci6ul
UwqCvRDj5nnY2Wgpd5TwWmULRVMZjLORBZ8pFuP5KxZ+/YsyVOr1CYRyDnRMnXBqjmFN1bNRPR6L
cYbus0vzbJz4VenvuHTH4AtZN9n4R3kNTqq+ZHaSIKKUHyiiES8mKTw/HAM8MHVJZvtwOqk2FW05
A4LLoE8DniBvgwAdNSSDjGzpI13cGZxWNwLe4G0sJGxtmTRXjeDCKz50mAUN/Z0LIP5lm9A2GDib
T5LLPOdl82wpVs7GxOfimMpf5qGYPeKJg/JsaT77O6d4YruJqDmOERT/TlSvXlVf0mWgKlq20tqS
B9/xHwHRU6FsmjkbwYk2zfR1ayJjpCjFjOA2GUkXFQQznGtBTP6y56DnAFWc8CxtKRbEOQMSL5Mn
XoJNS39OH8aLS2KjnwBF8kaoRZIRZ+LmFBa4LxTylqozgddkbVkNkEFKDjYA/+tgvRZjsMWymNOn
22KOEwLGQsJ6Dl6JSj2zagHaGKVv6f8IEEw7W+lN1eZYdfOsgus0P/Wd0PpGJUzu6+7K65Ej4MaD
l5A+jvzYKpGCl9JjbKY5IjCfIjC4StwUPJfbKzASU8Zn5sbUDgzuZp2P7LzZlZeFxQXmaYOe3fk7
mx89ckKwL8bSwQFzLlI+FTDJd33BblJE7p6L6PHAKnYi178g6XhOquOxmnECde5tiYgzaVbKMAtW
VL5gW4xXgRcD444vZrStkoHUFqY6Als9L2A0EZv28PFHwLzcb2+qBz5EeilLUOc0AEbuhXIu+you
F//RTuQv66R/Cj7jagl9s7JpzR5SVSL1kfaO6gIHb/bF+v5UOSjCe4inXh38pVPmPhYSnalMEUoh
Bs0OwqMBpk86nG3NSDU5VX5BkFGylPm+TA1ZfrfI4vPmGjXtFbUa/5/EnIot0Bq6t+vSmwo52ihf
9xPetA1+iap72daCgKIuXwBp8oRiV1ESvVE+A7i7Su+sj1svETxR8RTqChsRrK8Rc2ySRNMTYEwJ
d2UHWdFSq+ipEE8REcmNFdm91Fe5xalstNY1Ko2LmNtNY4XWgWyhUmLc0+v8u9nqC3mPm5ppXlrJ
1cA7YBaX+xKxhv7hDfwVjAi8T9KKiovDUtfkndiv7ksNjy5RVCGh7rUi2kjoUtfLE4EKE2IA4rq5
yTgN8zl3fEpc1Zq6y/XOR9yi+XKzFFl3+1nJyw0nfSEi3zkdE1n6KOaNaQhSbhUaruy/EGTRuGd+
SLDSzbDIj5JrgDKQ/lgyCEVDm10UMVQ67+SFYj0PIVpVr9Ms5QxgNW9ghsjCauzWdBb7gds4jSSX
YUhvtpJ+iD0WusrgN2VkjkRoS73Oiuyd3AwSh+ItuyfYYS9ckPPeC3r5lmbESyi9z6r0cwRhYhkN
36qegVS3CDYBsOzFh5gyBDDtbAWcx9ijc828lbQrf3EOYWL5pU6HjsSMkwdYGGjTB8+jSQDV+HX2
SwMvbJQr86AyKHIE9X/3YR1xubIECqY70UNARS9IFjgJo6Axb+CVgdBb7pgkFDMSJX2blJI+PcWS
GfqaxUMqDAr7giJxCUehv0pMUGupi9iBK0mzRaxOi3Ja/JaDEhQdPIMyHZ9ilfoB1nCMyjq3cyI7
k3xlFHUDvIYXZ6Fys7j0jXYeWxH37DqrVRPkbk6y4MLjw/GBb0E9HT6k/9nWnVjKQSkrMj8UYm5a
KFQ4sK//IglVCTCE3sV+jA7CaV98/6IjIshAQVF5bocPmvPJp1j/t27ZpTSMkX0uKEvPKpRFegjw
ZAk0Rta9/o4ZyFRWolgkDRTv3mD7XwGMmnCxEHPkhdHZWLCD7xheg2gN4yPdIFojCurD5OXEfYWe
PmuWU7YqLPgVmlR9NVXtlF8QtXdzDfPx7iztStQJ44JzUrpAo8hTyIwWW10zuN454gkNlF9UYkoU
A1W7porWuz+Z1puizSRRW2DYgoD14y61yrFaH0bcNGTSU8jQDkJ4wxSl3Ol7BZQI/38sS6oTLFpv
ughoQ/GLQ1HHMAktjBi/IY2bSkAy+3rPCicsAn8DCBzLbzq2x/9NEezkMWkLmltoBRSqDnzIPsZG
NRvacqX5c6P+ugg7Vi/J0K7hhRJKgf/zVPiPMQxuRKg69cCLI3BZnycJAxHm+mS8vqmT/BWssYye
kIFduvdqaNPfhh9Xci5m5wl1GQeR0KTVp1QFxd9Jh0Y1pA0wE3UAqdiSX1lmf3CygAfkqV/xNCXx
FI9OvFVnU5D8vEXpNH2jVy0lfg0hAlWPpgR/L5CzPqV9TwPp0usZd7mwn1VnIaxLK5XfOpu3+g4e
MlfYcEX2FLHSZIk9AgDkVL4aY6sKc+9diYX7C25yMDRmfDUVsrlZCCPxAfkv7c8CXrahLw1d7a5H
UFJZ2wIooKFGm9X9iyIMhBBmZi4kXbQP1tnfa4BRm769gclpwtMzipdt67JePyELY8OZi11OPXiC
EHv2BW1WoeF6tD6YIx8mUSR87UuAk4ODluF6kbZ+JEJB3Ox422tomj5qBPiOrMefD+N68Ck7u9yD
TLpptNl0uXK6Ek9NTLFRZvV+7DlcLNIMWRvUw91qFMHpcnMi3WfDqDZvh8GVKT307TttW4wVMAcJ
2bnuwnZZ0vzDgHP3VdoUWV+q8coCGwJ5XqeF3V5o4TiPTUqiVndKKp5T6BDQX99wEtaAeRnWXhIm
AH9O836EVFzE3REKd6qWKe2YUjTmjiRgVhCIUWye1AtOb3hLnU6BrgqlDCpj92CdyvtwuuUb65Ob
YgoiXstzdgLOgah1m1NpI/E5Rprcs2ic7iMD9P40Zx/OuM5/UjVcCFFBDDRb2FVoZVWczb82vPr/
g0e0hDVRfcHaz+LwrK0KRcLS4NQffqONruJFhkWXtGIP+bg7q8oJ7fN1e7KIKlKTEc3kI9jrZ0vf
8TCzyevwK+dY9BcPYkj9GlYnR74BJtGSE9KuGrUki8gDDxPk1EeF8yHNlycQTdgUCWs2d6LF5FZq
3pi9muBtGSHtcA2v5I6/WnaiXS2LfTqFNEOO2PQAtvQpz0O8Lyw8oA0s1pkmIS7EM4BDSmU3u3/c
Fvm294EQ0BKGGhWV6yE8g3uEmuyYGC3P/wcz3I+PCenIx5zKPqRUo8+1o0hN5zlw4ystrtIyxmgA
w18wuDPSPkccJuklurx5F7SsQYRWD7E1bKDzvW6z+kGV5obz3WyyFifrXWixLVbp3AA0OQS/pYo+
L9DhbxTVR9fKGDQF0/klazHQzlXTqkL62P5XITjuDL3FXXRnikLjdzbUzxARbO8vGbnu5y6ngwLq
tm/66znTyU/RsXrj3hixFRBGawAc1JEEWH3anDccC5XKVQYGID4dWKfGCECOrbdawbC6Q2liUVuM
eKFy6Dr5VX+earb/I1oo9euxOf1okKnHsMbn3iayJPRFvucpCODwfNUYShLaItAVPIT71waxqg6l
h9GjtM6lJ2e9IefuYShqbjACRL7X94Q+svjWZO+hCz41pDipzFMal5+nU7TdowHgkN6x1r+W5I1g
c95K/PRp49jJJbGxee0gi1b9iqosRi8GKkidvNDsEczt/SaY/58d/DF7tHVYWX0dSpt0FmwMJ1TT
Me+7wcO+BTcRkwBHjUeiRK8YO+H3DSqnS7LwjOtFmHYmQctnYEZAvg3NzweDLkQR+6CEXpZO/9JU
RGwx2AW2naNYBQ9zmx+1AmiGC0gMzS2H8TY898vfy3ufsAl01Wr0MFbd+51onnh06OLd+LcfdWJT
LMJf4PoyR85iUYFlcG3SagzjA/v+nGAdQLRycqysxNn3rHylejTuCk8BT9/QmUldI+NroNVYs0i3
4INMpHhN8nyYUvt6mMNma31hAyysBSkUhRWri+K9WFCrAbNTE7HCEvT+byyFm+ntJXSbSCo/QL7z
yTVpAE9WhgAzaNmQHpo1JcOA8J1eNG0pynQm1ZJThbHRNjhTvFlm9wUnqenI5ngyzY94/8bUIJew
FhoecvxY5dLObopRYuTNJWqseFZPo6EgwOxynZ4E8bXae6LYkzyl/TtXxJObpPLjZdk1+qNuKSlq
v4P5O4dGHE9051rwTWqo+x34u9cpsC41YxtufhKUXXpqIEPV1QkObxYux41ie59xexb8ksEoNuLl
2aDlsRpRVR2aFA1qQm52BTItKLYsq5jeyZ7RJLL0zH+3F8RGffj/vMXEeYVaPW6nSgbDCVCftTXp
OmpdZdEDcQht6Wd1SAVHTCYHuQ6iplraQmZLC4Y81ydXIh3EivLgXTr1bPk0cSVv2V6eZlqmEUp6
qy/AQkuvb2GIr/+Z24ccOE4CZv8X9cY7CWJHDi+FI2BtMS9vWjWBa5JHkdjcD/DUaag0C//i7xYQ
AQN1hBmkwnYkbF1uSyPV2YcV7FNAZ2ovQgDFp5HW7REwsgA7prVWvHTN+3TkAvMun6w+Du+vC8XA
l7Yjp+7eC1ElAPu4i0CvJ/lr2a4+msnAEmZc1UKSDSDyGM0oH8BMupouab+s3KvULaDMsEYgmlwQ
YW/d3oEqGMhwjEWT3QoGIXIjTR3CZZuOhLSrb+A90Yh/r389Ossdvz+g1OZpFEFknU8JhniflQhp
9Vlx/xz2DrN5IKe38MziGoVOUS1l2wGd3U4E20ZXSKqEVxfQccTJ8GSDwPSnJLwihFi2uulTw8eI
ACxuVHsIfqmn/QMzPG1csCM6I5tgU2kl/3WFE00Gou/zJz4+RPjzUweQMPiBwqf3yGDm3VNUt7O0
JgX0xG5vlKzWbx387ZPrGCFEWCLfeTYunUxUqZOsFklclqx2oVIngeWMjOnSDqrD3VUhFxiRY1Wo
UoVW4xZ2jil3NBfuj6t0wp+zaIthG70hRGbnclRsezSlL/7Nh9CaA4MwA8igx5qpIs5+LCb+3pdl
irIeb1CgygBkCzOSd2RNmhuVNshUauDqY4cznvRcHhlX/p1fN7ORFbTaOp69TPCcSbHze5vrJcay
YT3/tPYpi7yodQH17dxfTTyKtTqrbB22jS/GfbiVjh9bOQM5EziDh85B4/1N0Vc9mkjk73sthfgo
3UnN98rCXhnwRPB06AgVeelzuTPdipKC7p6t7qwAmQJMUSvpOXcAK9aDpUqWc3TM2N82d2z1Ce/E
pUe0eDaZFAzPxNqqXNeTNIkGeU/Zl+56uyqZSMdQCsgn6sQE30CdpmJ9VtvP6MKV9OfiWXSEra3x
CV7Lj2O7WnKydMp018DUR1dfBVyHvP+B3vDHrc7+9uEb7ju/QXTuiacy1tCwul+8dW9nQYHH49Mw
VJ1EdlErJ5iAJImahdmgese8yqknoPMGaSDopl2id5G6z5U15ygpecuEhAnve2rITnWgwEbcIQ0U
t2PIEk7wvGL7cPnBozpVtxfpCaIrR4dptlJBFMMM5/9ZJlBcsW7Mp4kwm/J+aJc0duVS7tA+4ziW
1RRQfhazY9WzhtX3dTzcBvFR4NzRNhoYqn9zACH4VWjZMu4usWfdvbJts0vRYD3qgBChZZMaLgUg
4LLk9x1Xzv4V0YuN8e68egaIAIZP+e+jt7+C5zV4uknwAHYlWiGCKCgSQ8dxqrqnqpHvkvs+Xu92
qlcLfsYvMDGYSHCKoPzlsQnzVlwE/TJQM8Cmuw7VUyEh+TW5L8h61XZczDhIVQnlWJB59RW4ofn+
M1/Al6ACbN5YdPvMUHwyqVQSqp8OUrIAZ3lnHHFYckzVhLJGnKfZMSKm0mgzTHJDNNZejzj1X8zJ
gbhak8pHpqFGReC1gBRhidi6Er5xXRw52aueOAUzLv+5irjzAHF/mZpDkkdl+290ye5cQdpZbVAr
FSioIPcQL2KgdNo4efaizkYlnw7RBRUtwt34ApMRB7eCp//ewaQJK+AUeCY2xCco3NLsCihzmvTM
CCnbT5jGkmqzWdtHn8/nCXTCg/UGJWZEhunidLHmzeIW6nMnFLL27u8mdesslPUYXMtOiTvtXTeo
j540vRaXb8orelSHr5BTpQ3bSvu9Kri3Js5GG97dllXY/u3y0CXSZk265MdKLPdUKRB1AqumqWXF
nVbsqvEDjQMXrgaPdIGGQVUCpBlrup3ioJ4OLMqFusuvGqpc1aw5z94FMtwiaMfd/c4J1osMHxPP
51A1SLMQdCZnc2XyM7ZGWJrGKbFYPXIeX/tfs94Sg7DRiV9fPhgbITgr0IAlvxgaxmDroQOcl0es
MQpD+4r/QIozcP/RTtfL1nCUPpDeCT0SgXUQuWqQZGdKDq2LdScVLU05sPgQJeW3BxPXgiF9WTVM
JMGsm034AEiM/Sxn9Gwkm6eQdYnDA8e1WJtE3zW/+claxsmxRhMWeK7j+Fx9KtS4/XI4d2xzlWfY
Y59zC69sVYFtr13lC1jvaQe/RPWYHPTAilb+0+j9DRSEfUyNZhlH3pmsUK0YrDEQdeUfT+xwuhEX
JGZVMOEvnXCEhUg5nHNotyOEtuIrxaLMSvPUgeJL30iXCw2Fwr/zUAk9Fd3hkCwmpzTIksHgeJqi
P8wUZ5zn77QOXFkzaOBysZo5sl2h2w0a9apiL4BdT4HK2KBKKWFortpmLKzlXxlp6HIf2dZCHZAv
AUIrEHbwXXretZgT9qjccNPpj8XNkkIz+K5Xqv18A0zyId2Qml4eZgmoCpnQmgAUoiHKYDlyGqVT
6SBoUoDs75SYOijg0FxVo23B6CCqxqKN/lMi0nQwLrZ2zurbhnCTSf+m5A9SXB0NVuUVxxJpjQq3
1G0bjVvSIqBf4bl9hBVIiv83YuAc8GLjDn0nBrGqyjBCj+EdSeDOsg8hEeMP+jeV4P/oACteIBCe
+BsSFReWezGQXaqs7wGQw5KIpMeDRTz6MnvstYu8NHJAZmwOUDQBpc9Nlj8vPO5QMsKGN5Ovgmqw
GnDM8tpAsYfUXzN9TSZ6EtRgLB/XKrwnPa2tMbbd0DobyWd6H9Bi8KsHvXGspZ9LH1NLBk+ohhvT
xugl1x/37vMwPQT8M37jWOTRAgBxoMBYZMavm2bPc6lfPNSaf+uLte0XHKHrD2JfVqPpA0LgeLFY
R0TdEJcJC+Dfh3kWfph3FhPPBIF23trY7+ZfTqpPh1U0c5ViIS3HmPI87B+AfXHo3jfbz/fgz7op
MTkBlIXkU7y4bFkv9AFc0u+t9f8fFvTkIBc60y7TMqAaoFV9uBvQ2puj36fQtiPlktZKNrAZ6HZx
a4Ow1ZTMuTUX5xC9XjKZZM5GRQLTOmyWo6/Y7lSB3b4HtYHcHE1orfN9xlYTvg28Zu4XGcmC8YR8
qaxEZK5wN8XSI0BOw97bQhxVxxAD67tF0IKZxODTTJ/KW5h2c2d08SZ1f6m+NbzdNtfHBVZsdv2+
7zEzsvasdA16GRwtJxIVPPvZOzNPAXcRD4fEXmB6azFHWzvO2o7NfA6TtmKN6hXL09PCpkCjIvpZ
fFCXV4kxJIRwAqHpcR7ufHB5Aipq5YaCIoCSUkvKnFbsUf9+zwrlEYDEuKqtvtmvkEL4hPKzKXw6
YrcMZmpOK+3WEC7sdG5fQ3cZgaShBu3EADiD7go1y7eKR2qcp2LC/TMUceQu1IQiEtz7UcTVatwi
WtWTojMFWHJZBMX0si3zoK4xJHRddzISVkJqCJbe0ueBckglqIVuMNWCfR/cOxaU/2ddj6ReyW4g
98EOIjfc/vSSRVKG9XSH8bR0csk+655RtNucAW0zeWOos0QvKded7XmFVKuswIuwv2tDvFwx2tZR
kbnLCCU4n4cGJMMNsD4v2+j+fvLhLypHYz4/PwkvNBWtNTtVOaNGkGSKpaxTVWR0wSqPwMPjEie8
QD+63Sh0jCqjJ18JTl+hZGwKi2JyhUaFlSTSBXtx5qTlsRnLpDtl1jVcT5AXQZv2gBeSgjgFZsug
uALWxnueCCtJy4pe/fqgio842OlH5Z4FkDnndQXb0M7fH+BZXXvrsXq9xTVa87YkHI3cLl9qn/id
7N/VciY6DjoD4UZqIq+zEehYDgplGZyZRDZaJFGA3BPGfgwR08qMjOh1wCC51CsfQLRXbN3aMiRV
ajqCpOhQDoBPEh3u8KVJt93NsIt+dqUqS1ja/XXt8lv8fxY+0L+uKuy9wQR1KRZESyYn7g31fu3j
MhSU4lYKhCx5yc1/QiXZfUBdM2jKi+yBJ+TOe6glc0WjjsGEjrNYL9+I/yBsiRZg29p6TtFIG7fh
ztkCF+HV2DCU+ooQIhFEyNpJGLkv1kGk/Moe3Yo/v1i+T8UlTRNgAqIblQMCRIFNEX8ozJA7Rh0m
FBjh4YZrdIe+36+9E+ke1gfUzNhtLc5cYlwklcb2QasySLvn1FXunbh+9GECGRdKa4whGz5soqg4
FrSXoQqZYi9QZ/vTQG2WdsVp8T6sYNQjC68rjzZZdcI3NdG0VeuOrSpIIQXX/BP2/eKw/ASMZR1B
rn1x0gOl8D2soTQpXk1nWdeJL0JeXBTviDCl3Hui5+2QwvxeiY/EBwrb+/4Afb6Ttb3ukN07xWbA
pNzEfFj8cANI9ftqRLtIXpeGtg24cWV9YlKRGO1Y4gl24byDoqw1B6OHDu2uhW7W+cXs2WC4VssL
rgxQxaSPtzX9TiByPpy0iiOuWxEz9BV6I975byfOhaymJsnTse9Tg1NonPjtSJ0GEdVkejuBlh0y
7RrYC7favwO1gBtAkgbxMoneItIX8vgXoxeIbIoG/G3NkRnCcKOSIGqflC/CLb7xof3hRM4R9iTd
p2rdq6oQXp8l5uO9cc1Qdm5H4ykF+aXXo23pgkrUa1V49uHz3vHN7EQJWln7j05IAmXYH5nT8dai
+sbHKZs3nFq9bCvEIzecRElXlQa984e1NWyK1lrvs0g+56Jc5gLYEGeqzxBdXdWl44W/x155JFwW
v1EOhxZp1MrGYBKfuf7KIP75Hm2iDtYBbmWb4MRvF/P0gEjdaEhYGu2VIlSAxFbXvF55+3THT2f2
3FZBcQxz+beS/iCC2zzzgh4UvgL7Ul2xkOs+r/qOiygumXh8lVBW4NBYMeETdv5B45GKv9Pp090t
hwH/FRNSY/2g1VBobevb2SD2Jm9t1mEs0Io/ZyEeWTBVuc9XB8GhED6FeBQrpej04QMONHYiHrDg
WDiHF7C43DSrUqHpE8NRl6EUQQJzMBVkb52iEIpk2xEIA+PA7lsVC76PZhtebRYDeBTX0EDhIyjN
qFdLoC5W8vYs6NUdbG0oAJP+zz1KrFeT3gDPS6hW1yaKB33xnVvioJdhXY56WR2tXDtcEZCk9KFD
S/Nb/gDPNVD8rPJGD17sFj0yq8feJu8r7H3PSQ8rC+SzxRvCnbqijdzcFlJLAn4XT8lmgOrtZztM
92TRtLen945O339Pf09P92fnveGJ3kP47oys8XvsACM8IZ//wnzqWGIw/gHh39vH6Xh2T/KE+SoB
2u/q2iW8iZe5F59aGMi+/uyDvvELikl1zRfi6ow4qQ/Lp1XXvrHgxw57uuqpLkftodgVDqlNOVy0
NZCNLRym2lbrl39X017qUc/eANW3sBagiOX1IYv89noaDtllbuhADIzvccvzDtigqaJRAgh+tugv
BHZsuy6wBhoTZeB9i+Di+4zecAldWW+mOWbyDtPLQHVxXcazxiCgYKzRDtsnOgTcJS/JasMZVdqA
4/QxVOmtZudnpGWQLoUqJTNqL6egX2vMz2yh9mVFx09Ja1aD2CIOJuxJeKv8S7gmq6TtXRRPdYSA
396kFX9239qXRNXCK89OJQhDogLXVQZlT9eVgQgjUPu8vM2qjxkaZqTey/YA28VOcRVnIXcDnJ//
ouB4QbuUmQQOY0ZOAeEOVAQcug45YHwk+fjyXs7yv/Ha+mOUJuKjpf/aaGOI5C3e8wv9hCXaxdLl
EZFCJw4fiBX5MjDmT0vmIkDdmWsF64b7/i+Tu/D+zvCf/XhRvfjrAYk16VeUgwjsZ5ggZq3R9sMN
qfd7iy2/tP/DZrq0XIKROdFGVORG/IRB7wXVtzmSL3ip7wF8SLteGHXCwr972XXzSMyZpPk52SeC
QrShJp5hGzDi/sMVgGfIpHF2TVCQMcVekxqno26Zr7vrZRSa/royj1pfjIKe2MhKKLYgpHExIG6m
UCOyk8N8vdg2rpCqhSjswUezBhBj7hH6AIQRuv7HrIlBwp8c7Vy0YROnTi4kFwUswL8mcG/8LoDz
yb5Uch0kRS42o/DRdSjsycTDISZ0+Hm1NtprOCIYbcLopoY8ojypai3UpoxfNefI4Q9MNtTttroT
19T0c84zlzKH89wZBrbTpLLsHEeXPkYVLm6eG1w0rzUg92qaTzPRVU+9G2P9Y15VRpCFwdQF/f0c
hXF4NfSYdk2VHw/m23ku1Zh+AvTkZ9eSnKFV7HbA95DWim7sdoTD2e0wnXbR6VYMibstcrNQMfTC
OdltuOGn2A8nmXuK/wUMbNnsACMQh5QzhrVdfCmGdmOhQILEi+BF8JEZgH5PnPJWWFJlTAcznbr+
EjrTk7PX0kpPrqBYS715OxXYcvhzg55Z5RU3+ydf+8flnGgRWDRmzpDKGR26xmt7WzCNBcWrI840
42EsgEjp39el1lvLLllutMpA/yDvGhpUItEGF+lrsoBIHVU0ZZWOd+Sgvjs7S3jdqId4HgVlE2GF
ktGoZtNJDm3d5v3BqG2buQneFx9Ycjy4l04AVFWlZO0WF6pz5ERqKBbOr1dGm0LFOymYxQtKwyAW
h7oQdEv3CswxaOjDESrdMSKDoh+g2l6qJDtcGNA5HHjEq1zV/Yitc7ZqdkYDmhm17pB5hgWpFoyq
kEy1Ni9X2r25CrB99XPiFPaue1uJM5+tAN8/7zzHQM7L4CkRU9lHMafUohgVQJaQwuY2yA2Y60Zc
71vE8CtvDzIl+Pmp8xUvxB37GlQGsJAEXMja8N9Q4nSJVXqnF0pwgqq7lfLzO3RC93mS6gITSPHX
pDpZweWye9MMKdBQ9IfRIAE2ABlAyJ7SufWlZFV1HNOCg4WpSKr5u2/h3SE9LxZJjMtlgcs9+wkU
16v1VuVVp/IsCsJfS2fch5X6WYob2iDXUAiNAJcc+an58hjdggYfQSKXRxCABgVvaGpMBHXus8AB
MxdoOZ853htUkqEmPRGll661kkxHnASFsi523jJv5YXfwsuCjYdbflIrgne033LPN627OdmN8o2x
m6NqmmKliVRNfKLkjbeXVYuBWfX2iN9DD22vdf9p2ZECGtM/sejUwMvXA+wGkyauuKK9wWqS3nBJ
AvktW9G9R4APfVT9chHCo2X/82OyGSRbXz/F5O6BySFP8P7gGJ34415YnC9AXRFKU7564vVi6ECx
1F8ZWkRQuIiDOmElafsJfS+Adgh5gPb4aoIyNk//+61aSbJqw1CSwE63yfVhqdHgVKwbNQvwd74k
r4/+hOpmIR8nHgoUCZPmZWoFagy8ubNmAlkfbybqsIbqFfiMtLb/sQZlyQsBbCn7KNSOCRFtm1QX
gTw86n9w89YMYNgCYCK8QWG+9Ayh7yzNdoZ9SvdEaeiHFpOBeUWP0gvOZKRfqz8fwwKTXKvahpaf
bkPFnnvOaxhikCpN8LKyA7Nfamf4adV/azOUWYJqjpdIpg/uwnQFsKIFPecQdjii2CnemGkhXzdk
V1/e0Nrk7hWaGELtxvUCrqNF9Ax/SyJuB3CNOBa00iF2ZMbinG+J1IRTdYN/6NdfCpsJ634fv4/0
aF3SVDdFnlDOqYF2UtPO6OyCdZpRCQgR74uJMOgwxreZGBuIjcy9HMciOSHbAq1mfXgUy5JmRnow
vAZ+j9Y+rUOm1vQGACYS0bVNy08vgrEhLsfMmRQi9AkYzw91rBTJjqFpRr3Zra7W0jiYdlRYS2h9
vfNpYVqJus4YrFc+7VYdYFT/lIu2Dku+QeMLm2zVx262Pkccdh4bDBIMb4//KodJ7vNSrdwlBi4p
3sneLvNrON6wUQxlm6VMF636EbaypDAqL2nYgiPcuNb8qVARuLrxqEL7tTGsFgT38xZcaRxKe/Pe
Yd/vA9TtTDJDbDBD3FnUScDnAONQ2h5hsMGxMKiiwJu+a2qM7gJbbdnnBwJ+kyDaC8oW4BUJBKHY
HgAfCFWtdCRjdSVOHqVHKM86/QqFWaprEYWAnaiKtXEy7HdVkDw8xmHhpFErDd8ScGvL1r010zrS
3X1Gmr+3EgAoISv9cL0kWsK50zBhpLa7f+25RWFQZEBCxq+GPKyWphkSIgUF29n7EyTBY097bn6v
j9MtSgMJ4d1j/k5Qm7Y1fZ6Xy5c47H9MAI6OI+F9gY9Qp3NHkDoc3KQRtd+slxSPKepkhrk+fMvS
8LliRgSLMOaZWgRf82eQTd+NGhG2f2A2+EtURiU8SrGJk1+MyzofqbXXO2AKUj5j4CItOccU9G9U
lKBfVlcO5mEjpnYevA2SriAYlmwh+LbzOobeK29wBJ1RiX5VWVAT+TIYC2eKdfu23evgwXNsOfqe
+cyN9KUi5UvNUu+6LL6vdTZq5QtnVX6ckG4T3mw6HwiP1r7nGSo2R9sTybiiha2E6JsbXkii2nCP
0u7PlX5EpwzEB58Gm622lq6Y2v51rtsgpurV9ZqgE+Sp7OywuIolz+J3EfkZMj89FFpAQWs69cbJ
nVAmQpuqAlls0ho6SsIrsNBP4gtoCdCIE3CNOWsAejGFeJYQXycoj6IB3b7b3oub1spXw7x5O2Pg
PL1j8fzJG9IfXhT2WcVC4TNUxVQ+dkD9o7Pv/cKfTDg5qukmLJdyy1STwrnOy9i8D3YLkOrxCdDs
CiNm7kD0jzLLiB4pE1/7RU/PpuI7ea61bp/8e+d+WuVQOQDeenAfVytJeqibtJv44wFJAx72H7/N
ZGxe0dbXpGpbh9r33UJJmeZdHwqb7iAFefv4o5gM0CI7Xh22mczYPwntaKontzg4IRPriVFZvzhL
Z9eZc/T7cskyE/9HZacegilvyz6u0cOnvUZx3x1cH5XRdwEjj80iNhyjXsg5/IgUbEyVbkovz+hN
3HxagZSb7IrBWkxCWmB2S0eI3+O0+XCbu7OryQLkUVtAlzXSmmiPEbaz+Ljv5TuQIXwIwjcjqGzz
P1GPlrwd9a4QP2gos1uQsIEt1UThYHw5fYvjyjYeAn1Dy4kY9GfpTd4uLnQiQUeP0eLjkrhE5+bF
ybQQmIGg1i8c2B33ZafbQdg+QIEFBCJISsgCxhzC4brGoZk2UF5Y0c2qES6JG8/t4AglpJ7oEH5C
zYh84Cm3xEtc4cSSY7w4Mn+sK69UYS5Z2lRVo7aydrv+fKahyoQHJVHd5a5FCVy8a0T6Bp+ynwZN
qLJWilWyT+eR3rTB17rp95WTQvWJ3n4ptkfNiNkUd+g9c5KDY098W/HdoYW4q5308fjupEQ8UAik
1Vz5BApoBhDhM03M4qBYiRMbBNg34Zuyn5UNC38pfBiKf4hxiMAQK+dnPcktKZN6IvGd+373i9ty
2KBOhkW8Qi/Z5eWGeuxrPIyFj5YK4iYk8UtF2nSibzcvtd2zafoaAWsW0pRJEd7RTPyx9w699ebN
I0Cx/OQ83LSwr4sJEKVVSoQJwZ1lmcCwgtCcfqrMEHqXU0ZuqX3n/W/+4zjkhd9ZzscOnANNGotd
8ycS9OtJsvO9xkV+4LB2xjrfYIZyuBhHPO02PCOW81kYa2dlaygk40hBVkR7XcEFtpO+WKqhmKz2
4lHv1HW51ftfgL9QUAPrKLChpC7bD/VKWDpIZWqdELzgfw8PrWf3LV4lzYcZ16alJ3/ybkT4yvAX
rpjoJrQWPTKbHvPTysm6TsU2j1aIsh5U3yCeXbJm5rdlUcDdUoKtsm87dXV5NwzkzakBWu47nP8g
kth8aSUF+jYp97TxgKQYnlluBQZ5M/WLc6qQYVWhvN8M64KhyfCvjqIoU1QOBgJ1vjePoVvpuB3t
Ps1j7Eh9MBICwx89PqPe8s3wJRLTVKrFsFOJM8TIkz2Djnxo9XKgvbD+uuNZNkoz3x3KvzQhNjw/
TjStUWFOu1I0RolJxQv37iiBUlkTtmPxlozvWHf/Ovx47cz5ZVg+qdjQ4dU0KSxYrantPUGvWKOu
jJER50uFDJ2iOo9bcyu3oDRBIN7CGzwcUyLSEo7CTV9uMSsVFJTJl5at78wwF5wY8MrXyzMaZMV0
ZKZANhd+a8tt3WW7y3nZvSZ3fipb
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
