// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Tue May 17 16:11:25 2022
// Host        : Xronos running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ max_blk_sim_netlist.v
// Design      : max_blk
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "max_blk,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [5:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [5:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [5:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [5:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "6" *) 
  (* C_ADDRB_WIDTH = "6" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.53845 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "max_blk.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "64" *) 
  (* C_READ_DEPTH_B = "64" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "64" *) 
  (* C_WRITE_DEPTH_B = "64" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[5:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[5:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 19344)
`pragma protect data_block
x0kv4+lhQkfGQwXuPNP5mBrIWffg4cOoMCXoAUFw9+o/WxcuujfMLOPXV+/+yKPDnAXE9SOnc++D
g1We1c/q63VNFVhHseldReS+2I8pnuFwyNl4g6+roye9N5juUhxA/rb7dB/6x1lGS35vd447nja+
6dS/midYivvIo7jPVdtoGMdVDtPCdFRSpKQbtNhf08TOOBl4EpCBezMr229Eru40tktvV1MIYfdB
9az4KCOwA2HNz2YQlZTZ7VWZUsDvgBeYhBn1qhW0VKMWanzE5kbgxpvE4o9zzTYzfiVlnFpPmajQ
uORp4bh8tAg2lXfddRd6W3stjErjegn3yWbZWX6zIUgNYMWn42skt8fB4RDXcpy37yrbNrGXkOvX
DoAeQra4HanTFtuV8I7NSiUwe5lBHdGkHsZO5VPrq8+qFMC6ABzmIj2MtA23lyN+ozN9b0kYlb3D
c8IMgkFdUrkHVeCs6AZ3JQJFUEY6jImlCLdY5JtcQBTNdlKxtPu5xePPjKGlUgz7qJjaCjZ/KN6G
WdMjv8Z3CG+gMa8eHm7Jm3ZYqk2gq2mtJJuWI1vYvtCui/ieQ30TcHve85eKf6w0Kre6hySuRHzu
jpPGBXX9+9iGYLMYF4T6PG5BgW1hnU30tOHYiiUM0yQg42OxddrRue9TXD3BaqcRyVvmNmFqhDmo
oEt9O2LY32js/P2KV0rbi5/QEkv1O2hRk+LBAwEMONQvsg3+Brs+booyV0E3xqcc9zL7yKOMlZgn
+S3115A7HQuaG1O6vLLcEBUUyek0vCwd3y7hR+FDuA26mxIIacUUlxggZjqQx4eKPx4RFw96H1eW
kZJptGUuIqAIwvWJnETiZce8nLlzODkQm8MXJ7lK43NzOlKI+ajaa4UD2wM/+e4zlNSoZh++ONl6
HjPavPO/PBEaiwc5awHtyYcPWXizQWXrDQTsDO4CczgfBeCWeE17qB6eNp4e8R7OQgcaqea1K7q8
qexWLHyZNeaDfkrdcTSMR8z/X+wY/y67HoPSRViaR8BvYnFRFI3IGsA3aHQaVG70i7DqOP7ObmKe
WdyGHvnbXvGAN1NBEelyYmzYZPk3fsEBt6/mntdFtfZw24d2NVAvt4iSfqepnUQevrEXvW1KEu+D
Y3W8+PeZj8m/j4kaUObQr4BTWe0Twhgsq+X3K7hbtMaC0x9S0WtPasaadDtdHd5uW88jVXHHTXa9
4707NpIqMYWXNaDp0SSyfjj/rq/5v5oJuWaKZnA9aa5/cnl0u80zRKxDZRGxMF0MN108M58z0zlP
zzMAZdcPlKR+qYygptlfSrlAVDwLCjh5Fp+2orN9sDj/Dte0d3CVdo42MzidTg0Edf3xzODPZ6b1
kkiy1uMjNPYVePMI+2EqtOK30Nldqf2/RLtBOijLG1lJWIJijHx61YVjEg9+YQCZVjINuwHJ3Vur
d7KbhZLM4YlGJ3HByZN78UOKWv20qV61+YmtwWAhFL1SEIapG3hi9VJdQVlESjpQg2riZOk4GINs
KPePX9R7hj6x2qXjVzYKz2Dp+zN9vz5CHpKMQWF6IT/buktnO5HMdMwnW7fLQn1ZEV3k+mKdAK3z
NmIFWrU9Xhvvjv5Is6/GoElAXb2ZTRE298efSczjrEvkwc6rLc8/oHExlWkrgdhuFzeXzzEXZEdi
+jRhCAVx1st2iH8R4x0eMUPewgjQcv7zORU95sd7n8fkuYCQym1ASMJAM5M2yKodD429mAPMn0tG
NSwPggg7N5uL6To4NxpKfZ7NkdHiJfQsSBGrEGbRt6vVllWGrA+BLaeKxzmuMbofIEOxsTWM1heA
Yt9gT0DlcGQU5aZrOlZmeSm8h10rd0uOpHQJszVBBiwxvb8fIdKM+uUhBGjrcuTm2hzCRy+zj1Yz
W40C1wt5U+p1+QkI85QytxrcsmOIMs+s9drjXdD7DzCJqcbA8uRnJbsmq9ZY7hP6uflc3KOTgCzn
6zoKNwyqHSJQXAC7oiRFZNSMHYSTg12lALekX1WPkg2kigQTND3GK5Qcv9pU+ynwJE//E5EZnMUX
asqqyn5FTQrmW5Iy5tQL4ZRCXzqCP2r/G+vSwwgUQGwd802KguHHYjREidpECJGC51f6LjIteyDt
mvcVp772V1XG1KjCs7KO6MOQ+8bfmeXIDTzI4Rj/TvVUigWPj57Bre8V2eDN2zKSKRj7vbIuB9rB
1RiyxTa1TKdUWDmU78sShG4bLDm2p8PyHBT1WLWM+QpVg111ueUs1GZ99WH6Y7IC1Ckn4DUtc/Bn
5vjOhAQ3Zahz1tZyUH9T9dCql4S1CXaSGOQS3+5MpKhgDD4W6OqUfKJN4ILtATnIJfXj2nF3g1uK
Fdh8aOBTe3zU1JlUS0ceTzJVHmzl9IDbSA95+T+ResI5OsAw2xBbD3gV+/R57inHGgJcLsI+PMf2
p3Lw4eMn6AqfPqMm0y0L55EFVKaYR69FX9+8aE7DwUxSPRyQjohPaX86qPMcUH6VOlOlPYEkRBtR
aKxqgHV2R/7Dmd4gg49BBbEsFqE4H1LbLa8DsQafvO6EEUQvQ/ErurjAjvwtb/YYwc/EM5UHXa0h
uOzmR1UdzOpxsuBcvQSxWGC48YsHywBqRWfVSOqeeRnWx5qh1OHKyVu5JWTAOTZGDyTz36uKEIzt
8v4OHO2t0+0LZ4duwmM6NkJF4gUbuui6F8Yi6oNEkuOqaAI5Fh9by+pECZ9hAIxTpz+x9+6aw6K1
jbWziiDegrFYJp0E4IXbNpCUR1OeBeEMWM8IwInzaMnHYGKa6JE9IJ8RS7tkz3pXGgUvN0hmoB0n
PWDu97PDZni2cAa8PdbiiYEhBARQJffSKp3mR5ksOGXAn3NTQnAa1hwdfDjHovXoHn2JY/ssgTwY
v7fFNP0X7NdlPCBmC4c7vZt2usARIWD/SUiOqJO1HckRlruqIuPPrqDyelMWinCjuyuwD6U6FABd
c2Asl8b9BzRgb47Aus6sp7LBkifMhXaHrfWmIWLfyCRrf1Mok2lt3PTcI0rhXGK8wogGvhvXtJqo
GkT4PEF8TmzkZuA7KyMrax2/g2w+BadMtzi4hI2UJdQWjjYH3FqCa5Zp0cv0HpafesLLJEhThsiS
tBLn5/gHdHfAHIBe6JKGnoAHfBmn40DQEkP5/bJMjwsSZv7oH+W+gKZBEhyMKbxdsG0OcZxP0RPs
IIr/JNOQYAiILaNkrlAPmw40bIzIPJH9W/vBwUCBsfdfpcs36Ztnm5ImgWgSWxKBtcAgQ0gNyJyQ
2DUhV3TiEPY5SUL5VTFuoUnJ82biUBUSuTilljuvDPZD3wCtDQVHVBBDk06SuMzuAs5bJocEpodJ
k5X20Oux+A/K2QiRAv014IvW7oQqL4skdg0yIgD2No3g1QRAXBiLbK7Avr4DoyF0yp/bnn5IriKj
3qPdCFESa36lCXu5W0gRTkgDgauAagdZbWhLcgI5ldsqsFMdzvr4BoPBq1J1BOhP/De6QeWko9yy
VM/LyXiH94lbh4wWu8p6z6sTaJtcCuwaEEv/S0sy4RtSNmIwoVmtlwvFKiV0Tf8HhWRfSQARuuiH
1bI7fGXu9+fKT7MVC8JHNOsKhzsgPHcim7ohzATYLk7tZlGPJEmS3B1+Kbdm4RVaVxcrbZdWM5Fw
t9U2gqNFCur7CkNy0MDpZoKMU4hnBwlldNnXIyIiXsMCcVjSg2UMjHulMdwrMYhLtA7VRBerWKM6
0hDjJ93wLGSZysuPzzVuTzbppcym14XeN9DcELgZUoNXFh3wt0/uFJKoKSwClQyV7m3xuhW6Zwm2
bfTb3p6Pmw2RMYB1g3d9VPInDyPt75bUwhxWoBr/H8Vnd1Ya9bLE3wVjugyGEtup6gPxNnc4EiFp
bHqeNmepIc+W7BZ0sM4MJii5BhaY2T8TFLV/Q0O7z1Jd8ZQXeYhCmopnjlvd/hc2Nm/RhlaXHlEg
Z8Zq5zwUXZVs86YiShPSW3Hu4dUWpzXW0BPzsS74XYyjaV8pTWXHpW/T+nBu3fEg5n+161TROMsD
/Q3LBbMm3fUspXKHtA8sNrYBvY1r9NRiJuG4Kj4VzzuFcTmCLDBsdNen/eEPKx0bXx3Q76zsryrQ
sfYIyPFFuctdAnOGObvLhBohzkRPcmlbkwdJp4QfGrco1FVD2aH3opjPzbrYv+IXAj0XmmyVvYL9
aGZXXUT8oghSbHAXw6ccLWY54E4vloRmknzv69CPaX84bJcMP9P7tpqQ3hI2PK6kOLvYwZuAA7WS
CeUDeNC+sAX6O1VEijMMAQI7sYxWbd9ekAjdK4Zktlja0Vh3hPIIZEagoiNrLZbc0Z1lNEl+mfqi
pYaUfAqPh1mLYq+Cz1n3bpQG2+VrrFv9JKwxzq3fP6JnUcVlOeniv8AIh+M6wDjjga8SPfp4YMX5
KEMLflM6NuGMF11eu09iCjuLpHx/Xy7TVp/Gm1wpFOeVq5+kNUsG+smywZv4ewYG5vbMFfljc72x
k+CuiPjj9XFuYVMiJ5ch0+GylW63UrVQDwiyLN6DLPaQzPrQ2bRemPyKFbtggk+zsQVtA1/7POYq
LMOSqs1HqZi30ig38D3CFIgmluO9dk33JZgsT47tKZnLI3f1MEIQdNyyiWZaD55eVuU6/6sJmJzV
fFohvmFeT/l6d40jt0c/c3mJHjatOOnAfuQFJgc35ehhzTXVf7CTDxB5RlbYVCF0pdSRfV0S2b8f
PX7CKtOW9tNX551BjwmDqMzxHiJqfCrxZz4PIr3072nX/RR0v4lVHwBLqSbBLWiMlzvgOKsBZkzr
prTM7QQGuxI0sbjhD+GG+AqEElwBdU3ru+rYNgzuE2yosQDazKJN3Jwgm8bFJUVO/lbfzZyB+Sec
UuFPtAavySbkHOYUIE/wEs7MfCpAwoHj0WDXTuEIUIxbsvVAV2jctwNpzM9E8lcoyj+/SwHZDz14
jKDNj6LyGNh440UNbwO2m4E4ZHDlD23gn6xNjEnvYKKrBI+fAhfqJAAPh/zFeR/Uw1WKfoCZGJkU
BhAw8YoMjo/mjff1hAYQVUnz7nZFbXVnEe7bNMDvBaGc0rrpzoAm0Kiihdzdv8Wuoac80tkSwpzX
elneP0NE77VLFhOzaFNQySGbfRm+I9gdh40vmIH4btvIvxS92uYE4FviNzMwQOQdtkOdjymx1g7R
osUcKtxz8d9tb8k8SgKewtOhkJuQZhk4/i6CvZRs8wfvO219J51MiXygH82KSAq5ssG4+Q3MfDOi
cisayDdgWJH3AUFYizsibDaxlBASEKQNjurDJYe4BP6YoGsYNN//xy1KksadIQ221DYmzHRWS65E
D+KBKMUeGXJprdLdDziAEW2/4aB1GYd1yTBkRaDCRFSHbpSBgWGsizAd5C9TTzCETw5y8OWLhNAU
JBsVh3h9/DwNj1eRmmni+xg8FTVxz9fWyrol/wqzILfBZBmsDGeivu/zQXCeSvLjYQIOxMOM6CtQ
yyUpYIuJ56uK+t+smJVW2iB0+6IgwnD4DYnMbCYbuNtpF5TwodOmnpOvEWE/u/Yc7C0VdFCLQuMM
odMCkNaGAohV3xGYl30tpcaVSdwPR7ZDr8gugncuvK57Z/SiOHHb/yKXbKuWfzzn/jMkDQB7wLfN
XpMcgtN4vWykSCJZUgmT886tk7SOpxaL8ZroEYI39bRrYGIW0u6f8VDvBJKgsSngI8PtkgW4DVeF
3Xa+Tnqv02D7rUMAkeQTyNzisnQgq0Yu/X7JntKLiNsWWYl44l3VkTlY4Spwa1/geNYHHDf/ZR8J
MASrc9kuINmCwlj0ImeusRyMPUqwDXdwUhtcxCHoWi14AXG7h1nwMOXqcWV0WyTY0PyVE73auNxw
mn+/oiNZcqhj+H6CIi7fszimQapEZiJZMggnul8r9Bw+3rBcJqzwoStJe7W54oFJ5+YwXtrU44Lj
aADq9sCg1ZE7OOXG6znJgl+PGMNJrMuHIbqoaQkF9wm0oWqpXiSh6jsT0HTSA6CGKDOgd3Qj5gui
tORvc6dI8KX4FvxB0Z+iksOyG7FPqALg5UQeGSqR7AxZL9VHE0jmT3BXUvo2jf84MNC3aX2eugg1
Wv5rWtRF8kMHbpABQcimBZo0zTuGajmOc4Kg6g2xiwl08USS0HW+t3XyR2ZBBzopMv2Vddw7V+ca
uUq0Ot1OIwKP1kkLedoRx96CIsHqg90YYwI75sajQsAP37irE9Fq/V4MGfvkTros3ufWWFCNKWOo
NaT0zIskQHSlQdWu7UAY8i7gHd0bsyhXTJ7hrIZXWgpmW0QBgvzXYT8aoi+6R36vKZqjRYoyVbS7
cP8UFq4DhtSeSQ91SEX68SE9bDl6PUIPPgYsjaCeYIzt+ZAvXS5U36+yG80xwbtTl0q3S/GxDSpU
V7lDAP/N4WvDNBd5aKNQPcqaQpw2XEno90gMVDkaW4y3XKlUPljMliXGjqKX/LbT2ha+Ik2D2R0t
yJ39YJKzJFbOwn/W78GC3TVIE2lFC3kl8lYQDp2MpXvS7FMrj6wxJ3ljtgX5H64SNm8MFzrj7Lm4
Z8gujNIhSBdn/u7zHJx3I6vltIdKyp8C/rMQwfNv0/3P1SfLVANp9duQJo4+5GxeksiHSj1amXfo
oOxkTWhhnmSSOoS/0Uz/5xNGAcaQ9X1XNBR2lM6zURuT+96UHxlDrV2k6eoz9+DzU1UZjUNF7VP7
u4/RTTgQYFrnWiiOyZ8BZRl9RK99PfTpkhekcXcDv3BVecpa2LOkdpRzGxEcg0MvyiOMuMaWkcCr
XJPlyzOcp/iHSlJb5QxjJzyY1vSvawWFIWCmsiMerCbE0xxm5VpHLTF6u4KxRDOajc0TlRXp5MhB
JfrpNbsfkFknKAS0tdL/aXhBJJ3wAEW7pakTdZbClOCbpMEja5f2NWS32Fm6P3Vj73LgchrXD6if
5+bZxZFXJLzMr/WKH1MvWBm5lMSYAEwzPBsm4r1NgXAz5b+inoyNFGIcoUh0Loi/sBeWRMvh8t50
6S+TmGJGzmG+j06ribxh46wdVEXWHkBYlQ2LRlxpEVsBu06edB6a5YxDZPz3UCA6a4aMhz1oqGJk
YP/dyDKXvHxgse5ZvcI40RwbniOfP+3fzNHRSKDjkVsMkl3rD0KiMGIXEu7BRW+GzsU+GdOItuuy
R/KS+Zb0YluDgc86QA5KWtthjdmzFy+47gZqIsIwkuZSDUR2nKNpV6B6WgIr3zT8f0+bAo8Oroah
CmiZzuEGWIEKj+TrpnjDYQlzCOy0onBZFs8AE2a/YdRwW/zUP9phlejwXENpqoIMr+ngVCTW5GBZ
YcZqh5S810uqKzGoxST5Y4EkmeLZt8eVhEfOieZwGynGHYfyuVW9lodETggROVmkYp+Tsw2QO+VH
XB6oyIzNoFoBRrGX2MhJjsn6KCuAhIpqIOyn1si6cre/YObOqdF13pifuSwtHcjEzB8MKlJPs2+x
vBA3WXSXIaEcc0LVnhj5MBceak9MPoehkqmr/5TSja4eQEMTUYZh22MVjBHGDPL2fU1OYoYFul2I
qG6HLVdIbjfHP58nUy9UA3mXLNi6/VIGC/r6Eof/dhCW6ObbBLjQs1sxypsh6cgCSKfAH4qHnRKN
pdq5EJmdO53X7Fj1JCymMWAyQfdzP31wvYoApAtLS+hLXNkMJgb9MH9c4wGm24XcVK9ToxQc2/77
5Xbl+/hZ3wbTAfxO8Ni8roHus4fvRYVXRjj7dRC15LzdDkGEZFb3WcaNlfrEoOkj/ICJKrWzfs92
fOFId0UBi6mLDiXCq+ttrIiTGdKq+9nDild2bPLo3vwus2dZQSqnTQSsSma+6Oqd+aMW0YxatixP
s0CVMcpRwKdFH1sBhyWLpzXA3NMvybAmda19G2g2QfJ2x/fnaUAGIfKE0R1CSK4iAoxpP2cE8eDD
CFVVPg3NBynBswj3iRunwFCVfreIPQWpEf8+x9I4x1NXdIkMzsZ7kky1J7B6rCNbmxJ8qh6nEVWH
8MyIMaOBOKVAwJ3ngssiJ6kTpyqhBB9rBzFxEBInP1ZJyUS/0ZlDjCilaB+gHTfgf26qfeqXXpqc
8aF6/CFaQJK99itVP5tc6uTMZzDxfERqj/yf1nN94AdbYqDJU/q6t0rNosDx3uMrV61YnvxseFw6
PYfKzLehuV7LbyWigeK6cc0MnEh/b9/j17DOgdqqHCyaBBQT6k7VEW5/RF7ivTxvCbuhO8PW2hVV
2O2sXBEP/N5lUxcpw0rA7+5jhP1a0vW276tt+EDd6WPKeclGcZkIdKIgq9ABBx+7bt6l4/NFJwls
8WZA+O7MRYEMKqwHT6yGMMO8h6RKHAn2rhGa2Ln9yReq0mclkaTYeGzELcI/CVDrN6bYhZyFiw2n
j5LMUZd7nSoZQB7bwHmsjccfb7uK63WfYWRTlX0hWcHQvbWOYHPxL7afl0aFHhZXX5Ue6ssFIW04
fIbuc/GLpQl8TVB2hP/j36hJmfpNgftRj9yUIC2PmBZJe2K92oIgcK/K6o7LcKjY6l26FVe3dFuZ
fW/WlNkbnx1dSz62O1JtHAE2EyPXmKDlREkKrvacIuLKFrTr7INvdoTuxWadjxUGuCVinQYrCnzm
Hu/nHwgz4+bgxWZxAryOVfq0IlK48RREB0nJe2Y9jnQFFRGZcE/afQ2w+G+msv2cqmKjIPKILOYa
bgQj9TqCRn1UvKk9jiE+DTtVjPXyaBknJaf7H5kEqDpoVvcNHVL2PWyIg1zvg2P9zbpPiwoEm+pp
WcmyxSoMKh+x5cXTJyrYbgQQT2gDAG5yAJKnbxTov/w2GiwjgpEcIUpfLVR1T5ef7GDkv7YoPJwx
uBeE0Zh1YrZCLrv+H9O577WsB8DQ6kxHln432YhXGBnH9l8EJpN+YJ41v0c1UJrRK72FylT/BLeT
ZFgAWjPVawiOF7utO4aXJe/fOExhGpZG/5AS+mtL+1ttVaHjS3N1wPW69AnLqGj5cd/pcGy2aN1j
ju+aNxinuf5G/omyFqmGY/Bti/8wlUqCeRrxqBaV1ZGpSNFeAHYbb634aPoggD8RV1H1kyFSD7iR
O8UGNMKwLPY8VTtXxgyFudHM4S+BMGnKTcrqBcPFRkCzjIbLgPI3JHQZgSIlIEBvwuhtTGoFhcjn
tH/kYdDQzmq9EKLc+XqTlzsE5arIIxqgzcOxlSYr8NZDOIMApdnccH4NTJMwD+p9yRCQSgXC3mJf
RrAZw0GgKEAdD8WzADRjSQhLxcQSaOPMtyJkQ5vRHny9sklZQZcDK7/9tkegkWMf7hbvG/P0rypD
VQI8+Hki3ANgo1suP1yImXQfm2Wu9S2ErfTrjQ/FjsjKlkaRtrKNWyB/jvNKIyisajAJkgC053xV
2q5F/IU1juXIGt5b9ItDy/mtJysAiankUit0KN4fdW6CvNWWg797+i40+cuMieAl0rD07lDiLRCM
fpV/zlgbnuT/fnxokFzUa992fBQ5YMAaSJHOdsYvoWuMz6UjH+zricap8hrxIct6cHtSPAdjZRc3
tTRQ9ohE5n2Ky+zVJZZng757q0tmEMcSYT4vGiSyKly8yU0qlCybEC31zVo9RulhSWSY+7mCBCu8
uxIced/by9ezyaYu7C5moSbF61oVP+Bk4rmRod/s/uuKGo1ufs3XiK4qib5DXdHeMYCzYrQlERRM
uoGUukYenbLKvqLEUA9o4sPF+r4hRMNXv9plqPpxSa4XufMUqMYGShMIRD8G1TBP09pQj8w4QZXy
NMa2unyIcpOOyF0j/g5YDB6Z0YyeBaj9xUBq6Vi4c9pZ+/ha+jmLniQXBK/N2yWBrsKoGTtupXMX
ASGu7hBYI/Hkg6tOkGEhReTwx8zfcZgZsMdYkdJOCtl241PhJouY2VYAKkdEUom/nxTHvg0CMWx3
BplzXdTStuCwUNu1BvvzaAy3wABEg7Sboj1c+K0nVd2M7aakZ0gCurZcknGb3dF6/wsgbHSJkLmb
YjAknGAiZ3cQOnBgyeQ6H5m8uOcmrX54A+LtoxNgj6Q+mGRxWhg5jC0Rj74WFCOGjI1zceDd+IIM
kebh1WSpU2c8JGYo88oxZ5ARaa+JGylfdD0nkdeEcsdDmMFKvX1+P550ZoJOCsOO4QkzmJNXVGhU
1CgzSlVzlJ2I/AQcEqLOz5Qj7wR8lhkTIukw1kzBPPI6GrybY41A00s5ziEFhfs9SduWGQMzyz74
XYW+UsOC0+C5SsWT3cKh1E8/qYo/fIgVI+Ys2ffG6xJjSztueq/Z41ZJa8niOZtFl8Zv6H4xx0j1
pyUIVgIBZJPIvCkyj0qIjNAcU5M0namH9rZ/ewYELq3lDaz9O4Mc6J/AxjW4VCUSfB5lNgA1mmx+
vZ4dynXD1uiHQtkAbFvqU1R4FTeC+Gfz0fuRSlY9QDmruvHRD/EmOy1Vt3ynhkkogrLNktd6z2xU
vinvbyQs6T8ydOkCw4NjSU9+PfhrUb5s7h6rHJ/I4MEJ6FuwA8j0T9UJiJ2UlRYj6wrUbGysdyEj
x5uO2MhekHH/pI3IV4Ofdi8ymMBDx05UGJFbh7QOZ2HUYJIAQPdWTFHxLZTL7PeIBpTc2RQCKn3q
BqZChCAZgKM1Ljt0WYcn9DYhy68OidSB7LwfyPTu0QyRFLAWs83WnoJPKy0uuJ8+7GgMZa0R/05a
yl7wPLCr8W5nVeKD/nMvF7ID1GZqMN6dH2fH4J7je89OjpIXTNhccMiaSZunrA/21H7kVsTB+f0s
0dBPB3dn48tXoDbPVDvt/X1iwoeN8/lr3iU+jqAsPyJAGqaFCe1qG2tP6s2NUfJFtGs1Bmjrbb+R
MS6nVPWTrBGFQm/vQTdGErig956ka9ABJqRMMsm0FSy1i9aZ6p/IWxvaJxV9A2okN/nbwhf3/JWq
GF92ijfSXXF3+zJI6Tejt1/IgSYil1128t49yb8GTpr43oZDwp+kjlYEsu59Je95Mw1gLFZk3N+d
iNdrqrLqhwFPwMZiLUDQykiXn/AubraoIiDOOqqJkOdzlKn0xmz+tXgH2QC2RYFI9vHaeKixQ/22
D2SpXNEhuJeUftfeVrXNkhzLD7A+tZaY7BIz6+gC+zSgYr1oUDniaaQ2XYqO+MHr980BflsYBEvl
hHrEdRkAwVwbVS497yJ1KIwI0y89DRX50RoAEtR8tu63vP6f2R7WELhvuRWugjjWdN3iJR062s0S
cfsy/e6K0SWESYOYPyz5stdic1dD/CdMUaSK9QFA7LrWxJ5PWWhytasv4yM3U672RRGCi0beAd99
peI0CwSmEB9CWLZg/UudejEtmWDRBx89To+61fZK/9of/QtCWDBc6w5DUh8bAHbQWxA4UiLkPBQh
d5ErLyN53YTCoU8zyM/+esym52G89ulvRzwh/KftXGYGKzLQ2vrNLq3E8ubdIGWHFWdOf3amRpKc
oeM1PkhvAjWiZgCBnu/j/RP5zONX9jwoBV5Nlbu0MYpXIgI5owcCEnUlAbdH81FW721YD9von5wf
bfXgwHg31JGymCdnDGG1VgbvI9Axp3K3AsZmLjBWhJw4GtY71ZaO06qXdidw1h0hVWjDn3I2UenQ
rfsfGWFzDLgaA5B1i41NDjyd4x5J4IbA+Nyen3Rc2w0ulelhnzD7IJMf2pq6P4c4zAxcf0n2tlb0
T9EhPEuwfDc5vmqfX6hI+yHH1SKGh+wh+y660kcF6LW8eJjuOfb8N3k8eOflfS3LgAy7BNgL1ing
K8aib//Nbz2n7o8J3vB2rnMMqwtBIi66TyIczwQ2J/UfBNq3T5oGc4l36HSxnpqzeRkNUjvEHwZl
nXEj78VmTRjRF8QTpsJQIcehZoYm7nuG/YE9n4QLi3AXDzpCDLyTqCE0s69INtxJTcgW4QXgh9zy
EpbCg8teNJsULVm9xPjh6Y2TsPu6o1DFCegutswV2JohaqSo0xGAMxJk/xvQmaxV9y9sK8f70bXk
HkBatfpFmlQO/ffAN74ZAmT7romy4QVQHGCKnj8JRJUExETPVHb/p8vwTdV7FHG31ACOnfOcBfvl
Q8LEOBtu5nzQG8KCIGmM2g+3RCOXvpwXBPasjpwxZJMt1V+AElMBWsEtg7S9tFoN0jzNa5NQAeRq
dBPi9nbhy5yyW6ksEvcckCvv/lspbETlRbnqTCrEDo2cAmoMZkZjPX8YukxPpubgHHAfs/ZbNuIo
NFmOk+1ItoJdeA6CRuAGKZX5pl3IoNeovOCANkII08lYItD4jpAsQcDc35vTfo5X90mLmjxiKR/U
q75ttK+rk3dykai1z2qAhsCr3T2+QAFujSlwRmxsl7yN4B30veLcKVZHH4Oiq1SPl2Ap1PC49zqL
TaBBvWiOwnsTwf0rs2FHzfUITftOTphmIDUbliE4WW/xkWKGldMQcPfEwD+HcWGKhWgMyt2kcP/J
3SS5KgYcTcMQ/R3voaJzIsCmzwW8YBQnxiKLZ1A8WHBJvB4PYlT7KUyXL2TuXrf3wfMSBRkniz7z
rdPlpq2fM2JHepcq2IWH272Mr7bFxTRFNCnRnhWxZJntgo5zNrwYCnW35g44QidndUKlxThz3AaT
NR8oIlaQSXO4FPZDawriyYc6HTO6/S6twi5G7g7VHJo9iyKX8L95Zy9JeY92Bagf7WRKedhsipgk
t9fOJh+Drh74f5KeBVu8pOmjfcUKMtH/qfRNRWB2EePw+V52yXczv5zBk4f6IDIjtS8ONOSqzPEE
3R/B2d9RQG9Yz3b8rJclroqQAmfUU1pndEamz+VdX53xSyxgPvS/PkRLMUJkkEJ5DDuUIAcOHp++
R63Vm7YwyA+E9+qOrHQbOAscCWrAxM1gyp0GYJnV9WDjZo8OyV+vaXOh8qV/PvHsmkm0KmDOHHYx
Sp4tEwHHUkiETjr54tQ1f3In59+yv+Rrbv8Q7av7t0KVoPyCvt4HbzwpaeyBThmhlCoVV2eWPU3S
mUoSo4uTnc23yvlVFcUpYlEAXs7CJt1WZSh0NAwDSbQNPJHpXgMVlgGYwhlcZq2Qy/15LQnbOcFI
OHBzK/syvtt5DPA7HXc+LETFi+qzKT3kkheV6mnCq19+M38bgUCk1M/EvJ2c3ASE46ocT/WlPjE/
DAWY3wHCOYeDSvV51B/D7ubB3hYipfP+8vI9UiM9/lA0uKTkyLyccf33eCMgE9temHYbtrJ3iwZM
LPniWAocGS8dqBWK8pvW4CpMlHC6wOo5T1rcyGNYLStRSEB1alaHom1OpqZB1/PwSQFDBx4QYBva
eMTnjVhooXr0i54Lz527fDP9N+83/TljDgrErEnpxFChh6z3fubkWmbHi1ehj1DgQh2aajILZq3d
zyJHXle5PkUcjUGd2RUUahmkExgIRBRaOJafZXd45meybtE61RmSBqxb8g5OPPpWn8bDtA5FAEQd
APZ5yQIrQE5nVu/iCyMceKXtYrw1jbQencqY+d9yXf0Zhnn6eN9M5k9vNp60ajCG44a4lzZR+QOu
KjqBbTTRYJAMUkg24whGoWjj/GjynQLhI/hPgsI4O/sO0daei/Snq0HcUCi11ip20hkKv2u9YX0U
FKcCNdPfdJRxZAczL+2ZDiFT3f1Q428WhUo8WDnJs9/secg3rajNFOUKEM9OlriaMH20gclVpBzL
hcWvNI9b++bW4bFlSbeBaDu3C41qQ+mr6CLW38r5H3oMzQ/HY1TmsHtLhdU3M8hqtDFqV2vEa9Gz
DhITnyrpGU4UX3orFJrYOW+MuxTY6Nb6Vs7sFZyREIiICWxPbPW/TJcT87jYnxkVpVHTqHZS09f1
sCH9s/8lyktlVwwZK46xiQJLKTOWn4HcY1o2TSrTx0kytyKmrQRxw1pWc6FTWsZUqDUVnzJ5+myn
+NFYti/yaPrm8HM331LpqoqlLXV4hKa9nAoFaqhACKBkt9YgreY6mhjzGTRJuti4JcRYd4sFB7cx
qCHO4REm/s5nMu9F/qrlWFguU5MiVXyq/xtlMQYKT7uMKFKbn7QexklWPmEJSlVxQRO2BnId1EzI
EN2JsJ0TTQT0WQCqYxQmpFoznPepli9o35Drunc+QuKDvcgQ/6sR3bdxT0u6pblXnGmoh7+m5pjH
7LtWTXkbK6d1vNrELhd6caEkL0cyxtE0y25wPIzbEWYsYjSuQRJ9i1JCQeQ+jth5wn58mZr/Cg3k
sjjQMKVgEtisgL/tNRua/aKsX1dEBBs39Lr5vwvL6vKp7YyPA+6ECaFodcWST2gEGxgdUhBL0nBE
FIEud/WCibtSca/eDf9twLBlYc9kHpgFjTe3fR5ddIq1i1JFfjg2ezOT9V1NOp41YotpBaoMpz6Q
4BYW7SCfCRByJc0i5bo3SM2/rzh3FUZnahVK7VrDPHBgXi9ERAUZ+z/wTrLokMpMe2TswkSY1NRv
TFeCA7i1LPmArJGpPV5znvyOnAQWLj1x0Alg3kLNvUGz3Abbs5izfdtpuIA18A/vMq11xsBdPm1/
dQeT8mjNmG7l63FPe0ls5JFNhdPDR4qMO4GB3yh+VdQgARTNlPRgQfEObQg2j1wLv9H+KlkaPeTY
IeSyvCZSzLKePVSVnT5mv5eo0Ghbd7qvdQvG1pCebDaC1EePa2ZhdpE6fLr1RC1W6wrEzGW9VBbC
DMogAeec7huzUXmNFhycoALcZ1D3DI5vyYQDc5qVt/w3Nd1ZJWf28LUysitBu6VvBq4FFu5uxFQV
FYXdSsXE2Yd0gaTRrcC6nnuv7ArrlJdZeo3EBMpvg2tGy2Z6N0jwaLaCZYOaXmxLemAnKrEToPpF
yCMVGH4bbF0MDf52nE8ETsXRlVO6GTMl9rAXCch39C5JGPK0McVvVlU5iNj26t7FoJm3dPkCedA9
91tnmNh7vvinmWrVs7WHwbc+4Cb0ZtfK/0L+stnY9NPBEvhbSBe1MxNptJLNZIZvePG7QYTN6RNN
07pdCbshlz9bUj8kLC2tXUMlBQsixem7cQ5B0zaRsYt0ziYxiW8y6W8ZnNUdKDyEaLwEd8aon96+
+wlZXrwielctz7YPajt1stE+qm2SeHMJM5ELkWXwNUyEnoT+8n+DZ+LZp/Qok1D0QkO4FtD/Vbq/
13TTa0UJ78hXnfYJQVSi3d8gxoqzXJdtm6lG7dVUwGs1gL4z8GDiUOcYGOXXzIKMiKgHufFlbNEW
0YbAK7dISfmYJg3tAN1zq5t+nV0LNFvRfxu7q1jUnvurNDsdyJF2eqCO1JqfsCqR7KtFqH/+uL8t
L7SelDLoLr2zLSt2TFEmMpRtey6KcDHuXKauQsAesAJp1wJD+QEFTvyNhQpn3jOlcV32pYIOgT+W
4uC8vvtqnCTlN2HvW3CTHanf018YBtrnHEomghbJnqpYyxi2DshlqDn2dpH1Z30KFaRN0EpecczZ
P37xwOXYNlWrMA/xy8EnXiDEp463tbogOLnTdjAH2T/Cra5zZhXQgMkGOVBpfiNqy5/QSm9+BREJ
vDk0hfYOp39lDjuJ8BjCVPR24sMGps2qaSs32I2l90EMtyT3zds9z/LJp1uZGpG62O+iGntu41ik
vDlJraO7X0F+zgJ6/430zyqHVPjoW1Y44Y+gM7UFmbnsmPobl2aXH3++1jP8sRIl/z5KDHhmj6lQ
PXL9o5iT9qDeMJUBBegai3JC0zxuoNUZIPw2Se7FFdQ14yGQsk+YCJBnIIjZbJ+a2LuUMD6z/QSQ
SbUCIcbl1rbwiNdAVij9opu7V5QCqKq/atsL2gPBtIAUtV2kJhXWQaO58fvTsELMjSC23MfjeIUb
m33mHDBsoJmEjLUHYgC/P20T2lnpGAD8isJbYyZcc69NlWdqX1DYuM5BvLpnluJ5yy/LI5Zpeoud
cGaoOV7JR15qH1WfSNYQ/e4IMFzMvYjueye6JG6djMJbiiGr6eDL1tSahFZy5eaX1ChEhdmE6Nvj
yve5x2y+Y5ARpOdPbx2LnD3X8+AhiglFTHn0nAtviUy8dlnPUyQEdLoNY8Y/5tBAe7pD/ASg9/+A
R/wUkCUDbUKollNUdxFFizWACNodZ39q3XeMbZbAWVJs1pex0kWfWBzvGr27E5m2Uxh7F5oXmXcT
T2g0tPYa/nrfC9Dlzv+jvugbAkGC/vCtUKexQ1VQKP1/K8CrK/+M+GVqJXZe63eFkxC5lJ5Rim8j
d+ePp5uZOCuZ4WuqNr/y1CpavAwtxZnm6+ePP7yvF/IIEv4cOky89kI+rB5Xsx7viCb+xvoKrdQ6
j6TKWaDwROj7nilNWg1TX47IQcnR6TGlwN5r7fObIHWpXfPN/MVcswrInC7zhARD+9NziRts5Cej
mxz/N/aBvs+JeezoxovcayT1q9n/539Y3jCjVZgEjZt6an5kKaO4aokUea4l489R4oKXE9zhNEuZ
IThobSdaZvrUFIWTR5dyl/iNgxwMydMl2hTanzLDg+lIBiJJwKflVBHzgjdVlDDsJUFXu8S0fm5r
Z6hinnD6f+/iD/7jeFLI//e8AwWsD9XZG5JzyOHZd2p3Nc6thG+KhZ49XXW4b/gIKH2oKpOc57tZ
4m03YFaoa2n8exM7daLpQ1NyVnerwDgxwuT7xBqjZZ+XTel2AEEOPXZBcfX10KNw0y58x7q1IMXn
kOYByFk+9BrD2MEWdgJWGObYsuoTruQZWsCS0L7w19QS1eIYEAN3EIRoTWl5uvjgrSyd1EzEuFB6
bUpxm1YfRUOL3nv/wbulI7jsjm1dhXE0lLFRhO7bFqmAI8eAzVX+f9rCF9mEFKB1V7jGdKOXD0Zg
dJ/eaSPg1rp7H+6oCfsFLcm5tkEkHh1owgDPxcUoNjTKzmmBhRmVpYB33V5CW+z5ASeaBhyT5ck1
XabLNFcCL7OrtTw9+yP4ITnbmjFyC41NHdlQRjSvybLpCLccdqTQJiqlvd31DC3rDl4/nYEv6kMj
7OM/BUUI1j8rJvC5rlC3YI69mwi3EQQe85F3HWxKzQQpTcfaYFSRjggbliGST6icN8Qm2I/mP9kv
lnEsf8K+RXYnng8e21Y/sl8fXnZk2XliNSWM6ClC3JchUQ/X3MfwzsmwV/k0tTI0dbXVI5iK0tdI
SONjWnVp6k2iCSK3iZLkbRc5yLyjNk5BjzHO92TeNzzdyEoK0eg+YmwT0qGUbDhVfzKClNDNd/HC
Xq5wcXkDBHMAy5oZ+p1FwoVNwqMv/4yWYhNiH73ONXADa53urypxScdC9qmWG+b54lXuCCuhNqr5
k9cHU86izxPzppejsyKU/h0Q6K0f8LQOpHRtAtxQ7Fvv2GgyNw7aldK3S0Lc5M0Bvwpoa4oc44w/
m4eLDx/8ZmgtmkNCoDOt5e4/eT7VZjUaGF3/8SYGKz2iPCTX7wCvBolMAkVbxeXa+ZNiH7/OownC
bQV3PvZ6X3riT2dWftBW2xuiBcSot5iBcJzoBd1k5xXtrG8vr1uk+o/clH7ekHmoFS5eZ8uMMBYj
+X4Y1VRHQpcYqJ+urgc0NkD2VRHy0J5ijXrE+g6npJJNa3kSmU+whYmOsOsSblf4h1TuOr1S7vo5
wemPZwUwQz17RcQbkyvHK5+GlZIiAy1soUzS052u8tWFEoqxEbdahMG7Gz9dLB7JhVuJMpYZW9ry
uYPQ8Sj9liAi6Sub4DO87pX20O9YLW+ZjqtK+5JXewZis6gPWKauoCPlbEdu7wy5Fjgknr0AvLsB
UA1C04+YkEWSZ+MGa3+PeI3G9tSPc5XA9Fv1/vpdBUMgGsUDK0NyAoZyGBdAAJHfoAjwIa4GjKnO
UnFAxseGSp8c5doNhNsJXOLqOe3L4qjg7OZH9YupbJyXwKzKoDKi7eaOhaRpBFaflLugWC/i5DzS
k9R5m/yhHIEvo/MoOfQSSFTL37JVKH7e/oba8PqDAFlcxCHzZmoL3A8eibnS26Sm9P6RBCbJQYI+
GegxR+j0cgF9AIiLFs4fyQm/dpipeP5LO4CrnAl983KdImCXQUuMa3cy+W6zeH7gC5MQGORgwmH1
fVNmB+HN4n0nyE+4n4P1mbKElL8/8q6SOgH8pxb7f4/3ILNbVeazE0nc2r1L6wPqKrWnvhzDnydv
yrWNa5lKLMGu3o+/XqvpkWRtltWUS1IJETZXwhlRE7WLr6Rp2EVeDgOVyzfWeJ3NWrL3Diy94j34
yq+ceUui6r7i6ihqQoBA1vmGEpAMe3OusQS3zXdlBTethupoEhanaECaVtXTD9Y/gEaYeczALcNS
AWhYAmFsQELNpmBTrk3oz0wWnWB6GVtLDj39ARD0CHrw0iPwnJyLWu9QHGOjifIj/izmkX2eT5iq
8b/plthWoA1hnL+Y6/pliDoXrMVEVaZlZaN5hPx2gMW52fztf+qpPGNDuFvoOaTDwtuhBfWn0qjw
Rxs927Z8BNrIQa5p/lDenSMGb3Ksav5u80I0xLWfVLCxxeJcM/z2xPnLHSh9cSKgRjtBzQzup4So
PwRpGOlTumSfvVFzG9dHTKQlJWNWM22t2vVWGXFlvWyqZabSFowUqLC4jttFnJHuHUGl6UNAYuzn
QHfXRwOiO2+h6+xHRcbE0+lagoL2EKQ/+/KUKVABquZqSwmNE2kwnk032mRcldWDuQqAtXhU0ggX
5q72ICrERLtgBRJZraMq5weL0tXKt1+WAJaciZuxb9iOH3OGHNYs/jsOHtDV9x9RoKWukA2K+R58
6Wt0uURz0SluJJ292h2Itcy/Ywvly6wh84DtWCDru567B4NlukXFKJIYI6E2/RalFkoC6EgoKFGh
odPWw8i9GdmUcOhReL62+wopPzg2uER4RT4vx8eePqo9DOSBhF2viN9m5lh5EiMGfPPFZ69hL2T4
1s33/6cu0OSspxV3MogJxoAkaB7UhcGxj/QpicGiXJdlsYuko/0beu/chD2EQgTcE2Pku9j7ytaG
0SqL5rjWiihFfz+wvxqltgCvBXa5XC+0OzjL68++WdG9IFzqUL/UKemFeWG6HtYQVu6IZ90WS481
mxmTEe3XjB5spB4aJRoUq/hGiONoz1Cc097bWfv4Lgeu4asjwfP58KxSWO7mQTaXqmlLHgePn/Gf
H8yMRKRIvoBz2dWkOTYy/T9Pi9uWJwmsswrKFu2qsX7dgIourOlDKWQWuKmnwjz41LlPMWvePw9P
5O+g5AlHj90GIzTybPOkVnNKGQxxLLUuKNNfF4UQQqlL7SDxSpVPu957s0L4WKMopgeZpwC8k9rx
W1+O+Fw506OW9bitMS97RGlNF4Y/ujdlAdUsjSSkHbc3IFvgIybX3cguTw0b/YILA8hEpYCneWHv
rBuBvD955uGa90zV4+I5duSbEjTJr8Hfs+Kh2uNlokphrLuSJbnHmF2Gp8uagLn/MysONPkcQDdP
9ciNbHMHJgDyYHvNFXT81ZE74ukw3BOIn+CUbQe/8RrqboqDeoqmPr/1u96xrKWGpZrUwriRW0P3
3Pv7xFk+ZMtXH2fI+zyZ+iOdErG8IHXw+rIHLEomdJdPoGO/GCz/KS5rCoz5r6twX1pX46i8B0+Y
pql6j7bC2x23OKTJo6XpKG3g82KM7jSvBLaJL8ki9dL1QyvCxu9z9fqyVOFFuonYWgQNwZj0ozIF
WKXDkbNFJaOYbbIVDfBa1LpenGJ4KHLWZRGGsRErP9akiWiDwB4oaOoa1pUg9GveIoM0vx0PCaM2
wWLbLDAMcmyPHstd2QT0odp5nNQz7DegXpu+5mHvINVx1ttndNNiit4eYN6aMW8PuROUfv/jZrQj
lf30g3t6M1j94yjtO5TzCgUkf8uNQDXukJws05WUEcW+OWAAOWmsVxJF8+hGz3jYtJalaPCZSPkG
ScsCtSiZU2uDwzy6cHohJjQmOkKXjqwaPhcW/cXcqY/HlFFJoHpL53oqrwMNQWZcFT8gcbXmllf7
8a1c+3uMmyDQHIJ7sRcd2hxvU6WwMGhaxh8aBJ5CmroY0cLxa0PjMjfTerGbHmFnLuD2oY1KjCX0
8OXQFwHJXRbYkFadbiiUd9pnAQeVHBPbwZi/VhUiwhHIq6umzOYEu9FE6tRfWlz4OUSaJ7M6cPdW
XC+HbGzVlSXwNVONcZzcrf2rySdRHX1ZtXO1jMGtxHSBeZloh4poaLW7YtNz+fXLovZGw7maH1dp
PzKwU9Hktub0J+UxMbAtR9s0ZhVFGbSGQNqtg/2DRj3Ft6Z0+f9IOkHG9+1Si5n0ilIYriFXi4h7
cOhFR+Qcf5n8UVg8IKd1yjqa7VAMhoHljC4lsbY15XbE3q+aE5bqtyTZRLVtSfU3iMoousFxGDQW
cqVsUZkawwuesrbfArc7sp9A4xM6SjE7SS2vaZLP0FVu36YDHxPGv3Pp7Jz8tT2e4jMxlOHPMK/j
4e7CZeIQ9VvJ4NAsFYXgqh6IaZsexU7fxGmGeiaGKOkzInWIHPMKvyogcoQU67kUyGETXns4Brlp
maFFLSxrgeh56tUNs2U3z8Cm+BUzNjsUL9g8ADnrYtJOXvDEdirCVESA6O7pdTMSwotzjpL52Tqn
MAiOQ4lWA9N/z8GejbOwfS5kwSaOXYNkJPgDWCcdvGxia7/iE1LFSM0fCbkcIB5/9BDK1PsGc5+D
hdOTypHy9hz+jzznsTVhtoFsupIZz91sUv+KhwJtmm2XKy6PH78DgwxCcdQ55/eodXpJ14PdcAnh
g023rjn5UGQ16SV8dOdvEHxpId8VZCX84PQHD3IEuQJe5ETLTxB04bQBVLDirFiOQblfc+k4MKbw
4EVeqM9mWDcRyK9TDNX5N/V7kWj20XUkTdvKRuVS6BmdQWUArKxAsuzdBmA0HOs7m18Y0nNszRnk
Qjl2+gxAdCgFRzybAFzKNNob27bE26Vw+TCp0KlUEcV0at7nDTSFulThAfsnir4Ggo3UkQXqUuqB
85zYLrzQUIFqVDJhyTYefx2VOx6gaowvHBR/cLZzQji7EoEiZVkPaaU85i0myydxd0mjqpb6OJ3o
CGeadg0MmbbHYChXT8ZULbt8JpR9MEAVsPi5rmp8eYEDMNvC2TwLFItL9X5frn7IewNkf5F6K7pr
vezFQjynVpFz2CexuBNpJmK4C+5xmES/xmHQUaNmigrt+Cf52JQTEA/UlzTNh/yy5uJ9lAoC/sJS
+59kuILvxL4JlvyLaoInjF9JsgbV3PWO1oCbmZAjcLGisF2TVCQferCp2jzSoTZFhwfpqjvYW2S3
LEMn0yRKO/GNQWsrOtt+IGJVfPlVJc7k2SFWPeE8vaXgNWbxUIlTvWv2CgMGHhH9qLCxsx745hEz
PcGBdzczAZUbB5hOBE9qU+02k5gtwqyzTP2HxmAim8Jy+574SCPSuTItfFHjS3GgVY/nzyK8p6H6
5p8CISMImVNaDVnr7f1sEyEF4UVsT+SgnfC3QvWkG/vW8/26o8GbCLTi+dF7tTjoEyEZL0Fcc74p
t/pN4Sfo13veUpiUCgjG4l9nmSb1CqFdYZtB2jR9cdwwlBoATj95h5oltNUnRR4ZkPillLMbuQmk
7tdgWP6k0EpD8HHIcOM1/RJBQMmN0wsTYX3kuHBWAMGKpnPPmys6ji7NUtMnS2chXhnO3K4G8vHB
5UKckGAIjUZdV3v0YA57maEwLJzLFJkC1iJis/Fae55sSQwcD/bzGGXrF5rorm6SvGkTyBMSUCq/
DhuKGRBZv5495T862siTojP8HKHP1dXXEHVH6GtRejB1VYOTOAmRvxyubTA5cTvMDni1ESk7kIqH
t+zswto1PAhsYDMWUZ7YG9X/JFI/OLt5V5sdPZapbiml0pYR6nhZTpll8LvDUlwXV32PF8+oglB2
vycsvYUkugj6RnaQr9uH5uLdXmPmRxdlvZNzJcj/ZDAi5EBWQhsTypS5fnJDgYzJByOW7+/kSNok
WEa4ZkO8sDh/3hy15IUZZymNHnZsQ4hfg6JVB4dxE0qnWjXlO79qzZlgNVC162ClPOk2JhedDqEY
SBWl4TOK2sL7ylp5NJ7iS/R7cMbM6EGVPVcPntI8IKeEP9JdbiRIUP7zG4oqptTmkoWqpkB+YtuM
5MBG5LwhN4GY0OMJxCbEMO4JOp9a8eWYxRmnyHJQuXxt1HjIvWpjiQUqfJ3mahMnEG3qH6ltEwyk
KjtMoOV4cIhCMHNI2qCj/dGpww4x/LUdJDx7iRAS+UJDeNOaMFVKM3zh5/80HZAwdeRoQK1ftSnH
wUJT8hGTuITtGY+xYICNFn7ehpt3eDyh6mavMddBrfsm95eC6mX3Pa92P+da/xp7KI6N+lLDdZ++
zESjQzljTHXBzE1HTXKCggWBZonnceWMDGMQ4bzH+LMf2eSRnBTr2o1Sqdh8Kw7ZIiPckDv96xlO
EI4gSDJTC4/qP/DT6ZDxeCcDwLngOn4k1OINWQDCnpOmzpoDmmfsTYDB6Qq7K/wm0ksY41G7LyZO
6jz/dCvb4VxMlXnZOr9YNsxMw16nYo/zt9xrBSSYayNYP1Z5Lmpkg2Yf12w/TIya71R3sDlTtRoO
QoN4urIMZfuDBavE3VEZJ3npBuaExLI5gBYCljkdYjzxAkrGQ5K4yjrs3rR5Z6MJqamS6n0IZe2y
hzy6LgH1w8s7rnTBqNL68SxWMUK6/60c8rXbgB3LJ7YKCx1O1+FnZrx8IS1WFHX4OSdJYpPARo/W
Wy2Pos4nE2mPJtmYxutowD622q9XMgR7vk/Qz75Tkb8C6BAdkMcrXeD28CId/ayiMadTB9O/9biF
cSioK/iSmQbxzldpZHID97ODOzhSTdO0apd37f/JAHntQJ1igE/AEwJtGQdZZHICr5QWtybP29gp
Fltmvjmldn5MnC8kpbaxwBb/CwRDECn3VzqdoKJv/aL7C4C9zkrfpjYVsbq4pn/nuyziYho+6cb6
l8SHGF8ydZkAA2d8oxGTW0HlDBgk8yr5a29gHKGOTzlmmyRdW8Ah1p+RsBOb6JvV7Q7wYTOGBj2S
zJMTl+Xs5CwrC17Av7M1VovxInNpBsPRbNjmpa1/qVI54OGe6vIN392v7K9EwcJkt2X/xz2va43A
l08cyWY+QsTNSrSfOZowu545ZqDGbDxvfBvp+zuVkQDDYn8WGC/d8cKHH/0pAVL2o6IIBTfqHm5K
BERHL7/wMKoxUkeL+pHw84vuaR4d6USMp23yal4qltkHVfu3rjNh34K04Q2xk9PqR/djM4t0wgkt
yyvz5ElgZ5LQpEKVgC6KQ2HBO/hUNpwUgI9kjXoBJQGzUYZRUguYeRsE11T5ctuBr1dC9pM3H4CH
z/B32SGsLg4CECbNyCBeO2lF0g+CAVu/nujayyhBX8kmp0iji9aj1fLG5GMQqXYIV3dY6S3IF5gG
NI1cFFkdMsiJGJ7YkPRz9qYArYmyVpkTpjKXHAOtkx149HEDwsVe3eEJ2SuhukrV5FWg7T1C4LVU
9Y/yKHPTakDlmE4L26d2v+9RxCeRrHhUsJuJh6sYA3T6OSMVlgYQ7MWjzDf+1L4D6ZH0hvsu+Ckh
XivpHWvgvqZwG7cZ/xTyRm/k6Qxq+W6Bvnx+8HkegW8MUodtgmAZ0fWjvL7JslgbgomFxgS4yOvz
t3Al9FaducJwzRpvsyqI6sm5MjQF+PbyM+7peBZLlL2CJ69cDqiwJ1fnx6g6lhfsmRBFWu6Y7YGR
1rqvry0f2xGgxUeN1sSOKH7tHw9WSgm+P9YK/d33vGc9DuaZCuKMywkA0vbUTNVoRUoZG9NP/eJz
y9MoJHRGJYYjhCiDXLEg9VZTQqwySrLtdimEOlXHKjE234Wcb9awcxJkoJOUGSIAMxUowbUhjqxQ
zzcxzdoO7eH7jyuPuqWiKJg18Zo6Y8bRDYhXiUgfda01/QantrRouL8mxWx5g2r7bXrzTaqURuSe
vndw2jkgTWjGVDPWQTnTErSCx5AyyfwZZ5iHS/iAW26p3oPWdf2IOWGPZNbIjkLXHwypMMyMOqR7
aqGMeqBltArkZAPFweqvb4/44vNzZcn8RlgftJKcptK5qJBjofuyQk7We8BZOk9z50lfVbD+OknX
tZumpwMJlCnvrcTX4o5RyTWEJ+MciOoFbEviSuoxIx61jqIczoB2+Mkhf2yZ8eCOjbsdmMLFclId
ZuemwABLih8PPTJ8kS9rjhmnKBNXmuso48EW1DsYHXGJC64mIehZa2Sl5fUx3HecvMGkpUpFW2aw
g7VbNmEP2SMrSAHvVGeTrcHB3tSYVNVLEXUoNzsBBlBP1+ZPMj8HvcT84q6El4AjWCA42vyVE771
5Z7ZBiwvmW8u7ypfU0RHkvCmonDEqiN+g3DMjB9BD8pHSvzL4Ep8g0Kz86VDX15OsfZ5uXmeMnDf
6Tp3YUPKsgqWgC4qpx9qyDeDeJGiuyXA+CtfdERF5DFiNDWK++3wuafpXNB+c58otTgve10uOu9z
+HrJ+mMJ2LsOJsOcmq/v443Wk0DRjXdBb20cD/5bV5bsD9w6aOJt5iIXfatu6Y7UhBxMqm1m82dc
QoqTzH1cLFSxkBZ0xebOb/OeXXwEm6dP7TZv6pVogql6vuFdSiZWLor//DPTZtPMpV4Fzda6vMOu
NmOyNSOipqW7E+UsJTouRmTUPQ3zmZbueIpLHfPSKf7HSWJEBjgsaEqwDKn88TkpLpyUcnbcwm8Y
iPumYQD5xTCTPa0OGvKp7CwFPKy+BfBX7mmAOj2VUkYm2hWifRPE2ciLpeRU0wUQI2ygu4tb3UNn
y3B1ZJia4BfSc3uY9lHIXJwuIY8DEx5kT6tx8a/fEaH/tm7GpQyX6QiWjjsgvZhT1iGT/Oeg2BhS
JVkTjv6QaVi335Ib5yKLuuZ3pKmnW8G0CfJnp1zK3GM08e9DXGkIjZSz+q6QYjz+vbHkYajR4pdJ
krGhyUSjReRwRdKSYZKRly2CEXVd151Gdc4yh6wGyfJ/M4XAcHsM4JWG5eft+qaq05C9emw1AjPq
cr3KBl13H1xB0omSjeW+Xp3Iu1lFz+9Mfu+1B8eKijWCVAt1Oj2Fp8PERiU60NBFJtqjl/mSkzYm
QkzqHlVEYeC8ZY+6tgyiO3x448+uIA81QERFhfmhL2sKrpLKfKu7OVDDNgK68VPzWKl9zkq7DL79
0maqgKwN6gH1X8oV+bT2UjhmwS8geprdf43EM1FCS9op0S5emqakKfSmtQ9ne7uGrD/Nx4bnLEu9
IC/Pv4wXF6NBSQKU54uD0GxzuS01jMQZLdeasfJ53CMBaBmUqxM5UMYWaN/3eQXxonzXvQxMtsi/
aKRW0sCCkczjgbHy0Cb2GIwXp0H1chXXlRTBqa2YmmkufXur1/YKpa2H6BKuC+yJdTCJ+CHXf4qT
JjJ1K/qsGr3BuJGHnrStJaXcqOmymIpEyWA+7MHse7Gv5P2WlVdwlOjEylWNnI+fC127f37OZFNy
2R8DuiOFEUJMsJ9ebLB1dbQKPlnfEtItZpg6UO2qZ72+TWnQCwx+9Hx6BWtuuy0lYs66bh5uCmuU
9ejL/o5IEk1N9Pv468Y+AWPkNcEgMvLj0c0tYggBYV3T8rcKfUCA5IAkRQUgI/ZlVlgFv/9RJL6C
FmItwf5Y6hSRP/X7hPmZRxJX6LPlqMCKb+JEQwblKnFrjF9IJ+LM19FQs7F01ovYVt50VapAC1yN
Slzt0pK22vEyAHXlg6Zju/gmRCDd3f0aiH90y/ymrX9V0UymyPPkv1VZkF6GNdR0n8gPydvMr7d4
dbUNNiTBawIhsouL0RWAHdFF2uXqHJ4ZDir0GAemPH47/nHDJFGc+e7ivpF4k2R2QjhkX/MrW/t6
mkRLfamTkKOVgyl3VAvdluOlU5/PSZIkmAU4nRq0LThTJNbwTjnAuSAA7jYpt1SMRvXFL16U5+nD
a5OmUZ25XNqQI2WxdsCMJdJQHsyM
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
