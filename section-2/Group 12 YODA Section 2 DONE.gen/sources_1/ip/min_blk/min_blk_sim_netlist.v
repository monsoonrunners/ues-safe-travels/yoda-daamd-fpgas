// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Tue May 17 16:19:16 2022
// Host        : Xronos running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {d:/Documents/EEE4120F/YODA/Group12 With BRAM/Group12 With
//               BRAM.gen/sources_1/ip/min_blk/min_blk_sim_netlist.v}
// Design      : min_blk
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "min_blk,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module min_blk
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [5:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [5:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [5:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [5:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "6" *) 
  (* C_ADDRB_WIDTH = "6" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.53845 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "min_blk.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "64" *) 
  (* C_READ_DEPTH_B = "64" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "64" *) 
  (* C_WRITE_DEPTH_B = "64" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  min_blk_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[5:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[5:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 19248)
`pragma protect data_block
/qagrrBCw9Owhy8oQ1h5BnXPnoRIlzNq+zLmxWR4yVRgzhKwVgMcjQDsGkXPnJOTFWHhROi7AuyY
Xc8kstVU0+nKhgoXJOi+eEoBTmKpfSkYpE3WDvHAWs7ep2/oJTHZqlvLvo/hjbaigO6jKBc93CqC
XgQZV98lVVzKiKjV+r2GkRYSRbJa1O8NEm4MAc8VXZeOGfnHnmx78UsbjvQfSotZbYHVR/IQn1rs
hmtlkw+ZknepVAcZoWAywwWcVQoq/HUCYaJtpOpAmYX0pg4ZXGx429mCmNKZ4pH0V4jxUPsxwAZ0
UsyUhWUNebH5U4QA7AmCHTDYGWKOLW9dqcTWY+bi9J7aa4ijFYpoQYb8R9AkN9XuXYBEcIcqe6a4
lOHOxzqln8tQlT4orUol7utQzchAoSJITdvo/OOX2vEHmd43ZgMZYcW/Q5ZCK05Wi1kBUTz7DJCN
AORO2rx2709sw4bFrcYFGRWXpgHqHU+o04yhqwhefhKvmjKioMfn3Au2WVp1x7B1aphr2a0RCFT6
7/89KKqAJ+VJSjurpc0Mtw8MZ6EfapRj0UKZHPTI0RZEJ6L5XWuqbi6Gblk0O/nG4iYnVFFhiRtU
kZQkX75FYvuk/Ctt5w8iuNJlpNd6U5N0JfSnLGa+b36melxTyCx6aVqP54KLTYZx3oMqMX4lG+3l
0Ho/2ydkmBGfFS+7lTNx6PpmeKOur55J8dYPj6HCQJqS7BpesDRHNDyvooZydHYk/vOpwQV+E0kD
cZPLCiKPSoMzcw94bteSzcSIiG6ze+E8lptVKX/Ui0jOE3qHzgwIeQT4YqjKyNIC5uriw4fhyAz9
4Gjc9MaK7r3MEqlz9TZyOeVKkJCByO+3yzQkXPlG4fDhCuYlU+5/LyfBORHXwQ9Zt3MfIJ2YUTfN
adpMg539fESS6w9JV84c6oIpzzooxtgE0NnwoCw5JSM3zlEah7JEbPcXm+PE9OaN45M5czJhlTkn
gjf0D+++dKqqOLG9zt01ja/QiJCBJ2XWwD9BbSEjKw+QWkJMzh8u7eVqAj7BNEvyWvhHkp8f1EU/
1sS+C1DEJngOp2u2f5xpRk/+N2UJPCs54DbgX7W/o9JSbxuC0egRS8TociVJNobWavnPzpzlMbcO
PTU7FTTvLC1hiF/NLmZnJrNkR42TLK6GF0wpCZ1mccFZHCXLUZLbplQl0gs85b8ToznF3WR3Irjr
46SKHYy2qU97T0VuvqXQZVx68w1lvRgjbTJY74RLZ7/oiV4FOM1aeNyxaBU4w+3MAtqeROW6OWgQ
b3hdtJjO/IozlZOqED91FJuD+604mBT7nLfsp0MW20TJKRtpAk0Q4MTSDGRKIrG5GnLsLcsiqmFf
QOmvSxLBlzWKyfr6MERhCM8XgFN9febVLOVU2FfEmomfADLMnwfebjtL/2dW6JcZe8os3KicrLJc
lfo8Tx9oHYGgKdNkmhknmFGCXlVVD3b7GJXLJ7UxN7HJa3+20HrDgPGXhDx1Qx3NU/wlvvSjQOm0
/e4iRMVGPCeErID3GRw0jJW3ItKOXwLuUQ6fc4RxluMHOgp51d00oAjKRsY7GUYCOv6aGWF8oBGv
lWD3bjHnViFYI3VgDvHOK1p99NZPAVzcD5VBaBpmJKZv9ems5EVVXgGaMZNIHYxRZFjut6LqgsHo
saQwKUWT1puRtQwgsv7twCsked4budRBBsC1/gMw7Fon97uT6tM5eBAY24m12++B070/V/KjyNlr
GLB01e1Kijd9XhfSnWxFPlRgLlScublLpi7johZELaoxuuVzMXkCLSQAFu8Z3Dtuo4j/L3CwAybO
FBvOETECAMoCTeGKn5gEJ27j1b8cE1AVc4n6u0eKFa0QkSqem5NPUYHksgWqODJ2F3BaDAVtW34/
2CtssxWWANu6g4RO1/OQ/7EwOYJjBufKdDBNWZN4qgIoXHOg7LQgkKV5jkhHCX1D1YWMSkgmh5r2
PKdb07Z0z22x+frPVnW58alge9gP1I3PTnExsIoDYNttu/bwXWuy/cY357CRsciC0AHfR5lWq08m
cGl0NKlIjMpzOrFXHFyFlW3na/QA3pfuB+sNqfNbbgFbYACVIzPOC+irWmCzlb6WhrdDmHyEEJy9
u0jWojKB5RZHGxJGIte9oM7Nvarvt6xVFaSfHFx6qOTEFqW2UhfSSFCn1zUkllXBv7LUYrs1gXqp
v8Y57l44Gjrjci/OSOBF5VKxZOtEC6EqRGNNJND9GsJd2LnN57uEsoKanbsIsQ6cb0cCZe0Xq81j
S5IEmxRotM2ePrytCMk4awCh561GLRCZMhqnsQxlILrQhnJdfIK2BcPjYneTuxWOMzraW/TECeAT
MKjeisDryhMC/WTTwob4gK2PIpU9G+9fTGu9icTbj7V82UoDigq1Hc080zMDfRXZxdtAafati6ZB
qcTm/AjwOdjs5D7EXi++8aB4e+H8TtyMq4bX2RhO2+UuKpZ9qZYdGx7vhaULtHWbYLYWaQNuafeu
WoPDFM2QZbveKzYAoaMjHzQgCssrzeoCLti3bAkoGgKzxMXDiDEuq9KK5mcr0owHSYgduRN0zIHP
Dcl84VaKABvpcaex+7FmEGr6QJgOwm7KvjMU1krVhAvZXcLPs16vLjfi9wBszzBnPminSQ8GkFNM
Dd1iYqxzvgf4LqxN9Fi6B8tWEiEim8sCRNwxLqaZI6gDRkcHdQl0HJTdgXUi9ZvEa2z5dI5M3n7u
acyU8nnZlJp+0we/7uy1KiDIZCiXnFY2MFQgYvI5Y4epzPluBPUcrlNlwNmGWwuVdmwHCfWbJWg7
eYtUsqw0Vl+9qGKOzblE5lOWoOdqoVAYGqpuHgFYaGtqPfaZMoQYSx12PR3xYcpWxkkbb7rUk+nJ
X/9myNjdqpkgINfsh4VskglEM1Do47o3fhqTdN9V7wrhZ4B8CMXVTUYQXtz5pE1B6aj2KDtd6dr5
eExOKMQ1oWonDsiP/Nqju951B3HToZbbbGUpEy4q9giVE6Vzi3qe2kdktD9H18MD4Ed3Pcd9lj4B
1uHV8DB/WI+kp+aFHOuZ2TvWLXkWvefaStJhobvN5ThrpBLRGsu2hLdoNYcCfmRxrGqvox8gxEk0
14UWGuT4mOo3vNwldrWu2PJCEzqVJWdaQxpnnEJBUDInqnb9a9a7j5Q+nZMp/xwnjp6u+OzcJJr+
dUlp/K7gKvvMdjQs9WVsqqwwTy3VGcURWPvilXe5KEXUUXvVjbyvwBi1mQeIVnLXZEyuC8U6vw6f
0yaiQ2MKcifJafOivNsn224CoCWFmu2+YsAHmOvaGrnsEifgwqwY3vyF6wJ+aQK5Y9ItnrhwCV8F
gmr6rDFKbXV+dvxiMJ/pzHhsm5c16eXDrVVcrik8j07jV1LyPUf4LcFV1f2L3rIZ53isXdmWUaR5
3284ffoegmsV0U0/gBFW/Q/eOycVzpJm09IZNfeqYZkN55vK4eavFEo8XTmcGbiS6rQf9GPFvfIq
sEOK6r6CRAc/hyNxXUQUKAGvjF3fzrUKQvbhSx2Yutag8Oi/t5XfmB9S6eLiGbkTj2ExwsS7Zg/Y
pI7zS/w3WXk/xavOF31WjWsAe8ffFGn5TnYTpti5yH914VOuXlY/+Q60HK3V5rHdv61hhhRf+zUn
y3PpT/WKKPKJHaqK45DNpYY9wTvzsJ6o1FRXL3jKXlQPpae312a5XfubQgPdYbhDnM/AymHPIwVC
Sq7u8/tcjXVwFec5MKHFXQYP837O/9M4hE/z/ELixbLscw1ODsoZlyAotJAEmql6j44K9GCclS0d
cv3MdA+4miAi1PSCaImr+oYHJJUr/nMXdjNvOxGz7+91kU2AXOWtC69x0v8RPJ+T61peWE3owpk5
H/EV2Cvn3u9iK5mxvpN+uPHoGeiQoM9lxYUIS2E9yQjTVXcqwO1HBqtUC0khrMcIvwI4Cv6/6+ka
hRZskF7nMxoEHRyEJVGWod5vhIS1+Z1MUVwkDdz/hoklQqUnpG5VIGkIQ7bB8wK7ICMTRWhKVMgG
o5h/GiaUp4CsNR4BnDXAbDYd/XJiHUWt4wxg2+x3UDSsUcueVpammPtof28hSvXPZEOtGOIz9Ch1
Vl8HDzIgFrV42iSPdgW0Fm1zbQzB4VQl6PueZjbCroRciF/7CuhrFYB0rt6xstBz+sFvSyc/ns3t
RjwW6nN/2HwLX7055xueDUrXwmXl3OS7Ukve/sfn9bNF1CPNvc/saX2isxDQSnWDG+QMdLhi+eCE
kjvdf7c8eHh/XtnOkQLDe7cZLjIRBozNknweesb4iQRohLIMqjJimc+59Yh/j/3X7aLg1xZOfYGK
eQAMHZ7peX7mVlPVrboN2V/H0zDQnkofyag6fduWl/ecT7DMdvIAQYOCopiMhcTlSMjxTGpw4iJe
TH7DGGBEuhmm9/6kmygUNgQM0pdQEeU3B11N8rpjFwvYAElwnxHU2Jtgcpu9z7oBTkfo1AoWWKai
tM59Lg0XvcxhUF0qJy9iMCAA+N17pTU8xbUuLtJQqKUKComIWTF4nkc4kJnKWkmFU7U1NdWDYLWG
xNlWejrovRI3+8pm4jCLAUEhB+gIDIcGpUk5E3JfL/cti7Z2Qq06QlRDWtMC3HVK8t4wU+XCRwNP
MJSQ0AHILUtiLmr8FgxGQywKLtPTbH3E86UtAs7LFRy28iF91TpUSK+fjqy8PKbOs6XfAoNtL4Wb
CoAyDLO+LoE9zkFpmVn/edO87Jj8lOUdJJA8GRjWxfg8VhOu2Zqp+/IoyYTehuVWwQXPf1U0a1ZM
XtAPBmlv4MMllehYqBXf816IpSHtmcN2FCal7Jw/VKXlD4dBh83D/BYYrGFmQv0mNGotFWnaWxVr
vHTkBh2yjUD+cIsgrY50m98OuFoFIFMyY+P1Zlhi8WOmzKLLdeavVWWiTy1g+IanO2VUTrS3DTQ5
0pP4X6o9E56dTz6t/FrMdRzos0doK8dpgvdqXiplFvASUSswYyeCsMtdEEBbaMzG+pEBA4/gXyXV
Vho/ZOKIluC7Jn2B99XujZTGDoSIAjD5IwmBho5htiquMxWQnaMoG+AIwgiibb0msl1A7h1zuoyO
uG3ROseT2IYWLUc/mg5+4kgNicNDr6zKlh3E1Jnjv+A7hxN55/HVxQIJbt0Z+HDvbiMmOANOd8C5
yO1T+Ac2O2Uz47w+Z3ld7i8mBeTVM0i0KmTjwK3/FeV3k2fgeKWD/TzwyCSP7qSjyZQzKTeif9Jg
am7fsdkgDZuMVCoQ+IAxdqK+J1wLINlRIvofEgWvBmrZv0D+FGDSCQrlL1OZ3IFaySJ7LXy9ieRj
8ij6o4Wnw9fcT9lExo/nuy9Ku2eYha3qmULDXtW8nZadTtG5F6JK6ajg7hJ5K9A4alRw/bzsdJz0
5ffAI3XoxBhZKOwk97q5Tafz6qFA/a1B9gjJd0aeveHnENhVEymAW9EdR5p2XqIr7fSvjoNFmH7F
JWausz5inHlwc2V2UmaUvm7h6xT/mCdY+7PfyAJVNHUG40jsj3HT53AHdn7vDkQqrT5ouQREFc+2
AHBf4+Ls4cn+Q1rQvp0NOsC2sPeOy5ropbOKByJQmKJi0Jbc8v4reXj3XCdYWxAaC4C4Qa6nU6JX
udfchDsrd5Ojb8q7oNr72cZCBZ3rX4wXBT0UeyvPZ8pK74Uv2C+Zfi6UNi+9388s2rZK5scS1wTs
oGDotUN+TJGn9ZcF86kYXPycbk8UZWCmHa8JZ07PMeFlkshk8PcFSkSfn2UQ+rXP1bMsOPPUVzsZ
1df9kpVVjfn9aF+Z8Ry2oQ4hfXuRZB/8EYKM1ZFkHy/Rkt/mjQQz3ezL6Es+61wG0UnvTMrd1fGx
FUaWUbJ16CSyCoakXJe+M1W6Dj0yoF75Iq2DQvSnPybk8bukKd+Xxd6CuBWg6odjKz/P60UT/dBf
4o6Teza+hb7HSh10+3obFPcdvQjVBqAcXy5HDvsmQu0qNRDbOKBEfAJ38fjR4c539Mg7GpQWUgPV
H3VE0ifEfOrjjIy5Cp3GZBcacTvhw8a/DGv+TsmrXTWEOLecux9/Pn+N7d7tjB9kJZEN5kBf4JbQ
YagTr3wP7Tbg5350iZfQA9Eh+sUkVg/nc2OeJNzHB02X31/i69sfMZ82mYMgQ40vbIL5m93hEq90
45iCGzPfFshJULq0fYaACZxBT+jmMSv1lZzV/EFO0gVDTBIPWG5dnDbwedvDDR8N8zz69i6JdxD3
vhBtmqZXj/+KKGbG7wTt/nHVgRSPOg13Jy2jTAY06s9VDe5IyHokSmnrcETxXNvvc/4OD1UH2LnB
W2yZLz91WdqiZJpM9R3quUV8F+Ofg28RhOTV39h6dV4CjpvqtCGZmMWK77KsAwEOLgCXYyjEqAdU
K9aQzp9XzLKOHgqaqhEt9ZVgx0dRdtQkeFe8TMhvxgyR/bloAORq1AqwO2b9VbeDH/JZ654yM8Cd
M3b4A1cw+cSmzS39AKZMbm9sxXSHWW3xaWCIeSCzdrv8n6wy7EaMskNvTr4YPqF9lgMwvhgHnlfb
malhKlHDJm5pwGA8i23Bxm43MZZxVK5NLwkzfZPLvio+A5PntAw5nqog1g8mIfA9I3x6NXZCQ5RE
SYicZw5k5MxWBQQWD3D81EruHTg2CgwVi4xOBepQnwR8uNEANbRgug1VNG7GWminYn8d6O7ZhNcU
9fQk9vOIWl+ni7JMILBRsAIhSGdT1Fsri96rqI2S3KRmR/rUkjau2f1N809ajQjLV+kGPEcmuBTZ
dTLhFwt6fbDzoMnQeCwztDiMUiqriebzgBxMCe0PehEGzM0fI8fpjxoH39qdSotjQbsj/ybkteT2
h5cRQQf4mkglJOaLnjV229cIz5WPhiVx2r36bHiDrtEoOL3R0KIojq7Gb3dGEnPkiMRimAYDNZh4
VPHBXNi6HNDZD4EuJEEr4r2JhmOowP0t58ytjo/+HvDHgf08p5tfSVQsyI76XOGmiC59sE5qYybg
IKvzppY/HOj6vCG/tt7q2FV1VaI+MNGlcEeSBa34iJGJsMROaBFrEmvOV9UmGGPdr7hs8SRJhvRV
c+zMIFAjXxatH/xVlSIY7zYReRyIuoguXkj8OpOB05wh/1kUe6tu8M8PMiWKaTa2iK6NbVyRmB1X
6fK4wwc+ov9tkLRTSVHLlvxTweyMDnoE0hNDyldVhApFmqBOE+1LfYX2UXPPaFsp2EoVvXSDu5X5
20hnnlheydBrVaBUKJ6pHSHH73lrPxaTsSOt9O4NGTENgtQOPmaawt1HA2UoH0WwE5Q77yr9juhj
gKZ6hwA8xEqaAun3W/bkWjmhv2NZNXZEE/0C0M8zYsupZT1LVYDJGL1Pl/moCzI3UckUkDsXizsW
1mShvwDYC0xuI8BOI4UDyEPzv09hV7baEMmYoTj9SjVzTq7Ty++TXIqGOCYNZrobM5SToDX+6m+G
/FlXTdhTCy/pJdOx2Ey9uiSKo3r+W78Il4wGAGsBC05yDBIkHA3OvBlBQrwmHAbqep074vctFh6e
nMz55j3Z+MKrlEcJjyUuk9+JOFRWHE06LvI0E6PPMDBqOFCpyNgrMdKtLzfJ6w8A6XVTGZzLlgUc
aIgu30GCeuefGoF9d1D/TVjhGRqo77NJDxLYvoTJd0CwTjnyw12qillUDmoMxPXao+1iX2KSwPV3
X9N0+XxkqfqxrPvkjai5hm/Bl95flkUKoHLPgY0/4gD8T4GxMFtsH6IpTQU0CtKsb/vXkuQPDvHS
9qd2weUDp/8bnGw6U5nWbsEBVK5Xow8QuKIwv95VdrGMSrtO+BSRUpBUIUK1fyq6+rrBfY8qnaOY
P7kfoP0Q0IqMggD2WhQaNFr+KWOEIwBFSP8RIFrv7zXxj1RD3zdQa9r78PA0keQ020XYxNW0DQoT
6b9TFocVrTQcMNOxqkrGrTJErf/RDq3cQEbs48dQmY4e5onMcCMygGE+1Tb7tp6iceVoFlHrciaG
n26XAQeFkoNkSiyjspM4Q/ro3eLoLKp3MBJQErwGdNtOwha86xgRh5WtC6TnD/fbC3SVoXmXXVrk
70SgNClrnp9dmH8GAJPxajfeUlQ0C2xBVAo9bRhBYddSiweiTrz8wHTKH5YEU0pNSMAe9FjTbh0g
xrXGT1o9A6YS4dUpGypN9TdWXnAaljZ97SueEjVGbzAi9r3WueKesaRQp464P4lr6AW5k9k5nOjD
jr9cldPrecWUe2FJhHofWLeNd3EV41R4TRbFo9tYl46XNoYY7HGSKuAtag9KWceewk6zB47LjnBS
9pdttoQOh4TskYt7Qn4i3Vt3Y/BHUNj3EoBAEZSLqK/C1BhNJQBdSqEVqdlS8VXMlZUYt0p+lo4U
ob/bZuv0C5jydeGg3hCKERU/duQB5sUX/VPgJr0RMlaJFc/Z/s1Xh/yGFz2t+bRRHPBYdQDwksnS
lNbziAKHMS76Wks2cXGz+BWWcZF6pAotr06nabMY3s5gnbf9iDIgcEboI91P937KZjUsyaGWpncf
jYRmrJq+LYXg97XgyX+44r8nl6ZE+Ksu/aTqGonzmFtiXwHiMBW432ajXgej0g5BHqag7+rNyNhw
TVVnGQxVIv7TqmIIQCD9mc6PH2hHcXXYUVzwlPppl00Mb1UiTOlMMKVke9/zRvjA21K/xqycaPYT
5NWTt9O5+2vfWfDsmbj9BlPDql20o+e7tsNX528NbX83sVUvDtJMeFZsfiVx8Oi1Qil4HEH0W4a6
veaxG0PzwitrCxKYh5HHcHMxCH8XpRW1RV9td7W1n1wYwTsZY1vw0hxQMqTcmzoBzOn41zH/ynkb
sVVbKUBKS2PgdBFeEc2rc/c5AIVj0N4hkR4yY4rb4X1D/0jUNP+QQTHTFu++XESeFIWg3gRr2gyG
VROaKvt537h7WPIO80zoeQbJ6UhxbIq6nI/Re2NNluu4Pd0oj5iqHgpzZRn9g9Al+8oaXxT43fod
spEFRl/WJFqcv+CGDdLk44rq4KAKjPpK8sGGhxJCxZSHpNaAtcXab7psKlpjwFGuc57GorK9D6tb
ZKpXNM8SvLLeHdxLlTeI3tLuO6DdrfG75lVfajmSkX7i3440t8Gumx+RLSYlC2lu2tsd2Y1AGIqu
mAruIT1TxX2AhIRsRD/bIT7KA9TZapQ4yvX7cLoj26p35Wqw8agaZHdWDq8PzS1EFeqHAWIpp9tW
eFIZ/kH+92yF809IfVwnlt1IEODozi6atYBXSSP1AqsqVb7MT0hi77B8yJE7e3a/5UJE6bUUxAPt
J/o9StEasry4fgmMxAyWV1xJetnoQCdG8BzKVC549d/fSaz1DMsDiDntEQ9DPt7+dRgsdQBLWMOC
Vr3bylVxuWUoTLL+iLzrdf74GAhVNMeMDR935EyPyDTjVdzyFxFTDUmNk0uWhGnvFHYZiqCDcdEp
coX4CAnKnVjoqGfqu7rPdqSU21j4Ix1PUrHLP/2KzSq8Spt1d0lo6fkhqD2MHB95vqVhb5/apOuQ
sL7VbTru00ubwuCwz43FeoasH4yx4ntp6h6P0CAnV5Yw6p73k3iqlffPRtvA3Oz9ULFJsYAEkrna
8aou+QV5NnJP7sbDKL3l3t4BXec1o4VkWdHTec8JIsnCwuJRuS5sB1ERDvz106C/EQZGtnXli+N2
Ok4xdaK1J4gmZSrX4T55+j8NZTzEgz5fSGya7RgMQwuA1ahuqm/nDzQTRDZZtTO2TGNl/944w5mY
QsY1qGVtpP+Yerc/p0EtAsBX/K1KUT64V4vstVnHZpfqpE1ULNIAGl8Y0dLE1Wc7R/qafmjcFn3u
+e/yxjRRLqZ35Jj9vT4lI3Nc6qOi1lxD283IK+VtyceR2PfqB12UFKnxgjWKyJGh3XsTVdolC9u8
ei8KL3qmvgtMNLV0q91EzCQ0BBnagULly9PaSuwzIZPjammVRsm833L2Y1r7zDrpr9KrUJCf0qT7
xMQTyUa1pmvWlc0adhs5AWnZ0XRCt17v306swYSq8WR122al34PAyG1q/U74jTTV+dinqd+C0L3V
rJUMUj/dMlx7GNx9PCFecdU7eVzZacF95NbWvL05j1+Z/vaiv+sFKZ6cG2kYXnZ1R5Vh3O1MKQkM
VnUtLmTjOm4AwbC+K/iYr0UMlo0a1gD+k7AecNIq9EJiWagoVh/8yBz2ajg0+YgZan9Ngmx7SdFU
dii0LZLnBOPElrGC1dwssHWSq+L9Z1QVoB/CHs0vdyNnrr2YggULviU1aMuemV5OdMkx4TdZB26a
bdLCtbF+L0DlOerh1JGvZft6FItqs+z3AYQSL7k0J4wTNtg5a+oLk9w2YJcMUo0Sne6saoRYkrEl
sGFz5c4oqRvVT+X9PQKG6dm4CKznWUDy8N/8l1A+QxjvibTqz8cN5RI5grKwMRA651DezpzdEHqw
cIrCZfjojkeZtWyzy3HgDdRCS+f+HC7M/icjI0g9+aW9Eqn1PuO7rMnvrOEK/O4CVWuirAlnw0a0
0CPw1+9rL2pjSIhe5z+5b8DYCwrmaoN/5skYtZJBZgD1JIt2oIEk08PIuv1HG2xrBgmhsH8as7gz
p8KAdvNV3wGRyPau7dGIt9LlcKlgbF4OrrcJ/hD0APlg8JpMkU/q49TSfNUOcjo7S8hgSl7Zuuvr
R6b9Cgk9FeKu7wM2kkT+HuXCBeD19EfUSWhfUv2SK4uN7eNoOAHsHsKMe02+uTwLrBu9uWZKxamL
94CN9+nBnkVItzm/AGKsriPiPmB5Wv4TOk+wFr3m6g7vVNS5Ynec2mgMg6W7gbAD+z0ZzS25Tq/m
Z5WqIdHQJtOva9R5JtsYIvg2d8Cp802TMf2RSHUS24o2KchFoHdWdlL95Zs9IWCSNgUEkhq4Pym4
woOEcbl/3MrXGzmrZmqGi8lytCTDLI7R/duBOwEoWHO3xye/kzjZbTjk+TilDbPfu3aVWSiLuf3g
MsoZ2kAQ3kp1V0BzHjfXIjQTQPYq2aiZZsY2vjXXVA7gn7XnbgckGOMn4qA8QFE5JdSxmV9EJddi
IwqkD9xsHNZWw2o1Ui8KUqh07SE80KT35z45wg7RGEhr/bJ5DNFZNPlZDvJJDk6T94jp773mOCKc
tGiGUi+4A/OsBuYM+9h6triHDVtlhI0F7xuFcXvFp7KBDguaaVfX6uQ1aGhgGhvhwFtBjAvA573m
WqGxo9DNtMrQupThGepfa41Vyg5B9Jxb4xZB3HoSvaVf7DELCHO5YpNJSYPskBoLs2BWVjDQ4WuJ
RpWB6ByIscs9ducmQQczE7KZnANkSnc1ekvnyy1NW0k6hrmBu7LiPKy0hyHxcd+NhOZQCDFElfrm
SrBtrDpHkccnvlyn4157XUp9XikGNZvdsVkSWOx2gW+63n4zIST62dqJE8Bcago/8qgnDvV646Yn
V06LNSZBt326W7sZayh8yCducCFVDRrcdkKQG4ihHu4SRAnebvB0DSeelikFuQGi2vs9KgO8NIOM
goDwbhjr+DNLPXINNeMlvGUMjeX6KzliL96IsxDnFEdWyE6llCKpL/JzGVFeb9xI65LxM14rWqgk
YpOUlyNqceDtpYvTEDdlIqtM/LiWln2vF4Gu6Tg4TJBK8tw5hmYrWJ9bxVdeHBFatXyGP+5TCJMH
rCdOpac7E5QeqQ1hTLV0JcsR7Te0VMsEMPwiVZL4WmHmAuLxQHPVT13e7ghj7p0oOm0/X7WmuYKT
vmDhUkqeiaJsoVqC320gvECpBekT8Iyp2k9xXbGV9XEdseNxATNyOPM3JYqOB5IfbIpzLKChhYhx
V/utB0NKXouywD84s1Xu/jGA88KyXpGveP8Vh7Idcp0paXGUttdIDIwpq75OEJpyqnkwhlOrzckO
7VTZowZ3aV7lhbWpymzlJKEbbZYzjqlQrTusj+Zv1Gn7LyamuqxBViCY+WCHTsGds4zyVktKReBK
wB/sKvDJ3YCP3nW2LdB4LZDMwLrbRkX0s4QseQuiqImNcCdQ0N/HpzqVQ7ejFu1n+/YM4UE4/0k0
pFI35m94LiJG7PQaNggPijjjP74lNyTuR1+Nsz9GDMdi8nOk7aNt/UKX67/SP4/q4ZMtNTGZY/YZ
mUUWIl/eqIWq7SbpoQlAfTD6VxLFKV98FpNqfkrezFFR3iJU/CL1Sw0aKDmOD8VZgge60PpCNG+b
zuaNTNAli2QnWQ1PhGZ7BNiRqb4YBBRuQKzJv72SqndxFF6u4qiPxAF8vCJG53ucP4TNOZuUK6uG
8JvNks9w8xuMzkcM+zGbZ3UuNx5mswEx929cpCoahNdTP2jlr+Zi1p7bctzXBSQIVWeTw1vbSld8
SYyIbv1L2kGejGDSDyRpJxguKw2uVOGrc4iHE5vKn4uJaZuRnI8uthCRyRX1MtpEPA060ohV2Ku9
Wpyfcp/Q9VBRoIoems3B/xXXLsZdOFV6XzvmahtOsC7hznYZGpGqC0sbxtP9JMVBW0EecpBrPIF/
x56UBmNmM36VGeFnmeqHMo/tkLL6vWCMjOVCZsRK/2ZY6VOUbqmeURo8CHbxEL0iRCJCtAHOX5nt
7Tu7pj6A6auqLSBIMHjrKXwN4A/CP/L0qqV9wWV5v3W5OCrM1bR41QcFih88+wPH5r/zPWz0ldzy
UTy5Ud+rctpt+To6XNv9l351i8M4HwVLa69agLcw1/MFa2PMinft4BqwF44dq8TzqRSF+O1xjfDV
VS5md13ezgNN3s6ntvsseDvMJTaZ1K5ZnGG9rTnn6GCW2Xt7Q1uvj9WyT7AEi+Za2VHddJBaG6MC
b5QzgCV28kpTFBfkLxAxVreljOR5YJJIijJ7yTG9DBWUxJBdx8xz2mOr1vW80OjhlP4LLtm/fFK2
wU803vPJTIvOXpODholE5UgHHavwFTm3TFX7+qr9wrzTUtOwGk17IfWjiBjk1OhV35wj6js09kgK
LoNfg2XWRWsg9F79I0mj/QCFBlkBbFJnytfwg0h3ymWHfKBF0TCnV5mpasToDC84jYgV4sClmKG4
Vxju8TegHzzPaNpSboL/RnCXOmKNztW69MrIN31Q4k5YVJKtxgsY6dGOb79HtmOjFyyTfRp6f1Ya
vXUOg8sU+GRllfJF76nZxuBwAlf5fy6FyXcgng+6A/G6OwhXKrx7QpHyzu43awlN6E8ZpX0Vj8AR
iCW7v5X0OPGQyQx5SE9i74BQLvnkgIjFiB9mK/kyA/inCC0p3BH5+rhhstXzpSxUjMYztaLFfggn
pa6IfH8DfDmcnhyZ5/RN8B7r1PMZEU2lpPdWKLiWFGbrQPEWBr36qirqujgKCq0cvevzTTYAiKtE
oHFo2kFCg2/lW7Sdl3xa+j6/9KmhRobujndfdlN4x8dv5F3f/5S6LbF91tgxkE4KtPlwj4I0iqxX
V7cLbSgIHifdxcL+GEbs1wqbTN3uWmxl5bpciQ567HplGf6DMyzs54ORGPMpnovcLY/ZAPEkNeky
K2E6KJ5qkWIABYKuIHce7sGjXH//Vfr0OCn5w5iEtsXRrGbRP6FAZKeaSzqg7WWKSK9eIhMEMePV
zvPsE2UMSyKGoJhIt9updWEG5DlutuqvaLFVh8BUVwdhqSzcknj1kDDivKkHNj5ozG3I4Ulud9d3
cshz/mogVA9TxacuUkBzl7fC0d7Z70kulubDdfyDWrKJ0lk/95urIMaI1dhjJ5zfETaAV4+a1j5z
vuJVXqmQJoHjmcdzAoteqzxmJEytHtiUNLN1VfWmOmTMoa1Pn6x7N3hbOByj7FYeV5/e8OzDG4K8
oOr+dXOuVn6CW2UteAbYpfna/+C284iyaDfBIsu6RpWChOwqcQ/38wMvoDXhUn8OAeD2lI1Z8CWb
RTdHmWbO1d9UMjEbC3pICDRmk6rYscDZY4ydOdx6G0RZ6JikSI7P2Zm/G37d7d+msZ0VZfgcTqW1
n2WEyxd5JpMMZgN9K8t79oUhwXpidCfbXDg2BZCPyfMQHwUfTii5i1IRGjcIua78NqJfr3qS+4X2
LYjrikJ8rvM2/TpXyuARDElu5ALJYc8/pC1R5fg6WpaQmEC1O+kiAccjFelJw6CTBWymtN6wldDi
L63knpRWODEOqfXvbo3GzlPPOSiFFREILL58/4akBkORZawz0xhE/MZm8PUnM6VTmnSlIECZIjgV
BKZuq+MpA/rXk8C/cqI6gIkmOP+AR0xGZckDnYVaTaavF3x2Lis37sm1N6GBFFk8w+vehun8iTf+
5ymAkLG35n95LltAs1H6vC/l4xh04TmRs2+s8ZIl1J7EZATjr7rir+O3ne/T+K7iSP0jgrTFs6K2
ibXs5SyV2cjp0AkGpmFTpCFnZXYqkRXN1uqmSiOchkDv59N41SjpBmDBphtvZeYZAJ+gM9Je80+n
Wrb57jwehVee9ngNB3u7ZibwQoBD72Jrs0Z2Mkn6GSeW/gPKVtSYYMhM4SEvQpoBqm4j/nTaaayp
ZhU30POfFftoWqUGUClG8d/97LL3ydYg5zHEmctPUREe8fgbyT54JBoVFBGlNfR/aKDOFzOgypH5
w8zd6Wo7LfMDXtSEBP+mOwvpDEBby6SY6FwZgR3Pd4pVBkJmhG1LBxLU0LXYQyw+3Hw43ZJoFSVE
i0AXRedd/7fdl6kT1xrf27dsX1taRM4vU75g9NYFbNWIDcLJXPX+SBp6MIdrHM9w5MH4wYhqDITl
IbS+I+UeVnVCMUZ2s1TC6emZTcEBdAhtyAXYDjMU7Qx1EncMhqqcmxaG1+d4AoVQ3QZzmCkTXvzr
p6CGA/XCM4HYXgeVWsMTotCiWlrtsc1NhWLF7grqSPUdg3cEQ3f50fIzPzdLmw2BFrXb1DmNEUxB
e+8REilf/nOXCpmMTOLh1YFFNQSrUNwco2wpCADitbs8DeifiJwxkgZWln1L0h5O2DK7Y5tvJcbo
XI58ZVBlC+2OPhttdB0jKYZw2A9lezLD5IZQraTZ8OoYtJ93ZBs/n9nXzt9QcxG1q0sBtj16MLFq
0ammDemHHrxkg4+AhSHAcK/WE+jjXtbvU8WFS8eLBFYCyf77iMQ6ck0Z2AjZOYQ6sZKCwiEl777F
VCvqNFlR1OsPN7npWvp62ObNwHmAiUAPowpLbu2VTsXByHHIIVUy9hLuBv9ze1wlTPtSFarznsxm
WdhQYvFyB5TiOPdq5JuweLDn7VL0jZZJIvBWBEseBcyfk7FdEjIbmbaAIy1Iq1IwxDtzTchX1D6O
uuXyi2VQExs+5jTes53X7YlMGikZDD7m+GLZ2dY93IuwFvtErisLVwKDA7tb40ZiXyTf2CF7Z40L
emyicuG3frNN0zDoUZc2kItBRIKVuVkbKiAGpur05Uao+KyI9r+6y18RnUyvZ52zMy/LVEisFTZo
DEkdmMI7AqiR8ulVtH3ftmcuVzTnrLJrTFAbbUxuKhMJVauQBSa+ymiT3jOWmZDg4B/O8QRufYUA
6UY8q3TWJCsYE3EOVkCYl2UyObXxkuo0KXVx+VRZte0ACghjvtsXnktLauv5vb8zoMND58WKZqDl
6CgtlOIAC79AFHhPGIKC6e89nn3MoIdJcaSBHJ8xsnb8iFQMYyauSZzOA7FOohk72pgdxdml72g4
sshJyCptCMl+rn987L2GKazaDGzjN2qn5xJApl6h0KP1RVJjETqSPFz5JZvX1BA300KEORb9sP9O
LVXiyMR3n6upHpW3ovIFSKubbKBrC6jdXeTM07TR2AU98ZihkVggtytXBlNSnK2OMthtTbiX0Qnn
kjglKmXP244AYga3BFQKyXE7RYdz2c3DEWGMclAO1mCrQ/JHofbdyvtskkrCJnHN+y/DEQSaaRzl
crf1Frtat0lnoBDsAdBaIp4Rcb/3eZTWLnYIEKPNPUyL1ztfXYqP7OIjKOA3R9w0TCxYCvOS8T60
6slOzwt1jWwWsCsjFQdRqVEeNGVBvVYajXNlxPlaB94Qn4UWwT+kxjyWW3Gd5XakTxQYP7KX3WQX
kXB5joXB/l8vQZKqZPvuOuf0mOyMrvn5NCb85Al07E7Dep/8576Ta3mB1wUpQEIHdpT4cRvaNX7W
7Ifd72ZIyyQ5vpElLYKVrYEyJFMW2r8rly3yY/4qnZyJA2K2gfbwNo5gZc1xstAtK8y2n18MVSRF
heib8MlcrL0D9FfVgA7UJX1ID5gP9u7mcOE8pNnI0sZXNU8+Nb1VmC4w+zqfnvkJ2H277TS8o+fI
GV1/Dyt1/XP0IYq6wqkvX7zYxHIUzkZw38z8b/rwwlRUvlKtcXYTJuEiBnZkaoiMAK/x9v+2+njd
hbNout0VmI9lQYeHyiELJ+dsjEBxTadWQOtzdfx2i5ziCDwggBHakIKyqtPfkBNKB64fIyBE8uOk
7MOo+BbVGrhVaj0TcJKkH47rRCy8lxuRwffYRwfwqe3agqZFQTq0q6Ek/qnriWz3CaiUJhih2Bh1
GG6TcarSVcQtjcwMzLC1Qa4U/KVtwEnLXTH/viXA41FE4oI5SAftVgTqzXiDUUarGzwQvNU/uWRI
BgzfGl31g1aIJN5dd8W+gIxqacopVGVE5m20eC0CT/Nvsh4YeDl046sY8QQUOwAxv3ulH73itcLd
iXhYgC8BZGCr5pUj+HXebVrbf+gz+2WmLzeewnZWVQoUGdZShitNjZIF8CK6VYwk0PkcMSaLKfkn
M02E0ptTgvl5LoFkRg2cXsk/DKHUm9705rOmgDWi+8f4sjXI19n3ckJ8ltA40VY/HVhFAHL7vW44
NJjwAE3XSc/VtvLffrJhedtbB78DfTp+ylEt/1JctQw8AX8LAp4lIEYLbfG3PHhYrOcVatMjo3sz
t93eGvHJbSQ6xrNoYBvz863kS1QJxduCpsiksZtqoJL4OnQ8n0fHSkCcYjIO386/481L9boYLzH6
6vcMZqYyI+fI5WhL5cG60eOeJYel+h3An8idAiJGLM/ZDadxHfnxk3ZrnYQhoga26BgnuifK0kiG
xm7ndLZeNrm4n/BUZHJf4MapLH58KhcsDoF+QcE+GfGcUUHfELdVUPMZQH9ZYxPVB1OwQRR/UsGL
24I2lgiIu37R3mPfaIuoLI64eyTcw8M+hgKer4fjr6x24Xp2zDE/kGnjI43C43AqygTXjGXmQchr
BQCWiSFHu+HW+yZb0Q+jndZJrSlggCDjL3oAk8op1p74CUwLBUVerddowgVYFaXTWSNinIOckGwS
RPO48NSuyvFwyXB03SfJ6hwliaBQQEzARCu/f/mbPHi1iw41fHOKQhyqLVibAWoyUVhuXrXc1fea
rbhYvfHXltzhJXMECP9rfDiGhWY2RPKmHhZz0hfFTcU0NOhc8R53pgK+aAL2LrENZL+jO1ua7POg
XBHrYCNB01PyXdDdIjMwv9/uVlFIOcjazhIJYMz7I4M+lBxH9+Pw0dDAkRnaR+L21WIKVXta7A3o
sfXRXw5tUxA0nuCBbxnRrpXun+VM9y19sr47N3GNUCN4pQu5q2k2la4Oh0rXksKAQuismL+70A97
IgVvJyppoeNWMoDBj3FTlb4sK/+QxCwfxEFcSE1fyqkggIf+Hh4SdS1kzV4OvjbJ5fKOL9OboXJU
zHltXH5csZO+lxkOpSS4rDLykgs9z5PY1ubeUv/FqVjPhhp72wszWBHB46dFlPTN8rtQA8uRNQpu
0eoskmGyZUCG88N4N+AQZlpaxQKJOg3/jlWtsZ0LoACc0WpWAnBcIPS+bM+1iLiiiOMwok5jfJEY
PaxtUUAOa7HQhj5Hvwft44nyuiwvY3GTk2qEcT+FJRaWWmN6ba0sFPOU/ItqEJQ5srRD1S+hr4RK
YqLb2Q0oLU+Wh3436PqoGzxBFauS/VxSSGLWaPHmcUkRJwhbgrJ/SsfzyGdmjM+xIl+recnIbDCM
lwzpsRc9KhLLKMvFfRYwYl7N+Sg0jH27vDgITAueY7mWmRCQzFWuLhsN/3Ll8nrVFcSBesZvqomt
SG5abgTVUixFE74Cz5wrYzEekawOQX24FTCUGyrp9cZ1SwrVEHdoFBZT8GLfzHOtdJjASuppjfZX
YLMRae0VELxcaEUuzMaolQOIkTiJAXLNjmHTwrsYI6TcS9mOmAm1ZiaHGwfSDrcLhV1+N+w5fD45
CgjCSN8X1o8z4dH4di6UAjAaqIuAL+vyhH0K3IyuAaR6R0155/pOV52v1krDQlUSKSQNeuFC17x/
ONtznxVoYm+E7dqM/Zb9OPHgOzPekSsG6kiQDmC9BzNhaqQTSAba2k9uqaO5/CvSbkc3qq75uYj4
7lnqaRRIQDXypENUcMuA4Oo4SKB731AfRkAdH29qQKKhphkkE5XMZ9ex6tLHIVWWoKw7cIOg+n9G
07W4c4us5Nrpth5OrtbssfFQSOxR1yesng2qAsLQXe1L5MqqqdHxM9o+QVIkvQmTRbVAvGbjL0IC
owczD3JMI9w1nAXhsnC6ZHpO++z6fuG4DPBelnrEq8CpmnPHhroWEeNtm6VHutnTAZgb52pIDUND
g2uzG1l/intP3XiSXZfwdhO3Jq0fje4nL8H9e21a8RgdINTijNOLhnRnn5a5aCsXEn7NJ72IFgbr
SdB8DMljVUzPaX2Y67fUX5fgWPdn268NqR3xLFPn43FvEzZf9GA7a5+kWMdremz+s5ER63DO83tH
5r7AiRgh0zMS7qbK60GbXLS1xV0/X8iCqqoygrTcQcvE+RjtY7hUQsxXfIoYxAqhzyBHWHqPf9Sd
ucs2gmR7DLNhw87IWwr8AXyKsba5xMbQG2YjY5o3CNn9mFax9sbcQpTw2ZmIgIcUw/uDGKnmFB+L
/MRo+nvlBZjbxzWfsAJIrW78uEKgeKTzWW49g2mDkNo+xzMfjcU2Lpar4zPrCzwwDqZTxuUtIYQi
zKaSlfq0CIprBvZdIfRaOmeUUQ8ub6DXAv7RiSUQSY7z0e/ZQT/Ozsu6OyE1NzWuT0COhMRyqlrG
GngXbLuItJFO/8a2F+psLpwGCVaLRup12RZLGrfOHq+n7V5Bk95zgztLR6rOuvTv7wKaOA5DhIcE
FvIlBeQaNOMakYPixlKQQRboS9a8L+y9T5Rld3/RAEGCS7iWej42/jpjx+2pm9+1UpKa8XOUPhjx
Yo4SOB0kLLAsrmZhr9dABH+2xWWD0yG3jp+NzeOSuTk/6O+iyUNvQFKTHLDrIJDxpOnNDhkyuqQx
xBkUaBS4OMKXojck7u44COHTSty4BfcTgmnbSQA/1rPU7iPU4ehNn6dWqPW3KO2T74RTwe9SRFLl
KCi6WqW4kfEw2ORgOn6yFoY3E3bhwJKLtB0Eq6aboZ1uOuYroCn3lBHmfkQ/cnU9i4XSKw0xf6D0
3dOL3TZRIQrDGc+SVAt62hxj/O1/i1pH04KQUniWzEGfhrOFwttxAoGKiZRDlv0Q0+rLV+bb0AtD
s++spbbfCvgQox6j8uQ6jE2fiBUFzje/wMqWImm9Xbe7gE7i7tFAyT2cQurRKWzGcHtTCbQS5R06
3ohG/XicTkHvixKEyQV7iHDoz72BpAoSMhXAC0RmwPWkNJQ/sIIz02dWiUq/m3BKyws4DNGHUsei
s8031qSEIleRD/kc+5zyVg1GpwtTeyOt7cB9iLcz4uGc6Xq8i96p4l6Bvq1EXUv10Smyc/PXgFqp
jArZUONHbBUGxJ0W5r9Mycs0Sq3iuckxhXyXrrrsglVcgXfD/LBKaZ80zV3LEZW+3ScFNG5s954q
SaqBZXYUzY66AgtHgxTMy/975C/4Ll68+y6F9XD2v7zig79AAcc2I7AFu5lX27mvHo0D6/w95UwE
Z6NvwjMBFLUVYPBGoiUSAgpP/OZeqdAkATQkVStTSLuiCSO64gnkFzvBFz214M8wvAlG4fa34yzX
SnCL8JUDBXjrI2OUWktjc/mayrmk2Wt6uh35Skv/EwCsZh7tJZziKJRNVh/S9mhzXMoyZD47b78j
obTbN9z1quuV8ZAaYixB9DK/E7n4XuJKVlnomokoh06h1ORxQQSDe+0lEUf+FHN1TEczKjCnWRxJ
ep/Q+u1bw9KOzyr0EkFZkqWUTnqknv1j6esLke4YNJrMMe2GdVATutTololPbD/joBwlqyqXhn44
fGaEOgxEYidrIWPLQsAHjJz8icsphRLxnYpypS9HUhFnTEVv54D8I5wNufLW2Qc0psnMrzIfenOI
2kHB3RuzibQ3HmjLelQOUp8s31Pap05HduMbuEVqKGM1phwhhWpqeXEgX+fPzUubKdQUcqRMXcFL
v/DSyhIytT0hcbVrhDyM24/c5ZvfUGN4GqAqUHw8RjQtzeyW+W9MFtLPD2zjLh6ne+iJkHJ5KCLk
fyhy5+2dw9M0YDq0eBkdlDXuO8x9zUReYi31Rr5/ITzf+WXjxggDZMlJHjLFx3U2DkXeh5zGTFvP
+jRClAw3/g64z0GrriQnPAn6sOs45yeL9VwKwDf25kKJrfq8wf7p9AtyGbKtotDA6WN+ESwVUVoC
xRsJy/ZN483s1nuf1Wg2lzAQk2hFoG5hS/va99+btJIxT9ES7eY9Borxwu42Z1JVZr8GU6pOTMzZ
Qs1Gu/3Oe/LTgbBwTaSZ5JksTVYNma2XiXeOdDcBWKzcqDoILz1XKUUCln9ZyhMzsGREkqQ5umkL
IV0QK4428y09f7RRbDygI6nM2ctVI9TT9IJpTi5pYT6l1olWubCL4gB7o4tJSKt+CWWfPM/leiTm
6Jf+8IYZ+w24PaGfk7sQlPUsOZoAXvBpfkvm8HeYiDdmiSU/sg9PsKypLbNpBAe4kfOcrQvyqSLA
azwk9QQggs5JPmiWTUuMKJR5gB6+1bkZBTUdaRR5LBJc+p1eReli9iLY4LzoOu394FYM6G0sU5Xy
CCOjD05cejCmrKbybcE/0Ra+9Z+eeWQExlAT0CDSzS97Dx14YCYAr1LmlB0RSdUcVyZoeluVqeMX
b2VpUTm4ci0zCzeszlciGzwZOfJZqt8dnDJ9SRs3FqX5GS26h8rrxgdb+GED1V59PJtVMBtyKjr4
/Ob8uVMYMDtwMnSZUCG+Kyo301R7f1Pvvy8I9LhlvckuEcNB4Wmaxq5mjPKFA1JHkTPy4ZDS8Rvh
5Z8GrRKutxrQpoV8MLbP0X6ICIe0Nh2WzvLXoVFfCH5h620fkZJrOlJaF3GUQjTijRpjXyOcfDUV
Pcz3r8mesj3vSc5r3CpCvau7ySpxEInCeu6oyyBfdrBFIk9L5T5WvvvpVsRFOlbzncjOQIu4hTRZ
1KijzAJqZ/H6KUC1Ly/pWPqea0gq/zQsVRIjrIqJ/UNxq75DrVL2hmlHu/xO7Ll5Id/O8tq3R9R/
XFKsx3QjPoWzPak66vx8twP4+NXzEp7+jM2a6HSiU8fb19OsSqNwe0T4AQ65925LDpkyHyk11KtU
PJ1yvVWs8fQN+k6c5eop0Hcv8uCVDqyrRqR3SWJimkq/A8a3zYn4KLWggcW/cj5rGT0zNGw/mBPU
sFe9W3jsd6OSgyTtiXav3A6hsTZNsaD1EYIGkVCuUtSbIy1BUcjkmDFBAsDDSHfvO0ORw74aRY8V
PkQpqBB4FtQEpTJDgq7rb8USgY2eJwoMCoLlmLSyPKeGLfdqJnFlXBRnom2s32hDjP0PE0W+R3Iu
pPtdRrEPmxWTcsvmp6rAc9YlDEXpJKJ3o7U3xtyPmoPpa8SYeYiKF3ggKkIszgI/4Rl8C/YkIASv
SF/1uRFakGP5tLt5FW7A58HGvEoXdCAKTpGj9W2gCg7LI9tIcJgFuH4j3++S7VrSLnnV16XQXfha
vfw7Tr68S+vwkv96Ytq8mx8w02lnDgyRb6+9IPTk7xi4az96eZ+snnro6k8ydYym+UnqC+hFUcdS
iwQSTgNVHTH+v0nUFQPVw/+R36ob+TlgJBU5ieF5gnH79odMkQnCKbv4vzyeiJhJUbtFyQ5CTbg2
nwv+T5e0IBw7XR6omq5IjVAln95nSuMA64IIS/3c6sT2Z8fptwXLqA6l9RwShVCdQGN5bfgGxP68
3X4CYiw0aVz34GNoWNPh9yRL9hGwTaAud/CRPqdIrQrlotfZ7Lj9Sft70CWO8mxga5wDexJkztAL
DDwEFvz31TyrO/89q09bxIdrt8nEZfN6A5weUeflgZwbjxZeWCNb6XzMzc/nBx2Tn7ImdqXMwEwG
nt3bl3dR/9aFpkFXQuDC0zhmOhInvAUKNwKQPAJ0cYOEqiiBglOB2RvHjyTC/MMHFU3p1nUQhIsQ
IP/TGE3ZMOPiBorOpaPgIkNPVE6qlnAcdPKW+Ji+/fsmapG8DP4vrruppOg5Bb48aFKcVNQDPd+b
XhheJAzYWuXDYvBMGZlou/kSy8Kk8c2KNqIIK4QvZD3IRYwqI2Q7TqCHq7+zmg0nGNCB6A5K1I/C
V951ajsAtfmzjnoytKKXCQpvDF+Gwd4WiN3C6nt2A6GbBeHpeusogiYtYjMLvo/5Llwf9VwvwQFF
TxWY/bqLkl0UpEmBcM46MFk9OVkvrxFlu+0XAcaLcyvx6M4Dr1b3TpIrrf9/NN7qKRrWZDdiqUGD
xQiPg/g0Qtd1yF0UlnwekMK6rnwufMRVf7Tw6ZXjvVGFwBpan9+6+yBP25mzkhvbYxodqcs/fiIJ
IGReNB0+4cBbmh76FidrD/Mbr06mRZ6dgsBaf0czRBWeaml3NqqfpD05Eos15G9Eai+IjOJjJRfr
eu4Ddkh9EUGvNZct2BDR4DvnP+6k1SAv2QaCKqd73Sx12DfHHy539ybvv2dEqzh+PkXARtgP4hMN
Oo05Zk1sVcj8yaI65atC0LB9YJCWLdFOJ+psuneST76rWBEb6Pkp38wXdHdA2F4kdMHF3xa93Wmn
djTz8Gi/TexltoHOYRQsbsFeATtIdmT64380wXEeVvSsXI06m73VZrsF5ze8tGPc90Im4fUVbmUi
cam5/vxlUjTcHJM40Oivlo2oLLc3pjrAil1oeJw8S2TshVcdNmCOBLLnWDTorbQ6E6GbN3YwBgCu
Qq7a7sYV4BwSlqqyw8oNYR7CKMJ5E4X7ZfN6FZM4QdzwP/lq0B1Udsnex2GIZ9EX+7h2TBd5IYrH
BZuT1Ly/KvA7x5NVOKtW5Sd4ERCG2s0O2I9VKqmqbBXq/kms1ab178ORUbo7ZjWpgMOgkSIOlCEQ
Lt2mSzuWbURKdUraaAjkK18DvqMtLexDc+y76YrA+kXAVxVSf+Ef+0Bt06v6SnGp2/Tw1yiRBFYA
8153i4XcC3e7GkQwJw1HzG7xOiFk9XW7t2W44gMvftV/hDY8Euwz2dGF+Z0oA8Eq5pcer8hhls3D
BElOdH4XO1sQL1H2W8tOZ5yfZFRIM8DKp7fddfG521wEwD3B5xCz/nRp72d4UVj/YzW4Resk4Lh0
q6dsedyNXIUT9EVGPUNPFwz0lMO26R7nFh5ea3r/O3KePewch449WI/i/cd6Xy/gQrqOUpSiYChE
sTwZ7IJUK1Zw1I7e7pwkqSP7YNN0/rqIgCJeyrxcUPuD0khWw+yxjUEiyN66VBHiNl5kwXR//LqA
stJVZUpCzaImJEJ0D01a57iPPvEYxx6zNxLpVe19x65UEMVEOVtlphVwOg9RJDe/omG4sknysIa1
zZS+R5RETxaFquDyvDMs/ocVO8y+WkIL9g8yK3cH+IDSE00vR9K2yuRWMDFHUOVSUCylwgfhnv9C
ZRGiNr9VYAXHHnVmD4hzxM+prMuoDgVsKF38xw8TX3BSs6IRy2kJdB3MJ8h8CZnFh5dV+iqOaVc0
p6VgueojkEEsXdsTyVq3zMnEfEJmPyl+6CPoNz7odul5DJcN5yUTj7ErLHALlorSjnlSERFVoxNi
zYrR58dVvt5BxpTN5sEwWF6GN5HKxqosuPxI0HW4a+hKnlO/JU8lgaKmpn7LFdELYqAv+xmyfbrU
f1c1ECtqzLA5kiW0ggfsSVVvRyzjdTLb6uFiAJ43HCE0FLB731HeMre/P6GjMW17MxHe46afN9hR
kupLPgWYLZKEkVN46A5RVXgyiIDTB3IK/k0jby019io9Zeh66b5No7BAcCPXYH7WzfWpwyAoYHFn
VN2cmO4idhQutBKe1Yh5tY8JQC4UwQ0gpjk2Z9FqM3n6DIHv2poqlJIMYwjbMW/ZEDK9wViLZkB/
QAA2fw1FYQoQeHikCZK6NJySecP5vRpgDH1H/Ea7nAxUhqkhbB5hjava0qA6o6wytl7HbIdg9gnm
/06dE7+10jgVG5AffHHjbZvME1TE+wAd2nsf6AMv62GZd7j1p4fDBvwUptyg/mjYuk0/oCQTCi5O
oBbIsqqEjdXN217mB42oYKqXtw9sp0O35Kl/hdx9yLAwKYDq0dP78J5xuQWoirkPV+aUYDlbQe4r
GYyStLIMoptqSN/vHTOxtcfdGn+tgK8cAiDOHV2d6haBErqTtUzILrlJYmpkVmdpiBZBT0Nqfxgu
NEC5jvKrKDXnMV+3trlFKwz9IloWIcQyeqOlFbg+jsiRUT/K09vJGnD50YED07+YgUadSr+sQ3a5
jnkrTrpZia1HTgq0LOoqHbplsHNrnS6FCq9uVcGO/OegfNy35b0LMiY2jVYzS/mYc6RPv+LJl6tX
ia+YvWqr63vOFiJxqWXVlo5ZqjAc3vodssW/WnyuoTgKQ+1UX9Utfmi2agXMaAVLpncbcJQ7Pdvr
Ug7mHsw88URSlVWp5TQB3zAMHpfzDh9+ZG9GAP9Y2pttCB0ThWoaysfWHKobZJlYOiexo83KCnuW
uKxevCXwE1/1N7qjQOnM3MFno7Kn3kT64K82TlVZRS99goj4MU7U2qUV2ZfvI2RsDPFqgDoXop45
hRbH4dX0bzRHLTi8dQ/zTL4tQDtEbkltRb8BMXLDop2ZFs8P9JaaABhbxi31t7SGnNdv0NNZPvdT
uvOSH0Qa/7JMcxT1r0qZvoWJhLCDa9vvyWtjNTMODGw9Y+YNpGJawN46+odnaCTZ/ZpyJ4smMS6G
O8oZXcovqIuHOwJfX81BYhBEO6HNUuA3JT93MeXse7DTvSWNl6u0rYSKsEkN7TgFixWrm3eBnUve
5We7wiYA6tV2djdB6zmyve2wYfwjuwNQjNiqRcAhI1ZcpGxkmFa6DMg0aieSFECKRSKZoYPTYzS6
vFMzaZxr41RUplmC6F1CfJYLl9cLV5tAc75JII89NUyMRzmn7ozh4aSWy+b8pLhYIZ7YoqY96GPB
01k/tN+s3Ye48AQBH2GocwUzHNa4ulU+UtKSroOspJDA+27pjyvQyPEfrs7+RfX9Ab7oao1iOUH+
FJIb0WpFMlE5utnSRAK2y5LORo+5skbQ2TfnskNHB8wu11kVmPK8bwH2QjDcOzHhOTL9dUKWub4R
hEagKj7W145FQvq+50qUUPQ54VV75+pNntpEcaCdP2rdn30vOell9ttGhrQmmPWr15jllgpr6vea
3jIVpjTUkmfmC/PXy1E5usT3T9AjPnSb7+j3clrBRLT+QPjP+dhfrzWsQhSOOgOSl7Lh/+gsufcx
hIwzef77F+uYcb/cFh3k0X2HhUvTKZvd7Zkcj2HF5OlUh3osV4ox/Svs2W4ZHn8R6m8HUBfd1FzB
Hrhxu6/OUWU2QDcBAC/hjQv+CH8TWUmnr27tleEmIxX9mDUuPj6EhyibxkLmaE+sh/YGoZOD9sUV
viAyxMtFH+wZRQlt+/ygCLKzmvDzFgjT7i7wt7oiRDcFi14cksX0
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
