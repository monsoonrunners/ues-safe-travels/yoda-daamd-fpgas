// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Wed May 18 15:45:43 2022
// Host        : Xronos running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {d:/Documents/EEE4120F/YODA/Group 12 YODA Simplified/Group 12 YODA
//               Simplified.gen/sources_1/ip/audio_blk_ram/audio_blk_ram_sim_netlist.v}
// Design      : audio_blk_ram
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "audio_blk_ram,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module audio_blk_ram
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.95215 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "audio_blk_ram.mem" *) 
  (* C_INIT_FILE_NAME = "audio_blk_ram.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  audio_blk_ram_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 27680)
`pragma protect data_block
f6dYi0mKO5UbyJgZ4zrj/EKOlr1QE54xEh8wolLyPvX/2IBchepAmwjyPExqnZB8PqwEZCe4tLel
FPlv1/e/ZpG95YLosPHYOcf3sBISbjKH8DvNuANSzIcZaR/STldUUWjphHm0TPDIQVGfZ9JT6SJ4
gT1CviAwKwyetiw8Tmz8aQb+zxeycctianPqlJRkfs8gpoJwpaz9yY/68N/i48hUaGNj9YI5rB/2
8g/zSbNYpOHjt5q3fIBX49NNx99duVuY0oXlbPfRkZbWgqvh5E8FNTCmIOS16pNMntvcUoTKiOjT
2htWLl6/iSqcat64jGrPbqPLtSvaFweckPXi45gEsg09aMl9rewFzl+bALv3zUIxjRBzq4IZZV5w
nyPb0rgYCL3qHiVCIKM5WaAn5rXk09tLptTnXkvIuDauXG+Nuajwlx1gMIizgGmnzb3SQyKpzP/m
ZwWxKr/K+EmxgtPAP/htHOAzWHymcF2JW9LJkymb7w7MvKZi8U23877Mcnch/11ReM0kEcYSqf0C
G0GIslFxPPZjQMIirL4k5xNFM5X2kPP6CGNwHeky+rRSgY4YDsjS1yfuW4jrQrt5OnZaTgCBzivV
8K+zNlb8g1z1/SAItLoIFOsxEFM/vp2/3U1Cvfn6WQei0MWSlmTKUHwoX+m2l2d3x+vAGUJwPj3I
xCB0qqzGviSByZGfzudGTE3LuvKNJGZ115bw7sA6Qy5i27drhGDUWESfpdhgmu/aMT8zcPiEdxnL
BCqLdGOLsgiGgxtL7OvsLxYJt3qe7GgzgmopA3KyAMJbkqqO+NlB6PNkMQYYn/5NRy9ChmFrhLpU
C0CiyaHcVN9inwuoRND/pNkc4FB/65pDQZOiFQEyloBaNQNY/4F6k9KzcEW2Ze0EOsYeH06nn4yC
TjCju62sLwRugfICuvom4qFBH6wYJAkHnf2iLbktjwHQbJtOqMJO018DAm+TcbNMlzEdAqBY9E+v
7WxEctEVn4C/AsUEEP7ltXkFkHGJLVYU/RwyqiIlCe8nREqjbxlNgjb6H9LVxM22vBTtPEqTl9BM
/Ya77Z0E3RkBqWA0+/raH8DsmCF4LL6YEu1H909tG/A5HvtIiXA2Jd7Idjfci55dvtO8pwDu2OWa
iagdW75WRR4YyqXOGk8Xb5Uu71VVq/7DigKhsEjwdw9uh/QKhLqZGm2DR3ZnOIJXXNIoz28tI2kf
1zGcXsP7AXUlMPpUtSszsBzZvmbrxUkpAzXqInrurpgKIQ/w/t6s27WpZk/jBIL/LKMJFDKezlPh
lVyOzaMj1uc4om1fI+nS2XyK8YfvtCVKK9ryk6wv8BEOHL0WLO0U7EhHU9PL4Nlio2RxK6SAYA7V
eE/66WfEX6Prla1kH2j/cYNIeL2mwph6LYTmorvTpF+Fo4iyGQYKty2qUHWMJid+Pxh3LCsZ/r41
i5HBnvPaWg8dWwSmfgXGbbl+X+zK3C8MLQxS7zagHxIgZxm/2gTZfIaajc0yihmBB2TiNccSffa2
m22ju+fdfzI3AVYaG/AZaniECh4rTxDfFa3Bu45pqMPYr7U+hiQl4BydnciNzGsn6g0eR2ZHiaKq
Pak+x2sRxBbKddMyx8qYiXWN3FElBj2hELgj/H+JMOoLzKTwngGgSHM9ai86XB/vc9K6+LNLIKOO
529OTjvPppwvwJcLUOGGxggTQAUDlzDX0cJfMnv5O5/yosgttXFS7jcO4u5MSDvmcjYbpJHRcNqH
psTP9wXd9d1pr3wORdfNKqxqRwbVaXxq2VLgzZTRJ/bIAcy0DL3h5/jMgtPtYKCKpcSjc7cw/hso
1lMkAdan4PoHkRWFFhdGf/+sdu1I5xNIZtafv4UcUh/Brk6J8zLxFIBENqiek9fca3Y6nBMlS3Mi
eNUMDRa55CyWGh+meH3iCz1j2z0YYX2Fg8EtUk7OzblnF3Cm+Z0tvh7x3xDKJ9LbZ/05B6VIzOHB
tHmmDiIJ0H1al7+fR1S+hn1xkyPT0b67ZayYGMP5OW33pCmxoSNRKpx4bk8XDn5cA4L+Sr0CzGAT
3uo/6fX9YnA9GnLFkteNlX3nPQE2tXARIBKO8VAEM1JzFC8lG4JGEN4TSVDAZIyVxeTFRYmLiHmq
wuLrbh7PNrIdMuAC6wPCiW698sAMYkupyJXBtq8OqdQSH72X76bULgxtGvrZ2pGhWcJqLkD+nVFo
8aU451VWXY79nEkloSRfobtrWUAQtVg9iziC4ZQsLsoZslJQabxUgU+rzLb2bBAOMrwXIs7EuyLY
Ghz+aBTdeF8XMV0EclQUoXsyI5kFUxQNSgqMOXXZ9aEr2dkOCNDAxSNYF5wF+qLkziSfwwM32ZRq
vzMyUR9AW7Ed5IKltMnWU9Nbe2/mUCJ/pvKOcgSJUtMwzQIQc1QsAxfaRm8L0PIJK+npV2Jo8Xf0
3Hi4Tag/xmK/sn6HuqXArvQ+HaVGHzSSzkKK+3hNMZY3usXUkcrTGUN0uKTXmYYWG65M1kqexX+f
Wxjy33r26cHizIU985rtcj2LPpqS6bBtd9wE3h6B6/0pKHRIV1SyGhwjTYn+M/czYv+uMa6/gsDS
ilFnbAJU9eltQU1dNPtZ3vGhl8ZC1DWjBPIUOHBtc2yErJcKvb9mE6ZlPvVD3oUMIgjveFY6vQXL
Pcfekew2Y88HhtkonRQlJJSKz15sIUBCB0y6FefGAVy8cS6ogxZeWa0vR2f7EaOJFANg0F6g6yfY
AVNxLNcBf1ryKFQcKjGCKAHwroloOTyQaVVs7f2Ny96OhItep1elD+SBNonp6etop1KYKuyEiZOz
MgcXGr+/uGABYtJgax6kbcjrQeHZvg/I9NQaJyMy02jMZ+s8Mc+zuCzHXwynpv0VpNtTOI03pvCQ
6E479Kz27OhbtkgHjq4pu4ebV6WTn2Gh12llERLWJQR58hCJcUkupM0pgTQX0Kgw3EvCi9pECGmH
PWlLyvZTyMXX4FCzc/ZqQbGY6cBDJTqKTGyel767eGjiuYCz+5pC0TS6gB1xt4j4gIRuKyVMcZyS
5cnpMrvesKIeloENTgcT006nshABmualCvAkNVgc41bSbRs4JAS6PeO8dlTBm0o+ZOyAu/ocm2tV
qygp9SjA6pIOHq7XpL3nB2WJcOQyEkVn8vijDbAVUIugcYx9tyUWFxDZFUn2NITMdXBCXMGRzr0F
tyW/AtnF1+C+tX+YKT1D2zeYYaDT2uE7E5t64JCexqUOMrE1ni0QbYm5rNHYU0zjHrLkuuWqsMtC
w+AjfPa8Or/MHEJBmVwqrQO2CCEhHIdhx8NiTSsXQlMbd+xNphhkQBPW4a9++AW/Lr4aT7nvIX6Y
JAQI1ajOzyU6+45Itmk8I3exzBQ3Tymmdw255cIu3DTRepLOM7O2swkjxSqc4jCjjq+PFbty0IW9
rAyXN6GsCDA3FztGcKz3cC4QsZhZcO/7NhQuX1DqM7DLM0iKKowrjk9BEK4s105wzJKJ+gaBp8QA
YtUHrJGXzVw4KVTq/WK33KZUjIpf5AozXC3dWzf1aLaCvJuQiioXaurejQxV2+JAANzpXqjyP2m0
Qp6HjjtTT6KCWaH2cDNYVQpYU7gJoYB/0mbxMt7tWCuf+zqXxS3Y+jxZVb5zHcBHCGc8Qyv50XXP
TXQmv0JBp8pL2pWhAi3V6kHDWn2oXqaRe6mYJAMVNicCFX+eX1oFrZj78z2bNYcyFTNQOKNzPIQi
m/WTUA+anqbjhAtkqK+G2tEshU+cDuWfxmgmX9RPz+1+hqwoA04MvwhWphXLWP1OV51MUhhsYnMz
y3igFXsA/Bf4mc/WgLyAm9inl3IiyXhhSd5pXGTwC7fDl1h6fVMGkO8yRrMSyavpBVySQ5xkzu5j
zJ6zPItrjLoVB7XoLqhT46Yil3oiUUh2q2Rkm0YRvfCbjn77JE7iVfz1SYxFSP02QJQMvusKW3h9
TMMbbxaB59hSPPDLEcp7a7K2g6fZ7UKXDy26J/j+nsKpD0UjHunRi+EIzGG3Dj6xfXdsyg12CLlO
JT833WOJZMC6yPiUJv14kLIIRJFDFoO9sEvxXp2hs4Qnd/2clGVyuK/CI4Gkpj1ZKt+OUdzPWkLE
M9wC3i8+NZK6OkYvMOLvpAczo3l9HLVMAzmTr4YDsBj2+Edy8/h8hi2pPKkTXG96iKF+pMxlwUh+
fhcKx+RZmEblS/IHsp94XhOF0M4+g8Tk9bPDNeqanuxfTcJwcOTNFf6bc6eg8vNZvrNsFJjU185T
m/uzTsQ+yAQbsZy6aT7lYCKZGIz/cABqF5PIkj9iikunl+WGfOuB97zeUNHBB1qaTBr57Y8W7oCp
hsFgm2oAwLpncDl+jSUwjqcmsa53U0zS1/8cjvTNejaNe5qG25SwmKbf7g1eIF2w8Q1ksZT1wzZ8
0rEGTKAJYELO7O0TEyJqWTBCy4vkoe6vC7bI+8unJgGid5rm99/qBhq9x1SJVsm5qQKyOc/Kyoeg
wfwaQg/at4jJhT1trUF2W2mPv53vU+TUrfTlbpQzIMT6BTqqHKf0cesXwisUxVkHmE9M3vLYYYpZ
Fk1XvfIVcdU4vi/JHvG5yg4RIdKtGQpcnG5R6y6rHfoIgoOWq+7qGAa+ZEc0p8KYxOCPLGlwrWy8
4Xp5Mkow/D3Bnq0IpWtt27o6vzsfOJex2O3zSwoibMFNU/D8Sd0N2cuN7hjqCUfXL+epVasPQd6k
xX9cXS3d+O2358DQu4+skN45jF8Y5oaVLkAbp2F6FVvxD14mqGONfHxnrmHMC5Kx1kKLjnNFcAI7
gT4zjS2ul7vcC0eEwc5ImfwRHeyUaaCxSdtfNi8Z+gaSdxjKOb4L5EB/8X6u5HKtR+zpcsM4aR+9
tNNQxBCUb7GWZGBRa0tP/ItmdXC53wFQPsddLvtpgOAIm3BCn4kUiut2CTFbikXHSHgZDnUW/bzm
uD4TAqHrrERWxOEvvK+Nt0ock3FwxTC0TEgtPK9mzt44RBEstIQa9X1Q2brpUL4xrHKxGoyakCir
kAhMnQCab7f+pPApx7GzcKndYjPbbGwWsFkHE9faWk5GZymNyB0atLNRw+fL6mn4V1CLyHXK/jt/
fCHoHBmw+y+hrUhetQe34QzDkFxldHtpiM5IZrvWpgP3ZMLvgeMkz8jEiq5OXVlMe0ZHV1N0WacK
1XjDbF0MtSAOBaH9SBMh/rAFWLg/CDJt2yPQllu/uEb1Wxh2FHCanPXAiQbIEFtcKNlvYycu5/z8
vPuknZ0DxdGxBBC9ejI4GLdXgZiN++pil6G25JVc0Wpbo14Axs0uAr65TQBp9DuXf+yeFXH/QC2/
BK71RqhoXha2vBGO7h6u3qCumutOQieG5j2tkXTbrvg9vu4yW503VHvVOKmBnuFgCF5jUN+hvWOu
KahYBbtqzDA+EkZjiRKeGkcPsDgMlK0R4bpcK0JA+2Ir5rZ15Ez5usixJSdVZHFyfDS6rtSaHenD
OKcVJnb6s3IZIjGdAbDSg/dVwI9JgbNXah81wXg582Z8TTA+iKmNSQl8QQ/bh1Ml8ErA083o6tj/
6VTkuORMTxafN4Bqk29x0ZkKh6LTMUo8K3DPDHBp8yUdTL1M25WQUEOtlrP0lByKOHPSyAmEfETo
Cq69RDbhpQtMokE/q0y+fn05uYRbjbhASy5qVWYLfVQiBviUeaFy7spm1NeN8UwcBHedcK61kMSk
C+RH5+ZYONycX6kBu2GCZDkFciwkSDf5NsTk3LgQyK8tfqL/mtw/CRA533GMuQBkxeSk1v/NNGoU
SPHj6iyfiMArWji4J5sob7SGJy4F/o0bb9m0FmZ+PaIEk+eGBhb9ZmMogSybUnTafdoEr/Kukk19
5+dUbcF7rTX8Zt7an63PBjC+0uT+7SaoZ9PQpAJIrJ3yHiwRHhoeCsQLeJ5xu0JVuk5oDMt0m0vY
Ygx8BGu1uvwCJb/Sx+Ume+Sd+hLgJrOJupzLAtqqwekBIqH3pHxodm42VCDMvkoR2G0lNj1xOi9h
qaDVTdhE9dUPNhT06V9y/EvzwGRoprC0Fi6PPmJZmImkK9Od4gPf8wPTmCm2d0Iq21M79zmsk/pi
W9MraXIwGL3Wbq+a4/cr11aWyLFaP1gUAMFXfATnbmyOGhBulLWnX+8YKQEumEuTMEBuAuALs+Cz
8RGlHbaw/F+rm0kpDV3s8h1nYOOV10ECg9fPv8KU3Ci7ikYLKiHGdPQUom+sQBNuqn2IpZlvLzHO
RFSH6/ok8RMbBBr4Sp/5UdkzK2pI48MolIvrCNiu/5qIdRz/V3HWBPzRSnMxv57+ZdFGVlGWD60D
T9zZ8L2QCvWNrqBiYXBzn2jwMs8XmR+6xwpShXxpFPwF+fBgQGspTNxYhTg7xWuVL3ENVbmWfvl1
unV5P8EztFAmW3iUHK0kHLUo6cz0u1xx/lD575aiQCt9O1UF4G92KCDMrC5vI+MUH7D/oIPCs5KW
8AjjqOnl/vkNLDWdQ6r8wdTvgtQPv0AR9n70Fa8/9HaWT3RsZCSFMncG6MX2gBdKTZuz8eo1/Rb7
28kVOOGaJCB6gom3MwwW44OM5ick5xPS2uu8k1PJQzd687xtXRaTdihA0EXRAXJtYd74f9MQ+aa0
OIH9GHM5Rl5Ihn20qm2/yNmVKJRPtM4aV6bP0/1b0GnifnWnV8BmhnUKOYfKnSCwIkwJvwVQfKu/
SCfDvDUfcbUvqLQprvNz6LGQAfPBFLuVWNY9CFIFKCfdS6eba6uzaL9Tz5h96b4yZhXEG1u3pHqA
zvu9qThp98S0eL953jemCu8u0ZQkb6FzA/tv2iGEdKuyUF5QKChIG0QHS8vWHWz7r9BZ1WR5lsz4
6QgequwxXRVOL1hDQDKcYVhwmd5ghIw+AtUlEHkDobAC5z9urrSOmJa6kpiqXi1JvhpbY5WqIfni
WLc/ud8s10SDqkByebJkItjjqp62OaExcQXxsK3tyv1+Rr7scSZi8S76dRkIl+s034NNya7fMFjW
2BI1eLbah2KYav7CjKMnre4BNabnO4v/NYv9hIIQq7e9DLEHUgan07O4WQW0x5auCxHgvaTGYmem
DKJ4IdgOScqyjjLBsQyUIFlDm6gX9Cl0kK9s3jIn7pAzOcuYwNEsofxB/sRrnVEKX8bJi2SUG9+P
FnZPiOazyOEJ44k+xHRrh9u2ewj78EWdljbj5wwgQGUuXtzxDn+ZDC0jijwIlwfenpNz4kAleQsp
INxUiXg/CaePBivMUOcks9CtXrFhgcNa/BoGJYHCb7nK7qGbtqc9mDYS058udLqx40AOL2cdwQbP
YoTMYoqSFzbngRNJ1z4hVfj/iEI/haAN351d56Hi7765U4vfdhNwSuTf9t13i6OYZS1T2tSv3XiA
nmHdAwOz913jFGcfTwstHh0kQ7H6OFsO0pd+4dtvntF8UUBqS8K5V0CwK9UdQh0cXiaHuv2UGk20
8QNvrH9eslHmk5zobiCN8e1Qnn9+U80R1oFxHe1AtWpBpfeOnBEPntwY6lr4GFaunMM1MlWHH7Ir
XtBIkEAIUBErroKjQEPo8RcI/dKG9gVKCRML0JDxjyRBlb3TD6CzJ8CEljzDf+LvFL4FnS+bPPzu
qfpGZOZGleAb2517h0cCp1dhPXnquRPPgC3xEMM8FQ8VbnfNXzhqtBCuMMpxyBMbD7pZd2ZuTjba
TXXyuDe6JLUPxvoqD7v+9t8eMXVVgQHiWmjAfGxGvZVz9wQS7Pr09jv6MjX+CM+VsMKMQmOmmy+c
se4TMTT4ingt6QIPDy/s2AXum23U2qgMEcXlHPanCiAkPc7nZ/SjYXD129hlESrxe1PVI3fJd5vp
OMHNk5vc33SvBnbyOOo+T8CA/dG08yOidLcx9cM+e/K8gHjhNScbwJ+0LqORrmFDsWpR1ungHGke
ebOTcCoKFq5GAcwRHND65qPTUGWK8kLfvXuXlw/5gYlSPWWr8cfzU+4tGLV3rySgQx0KUbpc4o2s
J2m2u51YHD1Kb7hk6zLilGtQqt+rDn+r7kzsMOF2MbdwCg9XU3qgCTBtV6j4HqFqj3mT8NA9h8z9
tCZ577xHDDrx5+BsgNCbXRQjR/8e4rGJEcG8WVyPVC2qiTZzIIdhcPoaPJ+kWclnVXkr7ziH+VzJ
kgdmhmLGis8pTEZ4530FLYoZ95+8pFVMGCBtoHv2b6p8/ftLCGsdJjrMgxUY7MVpIszxh3XN4K36
Q+5lxB8RYotsiO851aQT/kVw1me3L1SR9p5orvj3epmB/3Q0vxiRY8l0smj8t8PWmDm/05rOu9Zn
Bfer1PnITrjgIbo0pX6JWRCEBqHrAKoxezxb2aJf3dD3gE11xZ0R0+1Ix/OP/vroIDsJeshHzaCQ
gzzXdFuygfD51+BIzhy+gjyNI9PiGAxSak6Wfy9wZrIkPQyt/NnW83FTcKWS2vd/TeQ8Eh1agw1J
viGc3q2PQvOvH99zTuPcKVnblDfkKDBIvRf9bRjz2JXlUPsXamYITdG/D+UuKpbnfsWvmj/pOmZs
vaURwz7PqO0dmxl+PlvSsjJ8XYSFoZEbb0pubt0shm8E/YUb9J9H2CF5HJltthM1HVdTXeGqJ3tl
mUhofd8dMmNRdy6IdUWHGqQ3n81HXdNvQfduBm1aMjx7MAS5AxlJ8KmUnQgIzlasuV+2wwCy0+bt
0iszJpsx7nZlzmC5l4y8pP/XIyO5E0ldT108nqcL6Cv3rRkKjPILaWwekTAJlcF/QuZlzje5Ek9/
a2fKqhw/61ebKdbz3GkkDQRCE1Njx53EyfVXLH7isRvIBZFmuk0vQaSyxK7hj1FdtNk4HkRxQXUW
LBvkhVP0c6bHHHdSKa2o8xiaNwOo+7hYWYDUVm05arl21Ikw6jj27qKymv0M7GU1aW0epr6ULucO
bkMeGoGsKhqV+g2L3M+fuOGzF52D4cXG2W1eTjG2qvVWvdT8+C8C8vRTjJ+Mh0t7toyJKS99XFop
b6sfOSXeBKFFpU5yCHJVMOTr6zZJ7Bq3+Z7XtTSTVnamgvjEl6cjYk/SrA/A6zg/7MxwT1v7sUo5
yvZ5tFiwKefEnZBnNZojf1vOWRWcH2nX3DwmF/lg6YOEiv/2fTpwT4I3KPo8TCnJdCbeX4UYJ25M
FfH+TfLF5marggJrU6CtLKAynnNrkalxo/HlOZIv6dR+WRejNGAWiYgnUAvjyTfXTB9e4RppzlOB
DM7+FVOzu0imk8hamXdv0A3/ftdk6KTBR4sOHNycP0vI31coyGqorf+e6yuEfUAlI7h9Q3vKwi5Z
BmTFNGbbsy1h6hb5DKhBRd9w1RrrX/Tm0l7tTYou8jI0mCM1juUIsJldirn7hqrbNYcVJsPh3NIx
HHodjvpxmbJdAcD7N1UghJyU7l8bvNuVWqkNFtj0cvVjwvg67zyyp7S8gG/liKLp1xCjsc0oC/37
ssEa2Dp954Pr9KQfCXC/SnDmL8xD42k9WAGuv8mwI6UtRrOaiPMaOCKYpzFp5HnB6AfzH0DGQArb
VJnna2jpE0WeXTd8lTeO7jclMBNb6uOSl4MVSo2lU5IX8IO4FaSXBjCTn15S4XE47wfmvMceX/2t
9zbM9AEIcTYwLZFcVC5M7UoLcMTnGUqLCyWRoPBpAnujByFdHJTtjRoccv52F1h5XdPiA7aXGNV6
6jbt6X/4qPatB5Jq+aV1iMPlT12yZKZ/CBnc1t7Gzqyu9MiCdsXh0FB+NGBjrdUt/5Nw3ICij7xn
f61Qj6tWQXs8SAQdqvl1W2Q4Mz9JyzADs6YfZT1NX7UrmxemTopVIn+IJI9hEDIq+GUnzy5x6j3/
cb3cOqtYcmH3ywLzlxD6DAxwDPnjO64r0iD0GoADe6Rq0/m0h7cYnyeSceuzCrovbSb4x7je0E+d
x56kOZbIlLnjGLb134fekMo202jgTedOzE55CIaCmADTx5gAPyLXkbioDFj9+v9mTVrwjadEn10A
Vtpdk7JNDEkMuUx+bvWellSugmppXLwYKLllB1EpJqhOTr6YibafYKx4Au30gVABJk79e7UxKgqR
mZ1PRHdjB3ZUSxej4n333as3zEaZFPeTQb4sTU0Tzy05DJKeVP8Inm9/SzH6DkUe6ySORV6x6VLc
vzyJ0Wx5hycgDmqWaklfNcJOJzaiCMSPzGd9X8TQ7eaj5xmfzbJiW1lKnZMDyiIvlujQ95QlCUKq
bvukYeCsxbqTO3DU+lq/wXbjPDFAmOd92jGqG0NlBsst7exutkBXxTBUxsmlat5UPjMxzDmIrETl
ptDM201xF0uXgsUW3V90nknTrbjIzQGD8+IO2jhUaa+TZIICCksBDaVr/5EIMMF/6pXThMGhGiHK
1kWr43Lhru0WNQaKyNraccYvXP/q2phoMzpKxA61bJdDPAG+sHzQ2JNDFvhI2nuCE6ONIaQLeAMN
kNy1TLZHxaBJTYghzi1IyqqxU9PAszkSE1CjJdVWl3XytLro1zN1Dz9WD1BjtoypSgUGF0LtSpB+
DCIhggLy7joGQszSHNa16cFs2jUy1/zJK2hBPzhrtp0XPOFhaHQD5VBTHz8n+6nmixTKeUrMVg83
joCZYFnNEJcvpBVAt4RVayCJVaxJ49aDFmmJ0oJ7nIczE7iBRv2gAzVuZ+YdJySudAzBuP12X+I8
0EOs98IdB/c0NFjbMyqoGYBZPr0xWUtAgRuzus1+uWQwvLxcbt2l9XFH0vGj1+9j8jNkwYSubH6J
PuXo9nO9FLqFMdo+Yd/ml2B71ldRvyD75wKBiyY6PRd+4Wg9qCoP9g8bhm2dJcemfgli3pDSTJGQ
cuVZEO3uCpf6bLOsAufQWeI72Jm+niz0wNDc7KAGP6xJnnwaMneuf0r5sE94LMEMeztv+IKhMxF+
oxpczuRFC6BQ48CJds6HvEyOhkdy9iuNU8PtuGRpbfd5oeak38EZbMCPhGJ4ES+ZA45fyJRtfHmL
jFKNRgVLaUinY3XT2QzEBLIa/KbEcaDoWfFp4okIYO7zhpe1XsPmAR2oQRHF45xMeX74MS8IGzT3
ML+tHlaD61U0veAtNEHT7Rwe9/n9PQhB9h/JUm9tHis1oWW4zQtf8ibfIa9MIjRcHy55wOjKKWnW
i6ei3o8LRQZtkmZGo3E+U1nDq1gkrTTZW9oWjn/RzjAG0Ib2sffoaSAP550ULqDgCgE9UKiW/N7j
c0pnUVtxne1v80lnQpcQ7lJ2jUOVRy4Nxy/zgqkslDjoKdeg39/ztcJbgWvGz+wneECqlYZOzH7J
5M66as5YfrhUolP8nIU4XB01sD2wqD0A8ZgG1gYvYMKYcJDwDrrMQL/fJBLdD+zvsz4XYGbUuQKj
iBbL6naN6QyggSycOS0aavgytF3UfPZ39cfTFWmb2rCaQXUr41xW0sTZpClY8RwoMkqFAwPVEc1t
L8+kGxjssA5GUMQuilgdm63F1TIKYlrww2zd/dsCuwvVDs8n9TcwATTelCYp/5oLlSQxYyQJSIl4
zD5HGlhRJ9Br/ALoDqlpsCiR6j4XMmI+35diK6iOV97fFhpJqIRHmvroGnx7WCe5R+9C338SIHQd
J30fOsTTDB4kAEWYFbI0IKYwvfSaFBxTQXyas/REyxDP6mRmAEscwtJlEGZvbURUHlafmq/EBjcT
/4LDYLM9ggbhvazde37cLFzIUvI4G6NEh3wa8C4+jH88Kq2fD2TMERuj/ejCGocUf4bwLZypm5P3
0TDYfx9OVW9SWeq/67gaWClUEBBDr3tJcHoX60s7sgUCPIfsXKw6jpxFa/PgNBNWWTJeLzu+V6NP
IgOdaRkdpXS1YMwui9SiL/h8Phj5V4YzhkMQ2SsKH0zTXLfwLK11Z6o1SNoILsEZrqpxiIHJFzGg
LuH8bSWJ1yLhnnRInLVjxuP+LEkKJnMKoimyGkVfxcDanv4Tit9L7p7Hh7UN1VQCQJe1JyNOdt2w
IgDrAgkTTNaLTQ16j86hunsQr3xtmOYFSYNA/uCFwKoocgkDuQjouGC7pKdoJmNU9Ju0YleDyEJU
siO7V2HW4aoCfFCu6TNjQkr7NnevFOV/im3cQt4MhkfYslmjC26AHpViv+fD4YwWxf0jd2GBIlpd
kicNIrM6jollUHiYLmLP5Zoc2ouLRlA88NYrP8Y9s5RfHYEtYv55JbtuHDcD2hBes0fxl/iP7S71
/SwHjc+zlCxFXFkZTGZehEBx5LhlEXvUcEA3KBd+fZE0EEW1X/Ijqi/Co/iafpx+AyvQ8StVju9b
stqhJGoZoez0AU9mARWGnqk+owAXn25mZSYsUaXNc1ZDZpkZYkGCc+zPCxZoY7cUOqq+YCfl0T6W
oGTOjP7aOn5HAXz0PI7sepUEVcIc8NC8n7NziUVC9aT5LGv4kaSvSQV3nfgXm5mbo0cwxxSm+Ewg
SQmNb+KCAssyyXwGhDXWtOvFOihqKjH5yWTcfTEPG2nh2rJ8yS2cuBnvqayrtNxFdGk6lQWBy+3o
4Gy7l8l8AoPdOavdC+M0dv1lGMWAtzsJGL/1ODe7xhIoETq6wo7bkvMuSsIkanaDAvqHatI+Xsrr
CXJMNAW5LtJXvCd8vb3BvArxetKZGY/4jKJz32rLAIz041LXn0rDvRZF3RmXZtYAxicVK816cIch
GsEjouvOrNvp0r5J2L/DVM7/+8N1dEkq4tZqcWdA06V/O046/fZY1zcvESEKc8nqBWcqLuOJ0Zq4
CbQtjFmgR6KTCi/imdo774WBaQUYvAWh8hIlIVLjZhnMB0WtbNDq4WjMDkKx9Uj7hnSWSFPoNu1u
TqiXxNoYhga1xLiOSU2AIi/fww5tExeUtg/VK40ddG7hUnCJ9wwaHYrUVpOm3f5MF4/cLr+jdO7t
fLg48127E8c8MpVnqlYjWxLRCxV0U0QhhKh36hBgKZB3D18btgEk1SZVGpn0GUmV6EttJI9h9xE5
CVTWCDyvyq3G2oUFUElEjfrC14EjJKm8x2CaA6lzmBbTWvTSZzuZGZLa+/pE29Sg4jT1bhjCJ1ju
N7G3yG1OlJIslgPBSCi98/RflzcXUD6uLrtbP2k0KwhMDMytgXebjTxKohY+GED35Qfi7asp7kwK
NJPTDxFeqA6QvCjfEIyRWbC9hqj4bRPcpxb3eCL4KWTH7NxGvN9b8fxGUZLeS/NPE23S2VCXjtWi
qY0YhgGnG84nWqMmqdc65y4lWEEnQSNpfs8ho/sd83swTlTk2C/dTGnAzagvIviafueULynsVDHe
gGBjTQR+hoLexW5RV+nD/+wkr+OFF5JhxEWRoZaZ01IXaVsdvEzvUEv9fmPPZ0vvu+ruy6r9GZ3K
I5ZUEVFuAnTuQqaHqUt5H8UOgmcBZRMxtTTyyhXuSIE4Uf82vhZA6gseyu1PJrmdZFbXiGVoxA1j
tZXeYLS6uiTojYQ96c7VhEc0Y/30IoC9BQCdbj7pBO8YVq9m+dfgn+ZVLYPmNx/RpNf9ZaBZ70/g
5UBympLxtbXdiMPPl6AHmA3fY20Rt3oLsCvigXhqAu74XLVTcEeLmrrGdakt/R9JeAcbB12bBGyJ
66QucVyxW24BSenmlKe/YU0TIhXfQSi4PXHdbMyfTRt7w7MqW93KBDB0uRQjKc58IKLS5aFwgiXz
hMX9gTVtvJgnmm5hma4wqNbB1fhUTO3YRUs4B3kudZRxJLny6WcVk3c/cFqtSa3ZyAtSq3/q9ro8
OjWO4a0RX1uEdwKr8Do0v9qMB99NI6CrJ6ZSvAA5AkwS1KVp27aYjnNo3LKqjw2FW41nLPBNAT56
SxB5mkQ0MXrxGj7BrknUBERnTUTcZ2Rdbwz31u5O/WZ5gYO1n4ltD+IJ6h2jRcU8knvHICJNI+eK
InjXGwP248AiYc6ALeFovEa/y9UkB8cyvIP4Lzw8+FwEj0FcnpDxSy5txeB7xli5OuMs+sFIMJOZ
lEmhqQ8Xls+xHclK+MQS0VX2EDnSdtE0v1zMpLgFxFSUNRDjIAy5V/QlEfy1XOsuedvkzbj5B0sH
eIyNZ/pX8duB8eysNrYRPOYnSW9niV8zXkXsCMLfBGZ0xHkUMq2lP/VRYJhgj2cg6RJHybPX1BQU
2PTYC5j7MqJclZtPbAp2jzU+VnFrQv0Fplw8Wns65dSThi/KDkZUGBnpqn1MrmfpZ13n/XB2cZ/N
kdOvbBpT8np/WMHpeHirOAwMl1NgRWshOaxjVJ35u9mY2yqHDKJVWGDfYEM1vcgB1nWgRucawozj
pTQhWipmsnNxf89uPtS1yDVSISaMfYzMDBOIS2ZVCg9LiVevtoWXKyKP9O/v0t256uNXwsO3A1oQ
I2VN4BxnV5zH4HaK/By6SetkmxE9tidzRE0exxP2ereomuF0c2ZUeXwI9xEnt/AH40ms49aGFtii
RAWd0cKg7NkoMLtFWOL2GMaLPm7z4EnoT11Jh3s/LY4TkOfpMAgNOfkru3yXHsBShKMkcQMdbPd7
8eXNEG8fcumjCcL9StzvRQBQQMpSpQ5Seue7YIJEpiOZFLXuk5+tV1BGS8Vk6YY+5GehXjbyYGLs
NqLd5WVLdGrWBPoUWusH68031SsXQs2tZ4SrFDGFxtMeJvKWi9X2VVQKEHGi4NvUtKAIZmG/q2cU
k0aNw9elkJfGuHt8mHxtsKWnOHOrQNIDLDcYe8Wnm5+cePamNpFCHuxFWSV8KKMh11Tksr4g+srb
NGJ3C5ylBgoYOP84pucZPlQ9BsSc4NWR3P/7C38sZx6VOpuUz+iMKi5tNwZ5A7MMdBI4E5Jb6wUi
wo/BiOIIp7Dgdhekzll7VY2NMg1n3F5icufhzSbhMG4kielLrulUa/AZEZDsTlFvhInmnpbVlabP
wdvB0qFZpxPJZmC/TMhqoZg0uQFhyqh1XLlzCLw7m9j+tDUIArk+wBHbekDogygVgs2lzUJJbhOr
fJFqhvUFJI74v4jERCJBwKrglqGqTJc1m0CmPPq04xYvU51fb0cysDE1q64UkdFUS53g74V8NytY
nkRmnRB0sMgAk8jiUXu1/yNrgytrJqJfkfnHdjDJgqVHtVCA5SBwXGJe3T5DwTO94G2p16LLZ0h4
tEZYkGEMURpgQrNh40pxPpVlhNTOKygBNnbURLvsssMSGcMg9a9b2XWl1RS3QZLJLPVGnn0Pmj9e
8ahyyKSMMBZdYE5yGG416Gcmwdr4deq2fkID/tEgFy1b+hj6rwc7ArCNJGkC41I/Kz3HTo4YGgGX
YY2OyuFqowwIsQvTJ6ShrMl7Q/5TKUoJnCC+9a8K1QBriwVr6fsgHF6Z24d0jrOMkQyOOo9oTsUw
HkzMVH3+lGbRz0wVdbi0xhkFwE1s2sL1kTPSETx7YO2Fh8mRtvawYzr5yWzoDTbSjwDfmRj6PWcg
0BjXD+WPV7M/XUPuPKFKwCGjtAZgigUv1EMq/qf+crh6Nyix8gj6qTh3ONv0tAtw16nBMEEc6H+K
TMJT5KA7jV0gxCoP9//yP0iYDqOyrXbCcPcRtAtOAVpXYBvPWrSKRzTyLrA6KWBfU/T6LbhdAbiU
eIba8TttOB2tyNUbsLQQNo/qY9Pmor9VzywArH/fRmiZCKVqrf6ZOzCzFlQ7lmcWthM5t1RNQY36
qGcbKY5vKeRCDHJNJgIgDcOS0ES/4vHv8VWFY7Z/HUt+wekfxpc77ZxTrr6+o3GHeyAK0W/QU7BN
Mugm4j9b92xf/t9QRzY0ykrNY2dc5FmFf8NyegtATnfKHuEetKzLcjCOv1dm+vBX/oSh5QqWFMIV
Ivrz85JtUsFHdkqoqF2F6vtkuqR9Sx1J7NpETKJYWVuX6VJprTAu27E0SnlDG7wL6aoWaexlpikG
zcJ1YE/flVN3YuUP7H7t1i1UK/MtLqJ64/kJymrTuz5WKhRDji10+2wzg9WRbQqMtxACRqPg0YDh
CzvcqIfC0ms5q8wH2mk9KgxPgoGBMmKmpraxenCYR36ImhSbkeEx9ygt92fja77THprLqYiAxRMB
C14tzZSDmjobf/e7cSwMbYxa9d9D9/NCxyDM0TlAyNxXKjT5JtPr24dr857O2Ozv4nFKS3LCbYNN
dptHvORkwzhMLLAOEIyNYhd88+MjZsmQZOc7psJszKYw1SBo6cj8HQPShbholLfwTHbM/FE8DCfc
+fNP5t6yqdRakoZ6KFuGtBEISejYGyIvRFtVSmIgHlePtRuAszPiTKv5R+j760QQjmwrfR/M9Q4T
yqCZH/YEpDwprYoHrRXwXrkvGSP7f4d4T+4Wn7U0wAF/z54dTozokzJPBiFIrCjAcTP7ueP4Qpie
qIfqvSw6Bu9skpCb638YFMWha6Uf/xQIsqo1GYw/rkBnbP6A9uNrYQvuNZCXXT2Kzkw6afWCYcm8
IroolsHlpdcxTOM9pVFDpZ30Xix0YWaEEchK9Syf6x1+gSvDUGwoe9pluIAtZJNvzDsa0qA/jlIo
ioa9CbVK0dAiD4MYYhjo6hch7R9PnTaO86efLzkH/e5pT8RRfQKiyw9ANerfSj7pGek3pNULyzBb
LiOq1Q5AuDMG7zo9Mlwylz++npLbI3oioPirYBumSl4uykBkgR5nE9BlkI2jkC9C+cK94JoqQtTC
eFDBX7NOHG5EASyDhNX9dQUrYMKRkse4MgnH4wCorJxGBYan9aw9cKIZOu9DT2d51itxCuGWC1XK
epHbLk3xRyxFVG99w8akzlWWtnp/JVNvFDA3BqGq99+OoOfA/vWdi9mkzzX7SV1SPK5hFGXBaz4Z
ZRKcFgKpTQhfVhrN028TNFRv5RydXdTlerUha6+YajjW18tt+mKDidAyKfK9L0TPdSFZUMtRCoN7
b6Z2HF9s3NsacGec2sD83qR9mKxsS0h2aZuCk6rsihIm8dpHKjS95BiRSnsag1fUuvqwlUuUVvl7
/bIaDqPMNpeKhUnW5TnJscQB9ZFmqy0bZQNlM8fgWx1yaSho93eX2+UtQKGDFJYt/PMQ5RFfpk1Z
eIuVr9WZxtNsUcT+wMugHfCa50SSI6KxXqamXSllwV7z+JgR0F3q54HRKuhnfrt4UU+Hk8KJHvZT
6WdGfSEF1KaUcFdBbOYz5ANnziHyIe1KA+0XOFa+NUkULlyweeLpikQGt7MlUctDm7gRpXzrZ1ju
qfEXe9LE6+CuZ47ngxQGYKq7AOgtR8a2zNA+aT9rYNsQ85finXhz/VdPDlX/LA1H5Z2oz4zHQ7Yi
wt65NkytdWzI2rWJxd+io0RK6uESkLQYslBorSB6spGaX/6/WIrOwk2jTfEJ0ojrUC3ATKHhjD5T
Q1/+nWVWnxFES7zym15wfHKc4zsM8WP7o1pRc2opEUb1EcnUYZ5ScDICTGUW9Bm+TK68c3WE+/Ih
EsxHL26zbIPslYPOAkAhyRUpMCO2m5c33FlkSRZY5RsSq47V/kTFrxAGfpNagzgaBhO+VlT9O8b0
ntXmYyhlzXrkyZg/FaM4rFblv2tw/dqC/oDQFw3K2Ax2L8d3j8WiRy8gGIMKwGnq8YLhxqTRgFXR
jye1wnzMOisL/VVf/FoIqbZRbNt5f2DUeP9t97/qR8iNCLWUBJdszwqWKNyxjFm3LXYIHU8Vdg3i
9mQLXwARljKEFXKqf2PUzjShX1XKpqIGib1BDS060MyqV4zcg2FaSbINSEnHuqGvQxVOONI96aQH
3snYqRa00wnvpyJK4ubDUq60WjX+ezaANYXFbRQciUi/FYhUJW99AcZ+yTp9P3OOZn/Ra16GhChY
nkYYmrajUwO6e37YwUKN4wUy20lMLamy3JMhBFiW0BvDuyjFXRx63QcFUFUaSXTsrIclKE3hZ6XN
VI3Dc8nZSFDcssL0J5Ybv9XMHHbu3+Ey0/rrIQJgWorSWCbMu4jXfcjjT4rO9FYWo2BcRTFuNLRe
ntj8AEHUHLdvzeTS22RwkNhvIwzLJ1mV+2GTe8Rl8ipZY/NGOqoOfb0MM0m/FlulAvWEqx+ciDGY
875rk8Jq1MKy4vlG4qcPWNnGkCkpZ8SB/ERhFL9wsg44rRTMaiyNY8LMMKg8+PmZAOvg4orTDMvw
JF5X/zneEcSRlq8I0CXOddO7jadGwl8V5qvi+Ve/qV2H+1Cl1zIcuZn/053P5GGm6YpN1DMBLf0B
EotnYYFy/EwDb2EUPD0TZ36zY/KlaGnJxgJQrbYEEKItnPetESedbiaGF1Dc0cwlF56aGjo4UDd5
effJ1aYyykAt4TMjSMu96uTY4WikM0mEqtm1g3RfZvt9tMJcqZgfePFQ8jfFkYdxZOrCoPsjvIZH
kqItRvKr8VVmrdDFsbnb8RSImPO2U4X7LkII32IbiRGBHm7NRcQz6hnGEKO2NPlPKHe33rH9ZEae
5HmYqgc3Asz5JrfXjSNyCEZ15q7mbEEa+XL3yT/JXNHahJAMLZ1SzQCXJHt87HvEt7r0TLWnlKqA
o7dGlwrT6GZ1Xv/8Uoon84GjNTP3R43E7LtAoPaygVpHBKOHScEjNn1Ejfuu2EIGIwoYRCdyjhTr
QBcjLyHTVwgx6k/HKy/l7YCZ5+Dt4F+fKvA5SeNwc+O9kwCQNSKOlH+dh+Alhlj6HdUXC6jx0e8w
Hy/Q9UvB/1Fk4Sbleum1gKLD5IF6kBKEWWUH95rYd7VLSg+RM1iNl19tvbzzxMm2gb9ySF9pZ5a8
5lREkTYYLG0D2SpfYgSinKdCxYSF+1jzveTdlhNdBOpQGl0b3tLLWhZenZNOcqOgda8WEcqyVvh/
Yo7JscXMb4mAMtzdkBJm1KG+hPZojqJBiAzRjAGtjJIhNG8KVv7GWoEQsJlBJcS5Ifg8Cai1tcVD
mes2avcC9qio3Q6Xlfv0ERdrGA1qZOmg2o555wB8M9a018CZiETteAMY82YGtMC2anwxATJdEKI0
fD6H2TCHNk+DE7j7crXe3vGLQwhm7ypL4Tw7WpQ54QVJzzhJiPgs7qebiR+SarvqssXfxZBpjYUV
7mJHeVwSOGFmrCbODzmnTedHJl6aWOTwR1yDCfd91hsNDlOZwEncQ6Z3tl+pba0F8tfFCIthL5jb
4Rl3zk3hLf2bN/C6ZpUIy++ox3f8XQxlA9QnGDylkJLzBgcGuuAUgdC4H8FwFWxeRANcihCHbTgr
ObFSMrTphr9dHXJWH+SAZYn/xNRubQAXcVFupE9tWrykqX0i1Zc7PFH68VZqs844Uc43D/7X+sL7
eAEJ4wlRoOe7uug6LPq3ASjvzb1saDd7GmQxi5tUlY81hmhiBEUjTj2DRHg2q/SBEODfB4Xtk/E4
FezyM4SQj+emhPRNZRYU/RaP/zCJiod4bcvM/NWSTSmWLNe9CGLWEdFLzPM4mgItAZqrIjY+8R8S
r3t7My66Xd37O/yzDKnStnSx8FyR9SwWz2VY6Vn+W4FZYjTESE2rAqMii7AI8qNnSYULx1IzpDrc
9vxUyKvlsHLYhGQR2FCMivtN1j/xeHdOIBHwZ9yTG5LyvyBDfskodn6TmeXgqmb3sTwmtYXuQzrW
01yXFHCMJbYcjI58BSgv4d6JF/3zDuwXxHHkxFffEPRijg2e0g6v0J5Pj2ekaQYcqSaTDlYUnE7+
rJGT0lDU2CX/QdlSb/a3thGiqx9az/dspwkRkyHAb34HMxW1ap49QqA0Ml11/1CsQThckGnkMubE
bhEpq5BAO/u9g/wAewcVU5rsPGbVn9NuniyZUjrFoWzG9evaE9nF6YaBYvaB6S1MbEGCf1LjT0Ta
5pIkBtgP5b50l8ExRjLF20UzDcHSUflCI84jnAff73f0pApAz8Eu74zLlu7gzcCmoEqeQr0keGdh
SFjeI/AQVDqOdoZ2XnzPEn+eTnTR7LAe2iMNSkniwUjKwEUhiD3787aSVhYeys5oRtjUIuDjnHJ8
ky2L7V+ph4Jj4qbI0pmqaS9JgdWdmBrNg5RAPRqIGVUEbRrSJOi2T1a/wu8EYGP/ADvO5E3JaSVn
w1FWelI34ybmnzsA4PBTyWwZYyPmihFk3rZKHglvmkDtTKXN25y4rwM+PdsvSSDykkERfWLYx6Vo
U0KnlJqWoG3LH5/kiBKtyyfnaqa5uVuZpSijgAFpdLYSvyv8AbTaQpYMk3WTYCDhxCyK1ssCRJ6b
mybiraaagfMgenIlt1D1RkadNrR1kJh7N1XJVwZZ26bpycLCH1tTBkInvf5Hyv5+ufzUqjy5doPO
pRfvqzZEe/mPsufGFybbi4hw9fUBjG1qLZVoeEigzPJs15YCfVaMGHlTmrcCMrK+qtGNnpNCCBoa
K4R9lvaG3ZRcmboO+PZlIArqyG/AAlk5O623Z8msTy/5+674rziVAfUVnHW3rNYQoZeyYPKtt9tY
EPEMiKfiRgGKqOJH9Te3dgo6VnHyyA4AZXcTR0vv3TabWiSZwk5T7xqgLHochPUYV5aU7v1TMs1X
pbMAVYvvteof1X0vTQc7SIM0+9hlwfvUjhpbi0pWgSbJoTz/kQ1iHCTvhm38PN6I93B4Oq/dmhFL
Phj1t6CMU+t4bT0wPfc7NNrTtXxTP6tZEX/KunOvupZJTzxRJgv/S/HPjlRKIIJEQNMcSmAhYVgZ
E/iRX0wlLki3Rv7jJ1corPLd1JmtvVt9+p1kg6/UkdaMGK0LElT3tsmOrNREWpctLFb8qtuB3fMF
Q8jP1LwglYC0IykK6cQGT74yEIzAwDVmeMLKLqaKZZk78vTbmx8rDUgIxiEiLhvPrjKc/nB1FUuH
xDsRBtrVac46IO3F27O5j7XI9BXRQT7ua+7z/yUehpsvdEfFJwK3xjzF3becUvQu6iLIwg5uMhqA
n45Ywzrbzc7zEOVi56gTEprYT3gopZ9WkZsPj7psaaG3M7KfjAgjAlsJrXzEoWXFypKdnL+AyWjY
2gX+U4D1W+cPsmSPL1tywkZJPeiaYK+RoocA0k+u+SLoK+9EQ4aFzzV5/otulMBqH2iMGuYBEXFg
aEPZuMBpap7BeuEId6J8u9OL/p6p6bBNnOcuDsqzY7aYD5N//jBx9mHc+3/LoSvTX509LjjEGmqC
u4ENs11W1VkcatGZP7ZnoZmNH37Vg84B+iBQParLZOeSW6s8viyWmiece07+pW8sn+rUdYTYTcbu
JCxqZ5xWA301c+UUGT8USUB4XlIs+83nZU1B7EG+QoXznlQqQkW5l/gFOubdaeg3okdY+7vKzSw8
W7IO2cjci61W4knz1G8LB7EQucBGmKPpNSTEsObk73ctpzuIFaGbDkwNNdUWfiIS9JtCGxOlpgXB
7jUeQVhqGGGeqBBNg9J8iC7aHvYRns+9tptRAw81wZzrNkgrEOZylsuwIfm6le/C01Vm56nKptz3
DP3QpJfHoTHz4w/7ELC8X5r11KZJIdovjJpFmRxafe6satuAMgOmm2v/DO8IF5+3KErmUkCfAVlo
h3E+A18+ZXuquwSEQC/kC7DvtoTYTYtoHzUdLKUGhmRziEcBqbzEdU/unhxhkowhHK64dBR2uPZm
26R+2exnP9SJSjz6SvTMNZkddGY6TvWSI26LRr5Dcw0azr6+URaUAti1JGesc0vteubSdKbMkWkV
+dwcOPQo1Tlt90x1J45rDu9xcN2Rzu6E9vs3yiUA+uIVZ2mVUdfvBejz8tWCINBNi79VUdfHQF+w
tQbuIsM5RUotW+wTH1aHpo/myv+KBfCd4pLkJyjl1SSqykaHU5+BwU/wserdbz6Vfpx5Pv5M3bIZ
vd++L4rWf2k+nVPseI5+fi8HDpGzdpTUt3pivCVATahW2sKyRxLvElAJILY3OxtFBQEaIjot4qSy
b315alyDlGc42K/VEVpKdZbZ54dB8PyLPHz6YDheMdnvv1pI4zGGCwkVojR8bf70cHMddk171r+f
1LerdCjZQhf3W0qOPWI0XRqJY7P2Ha14yEqwnwnr0vRcqIoRjKASzs+k782mqysVGgzBMfVRNwAc
SnuS398RXm7523Wxi+gBSKVsASRtNJEciNwdb08uOxf00wy6Q5hsML2m/04434bhesViFBJjeGAy
2ACN264XMpYzOL1euR/PgLkLLhg6eYGLVpz3eVtiJXwlhHrEy6F4pVnkYdLolyYFXXRWN7HHZzdo
zxXx/lbPUToD0MCDLiKns3BP2nAdDUw5aoDeKUVr1rE7K3drh87coWOHKw6jSYzOGuIJa8ETEhmR
JkNvOjTB7VAfy0GORq1REmxjZgcOsP8opxfukn/gE+t+Weeg5RbA/6h7x81BoTfbMdcu+B/+jrh/
itgvBFPOJZEepyQxcKu3plLPxrFpOr91oen7/LoMqsoRS1nJL/fgzWOSQfQ4z5pc1o69SMn/mqUn
1wQDeH1Iw7tpywLLGkLBQzITyVbDHJDDdW5LFa6WNMxi1ywnGscxE8T58sAarCK2TqyEef4cwpp3
aS6t/wbgMa90iq854bxfx4vaEja1ofEVVAJpo8i0GHZKJ2GVu+2RTdCrdmRHCZFzLO5WiE5ndsDp
9nhYIyMfScnVCBSOqpYXQuRtWygZbF+Z0PbyVffcv+1Wyr73pvi3RnXogF2X4MqHPHLXCY7SBD1E
tY1ZoNABTSJIIiYJNgbsZuZqVL3P4Xk4uTxhNj/XRogzubeT4GcYyNWJrOTgM/VviozjUe5g/r85
S51KS3u+t6OBWsCyJ/ewhDQYI0s9/q4c95ThnZ54wlGzEeEKz+42dDi9MhBGEBxzdZAw8nr/vhew
VAE2LM5F+ypZpcKbtu/bxPqgo4Sv4YiIzM5xQw7rqK56zNwfGeYgW6N5C7ZjviOz/cU0TU/yOBEW
MdM5RUgj2cro9xhhLMZCqFD7u/UtHwXCLd+uWtfzqcYogKkKocZ1PPvyTpSyEVul9S4HyT9IAjEv
ouLundWsM7IYbgDAxHZNTcrxrGudI6brMbsqU9AIZe9O8NtyRPcDPlydKD2U7TMzUd0zuQ0ieb4l
+OMtovIgYZN4R1Z2LOsCV3xsXU9z4l0TcplXgQoZpzt0fDaxNKHHZ1u6wxSezZsC1VcNp4tDz7DO
SxSg20cAfnRRDBYT9pYdK3pK0ZcY4ph6qpUM1lPaowZyC4rz3Q0FDBZA3P7TrigoFehnxVTJPzb2
3eMB/pyVTOlcIyELRd4ye7gbMzld2n0z4s6RPfXhvDVnSYWrOWDOd8Kc2W4CnvJJRF8zWCfsmEMh
kMK9Xa3YCIkLnwm6IlCu18po07D/JVGah7XzWAG2nXgAv3oq9BuU5O2sTn6Nua51M4ebUZWG7btq
KRMlVrCPOT6ENlLytXsg/Vt89548hJH5R60gV/lamWHapFojV/ntblzWyEbl6JiLeHeW1SSkjM6O
Li4pj/6BTQFfdHOOfhFDQpeyzDM7MTwtOn56YBxPOwSEv2D1fjQic95EVnnujM5L9QX8ZNEyqLDz
b7o3yNUQaBwGoaKxa1E/hTaVtwVXJIlb3C+asBwI5rtg8FGottZ7UsQA4wn5mxBFq749qsbgCidA
MRTs00pdM4+n8BgOSCE9YQ7YgvUdhtOMVVwpJG/bxY99PqVdbRJbMIh3sMQoUE8vUT08NLSxpibn
GezPQ9k8gB7pudcJDb/FGU9C1IfMUkiS7v/50l12IIYYCNZIiLZHBYJbzLzJ5uJgULt88Fqhp6c6
2KMfgADeIa5BbZ0E60CuHBw+2aVsBzWbxeLsVWGZbapNR/utHIkQYU40uqIN3isv/3RPXMOkJNYz
COcKYBxzda7bJfwBIweUXOoyYnbVIVn1/GNUHnbgKj6p6L1li0zGvQ6CHnSR0FQsWMrtOBotmgdm
cSnY00RCVhq88uZduTi49t78rje17IKgp7YNfl55Yr1eG+hx6vKgGkJPiE8IGwQcfXjYVmcRsYSL
lCgXOpdby04XwvbJYS22vch94ZcUU3S8p+eMNoCtuKCEjoSz3iHvDNWPg7UIHbunj8Yi2FOlRwSr
XtMGYXPn5+RfFZiXFBfEbn9V2UylIMblIo3ffOcIwOajTgNHxm4L5jvlB75R5znH4QMO5CoS6ED1
MasPS8yXwgeq7KAmNBxMV5//ZqUqVc6Q8nl4VmFkkThcFoCo3jOzZa6PBbOoADPlYO141mi6aJ1p
y7Kjb7+lwxe75qHAXuvsQgdFY6PQP3oI88ti/xhEa48T1EZAPxOIQv8PdrOtrKKd6t/jbxvSdTE1
bKrezARi+KBQWSsX+x8gKfXsf8ckk7eiCYVQwEuSmxZeFR6SQQ0oy4gNKYcDOtiJVMcjnBHdU3cM
HUW+SLkD/LEVS3LUbVOzmFdvbD3YdrFu5ObacWdNBS3AeOKl5cdLOO+y31ztgSZcfQvKoMGRj8LD
v2fNHdorVrq8a66GV8y0G2PS+X7huJUS0OthMyZTkf4dOxy3X+vF/ltGjZfjLsnKQKBew9574iQt
9TfE9Vd+uAIm4T5w4SVv6CWLhSQFYhvQvE3kfafSmSVm4HWRUigy0IqW4XjL9mA14oIjgoNQzUw3
l7eqIKtxIPQF7ZBKMKa/nxfXLtWCN6pXe3AipZ1nLFn49TGnXtcje3wlvwWazSs3yA7HkPGGFU7h
QGTCmqnZyhBClVA2THMlBbLuUEdp7yLt1ZPk2WZqHkotPgWK/ZLSivlZH3dWzl1tsgMIZEfeVfUh
Gy0FlilWsoTkdAxHY7WrUbB1r9B52dBmI34RwlGXbTZZ7sZBTrqAVIPhioeOg1QIMDSpbVZ2LCo+
1mKJmDmTju5tpC2mPSCqCpQT5Fww6SY9QpwWCATjGoSbC3WP+9zYgug5HdVcXD5/TnKa/IDG/asP
ou3LlnaeWnSKAVS41jOv2umH+lnXqAyPegDoTKZ5GqE9+mkoXG2q268a/OomSzukHn44O5z3Kxfa
Jow56B6zMk6rO+tyTbRIb78LspJuQ/q/ZokWjyXFOj4VrvdJvMMtounruCPUomNLEW2yx9DsaAof
ysjwxWFXLKpByfRUwbipyeJh4Uja7aqAf9eHWyF4vwggXSuhNVTFjQjG6mjCYBzSKlUJ4GC/eXiv
YVAhXZhw09L9dZ0J5DBEgOOHBEE4E4Y0uAna3QzNfn31wRwJyrePH+PzZAAISB/FBGV+aLN4Ws0z
QGEkMv9FZ/+t4yw9Su+E2eno0O1+aD/CH9ZGEMiIpPszQRtkUSP57N8Ru9xcYwvkti4YEIpWjd25
SbH4KfHvoL8feaOh0C0HndycadXoVKKAhAoAl1ZbLbSCefNOIrLssE8z/h/sHIV5JIlK0KrQsxu0
Xi89KOkmCUodnw9Lt94zT10WxVSaxnO/Pj3yqiwWUzq47c79PVZ9cq2g+I6dPmjy7mKuKoOZRKFw
Hr9KbU4gq/dZWC1AqnP1L+Pgy+/6Iimms/oqMp5icCRrPwnr9g5WKmiCdQj0qq0eMtLMyx0dsU2Y
Q1xsCSKpj2Q70MjHV5hYkJLle+F5Awxj/VF8WSUUPbueJkLKkpB0XQsS/Ck2OsnqiGMUfNP1btfK
m4p1nN6iYdYy7I/Zraz7JFUaZ/jo+THFRYkA8ZB1w1cTWiQxVmZAILx2L2syOwnXomu8kmej2QTf
oEhkPY3PILZCkKO8ke9nLY7daoOfsFaoT+mGiuTR1y4Kx2EhDhLlVnsrU3imvGJi1u4gCXECDvcf
ANxCro0ASSdZK5tb4kiZTaHZN3kfcHOQF3Yjnfv0epyXOHKoTCGqab2kjracZD622rn3p/LJJ5xf
2QDNdyRFoAZeglcrtMqJapKpN+XM7Whm157T1X2ZZ1NXL3/CyKwUKXG+5DXHIv1oOgTuwS35I2nB
pRcQObO20nClbnePkccQAOuct/TXWUhBZ1Vpo69uzVXcZrGalG1M4FCmg7tcrspDXRpAgky2nID+
/7S4fK4EGf0Y5htNqskWSI9sKRWwOMwQ1XdD80j0SdcRgvSD8x3QeYFwjdgR4azpNUsEqB6IdLdD
hN80p+hUi1NWslYXMjkzEogJI8Wnm+cRgln/bMWcPv2f8oANyvh2Hvscdvlu2G41quNot8G0Rn3u
A0EVE7ZGBqz6ZiGXB6Ob/mPkUv5AtKb4j+WlwkGcZTEAkejD/Ez/7PiUPzRSCqh/PpLDAJPRpgGP
ZCizeLveiufNntV/1RWSCg3qyD4Bu18aI50mho99RJsF4U05jUJuxL21oAW1GZGAkCXRGfBwWSJj
c2l/Uunf3p+Q5Au1KynvaVAvYVwaxez9Q6W3SMY8TERV1KgHXR7SXtNuwh1GDNMe85ZS8RtxzOwv
uBYsnWW000YxNtGAe4PocY/5A6b0B/eIYvK2gxf36VGl/Icz/SmG/p2iw6Ig+qXz+D2Kk3BOsj+l
0GkS5zqQiq4cpDKExZxYf2eEFOAUyJBFufNjpvE1nA5NxLfqtW4kJK2XoG8+StPLPZ46vqFsBqi7
j6EtsU71pjtjQeUTJwavHrWc50tL3Bk3ER8dP54ImzrBo+oB898Y0NanGQyBNoM+xp/Xw7ICFnyc
ZNmwH81rUI/480SeJgswC/BCz0v4S+snR/QEMrg0kF/6OOJq5BYNs1JCfrxaDr3uUH90+qpsUJ2p
6oeS+leaT53JaUkApjg9wuTghmE92u0OHP6IL7PnmC6F80xWOtcqQrAucLCf5Cel1+6xkuAoJxBv
aT+++XI2xqHQd9YWxbaxsqUg5KbnvMDjcUV/Bf+8Pakj+OcIGxJzn8xidnRvhwv1+063Vh+ei3Vx
uf7REtEzfnYFq8czC2wtafvtjyZ7El8aaBp0idjK6pHzKYcRCkGryvfNRMIQT68GYq7lGnaWp1Fa
ghQS819yP3PNh3JoNbTTLuQa3x15TIDXJVmI8frBFIuqqZDYrVcxMwWO0U4pk0raDTtzdKxLcozw
0XWmszm1dZgMroJf5Djm+Xnh5nAznWGooC+bJKg1Cvxy6OC/lxlFhxkK+RWomDl4VB1EKNimSAvY
8TauvLapAB+LKqQvbxIVeSMwfCU8EIimBye1ikJolQoA1kqUJQz1+Q4mP620AnxNbiwrQQKww7bm
lup9DkmmmUMZ8UKGkPqd+zPpoC1GTYOcNgMaWTQ6sKLMuq/FInbiaZKdyniyEmUINWthlrzIZbHx
1E5slt+F1FZwcBzlDg7z5HCkIpaHW32l50NJ5EeJxhH+SgOWzB+7IIV4+e9IgxJF/WFG4WmkX9FM
lLVfyb6rwaGCzoIz1pIBArm0HLYYGBAzaQyqYu2NKF+Ob8xMNg4+09N8KH7dLwe+PQ6o7gNmSpod
dzakIYxBaI1QR0/AT9Y7VmXOiIu2YL/zzK1JCsrVIQzvbTCmW5S7K01eBFnOEHIiC9Ph30LVFDFt
otqVpSkpmeM+Z2w6IguvKo96oBv9MmBLAcKuIYteGPHQcX/z0MDWob7dBesQZWqmTqxReRjsDwWF
B6J5KxLkKAmAyxHWYzVGDMjQxfCZypOmM7+Obq0HlqfKn2vnJm+OTlDVcb8gej4uVEDeypjMAv1F
d6zisArEVitABV+ulWGWt1h5Xh33zulStuAKB1m9K5qIJhuSlbg3+ERjPj5OdV5SyVuMi0cKqPw4
6S0oB0vi2svF5P2AuwxmKVnyDjUETeKs9DE5rspFWUn/W3OE5QbPWBgQYRqoFtZs1rTKUzljVjSh
xg0Auigsiez4sE58ADxRUJ3OIEWTnyaPoEg7ZN5o3z/3FxPgOFth87AWTF1Cpg8wP/WbL0JSafRC
jUVa/quRvNg8vwC/u40dOiIuzWf+w2uQ3pw6SAk8SgCn0OBnPAI1ZdLN7DAgM6I8yRbhyInvjGkU
MZtXdUi9IIe7W2QPDw7RZTe47+FMMT403Kb0WR1gAwtFXSHg29qWFMPM53I9HWPgWocvyCqtBlB0
7fjRM3yBpjuHTf4BNQfdL6T4vqmLZ6ev7BovTL838okZIROLx75U2WXZgYWXpunrRMR/Nrg8Z1vA
XEan9vKtXgoadggOHA6dpvgPD2aWtdzJTySTHkkW1pMYRFxJUuym+h3RiCJ2fLaDgGdY0Shh2t3o
x3db//rMEdfKkS8LAUg5R2tiZxTehWNSyOl0Zd9j2Q5oDGG4vJ2lhz+NyXZxEwfqfeOfZ+XwHyLZ
h0w9c7cJ2FPl/dZnKC+z5GV/VEqgejm8kgxk1VI8SlMH2o4gpCs/26V5mY3ENRRpZpwLZ+8DbHsd
q3LbQ2dl0e01DJU9mfkvMGj8IxoNuRtGRzHbZBan22nnf6a3YBXzJe9rwQLRa/MbCYcaZkqs6ZIg
7u4KP012VCsNgo75bRSkw4p08k3j4TRtxRv+LRwGd/eT52RcHRUGb2Lb9Eh/1VxKJvrINhP65Ww/
K30eqfBoejFZvBsG/a8KrxA+SLinIKRyW7l8lsvDVsNosnbLTS1FCxIFrVEMyWE2b9hZ6zRRQ7cm
/jXogwNQnETolnQAjyn3WDP5gNyQCgWmQ0DxFO1sIB1QnthM+k5Fzpa7MPR26RI/+9L6dEllIqwK
qf+yfmgqDZZF4MyrPvupQCunk6yMYPrT9v1xijzFed1kH4vh0/c4gTN7ggrIc1AF3jPttdzxYU6s
Xrvctip2uVfSRhEMbzIxJ7vJWd5ivWfVTHmKwhwOngJ/x52xo46anZ2d21jmxeZOQSNUU1djpjYP
u3ymkvQViLKuxticOzpCCEsrwsAr5im7pjTi/mX3vX0rXv7NDiFhvo9g92Ns6IGYJCCoZ6P++qSz
O4Fc8GlH2kCd+VGHXRJJn5u0RWKYEKXP1MpZB2Dieh1iodfHSG6Vca/iXNkJsyK4lN+OLO9vMQKQ
oNxYNA+GiV42IlkdNXSRdBs2cUF4bGcBbvWwqH/2OGZKgASgKbQ0dm60fsp3o6S0KkmBfoV6a3rg
SPAGhXsrI58jqc+UiZYB9pIBS2EtEwW9RP67w92NDoGZTFIjp048+HWUiOoECwqLrp4z1Ci95kD2
osciK2R/CG/18tOMMoDXtbm067DVujUvZ+eCzq7jyIgSYC+fkHqQOq/Jc/rN5jqQdqgZkHhYgEV/
QjwvR1kCCQ+oqCHAkzRkC8fcDH2if//N4EKzQhlCVmMmndSjs96vW2eO1vseDs2t1WBGRcaQvVSd
KgsgZmn7meKwf6Er7HLS7o1QAizdBoNUWXyXEYoIt7C4rPa6um5l/muuAC+U3Zj3XP33JH6GuF1l
aGxC4RT0EetwVHwNB9Ne4U4o3V2M7bLpFSEWUSINqdTHO2F3lz1fqcbNnqJCrUKrDDl/u9y2IjT4
276U4lPeZ/BTBw/R39yDPSiWpGP46QzbZhiVXODvAL0nlJ1ENDTPfesH/q/t/s1QnLfg3NV8hNlR
JnmWvsarW79I6l110zqBcR4jhkLotNKceB/g2Lh+SiF7lBKnebJBzMAbK56xJr9iaK/UR0w65CrV
D9PhFEAmPvoK+mZ0sMaFfu2sJJSo8KRw4Qn3JjXFHOGDrrEpg/rHu6rjrNa0JnmRBGel93K8b7m1
BeWhWKAVc2oRRPIXfyrcmSuFQAMOF0J8tHavwkBvoVa0IvG9EEHZ0cJEwABZOQ6jtADoIzvlI74P
sCy3a2UsEeguITF16PxklFcsSZFDmd9iPORNLgJ61r/kNb/LorEjyOjXoiUxpHzFz1S1ooNVCouq
pNNTJunXUt5D44pztgW7Si++6I4atyhybkkw9Db6eXXgyI220bKYN0v4IjRe+I8SMpr2BZL6iPbL
hiBiq7McVWO7He3+2Av9EUbhTim0iwF7Jr0qiFyq1mxlC6gsGzCijIl6fLzl/x7BtUlHivLqB0p9
6Qou/nmjamhgHU3K7HBEUWSqkEvwpmFxMxaR3/gmyB5+InwU6erCLJXmSB9PAn+uRUH6xczstWOb
V/tAG43zOR2gWWS9mxpIdsy031N3JoLJ83Bha/7bPPMAHkUm2BS62Qrf5hDYI3CIKXIdV0rBseZo
KARycO+ja8o0uTZlxCLyqjoBdCal1rbrmPMbWkGjC0zSs68Nj4wsvJ3CR94UN/EMwmB8+ngnfC4X
yD23IT5yPFtrW0gwyoEVK5N7+4l/Y6FzUAyanEBlkW6PRi+OCP8rKRYcdcuxetGeSBbjg/htNbsg
y5Qo98jAzMjwO3PSKkUful/2QYMevZnYy7EjlS2Fc1X+EB5hDDskSfibGMZ6nRC0LPYuF/kJs4m8
JXaF37QS+3dIkE7BQYYIZlmvLOgs0NfERB+fZbFqAMA1x00D1qQmxgWpy245O+LOUCh2VXUOlNxQ
pX23DhQ3eLlaklIPbcptpzr9g0kqgxhmvNgBa/YS0QAit6/hDkCYGcLvdmi2cIk5XkNim3j2cJn0
uDft0URwPVVASO/c1A+HuKy7K5JWxFWxpzS9Ps1LTp9K9MK4bcUglqW9194mT8LQb0RPcbFR+HPI
9Yj+AhLY1cQsB+/ikTXx+T3H9zytNIRvQEZ6JHJCtbAD/HTUJ+lYi5SZJ4uM+mGhMN/6oR0J5Usa
ehsNZIEOeHmIcEGYKUxx4dzg/tPzJTJRdkwJkgLWRhwrTJy+byRh0e5tR2r2HsGsPWpJNaUAdt6S
j3eEvtBHEGTJm1mLjvVDUZJ5M8Ue+KsIOtSlyfsTheOuNl+uzOOa5ibS2kazGrxxNjApyu33zLev
1SSobvwXkGZyLEdF8ebEowdGR6rb0hRKRjKY+nxPOk+W9Kma0i4SwYSSPbkBDPYbip1AsORiIx0l
V8g29wNDUS2XfanGzHTesCXuMFNaB7n1H9UEtKTsN+I/TF7UJn9HxRvk4BkjWQ7L4sVJMIEqBxfi
+jGgxvWY+nlfcbJiVXRb+jJV/PYuTxJE/JNra3TM57JhuyonRzR/CKYDLQUk+tAfsvz3/z7HXvZR
jTNDdtO5kfCOujecGxI9xWQB5g0ZX6t6Su+Wmen7QG2Cvzt34R+TNefo+VOwWS3Bn+aTSeqPr/CA
63l5N+e7UTSPgPk+vKy2lrXqVfQoIMgP22YskfyHz+gHljpASgzJP4BvxqlZLmG2l4Y/qPwoHV9G
h7bRdijhZ89vhEL0D2wobc74cAGqUfBIjRHeCvHc/fTjUZT1+try0/hg4lzb9I/vyWQ/jelXFYZz
m944LLXEo7D9cLYNmAUZzfn0joe/jHaV86FQeK5QCagtcQ4XhoT0Z5zcsAbD51YWGjjDKRr0zZnu
soM2BDKEt4u75Tjsf692/BpML/uc2QzWhaSgVNO3zbVBc0b8ryAG5RaKmE00E+4WrnCYxDuJDSSF
HtZH80gmkEowSFCcX3onw5ew4sanLbRTjUrwlCeecupm7g3Ko/8nD+8Ek/tgpElzYgyrkJ5tH19r
MhC1ViMcJvs8sCfB0FXEg+7anoeNgHHKQY2E6lO2G7X5rBZL6dbKtmhE4xgo0rDVUTZCUtJ78nvL
YrAWgie2iR9oy9cXLYixZ1Xzl9TwQIdyJf8yY4mpifAECtZj1AtmMxQbk8cyTpOU5GzaXaEvsm8j
2rtnCFx+EbhyRPUrMntFPHIvCzNZ1YWT4vW7djF8bbM9qgJDPvvi8XU4XZTk0FjF/fQW47+kwqPW
syaYlNjFp0z+jJNYWPLZiaW9xuf8UX0cH0g9b5YSPCeUX8pdrnx1bj/bn2r8XZVoJlAO8e+a3DVe
pg/uz+K1QEziINFGoV8UJiSdth3mrzooTOxDPtOjVAHhwO6L7jvcjs8c8uXGNCBbFXunKuw5/oDm
LWCK9shWSjwCa4v9cJ5EaaASqagHGkc5j33Pfn7XOQ4KEmqkdtfm0TdQhPbLj5SCrYx56/w6GFHD
6dwTMK1kfE3poKiVWHp5LSNoUtrGN2y1tE4f2R62sN57K2jP0ZlI7djhutL9sFf+Pb3laZy0pEWR
Yb2/RZPSkMSYlftNyDxQLk7WatVSHCpKoMFPRXlz6cd9w3sM3OTTFyHugpgcfMoI7QsuHB+oAt1I
RNW2GMyZzmgwIhCOQFlL+5zD7hDWNngAzZTfVieDoA/lW/iVExmLTqhzN3vDj/83n8Q0H5XYPVZd
IbXEPRlh25CKU9ZrwkOO7c2ybw3PD6NzNibGeY1Jgu9ZJW+VPWvfPMmgqirLbxEl34MgA7EhgsoT
xTXZ6OgDeKTGhIJjmwj5TpOLef8jmJsMh9IQtu8CtyEEIob15Ii1uAz2tQ/HliPIQ/4CBmzQEu72
8qNvlH3n11DEUVf2zp7E8gMl4MOrxMTaPFqFGNJuCopGt57xw1T6BPuo3/qhnZ8N8WCWOGKW70Fi
fne92WJsMqN4Lg3tgzoHP/C8sciP+5DR1l+SsNEvkXnBWoGlOWW29FsslqZA3DIneGSvhL6CUJqU
CFso0R17Wet2qHbsC+9MbRh81PmUm/nw8HQnNcWqh2MRflZcFj4t/JGqD7TYiKm3xm01Ows0YyWH
QFoja/JsFvujZiwsgAj8EG7lLnh2om6JTlV8/pU0baslYPbD0AI4JoVKLzCmuECodBeCCaQ6tsB3
pL4NsR38t07ZjnyRxAa2EojFQJwUbhlOarIQKnL6pj9wSLluKgj+XGmruJWRjj0MEi4wNdAG4t1M
rTMFoQB1ucf3NFqsE0pRcIMC7BNp7FeR2g+zGfHKk1suchBA5EvO+wJqYU5erdMLggNIOYhybRnj
sdj9Ol3gSzCuJJHutK+mh54rtMnz1M6dRKad6Y3oMd9bNWhTH8hqD7mxOMOH89OicdQP3GWjbBuj
rmVM8yVRhrAVGv1YLkwhcDEo8TPaWbM3t7VhgFPfttctK7y3KpocVaLCTE9meRHZ/B5tQNDeMkJp
WLoZRu9vkn/4MslOhTE/hAMID1MDuMNRdBWU3uhbwz4baD6Ji58L7r6dcRklBNIiYZeZu3CWh9iK
wOECNYG29JSnYKCryr+boJd2yaFuBGc1foyvSOJ85tc7rsbpvC6k1OV9opbUZ/hsYotnm9CR0xdD
PxPZKLAW8hxRTcXhqKDDcPqf1fKm4D/Q3/YJx335GHkGxW1giFvL64mGhbGXPfqVIwpgoH4vdbH2
nUJe9RiYPhZEEiWwLwiWPt3Jxs4KjioKq+Bwl/InU10utYdnakF2VMSJgK0OHzhfsiNPb9ar4SyO
4bp+F67Mm5OerW3eOoCCzixjhktW0RYP1rNXHXA/Va3jxlu3pby/hBHaPUqEecjzz3uEYkQ9g1ge
WGKlBKUB02D1Aokz42R754yXBQQwVOq0qd4j7Rs+kYCHAtf7QObWn33SNQ8/hk113PVNQnvh3/8V
BCs11gUdRPLXVWxfbkzRWiZkyZS4waeiajLaIXevZb/73uP3hYuP6mi0XsQcM6CiD/HGey5yDki+
C+Uf3rVJ6lzBe2jTSg4YeF1srTIPIrMzMJ5luyTyxgD0AUG5MJAWahIG1lW9V5A7PPsMljAHWwhE
dCLVOuBQ7/QTTCyEiC0BOansw/XmQjJkViZbZw7Xt8NxlUQ2ihGV6mp40d1eFmTrOSRzhUGDrgv8
x8N5+E0TNMO04aclP3ZucX8d7KLjHUS38HsLPqN+L5gjiTm6c2pW/55MZBAhb4S6VDVMV4GDPGFr
cZ3gSQ3RZOiA/JDZTSi86hyj/hdUaGrcz61h7sHMo5ZB4aV9KvkEE4MKR2mxUZpOe7swTfeanoV4
fwyW1tSuWYS+B9yoj0ubc46Fz3sk1XhKC7YvMbeW0MF29iJ+l6ea/TGlhGAdb4gjQ3tjIRuIhCvd
hviQz3Ve/Mu3TgtamLRhHkaV54Tem6TtToEy4A5mW+XJZ9SkKqA7w4n+g/2sYF9SyWvPZJlTs+fg
3A7+/6Eyyq6c/GM/Ha20R9IFphKaG+KlqqwzFF8fwOLm8zbtiJ5BqH1TAPUhqie9D8wWSWZTumxY
3KFZjG4454/MtX9FytkO5ifwN0oJLs66hMdEUDkPy4Hx60FHZMHvXdxr0F/lZ9sFRV0PCZW5/Q8l
TwHCoBgPvzeC2KSCSu4+4x0n45pcX+66R7dvn7KSk99nBPkta4nTX+hmmES5leZR2cH7wF5U6Quo
6XI1+HRBpZo9fCpgtZb6/FZkHfZ6BrnguHcpnVz7Yv5OLsdlB88X5JzrnpA6W6FqE2JX6EnH2pH0
dkvDi51VA2oRzBYKpHLajGRoo376lATcMUHzcS5m0GX0UmjT8VQz1IJNHs2S2dPyYXFNJYV5MEaI
aiXbPOLAF6G1TXzNsQRRzdmj65wXnQdKkLJpNXn+efJJT90t2dRvQZeU5XNUwrmD+uMnfXcMc3CD
LyvaH5rXFQuj0b0OA5y7auDl0+gFtj07Mc63ykqi/UEM3e9FtSTB8amsyscNMKlEzgw6hPOn3KvF
pLOzS1ceEWoVzU58naZr6vP2Nhr0+84U4i+MrA593hWAhwgubIaNccck55EYplkbbeKd9xKDQcjt
ydraSpCEGbHC8vdkaQTM4asXdr0TmGmy9z0voVPE6qsvqps9SM8D0PiYbxzJGqGGJsDkRXu8XdKn
9c0oEEjs3scw6f+QAaJbJfoiY9Inaarh780jNer/WniwQhuTHWpibEV2ltfktPpKApJJlPcMqFu1
Yne+6ln3994AormUOOBuGUB/VtkWgq7kLleTWIfQBkSS38uddYrxwzzrXeC7wnE3zA6RVHgmqwe+
iR0TaW/HaTr0tc4PtZqmIg2/ywcOzvbDd9zUSzragFs+e6+o7xsOo6BhlYuKV+1HVKmxX+UOnT0A
yi4z/wkVb1vEf6A70jSNlWoLuNjsHp3iyu3pOvoXGGp0n5mMsI5Y18QxyWIXj3erOwMij8R+biKO
mcFALsUiPe2sVnqxVJkwNw7os9+f6Fz9ywaV2ZIPy1yym8AdLfodwKfW6F2wPYH79d8sVPG/Bmg8
NcVK/D8of9AHOQdzwyFK/3jhso0JYXJtLe3eRYXlIQLrdGc48uaYlrDtxE/t77rsLoR/OZGcBIL6
LwrLEptmswDqpQWSsOU7pRh5Be/wJ0LghOWy/j4U1rHftszJGg6PHs1h04eRi0QAX8GcY+ops7Se
ukMCe4taCLLGSeL3EcEpJQOif0PXxYU82KdF/p6QtxokhrzS1f++YIYKmVjHFoItbHmbcNgAZ4YV
/T/DycpU+bL6mxZmeVMmbyjWhdiK9tPHwPaE15n11k1VQhOmh5kjYdcgeRnRFdAQOggqT/ablW3Z
rXPO6zhCq5kRAOUvyIoWnzjGWEJ9TZuhHwmdsi/7hV/DM3DRNFfSbwgSHWnnQHcPcNfP6u+rnyNW
APqi6bIjZOZiLCHEXu1CE/U+vPa0lv3YXRiVoZkcL4PjeBnxopN1+sDuCaHGjbEPbPc6XdvZGsol
0DyMv0WaDw6r61H+UTrxrc0UnvL6TLMlM7DQvXITkSr0eu2WEXufq6+tecnQtggnKBvG0Kmc7yt8
Z9bhdyVPNzFgwQnJd44inrIQ8lONOlHguZbtZrnGiWHl7ablaJuVC6250s+t862ybaLT2wUnu+NZ
az8VKMfWc2QuBkYVTh91TKHb0cmp/EiUuWddBmofoc+39Z1xvUeCwPQFjM+rMtdfqtA2D9hUGygL
e3w+yBXX2qSYECqT5+1CuFumHq/FIJ/nJiAzb0wZuHXnA+1okevlpg3y3nnaF76lxQEdUOxY5WSa
RXf9zqdBdcMoBifPD00EddSUwngGvNQUUY+yELWvjyC2MICPziZsqRNqwc6X/Op9JqGTli4nleT4
IesouDkg1nN24QoVijob8gBm19KFMI8qqo0GDMGp+FO3ywQ0g+4nPvEWqcIBc6u13Ra08oWHF35L
/KQsHGkJQqmSU4kaclzI8iF1h3A15JsmNe1fXWNm16snj2ep52NcUGKJUhqO8PloDv8crlAHyNjt
TV9so5dvvqBPWOjXIrQM3uEBH/YizVbByGeyCFI8sYIIwwdINKPMiOw+eb9mAN/y98vjoEP3G3Y4
SmIEYLdl5C35IRuhsm70d7zLLNB0L6mKY9phePbm/XRHFKj6rzWHwESyG7Gl05vG0mBIYcjKEX4K
PfkRGfSy15fkugZr1f0UHy/OhQXunk2XQ9aX/vxeqku+WtxJHRis2LNlON4zFXx4ZtIbHCiZuVLf
xUKSLaFQQt+OnDbEffutk6h6oMgd8GYwq5neDS+4LEoI361zDzaMzr/jC5uSWf8mfJ7z/fzzgQQj
KqHxp7ekxkjEcKM2L/MAJeXlAQqmdUToeU/0cY02FeY0sufYqNQvpoa93eclk+2YUKsAD+usbBfL
kvH+EM2RSEwAVMrTIx3c4EnieQnToY+e+ws1mGdnaRMQxkNCCjkaCOanAKUQWbTyxwzhgjtfgMxZ
X5Whwn/qULgukrpsS1M2bd0GfaCWkNqbyOVmMKvvnAm/FZJkivm7/In0SHCnRfhJddIWQxt+4hHd
0olYgsr/ih1lB8qhx7qYppvuZhGoOB9DxfJljQHrq9cCoZovw1Y1eMy61++vOB5sel+bd0Nn24f1
Tw/22TLtkj/RLDqeC9ezhQhNkKGIBSv/VE0igx22zC49SYtKY5gxX1saBMuNJb6IDKC+H3j9gLAC
W7D7UzDioN9wcFV3oYtGY/yCw1L08+DoBQB73fvkpaIAcYG2wd7MW36Y1Fs/srxdet/b+uvORBWO
QHoOY+AmMMx7DFzrUJHmYeqOn/raC2d9sOccl+fsW3rhKL/vRGHSi+zxCfFlhn8ckj29r22DMOc2
dWIQj3DQcdBr3VJ36FNEAvTOpmbO+fMYHyZcDNr2OGkdPkp11xHzqULa+4DsfllyJo+AINAQbT0o
BW6ZF+D/UUrZ3ewJHSKsxA7NWTeOg9DdfuRZhcAAUtBkYNh7oJHD1Q1pY6VgpFOs8CRQ4Q5MVo8P
8IevV3vaJW9La4X8i4ax6vESjyGX+5r8oc2YtH0yJHRB18c72veaj7hCBPb6zjayJnXEf8Cqt8Tm
BpkkhUqOwYts6jT/RE/KyTEHZtJqMzqIEiVy2OOq4KR13Ds0PhpyGo1c3bw3LpqgqoYaelrcMW8A
VJt9BKND6CKHooLyDQWkc2wh1liWTBRE7kXCvnrkpx9AlQAiiw6Vu1qy8wdn1St0EQ5/FpbifSmK
2kwsTI+CG8aggzqfiYRsvjNlFR11uPgXIUbVK2toFxw5rXs=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
