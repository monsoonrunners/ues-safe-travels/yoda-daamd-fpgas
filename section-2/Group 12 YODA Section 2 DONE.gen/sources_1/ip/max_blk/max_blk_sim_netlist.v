// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Tue May 17 16:11:26 2022
// Host        : Xronos running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {d:/Documents/EEE4120F/YODA/Group12 With BRAM/Group12 With
//               BRAM.gen/sources_1/ip/max_blk/max_blk_sim_netlist.v}
// Design      : max_blk
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "max_blk,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module max_blk
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [5:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [5:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [5:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [5:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "6" *) 
  (* C_ADDRB_WIDTH = "6" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.53845 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "max_blk.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "64" *) 
  (* C_READ_DEPTH_B = "64" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "64" *) 
  (* C_WRITE_DEPTH_B = "64" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  max_blk_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[5:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[5:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 19248)
`pragma protect data_block
VMkfTWI5by8AHBNwW6HJFoivkQ/azAdG2Zn3Q8CVDqKwmJ2pmiQRjdIybEz7H0w+wxQMZK2UPpOV
uHOuK6dHQzECOLjLEZexmmbpfFMeu2ItsvKkFLc1m14VQl3edmhJPVBaCvlCIMS0nHMzNSC5dzda
WwXiN/9n4MxO5JX5KTvp1YrPdKfIPIaZ4EgDYcjLtGQxPtytY6+N6YhuFCS0peCMNHMRG/dYcuDs
67TCCPwvydtEbrvi7l5rUE6SNJY0YpHFCJPIhur8st258D4ZI2rvelmpQ1Sx/i6Pl23sI7g6MMSf
VGvwUWw63JZeneReDhJIHKpOKNfEVVrBMurud0X84nriYtqtg3gGMBoIHu9/sI9uKvQyTA9WxCUA
rFsmkXLyfLZf0Ji7C9/wfzNZjwaNNSK63QBfpZBEgbea1Cvj+3x1AUphk4Hoz5RWNlG1Bgd2KnDn
A93CpAEqkLw5m5KwDwpFcAmzA0d4gykYqO5jq/sXz4nhY0bcgWe/6c2x+Wsm7ES4LINcU9184a2i
NnoAkHYrTNUajqp1N68N4TcDCmD1o+2TBQKDq8b032scTOpjtteJqFVqRmDiThPEFlJzQ4dIztzh
ahLyyuooznNaSqiPRbB/dnfAlBncYoovwHRlFXXvZV9VGpfWNwb7r9ZbrGCp+YEoSD8dBI2CV93f
hAahZ/9v2rWy25Fqv+7BtQ6nJ0dHguHwv3TUAo9d6wTf6h6AVsy0zFgpMOtsKQYD7YbJC0yViTe5
Tj9CdzaEhUrXDoNbRVvncR4TsWrfcsXkXphz+IvI8rgut8Xl+EQupfor7JMS6DaB7L348iIhu4y3
o/EqShm/g7DQ73QRu7tGRd7W1toHiMXYI65GIPkhYRALMcWID8Se0uJQSDExwkVXgHZJvZhuWis3
y53XN/BhSq8FmUKSCt03Kn40tUtu7TeE0fDyYtqB46QcPhvnP+DKKs+7wfiiHQqVrWJ9iLogxoFN
B/vmX59w2Ft0Jjmj04RbvTtKwVE7ci+4iQDaJoCZZGtZ9H4JvEb4fqcZv5maVCqRC2UasVSC3S4R
g2Rubov3YUxcFqmfUFuZxnd+Jz1y9jq8+LJinEjbqVX0h+5kagggs6P5OQKXRoxkCfNiSUEfieKC
6YSv/mUNXH5WLM+YgxHcEuSsDwylTbHmoXMO4tV2Uiofr4QFx6jAIqzGhNf8CO/Z94LuoqvK0qh7
QGn/ltIwa6pn34WeC0+G4o4V3xL4JyGh9998A7eXPJMqL/T4doj12X2Ulmd/0lioLkCAuwmYPMCP
dupTy8SCMecWsfcvTL70kwMTQUOIiHxF1RbTdnKbxhnHyYiH20qMe5JiK3zvyfhnzEwqW3TXDsbF
isZvqAQ7nVvGtAMFZsw8f3aJacfoJjeeAgmiBL9ONO9wVVo6xxEGhfN/uaS+gs5G1kWoxXOrnybI
qnsDGN00RptHiEbHjF1PXlNOjKI2ftLVY1VPBauzrLeZGD3Bt0PN+jCCnBAHQdarfeKjYnPBQC8x
0rrKj33ILlLqj1Q3Uw1FVCEt2MCo5DnNtVW36uoZdhuZDWTvwTPrr3i5pCsUkfZqTMI/vbkYnxC+
kE8lcgOsmj2eigxgU6nFYYogNDHgCL/zSrITqXrU7fmKB29RuaKeSPzK2tYFvY4WEixcpVDPLU38
FPPO/k+8vdLA1WSE7Bj9CoqJ8YDvC5ve08rTfREsx+bQrCccAgm+iTx9xYr/0q8zjPF2nEXE4pes
kcm3QTEF26B3uvoGMVrC2xcq2FW6lLViViYuCCOmri7kHqZOvp+fnDXX4TIUKcOARGXdEBwRz5/K
yHWf8o3+zg2T9esb01i9rZVg5UYvNPwepKSn86Ya6mwTQYyU32hQ8wXgtDZrdD1zIMoaWdt2hkzI
RlOS8nRUCrwR9Y1YbuR2zSeZoMjgi/QQmWALcFp8XHBPjOryQdGsEIyWIjIEQV7+5mfcyIa8y/Mx
pB3GcAVYepcFcFPAyxVIUaUB+r7PjfB9gj0Xl4HIHMdry1pVG4mu1ApjRIIO+Gs1ACGRF1xis+jo
QgT5Zh2IKovb34cl/a35KudAXEd6BXUcWuOkfFSRj2hqjPCtPRc5eKKLooN1ev/R0XpSwtNmJUO/
ofGinZvl/NBYuHrOGrc/MA+elNrZdj2OI0oNKGAJm3SuScBu/qiH4007NRofsQtAZ8AKJU++7pLj
6RV9eCPpWjhQrwvjTZFFioKla6pDfc85Y8/rDZTu9j+V3sZra/JO/ggOwyZYmy9I2yfXsv37KEVt
3hpkgX+jfuQy34AJyhIwvaHNLO9q3/iE8eYxrxpleQ5FraK1YZ0iAncMmkwtGd0lwmMXgbi5zpXK
qQ/9T6rnqhL/fO4+dXCrU+88oR1tIF+iGr9m1kVV7w/D4L4Iai6FQNaCY35EokqmSP3w5352LoQ/
pGWRNzI+2jn2ftCmVmqmLgSj7TBUAsYLXH7JWXx4EP42EgnIbPd0z+mY0UKLBam3Kxzcrcc71uZY
xcHxaFhP4gK+SDOCn4AVzzuUDm8VLFzAbQRxczS93ZQid1jEiPc2NJmY7qcZWpQUx4B5pQtn2Vxp
rUzzULALFL4kIeVhxx08EoThxdL/UmxBCvCRRzP0WzXUYQRGG2deK6zlcdm+bHXoojy38mRltQi2
wMmMVJcqZyZX9fWT/Yc0pbGidT4kVbQcmECdubgJDQvBjy2xmSTzIIHg8uZYxkVTZ1Jc33MH0U+R
XdRCtyE6kjXNTvM7A7oY9FNqXfPwuOrG7jMRGhbbLIu4GbUnf9uFziLMMa4yi3DFQHzIUVn0fB+i
nGjr4OaHYBd1nZY9FgpMPBB285MO318OHTe4cFTbSgc57IGMkNihYHUeuQWO6kjEz76R+LchZJNC
8/NyrK3+mckayR0GaViltLkZfJ91e7/sUlc+bYhm6Tj80iAVF5Sm6l3UXuyi1IJvDVtVfeiq/DlM
MUT7yaUd26sl5OFiqoSYBRguTJGMsOR/8x6rOw381zLM2Y2FJxgsJ5D4mtD2JtdETem7jSDRgkZh
AqP50qGPDhc+UPOKXr3P2XyrzzevRrf3MhfnOta6HWAJfceacPELZnI33d3li/3k7W0HoClaB7ct
FfkhBevu6BL9S+k0ra0osg1PWGEhgn8ZKCtaud7gt/JW1r62Tj2+ecf6cbhGTL2Ijoy+oAg3UUD7
o9zfRp8UQvjeanivbHysfyHzv94TCErifZsEacqTVajBERzy6NWsPen0fiMuxWbRf++G5Ij5wcwR
HZY+8xB58MzoO/ZJW7c3P80djbi3Ewks95WtmVUuqqN0qEnnfJ774721BXRBTS2oCqLhezjUWus/
K2QQKEuwaMMDtXiUGTAwkLY80Vpkx+/0I4ZZfTaJKIix1uzSeOjPCsNcozjWADcc81O1NkOfm+Tm
3yDIohZktadST8viJfKcrA0ZLOpPouhPL2UDasffqMK2FzU2xEA3V6UpwArVIeDRS7EbkV4qZ/c3
NVMRWg2xsnF9Ii4cckadPy+ZQY2vAoZ6GNV1LpYT0YTWDI9pwuUDdBcRIG5RYpw1zhzpf9phmIqy
+ivXR3czvy4SDy+SNB2pcxLIh1PGrM87VgLyAAwMKMAjvwQQDC+Rf5ms3VJmpBipPSldxF99Yg5c
tP+tsqWkJXsIbxDVI5qiHN+sfbw4JORvJ6/IIuYi6DQuZ8wwQD42aAnLhGOpxW40tBipXf4eyFQ+
4kYRrS9jGZzoo1z1YRCAz7D8kuUm1W4qkdf/ZjYrggXs7wmtEXveTxB0PbA3II30+m8FXAr563a2
yhEMbsHQn6WE7km3/eDSaszzUOBedtgw/MYrTV5xgFVvA85tRnNpujRVKhgmQUXPeHg7ADOdl2tA
1MBmYWCF/EOcHZJrWf3qK8s8p/b567D1p/sMv09wvlbAT8CNxcsDNGKhUTH/t9JqfbDHmujv53Md
p+VtWH2F5aOQ7/zQaOse6bnDoQSN9hCTs5cMZWR1vG6bltipt0FWNhWQjzJTJdWkeC0IdQ6Tzr0F
I3lxrilKJ7VeObu4wsMMPna150HHhncyawSvxrQAuYzBQpteSwfZI8Bf0PuOt+qKHW5WUH+mM69S
MyGhtEXOu67yfpcowmVWhX5kvvTD1oNoAOj6/HnE33JChh09rcKQKPQ7eUuTgLLZKpvqWRTwcALo
5Qc1a2WBKnA+P2hLy5MvYh7VO4tfAnZntQUWodwxOKXqrJq+gXi+HtMO49kbzuOg1zueZXqE0UFl
Fo0gpNg6h2ThKZPoIg0DplInN5hPTaUdCluxApnklYwDTfc53mAHzg73yGuhJcxeugVzH+kWYTs8
qpW2WtIRGA1pmxouHNZtq9Ae5f12Od/4En2t26tuGKds+An2HAjfx/V725e6JSqXByANZEjPYMW3
PyzSmb5MxZeqjk6aOuoGZNqhRUHJYbP0K6AMraDHL4dT5XCykW97qufXEFp96MNQn10ezIndhl+G
t+ECN83+vp3Tdrlz0ifT7MgMadIjPSbzxKz/lm9inj36JIF5+ajiFGdyzqW+eLbnsxgrQzeDVOhW
l8s0IitfIkrFwpzaXDEmbNF9WTLC/yqTjejAkKIvjIMYTbDdzBZfNZbx8xEcRBPd5GcNFSYytbfn
erUSP7bR2qDo54gzDidMoX1RPXVVW1OS+AmURZWQWUMfP9KjheBx2vTBe71uOHO8eDz4DRDkf+ED
kj6D9i5cJanGSlR0x0ziW/TAWNm17y2PfjWKn/RMlGDkPttnpKnM/qgzRRDtF+rJ0JM0Jcu9NoHL
Szos8Gl909dYY6AxQawR3/dHvF4HCf3A1mUFh8AqXJZa2x3oOUlOPfHQyTb/8r47+PuLC2QYk+z6
eQpX9e9ZQZs6oJkIwIB4fE3mG8W+z70nu3/TIyi6k3wkKNnfsua2oWDWsOvYyPIb3eXBzc6sFarD
qdkbbo62GO3NLfLsMwyjLxbFmbZXkisbin3aJykXFJsE83Jxts+O0xppPOwgnk6JK04N1yj4qoWn
KF/bvOktyZI1OUaFXUFm4EufqI8uKY/m/BIlAucIfJ+qb5QLql3hgNQuCk/uCV/rFlgUDyPqDeJj
gSDluc7HttFH6kSabtFiKz61O7/62coM6fZFliM2+UW2TC182JfUnoqgBqvNmqEX+sQAszAXbnT+
Fq42/el5XRU8dNIFymPr5H+DsiSDwoenWZHzcphtFNZG2XzoFP9IJhApycbkg8gw/RtaoMD+Nyfh
XP0tHgdKmnkd9KAe4Amj5uH6qk1+iJc+KY0UiaS2NkFV6bGdcRKspzd4aw3nP2e/4qhfMZURQAjK
Pykf6LbfQrjKSqxArQwt4Rm9Ysi5+h+HmBgTbE+2eilXF2n9wiHtg3TIVCZrn9uZLemMZR7R9K9+
iiHeJlliB1kMXm3FxH8f1pLJjdJoT8mU5mueI8DiV12IVXx8oxYuNBLZ7zPwewCDtqpGPZYALz59
iul5Wg6YJlCEgxthljgIiCixuT2eQ5+nxoNJTILplsp/mw5DeKU4a3ZzFXYNZLFkdh3quHHQKE4i
YB/4W87YwsS37b4so7ObFPQADQOYNzz/hJOeG6kQGhVusdCxoyUcUJ12xmyihJMbh6693+kXbMWI
xvZm6nLXWYlhn6pGslB4G95unDQ6LbBWAeVx4BeNG9PMgbK0ooR/0mQogoJWYBKEYN9GP3jPF0s0
WJRmJ0hGfZYfOR3v1UlOFyMNOiUbVlHmxthy52fYKEKF7K9ptKo18SgMJTzZub+lH71C5FWoJ27n
OIZl10I6L4trdPDTHXBO8I0em6zmQxPqHL1PCfpe1uvPrG6gQehMpvB5J+9of2hS+7vU+EvaCgBJ
VbH8so3CadbiXqkUIg/Fdf1bSl8rYXvnVEwvIhfGgd06wD7/xd9IWdIE2sAygY9Be7fPejDTeyGh
F/cmDLQrqxT8GIjgZdjm2TLTwtyW3sUHayqOjNoRwMPHS4OjjqP4gH08HKPd1VJgwDQucIXHgV/i
rern4Mr7trxnD+FHK8MZULnDEnadMI648jLBDAvrTVMZGwC1mXBPhaVYpWiazgenLkIxK8pR3Pbi
BIxOSyEIzzA7Lhlbg0t4L2H9Xkp62fVfVXvdiy0qlAzTA2oWtL/d/0eY4sG3UEJG7WEvSaKU4zhH
SrcGdWrxlMoWePaS2/gN1pILBd1AAJ3jSC88Qi330WxgJkeL49s2D5LOEJqiF7IDQV9RxcL/VQzx
YRsIkVOBV61cGg1zxP6YLOvAx0K6+7jaDHYqSSFvxcWUP/F+WZw154sVJFxnYPb5US1qtM4NPkgS
RJrDR88cn6sQp2pcmIlItNyxdKceoT4XIkYZhuz6gj9pfBnimXfFBoH6YdLNHN+d0ZRlbI5ppe/a
ikCVhI+z9Raarzc/uPcGZyX2fsfAsgexBypFRPK+xWfTYCIaNEljl9VOECzKBrFxuOCeed9nl966
s2d+GlF3Eqq8ed20X/oI9w3EK5RRNNZAr+pxZ/rdTEmUBooZylFb4eJAK5rJqUiS4cj0YY1noVnt
Vj6f8/QNigokxzO7f9dMnHKQ8q+sH4WE5XWs53sLbXADz8UT7DIvkw6rtv6Qpg+V1zOR6UN0dibI
oX0F5OpMngJsrswrrnGGpaq+LREKnqSkeHDzW5PvBWQ0q2+5XPhDCsedlUOlbHn5uFPGpNHVhooG
Ky14/HitZt8LBq0mlHnMd3lTkFrk1fLbFIPZI59cxYIAOto6mI0VCrOdCs2JxW+q5Pt0YNbWoCQH
sg0LlxxaVO+0hXtv3wlDjkluTTCnfHUfVC0+PvUNtJ1vZhW/jXwWoOBMqLEE7C2YAHeSDKYKVsBW
2AHgkKYK4g4QprfvxpNQ3pCuZKNPay7wTzfalHxUw7fa6KoV1FTXZrf5Ug9IF0Xxo2Jz1Nuk7qQs
bUqfpIEaXzMr1bV1SItjMby1NOlbkITMoLgCwBv5yz8154BLGT1Y74W/70hy6ZpoAnqFzfw1Cdfq
RtzEZDM2OnGHho2xAM4ZMwVCW1UW7Fj0VAKk8nGtz/YbBHj70Z2rOImHL+kgz4V2S+t7BxBHoKz1
qSBk6Rd9jqNcc5QCFEw42lfOdA+9jfzi3LnJaCGr+lmCXrdYjItJiA5hmW5MP8U14TITuraj/Rfz
vDUCYyuI7OGCC5ixwo4eiWGcS6DjiACK9XKovV8/JQXoU/mblp8i1o1hFvjYwTXsz2D0nmjxr6y4
clgb5BZ0TjhVI633MMop+7ffA6Wz9LfnKt14u0WTri1o6J9Dhs1cuY4Xeewhm4tGI3SHc6rQFhbw
/vWTZ90sYhjvbX/1BzberEX+Kt8xMubATAEZKWFYFdRyabCcHQl3pRK4Yy2MwZo9LOyMRupJooWA
lRyzEwpNKmpkvIby78XpyzDiv71FX+BKFVBOQ644XXiVl3egzhSLGa2/76txoNnAIS8x245/M6h7
cT/vgAlABesm40fncnxdM95mMwqbxtIb+AsyiAX/ptTxS4ntwg3KBDA4lrBINGZkg0ZpYHmsC0Lq
dU4uDE7I/UKtmXReFRJ7g8SF4MfAA4nmpW4aPf7RjcniZAIF6swxiypvJhbBtuskmcoPtQ5mdwb2
v0o6p22b+r1VlP4ooPrgcGnEabqFKvS06Cv/FeBj7+MVtFXbP8fdjQ0rubXgzHIXfBrQPzppeC8q
XeKGiDVvygeLS156H6uUs+dcjAzrAvn7JFk4SI1FGLx5S/NV7Ce7Pvsx26IyLUuoBYQUFmrPhWuX
ePYFcizcBQvYuFLo1NUh6DpRMe+0sHi+5cIopToxlR/tNkdQGCNtmsknHPLplPJX8vgFO/mmbpmH
gcWvq7zmMUQ/qHw/QxstRGZ1HNBbiqFfadVUb7tRVKaXUJmEo50lt5zzHmH0/XzaICKtHQTeSb4k
832O78f1qzza0o4kyXiUXVKUiFHUlX91uskdlJtTluJpgXAyuNltrAEz3pEP8mQHr7OiMrAIErto
iPstrVCDwMkRdbXeVAiuKmiwa/VEgiSkr3e0EAFCdQJCXNo7i00bsZqYT43HPvdLskkPx0sbgkcW
r6gQ8aw2NtFzt0F6424hrzjTlYM1syj2tT5Yr0FX+DRZ5PULMnU0KLgEAMvSFM7kmFyCgkzm4xT0
M/oJtBUiATmpb5Gk+7Ny7qc5J/TpPNawe6IYx+78OZ6eWFwGy1yvm6iDD4pOw+PSanImdBBx6P9C
B6oBHhw8rWA39QTg0Zl4aG64MX3qRUg+eaDQYAlftrkK2gNWnYzQYh16hYatlwYyA7ylHruuoc27
XnouI3IMc8QPbNxRvCzsJKqB7LgnRInwn4Q2dEjryL/sLBwpF0c5oCe0QuNf2qZpLTsqUhySUK34
jhcPOsBpcK3kCyUcacmeRstbN0jHJ+mckmx2lma/FgkhZk1w6a7NQpau7tdibIoi6bMdtwogIXhg
eNr1q9ALE5tzUrG4XMXSYhY6tKCSUdCA7n4VWMwVZqoknhkRPW0uQgz0cTT87uF6OGwE260xv/tf
KoNhl6HnEwvTmFLrR9YKQjNfl86K0QeBa0Z/VVB0ZnQepw/rh+Q8Iw2aur0TUZ5inItFH1U9sH+U
1F63PVVITrKpLHlYzn+oMhXI8HcDkZtj7QDo6PSAPfm47b1xcBo/CHMGgONL/ASC6jt9RjnykmFs
ugpRS86427Zvw2wbKdDy19tnPxgNhJdwqWYIq5hbYxTSWWxK5SMBzz/0ceVAOMTw5s32aLvH+MA1
GbXUHEKBBDUjVSGVy7hrlX6sWInowszHBYCUbYIfH+JFluuCKcCwWjkLSXv7EqD83wQ4GaApdjGY
jETkoiYPHV8W0eMKUiEFFy1LlfZsnPOh7HaWni1jZvGr40AGbmAYifmSYCsITrl7mxoy5CzqjRBX
sSbT+EBf4FHkgnrTAUHWWT5DcUcgY7X/XGdTPiUuo8mCLG/oxBi+FsDixmpws63ucSkYuNqGnxGT
1NkofP3yCJ+FUt8uScacWHsLEyqAteVSpQK+nQsvCgETRXJoGAFvuAFxPZkqgLjNm7NrKX1vmsT1
P+RPfnzZEBjOy9CjSlnQe6qx6A5kG4iZOlFC9img0bo9NfBJoTkNaoCeeVcVJeYhZHqKXoAwIP1H
NObgRXOJ6ZD/JO5oMxtFULwSLERUlnVbH8lidtCn5i0iPXGGxnUVLmCICZmsPSpwZ2Ctu2+ujmib
I7cyZRneB8gRr6a4MtnqQ4Yovf4WqQGuLy5BK8mxiND4a+28cXKY3CcosOmDm6pWmSC586Pp4msL
edgrb35fxFbxQrANWelsP4eVDnkSVLBfO3XtFd/k92QKJ9MNorgm17hPW/gyQycXcprOYb2U4mG9
goXZlWwJ5gaZsnf68YH6yNqa0ioeEPgapaUSNL4ER18xmrky7PclamWirqTtAvGOWo0l6boYqpdB
JwQpIbRa3fWgcHLl6RtWJJfX3lK+3CjTfrNrTmKbNZVCyPJndp7Q3uZsobXC2+0r8/BBR3TOnFK7
Xlb9S4tGLmb8IEfmXQlT5cV9InLr27L0LOmBiiDNobzGEMjAX956MyFN3CQinjYBIn6jPHxT2jQX
RMP52X0tk0bYaTO63EbnnRnJpPC09m1fcD/UbNiVK07kzxSUJEdgXsJekoYUVB99+zdRhLiAY17a
ioylkZILShpIRTVHsdwtXa6jetbKSlJMpxa2P2bt4I2XSFKdvo6zXrGrOA9TbXaLPaXg0wWxKQgd
RRR6a+OHbnkBuwtnL302exGXf3eqPxhQ1oVv9Xwtv9NCtwoMmPctwLxSHtLEcu0IljcajgyNlJUd
aXEOHhEpAKuWCHQH3T4CGiQsQST1LeZYYhHdwhqcYXEAvGkx5ZhqYnMgGhlXYgVyLl1bdKqVHvKD
bUISUpoDG8gXxVb8EjFg9Nl31fb7GZylDoEhrxJ9Ewl6XBR85o0VVDcZLB/CIFP1KbthMlIoFAjN
SRt+/U5hm80V5cBO/Q3l/w41JMsmqqPuQwGDZYgILU32ksirlAFpJxSDibPv9iC/RZURDeTOj+OV
bwldJEQCAv7T5k/IhhfYz9JxY9PXcCdeDJIuuz6wgfVoZ2KWg9pOgh7/e30UaJqZbuJ9ACb3/gqJ
OKHdVIogLAiFGqxxTFOrhYZ4oNFnbu2PDYGsiQYeD+X0CrU8A7Q0NRiqKGlbTDap3Mu/ZixCnTC7
j4K/Zduru3zeBkpuO8/Nw22rlWhAwcznu6NNalgJNh4CHXI+Kg+iXAqWYELMNCUsERl41eBrqzCG
jDfhOXP9A0txvtsAA9czyZvkdoX+IuFng8ys9fUR2YsmgK+TdHP0e1s9JrCDYcg8oiFINIh3x+qm
wZeuzlVGGriHkqTZZOxWnmKijyKmVDCjQg+VF30qITNugShBk3MS6/SlyMuvPi15bA8+IeupMKSu
Ru7C6ll3y9QH+EUO17TRs4TZXeQfochMIdzrOktVt5wjSuThkOuC3Qr8a+ZC3dT9d4VoDtU6J0ya
YFKaCemVERBFSXOBQ/GGr0NhPSthAUOcZCrpCk8cCZtRJEpE65+ZdExEdhmPL9OaZfKU8yHlSAOq
1BPxyDyyJ9coe1T23m7QPU7XsrinTvyPkf+qvTEHO3NBJnYTVDIuFysPqk3mqXba1RPEM46smJJf
viQZBu+ZKANAStW/pZkxIXTuhxJfWCoIWs9l8RAu2ewYEtTAvDLp31BJf+/h7VpNp8AuCW+kMsKb
4rigxSE2q9qyk23Mg8Wl83OFwpoSm7O9I0qRg4kzpXDLqqufUv1K2RTOm2iHwKZSVRBrXDB7MUJB
QzZylrffjlOPLGteKFK1M0wgE/wTHCsrPLeCXEwqROZgfBqv6PMbuYYU5zfto3wd8xXFRSlNUIsA
MHb6eLTlXZ39Zdv1zvXcmHJIb7hjRwzZtpXyP9AxQXmpNlrLWRUuQx2nb2FoLlEcA7ntZHp/2Wdz
vSbSNsSXcP5K0xuy/VVOh3nVSU/56LY8r2D90bfrX/Aw76bgH38r0n6yaSaNFLE+nUDa4xAS63b+
T/5ezuv+/vce3OuhLv8SZC+ImR16FRDB3rB1IxaPRE6T6vvaYjJcn1eCC/XimOJ4k6iLJ3Tiz3GP
Dx5YoSbEXvBF4uqBrCG4oU3cM9//E211EM+ExZm+L7YmSMXLdyKgY99zu73Dz82P9vQ3h7tqbA0Q
y8c/vPQcwbYSaDNkj3x1J+QADrq6I+YkY8bzBkM4+aFFP70DqqcBBlQVdNibeiak3qbRRBbPk/yP
UkvcXo/1nSKNwNF6L20DhYaekGwHXYVtcS/PvzyHgooEOAdVR34sEeXsA+rTtHHfk4O2iiXf5inv
kr5rAoWNS/fbc07mw/kXGT+lBupWRI8RLVybAW1klzSQIYOlfNOHZD+xZ+StiIrJtQ/aK2Uq7XJ+
2mFEhKg+nzDiRp3/cUnvOOS4LP7YpOXLzh2Z2teV8tXpkf5dQOfODjJqW2XcSoCYbG4awt6njzvi
n+KPbkhigO6sKwxI+0CvOFqA8H1hVPweP96qcaiyD++jzVz/wx+lTrk9Nqu+aI96jlsvgsGwpCr9
5TBmCjkGngVnQ0F716Wifv/gBuS220XiRNJt8u1LnwkW2MXv3ifGFXWrIJWwIvnwOPNK51HfyRrY
ZdGEsaVZjRraPpVevwH96VK7aD0PrGGuczeun8pn/tIySY7r6NRMuoLaHdPS4HB0UpOUSSKg4pHe
mx1VUl3VytL4x9qKQrvh0Do+WcHeekUPesvG7UwMwMshQVHCQTpMGoqVKoyPReVv6v9XPJF4FZJB
xgbukM9c84W0xrps3yJ2ILDlVrgi82lAobgjQSuWWfFxmEiXQdJjKC8TqXGXc6zio6uMrkbjTHah
U4afWrAU/2HJHGwDNKsMxZWBUC9tOvaTrZBNO1VDT/+40UGMbSDCy2WauoLbUdCrFrSjWUra5yBm
tAzVvC3HTFUyj0XXbjrulLwH3wFleA83swz5rwY324NtiIWWqnLSV1+lcapWijQuiNSMY+0ArAK9
ad2k2R93Fz2BHdTHDLWIT4hjJ3dZZKPbK8PUsKBnQ9uqwjx6j4pZmlIgws2SCPXzJoBMXgbBlM3v
stpHM6jtYye0wB4Ow37NIf3ovDxfv1bgMbDTwLkwHQtTgvCEbewPghWhqj412+5JvYYnC8ccL05d
0JE2sH/eRN8gt/81b+A0sFp6VseOsB3DM6SIsoaVbQhGd7QKyf/BN0cQ+wa84RsT4GerksEzpxX8
NBBFY68QS8hNBRLB9tUXOryfc+A5bcZ0Hru17+KBGqdIzEWSuyWJnsj1epa2DsOY5Ek01UiwrXOO
/lNnLVq2cmCPy6ESevAV5svbmS6Vxu7jzjZzUCzt75elJRSYhRoFYTzEwdljm7UJz35YgTnExr33
YIDFgg7lLsCm1y8TQx6VuhvlIv21wsu4ij0nlZn4++3OfK1LmMqJd0PolHFhsdkFFbUJ4tkWZ78x
gXvG2TiyA3NQV2XI1vmOvJ6EJhk+WStcSCrt0/PiL/EdbJNUZLaMv/CaEl5OFLwS7Hj9nLRRpN73
3+JBOrSpEFvLnrY8LY1I5Oj7AH9LSn3xBbke1aEJXgozBrUfmupAX0MWyjqJpscIRPYhyeeh4lbd
2Rcl4OEcIdusUTOtwoStN0QiknkChadARtKYQkI5+1KQH81gTcfaG3d8AE8yinPinvu6l6Zk5jhK
TE33jysWOsCd7TbDMPnYKcVsaJo8VWrMXgPnSzozOTKYjrh7J1e+cJA4jrjhMg2pIhZv8Ffp+HpO
MrCMo4RIYAg+lgfJ0edjh3k5jOK3QDvGjuCifUmWASRncbUS/RGv3zQedha258ZGs2PqrdJSnR8W
2xlymse1KUzJvaiAsTVvu4GA+C3oBB0h9o1KEvm2Mau1fNyCDRvh/xN0/rCOanNneEYtYPf3X48V
tQmAAAkR+BpzvjFB1vo4lLRPYnDEl9kVV146U+yVryjP0lwpRzG7n2DCwhltcHjt6AQCp6x6FnU7
Ds0oLXbt4zVJei4DGAWQRmiv0Ompu9pIvSTdLwnuZjrbXdC2I0ZgvW2V129FckPKlkjmf+zA8RpW
oBATuikUplV6Z09QR8GfoAnTXzGz14XzqC7bJKPsP30btBrnii/yTR3Newyf3ZHLbgZWrFk1nnX3
BT3G7F79n81wkFuqTOXUjkp/RYimQamhl/37XM3JGgqd03gXYexsU+QJxnp757bgQeXKBG5xI/id
Ft/KVJtxCYj5t0ImM2JjEcW5WSHlSdLG67crbBkExBLeNj6xrOXcOEh2GAJ4ZctExB0XQMrii7NB
M0ZyfyqaSN2iLKhWvgZv+kgLXOx+cNqaMWGZtMHsG1d4YOuzgWS8XUTZ9LK9dMqzxJluTAc1s+Qw
tR2MzUbU1Io2yw0wpFhq5O5S8glPsnbhYvUsvq/7/dH0jqBdTyinfz/VbAYO2YPDhk9sWkUD0yN2
+cgb3/ifWEgIBh0UmH0BldznFUzgCTZNUanJ+0I3RFN6jT5tCjee/sT58t7U8akpv67BM27+MCVO
k1/i7I76KVtyaPdITzomuSitEdcbSeyrUAJte8GgebSauOb1WvjsluyBwUhCc/6qQnJc7mmzsNAA
JX2/SWo/Tf47X+cjoxM59oG8wR3g0ry4GfGD0pEgjQMm2HCSvIRF99TGi0IXS8Rc9/8g9MfSCxh9
aVkKCZ+CeWddNArrk813BD92s8KFLzWWDa2X2ORJ7jhk+dEKOQ61y4sTfkWSkc5nAOUFSQ604oeH
UvpV40MguuTiAkxRXPYrg+9/tdT8VhI4GMBJO7M7brDrV71aH+vlI1TK8VnlbSvmUZKWuxPBi9KO
k7AgbK8eJQGL1vQk2bQkdQxLFHMhLqvWTKlJSP8P/HySDD1NtK5XPN47/VrXh6cIjP/IClnv1spF
vNMlhZMVqYtfHQA74y+tsofQWPMAwSnEEi76+5HQlG3Cxo5CXie4R45+LFHwhVE94xs3QDPLuhkW
gjdh6e7CAaS1L4xU4giY6oMZPM6IUtKDu0wtQftgjW2vVvHfH7126WLLZQM1fRtlEo7uI4U+ErWO
8LUWQu2sUl1NaQbWnBsgx7RhVzJZ2+O/KlPJMZsScmJfk9pw3jJx/5cgTe3x2CG6Dp1WmChk+mo2
YpXeybTtmorTGhO1SlaK42J6oLuiOIiyV6agYKLMs0PmS+GU9t/MZ136p1H9PZ2zjAfiifkezY1f
DTuKge8AwVlvv8zWapfydPpTfRF7OD3OmLPfNbQ5CIyhOFz2W5AiMeMcWCNGL3byJhLSelCry5wJ
Dcow1taUj06r+7GUOnzE9yMXybi6EHSHgqmlvQkNvWSMO38a9Tr7utGosB+ZRTONOqOtTE7+XLuT
VybohOWRcOwNmET1pqe73Y76X9zfqmniJ741AQHdDqGHVET+X1HUKLMQ76zWlU4q1ZdMOcUkf57C
y2B18lpZdZ4GGCW2GhcSAsjdCR0dTf/w4OXa0WxbnXX21zxxFghZtsTAnkqgQnkB5tK9GMv/KqLj
SEmAkSrp3u6FYT72aWlmeWU3W54pz53B0IK5XQzheWTfOf1ZwA7z5ZlPabjDaneWgVneelBpVSl1
2uPp6Jlw6ej9EQIl7BVCP6ZkPvcrIM7MQf/d5wra9iKU7Vkyysfg9/skD7OGPXKtSdLURXV9iRb1
DD36Jaz5fd+qXkCBOvCT/uxi0dHN9XziXMNWzf6QFBeX0usrsEJ4QsOEgIOgYfyjoZCW7PKealCP
xjpoNoY+d2HgFSwbXK20j0MZG5YhKsVBs3/MK9lkTPmfFU53lNm5/0v2VyIkVcK7PLke3OdeXYMu
rqGJmooRFSV/AAtZWFD+7nOyYu+XYStnBdqvhSj2RRFadPRQpHJPBHbq7ZnkSmWxzhSTn2DyWeME
mdIuXOcc9PXwYsxHWhyWcSz/J6KSQu8ygsCI1Xo0q7pwbehGNiTrSobMnro6UMWOSH/RI2sWDWZJ
WKH9Q7mek2TQLgV9XhS/CYXsATovxZMVg8S2JM+78WhOUEI6grNMm9zJ/kcFpPNJQ5550qfXtqIQ
WJrB6R+NimjFup0IGQMkUObxAYNa7ymSpH9WH+wtE2B1YU5DKCl9orFg8KhEc7n9VXzF3p+XrkUC
zvDwb+a+L2njpzpwhVrVV6MlohvBEK9IqR/bcnJJjgZ0cQdjKzHEjqxffVM0Bd/WehvRolw2jWUE
0yjzpj2YgN/zSKeg+gigZy5FDMyAXmbhvzzbquQNvlPdPAJ1NVNe4AA8IbLkCz/X3dNbpp4Tv3Og
TPkBPCIMMlK/XKTRcqto7pd5eWqS3GVEO0UgalHFpEpXQ4QM+uTThqeMNrldrNQFysmPaTq7RMpG
huhb+5XGJZqGnyweuQ2GDtk2aWtVAmwpsLc3fr4WmD8ZTZXtHHrCbI7pevjnhcT3SPpAFpyvlFDG
Vge2aC8knkhaJEs/HljePIFbBVgII/KaweWhPBXPg7D+2w21gE3RSNHhcNGgKZ2NbMcPiueC4qD7
vfMiMRvfxkajFqO6MgBIweKWuTCkuqsKHeyAr4D90XYxo4nyikkhrWhDz+7rkeLMjwSCarpo0UnD
dwfi/oKmSz8cVsJIGikWCPCyKvhii5hoscv+nLWYhrX9W3ep4SF3LQJNeTQ8aky4o8vTiAlnioeB
1x7YGl3NWlYuYnzb6JyUIaacX4gGBMnWaB+fQh+Bc/K9g6Nl9bapf12Cc0Q3hA/HhziiiW4OJOw3
bNUrLwmC+16/Ij4aXNboNEPEwWMxCxETpN0wP/59eGnlsUqHsu/6S3locEpNqwcNSf1v2NRXzkZz
4ozRu9yZKjXANRiuyb2OyCBdtQc9tlIzqcWx1fr0siTGWACSda9C4wx4Ck1R9vtMUc8dL30sq/C1
+bwoC0awhQN7EmMuBLQdvUvrbwfqKTvmh6uOpjPRrt38ZtUIoyK+fQfG3VVofbO2B0fwb65pEH4y
innzQSTYXKBar8F1Iq2aMQyDcGCbkvi3ogVbuzjlhlgDoX2+w1T8y0lHOZcQbiHYgs+3lDOsyDhb
wIo2g/C+9hDG7Qffw45I2IDhH5OUDX4yE6r/jIuEVe03X6CXiUH7QSCL2oCWAWqAbELhxihtfWG0
MRo6q7xbk0BVzgn8AAPQ8C7wsE5kMdb3aPxkByFsMpihHfa1UCQloyjeT+1PmCptr/3g/Cmx/8+x
18Zv5Tezx/a1UmJf5rp5qPmYst0BeVzoEwXaMHsXYXqEcMgJESPEnZNw+jmZpMTP4O1wsKCdZ3kt
kMc5BpKW3pgBcDmv0btWKxo4PJXzjyGE41QmyvnDm9tKSf9fLIF9sHXqDkQGwCy/BTMd+GMkvp1z
uoTV4b4W/EO/xfrqKUh2fFPTb+oNEOX6u+PgOhTveQa6UIaddoeZTN8LWHgfkeiHLk16FbersnlA
jYLJui43ilpjYj7i0zdL/bH3dyckxSCBbqi7AX59Lo8Iq0l7BLjwUUwglSWcq4BsmDMSwGc3ae8H
J47iP2+onBxDc6h6t1dFPgbvaKduQGRqIbz1eUnQzk1ZdC22F1zldzLz8ZVIoAl/5R1D+2kxuTn6
u/lEDe/IRAxw7C3SNeRwCLHjrlWaVNjgzQjhqlao7kXmMCFJNsZWotaAMuoC1ajSd5S+G2U3jjVV
cAsKX0vKJ8b3NPrGLuYIYjY/eDeNERkzWBSSyxtAnQv8/HdOEyqIZyJYl6ABM2knASQwL+xVskS3
2i4UaQvwthzA/pcxl2V0ws1ptSwYOeuu72D2/GcqtMznNq3KuZ+2MQqFwQgg+SXMnuQKozS3eKAI
AK+WfStkiZ722zX9+qyoz1NvlQwuytI0TvZhdza1XosHqnNACnDI6p1SINhOu7SMq2LdKR121Uqw
3y/8l3WfVjF24BYlRbIeWJUdnbz9OdFkQHO9mmmsHaR9adcw+YxoyLjANKlZzye++CyBvEvlMtYX
5Y+3O9PvZkjke8suURWeXv0LlAG5R+3DyigVePCNPpxFho2iqJEkpKrtybAPDpD7jdLGykxR+K1H
NSWsxehGy6eShD8U/epCnhcDWkBvHhTJh/6XGDYe7pOlzU2DSOqYljCqWuLH/TywfUNPO2AwquSm
Tr2oDi/dmNZESdK5Jt727T/RxxiWB9xvAnGmGRVnwTHiy8UkPS21imEF0aWz5mDDX0JYuHh06WHY
CaQskQ4Vknn+HKrDws0FKyfhT5fHXDQkVk9d9nJtlSenfzRixaU9ODhaLNCXMbBnzTkQhgZsqlMa
LngxTTwrpVlNfc6nx59NXhLVdHjeEMj0v9JaZQc9aOCz0WGyXY2UZnZjMvyD2VCIxvnQWbegqNgV
8fjW03aRlq6iuNosBf1lcNbK9UPeCciVvAkXJAX0sqhVu2LRRhzHWClbOrd7MJcut5O6BRpStSqn
7F+rN0cYl88MD+2MewZ5AwnbtFlwXuGrWStDbP/Ha4dW5RMQcBBz/+CixZz+5GhZt2jDhxrGcVRE
Vl/QezscSbETdzvm2LOAyY60/p6wCm8jjbqL3zCEpoK2+/DFpkLHok3AKdT2PziRno/IqfTHWTbP
qd9/aKWf9rUkQD/jR3SOCrYW89FCtl35P7RFs3CQ5kbOzdlaCO5sWRKOFylxxiCWMTdBOBEclVaU
SxBwJ99PBvqBcIElsbuCGDteTFfy3FFqLTn5YbeI28fJB3dApjtcFrktZ+bTfSxpJbIMh6kIJzOm
fNdyTUaoG48t0WqV4ccScFUv0coUXx/D5r48bqUgStJaUMB6r3bYnwUqAqluWliRLvmdeQ81tSbG
+MMnV8WvN1SPBql/Uj6OmF7FX0tQV8Uw0z2fA/91JdajykhEY/R9dfqLqGxjruWSHqoleF28y9UH
p1Q/3g8qL2RlZcLOqzYXfmBXLGDHzRCG9GGvGXt7iZMc/bjAw2YnbY78X+xhhy8k4pTa6MOq4sgU
3acDSFo7FvHpJaX/fQBvGUndngFNzsBo0USHJildHaPT3DkIt6lbDxC44fMihld8sNSqsFelo6m8
30FbNIa/Yvhr8eVxyHgpseNWl3prKXPMKd4JqhnM7JpJODnQslqfT2aHpxs7Xw2aoST/4V9JU/ay
D0fTkyN8QHKYwymrPBlL8hC1DefvXsP2J2u4Tl5yXMGgroPoi34aLcPEUPk1VctSG0Xoa405eZTV
7WbbEYKjK2ohMkqWdUrc4NYrflSeKI0yGU7+Wu8ACLM6JWkbjSmZ6Qlro+OsT+eldCn4SbF84O7O
h/xPQTu+2kLk6Jwr9JBFxjJ6ylDxdbQjTU9rLd/CEUz58//fswSeK5YcA/rENXfzEYTlc7BAQUQS
daTyG5yBbSTMe5VJLZ7kPZFWDwUDW3xIWd9+UvSqzGSc5YhgKHfC777Revbo5+97up1TbKAsv3O5
s6TFMcpbYQeT3newvqWROs2hzc9cvIfjRMljYIIyUlOiHAYsLlaxbEcpPKM9TqJ8ayPfH3Vfxhhj
4Vi7ZxtGBKiHmGOTaRTb5QtAnuZaaE9jzmUvVxehTXh+VwyYh+zcFVy9DxATm8LuYZdRDYVb6tXL
zS8OAvdPTR5yB8ZsZNsZWnq0h6QWFZ/4iOrD/gpQWhdXsqASTsapWJ6JlhxOKrm+p3CxWP3MJ5Xw
DnOyXb8ZfqtWVmCMVsA5cRFJogZUGX2L+ll7OCJTL0Zlf7a5ZDeRD2Ma1tFhgxS3DiZTUdMr0K1F
L3NiUMPGJ/pnVsye2DP9SkPNnSUSVf0HGjDCg/6C/QHxOQbDZqBQ9cCq+CPVfXH+/yiq8FrXwAxi
+9DJMQP3KD6DWOiSKN+5qesOKVGX8x9vYX3bICDphBZ4FZs/B/6YePxfFjokow5JOzTjDIKL0R53
3vVVlToo1EXnBYJOLCfmfWEGe62CM0hl0KXeu0T0s3Kml0sBO3PEDCBQM0T3QOFdg8hWgv7EiIEk
Zx2HBRK8F9gzco1kbGakh9oBoCzG3wbzncY2QQUHjBp6uM21hVysF1FgkiMGoJKeu+quMl3sMI9F
f0Qyc1ZUYyhCON/asVoYhGQ3ox3qwllLyyaO+KMHwRN2zTfteEH3GABSapV6VqKdOkAt60dWJxQm
jrAKCL8gvlQmpKDKDJ4DRCT5EiEP/0mxXZQQLQkO/RNbjzxlS+Hi7hSfCVZHCVPbPIftuEq/f1wM
sazyFxnFcMFmVlyDeXCbIS/X+j+U3MgO9ycS6xH7iyGdco800cDeaaRM7+StZYzeNq1AUZQuTaBN
cDgtRbxSsWh7Bq20LPWiYwkYbWI2Zyk0Pf1kUoban/z0lq+rc1an0pMiKsfaSFplPu9N51C3+nwx
5jeFTQgVAQGux0jUzIrB0IhoQqpBGeLEA6MTI3rPPZi2iu9A3U0PLDRjpRa5qR3+N1nTKv0h6H7H
j1nfkQarIRvOIw7i3Vd4Qt+cgb2UGCGufL9oABO6NT01B6SsoxyfODeT6tL5j7ef/l91M3do5dTa
5Q0NBFxRxLxPbMqIIW0C9g5Su2AEm8Cu9ZlHrhmLy7qxw9gYVSCTOI1/Z80VqgZ9vtpiyRB14l2v
YKcAO++LPhmTUfAV3U2K4ZCIGQs2wVRseby6El2+SlSkusz1JdRbZEqO/mUsstlYnmYn88TQWjcX
h9SuLjmVQ3wyZzHWCrXdRvrDjTpkQX6mhsq38+C1X5DeN64kOilJKXtTaTJhh4UOQNldiPZjITrP
4ERu0n5G+Va4VkAi9zgV11mba/9eHUYU+plIpdIyIQyKXSyrTSLGXP20I4neSXeddCSj2IqS803j
Nv097SMobkLCJWH7dUvlIbbB99YOdpiK7XXL8aEcRvWI8L1JpIMvLjZSF8hccojopIvBJnnB4OMs
NijJp5uA4v+bwJtSP/D8YTWNhFoN7CZ7khdISgeuIxL5SPgoBquTndoeEMXTeEsagcWI0SLAIMMx
0xIjbkMsLkT03Yti131nk5uFSp8AQ0lTUSmN7r3oJSF0O5sFbWSmhGYC0REZW8RutBRO2OrEXpMa
ygyhn55SRyn8C/K7BDCDZm0x+c0mKbSQBWjERd5QyjiwhadypvC96O2xVxXFmb2dIG0XQ0UBjCHp
vxPrgVOKY/fhchY6l1f+/4AqkaEjrqKjLv1eqVh11FKS4iXfQCSwJhGkT6NQC6ql4JJZKTlXheSc
XQQsiCOQyBCnsPVRGOavA9ibk82s8jZvz5bcWer8tmz87eRHwbD4KLijPhr7eCGvR5tVNvao2Z5J
QWOzDyRM/Th/VmKuI1J9l1UyJ+sCE34OOE60Gja9KES1kDizWX6jD5uykGqaPt0NdFkTJjTH1rO4
MksOCB1KSaS7CH/s/j8mj5yENB/WwtB+QeHnNxuxBhtnDx+QjnUyCB7IvrvNwhYuMOIwJ6UTyYng
axXwakOiLrGjub1sGDc2ZAxbKfQrQ+TvN3d6UkUODINdtOF7w1WCIhWFMPeIb0RKiQUQv08bcaZP
ByVh/TnuBHzoyPX0Ogkl6TlgNmDMhYQvArY5G365w3ycTnOnjQW+/0MIVZMRmyqyF3lRnpkT7PdN
vELShpQRix7hNGgy7Yi5eV96KnmTBr+2kTCVdFMkp5HVGjOsQd0yweG/ltKXcawt7qeM8vJBTkiV
2q4WD6mlWxTajpwA3ngXVSNFxuS3/L0PrdhXwWL5lNtejVGGZQXxzgbKN4gqK5aix1C4gMj+ISb9
VcMkKVYg7f/EE9NOXCuJ6poOnkwYczLwhuaLGBHLrhKr+SDQHCNGBDNgGEGw6k9IIauNBNSZejHj
YV7Qpudav2AtrGuK3RlSMBRirEty8BK0tQ4OQzxwE1JNUyZQWzIlvyguHdW4ebFyisRVIPnlfPIG
ADBe0r3Gcur+s2vOXmMV5ruW0w10bwcpx4dhbDrl6Bcx5rqFDRjdrhcJHfhDJb40Z8p044dH5t0M
g6WKm+5mUTiWjdNR/J3hI17TS/7SUIBaKmXttJzOsgk7MCbNSw3DuHJCedI2BCe9Ci4TiUPvyme+
VCkCAojA+9SXP5sdOAsrbdI3PGrgswtoxluN1SBIhQUFx0kT+TG3gzSoMDhou3p6MX+oJyoXocnJ
U1kUJZanzjn9j+r7sifsVqZz/rAiJS5kAJEHN+g0erYRPq/W2SdtgLLqijM+pr4t1OvJ2a/q+DdW
aaR24jDnT6wyuyuF5LlTEqstIGxpNWcxT/4DD7++4Fjihq7N4XUPojTCyNbT4lZ4naDG5kw1grnu
UOxUAL9QE7ylDOoodn23iSKjJDc+qLM0pZ1ro98NQmhOcBui5KgNlEhu9UaA+MC+eyoT0TAxoY6T
Kg/xd/ww62mxSbL/C7ZLk2DqHB7AMHs3p72Nv/1k8RRLxZDQbEBtYd0EfQvRr5WH9wrfKBT0uy+Q
7g94TWARw5KzuOazN52KnJtFhAN2GigaNj9ALGj2+VXq2W6YuJYfd2odFqSKcWUEVojhENgRLsG2
VHNAVh+A+IImJ4VHKxxqluZ42v5TgxNlvG00784Y81Gu9LcRAoVykNdrgSkrhLX8EagBWAeThL4V
Qz2j19BsYHSOy4voPuh8TSuyQWsqBasTchu6YYRUbTQhdi+R3mHMwgsaDqb3Xwj+O5mnNgfwtaG9
cbxnxNF/U9MGYGQfy2YEM84epTKDkt86Xz/p3A7Abmg0TjKE0jkAiDZfB7KKenxRqy6osA9aWkzS
+OGw6zH5Did6LKqF7Es1OXoGonxrhZg6UZnPHC76sRji2uk0jpHNN9HWGPA0m5yXzFakoYqYkolA
vVt4PW5l/OsMbej/HYt810WTwnlXxCUX1V9MNHdEln+31A3HU0DkLc9+o68ihC/0lNx7HMLb0O4t
5P08+h96MZaTJ1lyohehNpdqG6/9WdqDDRB5SW7sXFkdWpVh3Ddr0btk1zQedlAXX+eiydHqazze
3ovoclTGRatekNC6k6DOrEPEB6ZFSh3Xzh6CDF88XnIpf6ImuCmSSAvQDz7vqNBt1crOhIqWvvqr
Ynoh6FRwdsAbEnh28iFkm9kO25Rls8CU8J+aWq4IGXZ+UGikMh8E5rVZiqRDePFY7MtaUbQvKb86
aKIaX20z+56Vocee4tYUyJBhoz6kaqqn/INygsGBA1Mt0ipmAME4QsT1LAaAPOnlY2rAI87sHcej
xDLfJW742rJMT7pBcAqGFMm1jNnwavUA+3TVYtFXpla1pVq4Vlizqy/bVizt7AWAUbilaxvM+1vZ
0tr7tSFXDf97I21UXjs5OEvPO21ek/OJUZ6mwU9DyD1kqbexYIXW5DvyltNNVwqFybNOckJQF7Yw
qT/iUHGZLjZgLFCgCLMUJ4Cfc1hUugMbEkIa8cWz8TnrGST0KzGCmaqhbjvdY8+Ok3N+0ZExBnIo
zTRl/acbMcKXnQL+z4Qg8qOlAm0gZze6gcNyp/MUgL2JtZEKbC91fB9/YyB3KP0iq9s3S26GPUkf
aj4PQjUgWYBiqY+2rNOMFkiW29eEYmugvUVh3zqpMsCc3hPYsJZNwxCwG9RKEKWAbneE3gP2/af0
r0hNvVNs/xhWJujgPovzxqEaHOsqypk8bPhq/I8sOLUhNbX4gwzID9VNujHOJJpoGvqHojFqpbIH
gJptwcceKyBadxsqGbhYxsolHyMrC+/hu0mZ+VFUkhaqTeRXw0JvWhnkt3Yds1mjC8P9ao88cIwD
fE/PB1DWcdv8Qn7Mljp/cKVUHCX2BnSeeDgBLOeV85qEavLYrP8iZHqufz1YDyzTtwpLRpnI9Bp6
D5SnA20dBmeDVoYyvzqTgme5ob4UFY7+pIgVSjtje1MI8RpUnEJiDy52aWT/cdXs5r6UeR3Lxd8G
2w83M9cIhWUsMSJ31YwcC/10sDlFANlFW6nUr5lAlinCKLrZusB2gVf8Q5NdTDyk/wZRUkZ+1Hgz
Tg1BN5SEke+fsXMtrKMP17PoJbY2YNUR3tqpGcfnihVuakoABskwv3lweJKogUtL59LOAUs71Mt+
UOplwUCyVLHEYk0Qe0sLJBZw7dgSW3+HvAlr3dMKq5bnBLY6V+CBzDxGoftUjpveurX6sGaqugW/
MHLPyqhtvCKA5GvHJo5Jo5vp+YKMQ9+VClZmLtiuYdPCHlwN38B4lsP3yiib1jDXW7roC2z2Bc10
5buso6o3DvtsuLIGLPc/Ru30AQ0RYd5osm5pC2zF4qOz5Hg+EAOB+pjVaH6GtQOOiShIZaCWMa80
IG730o+2oV3Zb3c9eC5qxcKT5dhsDF3/TUVw9H9Iudk6ETrQJbvqpcYb+FfJ5BEPULRUniduQKFi
aD6sIR0YMqoAT/iUgsOQnlWurXnr1KVpN9YuH/qkCI676zYBNiYilw1x8F55jvbZPnAixpzOIowd
Ne64gYGHZsHwS01qMoZJ+T0zyZ/wq6+2diC0ZB7+l+2UJdTEYd2WkxKk/kTrjSZCyOiMCS2bhUi1
unSutz3GbuAzHxUvlZuLLLkj/O/azbslI1mzS6hxEuBEUMyadntc1vwtTGtliD1Kq6rq+C6reXTI
/wZD2CyDnTu7dSbZe4NT9oKRY6adAf233i3EOdyMRPVQMi3tzgm9Suadh0IXruEq2aZqFy+DDjux
v+QLZ3vcBXD+Wj2mxiMCC2rZ5X0VOmKcsIcGpFpZicGEpPTLBI7MKz4a1z9lCh7ACdu+fP35jtPR
fKZc/Tf2EhYsjFD7VmbEvrbRFicbnEGPK61wRaDKyiDyM2oOIkQjgSbxBjYOqATTaAw2aO+CreBe
LoYlB58n8t2qOOhTV25LHN0QgrYeAPEh/q8dMTcYzX4yyua3uRR1N/hczaocoZfs0pCjZ4Ft+SmJ
Jw0Niay3Nj0ku2rTpx+G3XHYRHnbHoZlygWjf9S/ldkvgms8SqKfYuG8dtjxW0RlHvFVFRUzznYZ
fS/yfdzCzZpRYd4YqN8/rR7teoLnEYLRDj/lAyV9rqBOXtF0GiQ6m6JojCGFkrOeHPLEUECvx/F5
kjB3GHnxxEgTQKfR8ia7LTFj5OsLgOPQWtTYvYFwhv8c+gas+1EZ+VXVgJd8N0tj4A+676lbF4el
7hsJIlskUCBQH3tAyTxZJBdRcEPWGIoQy40RH1CB8tbFSfeU2A+DV5nz7+MAU5u41K6BC/iqHkpu
zcSfvsaKnzPPezL5tGriZc60JNe8tY2oGx5I7M6ksAIr+9yh2zxbXF6gy3iElJlmrCb2yGMuZnvw
+FBVfCGB2LGZNYpLXypbMemgBatXOz2RDZzydw4MlITcPkpoaCgLbqosJdso8NNmBqhw3OIC0Nzq
fNapTWnGLj2dDU61Yy0JuAv9dojyv6QmflEIYGWyRBSXkpp2fSLZGoTTxoMonqlB+szH/nco0pdn
FMhGjczhyHHTsYzhMBNuMGR/oneLJFXEO8TuyVu6Tbq1JEb5MnV4J9DH82WEAJlm0QZqvhECjMqE
aavMq62PWmwBkrCDCDz/ccnEa7yPrGuhBtotjz2fwrTyWMz63/ygwfdR3VsljrDkCJoowMRvC7Tc
eMyjbglKwMIqrctYSjQQ6ulzazeXLec7JTxUGFlmcfTEXo1++BOFUeDlkQWlXd1sarIJDyRMa1wg
7SKeYgE8XpDS4+K2Ws+6lfoyeNnuhp3p/af7508Is26TRWjC46bxPImaK2/k5PjDo5SN/xmixyMI
boHOT3xrR6us5uXngMaxrUS6cn8QfYchI7+PCZ8Q0CcA4c/AaBVgI2YrL9MVZYIcnYyTvh+oM0xJ
KyQYtfytkiM8xI4fVOg+Z6Vq6GcjnXFzqPMLKukJeVUMhcz+nfy1Vr7PrvGkSNaAA9K8lgj6yVpO
snbSLgjvuLcAlLKJLiyQcKfisQakMD4J/YseqGq+MXTkSoGand/i4JiZwNXpBbydzon5lJIXjkIL
hgSBPWNGkpLuyZ7CppHRCkhe0S1ztAl9GhwpwSKxWbdsPYtuLmK0JUqc912cCxzZmYRTaa6v5vyb
jMXSyJb1JFSyBERZfLCKozpNuwblnw8WwZnF38PJAGLS2qBmPmnjAqYyzCcZuffiMdLg3UCNk+1j
ms8MpHhs0KPWyvVYIPRXjTA0uuCwCxWnjhLC7Y4ENoeeamr0GJN1fxfQW8E/IjYcvcLkbp871TpT
Hb8hZkCXIVnacw0mjNO91SQ7EHFq0o9Chmt6E52+JEpjqKyhT4iPVj2POCQ6Ye3IQF7Ljv5fvBye
0QMbP9uLBTvs7Hs/cXHXegPDjtvT3KBpFM01UKgXp16vPq+PXkBjv23PHDCYqIVjitdPX051ioLX
oQ/wCQXK5WuwhXoeAxtd25p9PXuQ3VaBkQbma1Kxa4Ptrk0xIMwLoHosS5a1AN6kX33hXviaqGjK
oocfDXPmiCPiEqzsohz+yI6RqlIDvkfZivua+6+uOzcMej1CTwQAN90mz5PBN2uMOIWe0+R9krlX
DDhOWERkJq2xPk7NpsKuVNfYtdTZCB62wK5APP+GJiFM4SMmm82SY7AVLWB/LrJjhu5lyD1zxIVO
7vodZ9wib8hWJWvuZfvay1QhMgrz8mFDWxg4fIccTXIc0u3r0DPGzmpOn6aMJl9Esd9kWG9NLAmQ
NeVzc0eg6DREdkDSUl3sInwxV0j855bHbYYeZM8Pcb4bWsDoNA6sFxIYwgeM7LJaDqHVKzdmft1n
9z0eci3sUwqmzoIQwhKHRiiAsUzIEYAGX4yXZqj6N3A9RJcpceh4
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
