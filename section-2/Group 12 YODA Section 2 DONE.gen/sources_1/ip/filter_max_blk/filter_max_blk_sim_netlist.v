// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Wed May 18 22:11:26 2022
// Host        : Xronos running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {d:/Documents/EEE4120F/YODA/Group 12 YODA Project Mean Working/Group
//               12 YODA Project Mean Working.gen/sources_1/ip/filter_max_blk/filter_max_blk_sim_netlist.v}
// Design      : filter_max_blk
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "filter_max_blk,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module filter_max_blk
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [5:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [5:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [5:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [5:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "6" *) 
  (* C_ADDRB_WIDTH = "6" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.53845 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "filter_max_blk.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "64" *) 
  (* C_READ_DEPTH_B = "64" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "64" *) 
  (* C_WRITE_DEPTH_B = "64" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  filter_max_blk_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[5:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[5:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 19328)
`pragma protect data_block
rXW2cNca1FtLb3+HnBxxUujOdyZRlCqyhbf/FmsfR/ze3ppm34zp3916JNxyj4Md521tlCqxDCD8
+cM38wk7JqTFfFZ3IybDrZER5Ps19fov9CZ+Id9yMUPuHzVg1xLkFOnt4iVuXRf1EMiU04R0drr0
FyWZeHOtnxTz855qBJPNL2z6LsaSacrBqo5UQpoyXpL0m6D6IciGtDNPPtYikN/kmvIrA2Y7u3t6
4j2uhWcMDz0hN8gv5x3b8DOd0DGVQNPyacYyfd4twiBHUIEkiUuyoeZpdApv8LuRwvjCtZ1Q5MsC
gVJrmQ0JVU+CtAZigBr6k5x5mBJOwZkq5WvtjRtiAcTdz3Lv4+vmOX83SrV42F+lk+e+XvKCtB0j
H8LZAEc9/9SIHc/1gQwWxcMrwXpGHckOfOES/E6iEIbgq2J3N16GZ9YCtS5kdlaWh6V7okfAiqXp
RPRx5lraKYX/3pMmO7f3G0Z/DtdR1+QwgYsQ2vaH0ED3hvs8W1x4KwKt9y46ptkQV4oEinugCJy6
1pxFcZuS9H0EXwoTzCxM4VGquI2yWnmrY2tAQbGFgZgE7GQ1mSdLljD+lH3gIC4cULSaUsbO8v8u
azR0i1o+JmeG28Lu1eMLZO2FQKao5s/xX6zPpVTkaQ2fIrstmqIn65+BXkBqVx5DS2BUri0n+Mi2
pZr5rE3itFuRhKfW+r32gSaZKdZ4ECAjJ8Pn5EVKfc3twME+8rGKFaXiFCZZkfxNY5MMmvlSwKZ6
S3L3a1tRQLfGmKsHSL6L0ixhUHPANMdlp4xWzwBglfCDsgvedBonoFLdkpf+xWLe69Ffj42Kr5Ez
N/WBrlw2tWCcsQ67dBqV+7kJ7fvGb20bsZSv1wpzVkDgSe9oA3fhuFiEul7TNhj8qraVOD3oB6gs
QH9L/r9sOqMeBGTZrOfNv5Re1Ah0IK6TKey9aIbHMi6lIRyn977ZCWrbxIkHcglc4L1PGTNHbhS+
sP+N38zGkKDSJ3jErwn39TQGACM9jPA4atuq0YzHKkd351mrm5mCdvk54y+85Nl+il+fNfGMizFI
cOsLA2GlOjBnollM+gQKM99xn9eEtK3jipv+Useci3z2eInvlStCpy0YVjS2CgW/SAZBOQnbCSxQ
R/i+71uFqHVTJlfxe+pNaBBjkxWYiw3q7bWFLFOkknQnHXIAEsHXdpYfh7dZ8y/QjjHcbTW6x+Iv
NBkU3BELmbLknr7jgVEsjMBA3DTdGYPnvwDmydvr5bm+yilutre2GDfwv1TfTuFkpe9EoldSzReR
YFpgQOjT29/vWVa+rRit0T8MXkrdcz9xLiMMdkHP19XKL/gMsUhUqHHSbjEHlv8w9p7yIySHMSU1
CLq/c6ZtnV2vaCmPAiymY5J7iRb4myJpMolCdoyI1bp3+8LlKtaysF9RnAUyLjysYvpmPsIzBoRZ
W9EHL5ETIUwdqYQp4lKpDESDaZdxWjUf4/nLvpsjJ43njrnK4dt+4+UVPsTuawe70m2gj+VYfEYj
EBBXMIVdY9TK/OG+GBXz6AAiJD6PqhBroP6bCzoSZWRWFDPcThFahruKtT+90Jx1GokUsSlYw5Cv
d8975NE2qHx1VKx5/AybzsfdPNNmLrCG7sQ4oDngNKeEeZN1J20fdpFCnj82ruUw3YrghDAz3Pfk
Ns6XlUSgtmISQXNJzaIqp+F+oANr69FNI5XKXjLuxpyWCP8vVHBrkI2YdWD0SZg1wbtookncRhq1
i/WpXnHyrI4b6ae2RT3Yd/ZP57Trk34hvfu4IZJ8rndEh8+/76M1f3FFQnj5xTW88o90gyJQoTtK
caN2eOVlkKRXW0J9d28W4JOofZun6OPfq8PzDTDrfyKqtHmQddMjyvbmKcrbof7Sq8BY584WsZv/
tpMzaIwFuh5n7QkbSjW3VvPqo+WDJ1zt9Hkp2UgVywMZ5tSu7/MQxt6qnpi8wUE2JxO7rJGak95X
aqh2Pjnk2y3dzPB5q5oVaXbalYgChdTVjLmcL3ovogVdW6tY1AIFURit0dqkxvf1nMyZZKtMIWno
wgk7hWuThEblckjDxbKo5sS3xZFmZH0wQa9ok1D8dVkPxavfRRbcCJWSgOTqUUivh1wLu8duzEWU
9yD20E4S1ypVSG7QNb6d76/u9R+TvgNWgmsXM296LIpwrLrnccQY5Xorz0zru3yLSB/cPOPr9NS6
RMY4dk2RdV0Q/i49537eATcX+3A8P65aWasm7JK7c+13IeH3tIg+Yjgp+JyA0K1cSZf8LHdXI1wJ
ACQ6z7htnX53ZTRZd3ksDnLGfJqnfZdWyA97XBW+6oTL651IE7/XiYaV6ezJqKHMXwa7u8NrKHr5
bDdgdWJ0S9ELETfJtt+OwkQzu/vwwonCFuiHFQ4hYOh1h1bFBZpxka8H4lrwfEoifq3qEBMVbRgh
9CY+LjwTUr5cZzamm189p2U1is2QAxjx1rWm3ng1yO0wZ8XEltVMQdwr9CUWQpW3DM0pU5vs2daT
eKtgUxzF5YL5FuETMIz1QN39ij0Ef1oKu70GRtCluD9Ht85GvrGNyOIoqj6b7xGMPkf327a0sQyJ
jDQmgO6ZjcTXNEWPH5W67uGPWsm3qNxiUD/8jpOCWRmQTymaah+oxTCPZOtqtqj+iUoN14stwVNQ
+sJMqflnT1IwmBKdh8RarAzVdNtXamBjVEgLFuYS6SpVhbE3LmtqrViZdv6kf54uTMGxfNg188yl
I6KQERmiz0OuDeIPHk5PU7QRr+GeAUF73UI5xj+mY5TPSOM6OyJAw939PBELSdFYt5FGhal6DtL/
8+MvO+vV3GBgPH0FM46B2N6I5JhFLXHzpsZ/e3RtyI4vqyWUbBHJWt5KdLW2c/pR7Nts2dkPdx8A
Ovlo0e79e++iD8fdBjk54gawlh9TxScIBt8H+hKRBaxQbv2TGe8R+CHsJKBDwZWoIGdqZxXLJXBl
ti7hhVIneixsHd5sLSruLkfNpst0NO88ky3MgSd6cbLpvSwzn/WM8WjN712CioZ8YYay/tYJuFHp
L1lHAJtPjfG6xkLTkBABM6psZGu3GXhzW5THOTFL28xSUY0FS/RoyVuqLPhNgh7LuDF5fRa7CGX2
xhmhtR30R13WIlw5y31Ko0qz9Ub2wAqWX9LKSzdIeGdoA8Wqx4kwQp7D+eTGgC4nXJuZY0ZGAEar
VEjqbj4rwhMnSbh9Pu0Q6NzsY8N+pFrjRvwfCVpPpN3/iGZECoAn4Hl5WZIEuMEjQ22oX49aqHvo
3TI5z1dVo5ojtj9BOxL80zmNH0m2RqtQYKgtJ1APCfVND/B0lE+wGiIxBZFnBYA626hdvWFjHyez
LdM5LNDZd5OSXyRdbsgJnmX7AaKLZaXvOjfFSnWbvFbJf+eFa9CVnXYOjLsZi1vqY7mUnKN8FTaY
WfRmkRpTXTrY37S9alrCsS21NcPqsuy99/27M5Uyaty1DEmfa1MM0gwcbUKtxBhy9OFd4HnTeTqy
3GWTuv/ivcBJ5EbEmgNJpNY0w7fbBlpMKW1d5IBGXQhaY181ytHI6h/XmrzFEiAhTqH19FzhElAO
vrAlmEWUz0i00pP2zPwqDYuSkwWwPqq/Cz7nvNwZlyiPT/lxJ4q1c1apXtKrKROuUyq41kqYYLPL
VJl5h5wzATWyRNH8SQJxZEgFtY9wpsEB/wgoYSxrjwGpqEfhArPgQaXD91I5ZIR5i53eOCRDPql3
lTNpODicZD5OthXG0VamWvmOy+BQ7kYvkQkHwtECKkpIlAhjCMUj8GZv/2kkA0+FBI/3VfCDbh2R
HZme5kK1u1gTQ+ediWjdU8hODyspd0Yh1C7yPMhk9ecla43pP+y6xKvuwV/J5VbmTN5RSnIXqKTT
/LfjviPSxjn/fxDX6GHqkJ470hKzRR4qNIZNQOon4VQ36rUIM1XQBfXNI0B95zAN4smN6XmKU6Fs
XLS0WZKcZ+eNE1LtDc+Fql5XyEmnxRSeiCqsTaHCuEDjDFc9txTL7PeFlgueLU/XIL7evqfKdMOB
jldjrqyXRVkur1Sg0sNCKydX2FoyyMfrjpPa+gV2wsOmr4JLbA/2x5Tji9KcyYiWWI7teBI6WA2R
nc6NdEC9OLtRQaJGzXttqJB7r2Hg1AlDmohIIxZd3LtLFixwjJFcizaVXcOcWSjELMRjWzoCjI73
naVrTbP259/Xs85emcXISZA7QRW5aHWLLgLpcZTlycW34aCHjEbmyDhR3/0Mcfp3SpGTjsq3c6Nt
kfrbZ8gKfQpRjl1Aa5quCLFInqX+BF5gfANhaZ/K20brNtwJQZe2r616q2PPIfgAjZXwX9Ov7aHF
KtU5eTs/Mt6zfKuXF1Qq2p+Fwn9AoTXKJBw4wR1Yqsr0G39U1dOYORiqE0TBFMIdHK8cdcQtfmbk
I5Mq3QoPICDaVgxe8wMannbz0HKP6u45z2S+0Q/wdsUFEKTsqPQb9K7+O65NKuukZJH54ldwt0H8
ea833TjLUiA6atuf1TGk/5qKJ1Fs0L/9AzlnX4q+iHfhYzLwRSz03wQHWes7b933ZN8BxXeImA9F
0CjEoexVD4WoIkFkFhJWSSKn8nkhN5ow9tSY88lpo00mfEFsDGQOUdIfDibXkJSLOlis3tpfWqmO
BMUgxj0ERQeq70ixeteagi2hpSi0XqN+FMRb/h95hW9pc9vFVa7cTTR8nZAjWWn86Nn+c2jobg6t
dp6wLz2bTfErpwtSskXp21TT6AKQQ5Glcy7QDigZ/gB6MuoRRSNtYd5hxbRb8uMMQ2VZE8bReC7U
Uu41v0eYQDeO//mb0V4SeKc6+N8PttBqFVr4kF6sp4jwHaWMMvBcT6A5MHTCTwZIRlSRzZ4kE0JV
34wK/kgwhLzcMjoLvCUsyVAGMmR41J8R+NE/uWkaSKWEgAuYwJu4RduYxhFqLNJRreJOtH67nMXA
FO0jRWVOMr9f1+u5gCSxWvkJgEVZlsL9KzozfcjyzydFgcOWqJs3/x3ik0oRRRbJJEWxeD1iEcES
ohqOT463tv0kIF6d/Yuq6+ycJEP1P2TTS9VgjmZICggyQjpxWkeCvBm+b/igR0RHj+3Px1BFUr6z
z1M/WsurPBOSRWsYoE1g9zaSzKpETpBuGbVzJbYFC3dlJNeC1HaWtYZQ8yxDkMBmr3u1514CaER/
kYXeWlPNVS5wrK0pUJRGzUW+To/wT4wUKTWyU2jgSKc8jOHOU5yaDWl42xnpqcjhkN++gK5PZOks
BxKpRRTIqaNMSJoPOk1CAW9XxrZOj6EG/yy8BdXl87S244aWSdJbn1Vm7k7HAgd+s4Af2nFmprkW
EY1Q+ZR37ATksWOJUbCH1vgs7hX9BZvDvKNSZD+wpuqEEjK5wVMe0Zq2PGkx+whT0tN8XOBx1tMW
wrplBvyA1rWdt4YAkxMhV31TbW/tGat+kZ5qJDLvxdGQu3u4lf5vQW+/KEgaNry8cDCLq3YUa561
9H+KaiTrduKSnwMW86aIi03BIakyc7J1AzymQlgvSSbTl2a8kmf2faWJxhbItDKbF4nfCF5DoMZ5
gV1xFYXDi/wEltO63ctF3oYSNAZIbMDSFrFs2WHGYeYijiH4ByVv4Rbbh59fFrXjVMIZv77yCuR2
ig54RN2j7vYowsymqJyKkxc0cV8s/t0pm/XJFShsU4sys5qvKFEPWfmXXlF4tm5gmjv8mQ/EcR+s
R/Trrb+2pAmh0lKo6UV0qr79KVDVwYu0Qq8q+d5knl7YeSFvlmb85Taso511a3Yi2DQ1PvIYLIqd
AffrHy/c5FzbJ6WYblXzfA56+uFL0763nY+eWwHa4R9fj0+F4jajzaBL56OghtGzZx87pMSH6TKo
G5EqE5pnrILZVUIM59lzcCoBAV5FTwZraDUaZsUFDrYAj1IPMvUoHHYQDciNftQcafaGEUYv6Pxd
pXPgwOgEPLsDnyqdMh3k8axMB9bvZCzre7Q5I6mjhB3oYC41wWsxEJMVHNIy65lFqpJaHfuHZEio
0Cn1wsI8JdT2xKLf42/aOV37WxfTa1DYThZWHqCh7qFPzRR3WPFVbfNuyFXONAGbk2Nk/7nlLnGd
uzctMAujkfHspi9CkXjlJx+l/FldYcJcCcZJfiwyRE0emzYDieiM56+rdPdSBPjY+wAHJGjA6kOm
QAJYU45okbtguJyALR5mHE0ThnHbpsBKQJ5d82eFyqn1h8xlpsOJEyk8Z7lYTXOi2D81IT9eb+sQ
vZwHcSvJ71iMLVyE5oU7oy3dQ5f9d/B74ax1IkLcmMjoCdmOG6BsAEGcuImeAPk9edH1LUEPTZkt
KlbuMgxfMO0oBNlurh6LuGG+mFF3wHpfTCrNmoVkpRDJEGjZN/9+Mli6b09TEkPPxhCls2qVVYzJ
3D+p0M33TYIKjyrgUUSKoZhNFIr6CEEvg9Ut6YC0qFtNkt+gOyK+6y0phzj3zsPn/JpRdTlkKewd
a/nxpo2OpgEqK1vWxF2IK+fEJGLsQ1DO7rnmjuwSTGxD7zWCfEjlFGeTvuimvZO+cPVqId3HNMgo
o3E8vrOkXatzzQlZbUL14mOmwy/ZF0ewVn6rgVRTFBNIfR2Z239srkdEqSxrYfnZI/36Ry59qv+O
Dyc8rQkHxFvVu0PBG4P0hFfNSV9zhPpvz/x86SuL18kxSh5bJEkvCrX6mYLuXqeZa192JSZJwm0e
pxmnim7t0EIdKp2yfhreZ8/VqISPyf+T2LQ01ovfuq96sR0JTu/kLzJuA6qO2VF9mmQIJOzqUmrW
aU325Euu1u+yJv/kwL2YkfzVRcUcZzzQl0Qsr/2+zCIsDErpv0oBtdXk0pv8GPSNTdUhasVclIcy
mNQaK+A1DCQJ7HWbnOjbAD5DetKe+FtElM30xGbrNsrqUtwFbmtf6zBUHjB/VNzVuFdBeWhNiM8S
koUAQL/gw7sBnv8HjxWRzBUNJJAdyCZ6N8b8a0SDFhd5cFF3nkFRGPWXHqirvp0IbIFn/BfHsOac
2boX7wloIyWsuN0Ka6dAMhJYyfja4ysP4oSzaAxuR5HMfQg1JDdYGOnOfcB/PcSA8U+yR9WWcDO4
GjYfBWffe+S5q2wJxhK0PqKDV8c8D3bbvxPghPOHGS6vMzUOcyGJ8ONUp8VoJt0VaVu3oYuxC8K9
H6+zA/8gL4WEkoq3wi0c1lfl2JUkhwnieCoxtca867/FaJru1Rso31TkFIZdGcscupil8tBe+49w
dAEoL/B+g+i+xIINWg6f38BG9xerzJv5AS7OsEjVFyN4Ea1sE4SUH+6T3XtU5J0PeR/PZDdBm5Qt
erg1Wj9kSdeer25LqB0VUfEvRJjF62uK6QAOt9ZR1o9zx3fSmL3vjmnDRCr+fWpkBvsEE78ZIQJP
LONDk1AhNP22z20IQcSg6OgxKCUILlTvAJDOU+UviAXDKtoP8ugvTfB6dUDfx2vuUdlvJNNMA4DP
dSSVjZSe+Xvb85b7BrmhPDXdnxohERUPfmyCyxEYZhzUtn8EnquRaS2rtTfZyt9YCmjdoWO8OGv4
/Ob/tsylpwta1r8nrLHV8NHaAszespjoVjAGfWgYjLBbXAWj1Yi1i0haDs3TPXAisLY+6MALokCs
mpSegYfV9ITTSRTCdti+/a5UtICZan3hw83XIcAuPbkdctqFux/hayr2taDeQOYty1ViZaO1eLmp
YAntfRJ7i3BkrshqJgJY7QT37WibisQ6SVEjFrsDTd6NuRApZAxZL/pwDV/LXpzu30wzAIIGzk6Y
bX1Lx1R89A0FrrOKh4GYtVXb5TdkkiJ7PtGyjZM2bgFL75ofOPRuBrMdoZfXJaLY9RbgZ4zKsodb
H5NC+6q05fThzuCSG+6fwdVCl9dzlXzGKuoR3zFC0zChxo8m0BJigel4ARpXjCDBXKs3FBxpXUOV
5Hs9d6fT8rcQbFyvTYthA3/G4hD6Ur2ys4iNkeVOUstcJTIiw2l7eEEk59WYlpk8QTpNfBQi08I7
7xH6yvQqh1J+ndCJgT57F/+1ITB7rg5+8Jv3Wd6MvpyCQcUmkllgA3QJ9JFE9SFjl+UKtVG6nJGm
C6uPHdqJiXfssJOpmSq+lakvqpYJEpzaWx+ksmsaToZcketWzJBPNgNzyXhQJ1x4Nee2IeW5ulFc
AuEqXXRdILHy7+yNbHJJyIFMjErii8TA0T9A9MSbiU5JPueI+3IhLdzon8dhHbpoj3et6f6vW7Fi
+y2eVK1cfiz0nWIQuqn3zJopXm7nhdmyghuVjuy7AMeRNjgpHFMj4GrKSiXYbAkA6Ot6vXsMO6xN
dTC6lZLBRx4YZx4maJ/JpM0IKomBVyVnSx6A0PhcL+AgnDK6BtPOxZ1n7IkRS+7N1QpWijCpFISK
BvPEiL1UypUBZeyFs8pbXh8c1oPHq+2iu0axAqkH118t3NwgiuXZhSivbfyedob+TJxRxJIfqonA
EQ3Shl95viSPgevSlC9qTkVrs6C3CFGZV05PhkOuwHAuhpt45H49l1Wis04OmFHXdzajw2fUCUdz
WXGaGeYqi0VIaahXZaztTgSWUEZHWEZWvUeMeKVHjWQSMgCgITLjsoglxx7/JH/reVEO+9F2xJTX
EwqL57QovvcPEDhoEYvaszj51aqtkb4umoS8zq645cidH645gOtOiP4lo1ALY1VW40jPO8v+xIJq
gYWjN2yZcoCOa4NWEuFzALI7+w1t560cAmbBgaao1aqDC0PA5+L2o8lDOH5AnPxrnOmAVJiSr3XS
O8eDcNeETlnxzqaK9BlyECiGuOuRQPeVduk11dDPal1fmsT0PTmRxoYL8RSmmQ/usMZ8BdSebnev
iEwjRCQZ5Dr+qzm3x19xnwfJ/wdg3MwWUz6NDhL9fQMXQ14yZZklk+CZ0B2yX3QW+w+Q7tKsKy3V
GFGH9VAwN6Ve26piBoaOyYIOsOF8/hPxpSEXlAaBeUvV9TV1ef45BzLqhoJDRLJzwsSwa2kkURbt
S+SvEumI7xVH8URJ9FXZSKjWj9pFFO4GXzEspR78q/oLHTW6/SL2xipBvqF31K0/AGDcjiUMOUkD
JE5f9weavOvkpSIX8iLMMnwbcqS3f5ag2U2dF89pm1lU8rzxZ9bA02aTus1kNG/GV5W5c53yiZYg
Y2LndJRswBUzc7FScK6qRyVb94EqLz2ddBtLNO5G3qaFg+1MrT8d3yUcF6IBVPWyOHD+Y4a88mct
VVcsi4zlsjUmmIAQLt3GwyNOTTLpc5Mq0wzS8PW0z33tdNDMQAnEGjYA2uM/2T0BlzgPfKJVKZEL
hVk+Ufa/1i5rr9LWUx27NB2roq2+QMuX9EshEG9TXheHoNqhX7I7Ylj77DSrU/fuH5Fj8e+Snter
lI8qutQv/NZIidYKTcXaPQc4Uh1wdkYqz234qUpwit3Otz87g3mq827fXx2V8DCNq/YHKQAwAQW0
AfHhxL0c2RtjzHr95Ofbt0uim8fU8LlZIzGN4Ot1VaVxaPs5lvi/h4TuMe4J3xQp1badmH7oFcyw
TZp4tpWDD1sGyEur6Rbl6YjdIyaATXkGzcDt1snlNohgDu0yItrmFqcvaHNhk4H+qqQgDqyxjwC3
amWG6ptZbUVp21reFw5ZWe8I1/P3USsFQQs0wNm07ZCBwQUJzcsFTMcR5qaYyWMAaPjQE2ZFqUX3
jI97lZcDOGDaPahtxViRej3GbJuD3WIv7oE+wgABvv4XB3lbE3t0J1JERKHQc1Ud/mchdD2hwV9K
7bDRlm0gjnp0JLhdlCOYZjAfnt+yV4nxrHkSVY0rQnXyAQQtygMy/GBVzVpEF1oLvjb/4tPo5WFF
91ORQYEbiT9O9SpxPYXmUodH3ibQDzD0lHlTIwwlcb9YT7UKjf1uaecRxCVymiFGbZzFQvl5oFgE
qHq0BCr1xTk27ZYswO3IW+pcdJhi9QmWuIQsHzZlDeootMAHMFY7gBB3Sa5MPi7vxLUKDpoAwUNw
x3v6yE1EpL8M5GjZvPyAGtbn5xZKooSl5VIokG+g6JaYn7Xf4xcbzee8kiic4SNoHdzJ6LHdzK4x
dgVo0CEvbh7p72+tFk4rpXNdgxWYqQLbgWyLBvetS9dBR23JQiZwWonA0vxtpX9SCcMDGcU4axhy
kqzfQB3kuSsPpuFsLeAeXeXwop4yv/0/QHStmuu48VhpXV5wdYyF9nttGYPO4eINwjiUJ0SQ2jTl
lyi7EUUdIHKGX9wudeTdLBtYalNfBObIBSmkgArFWHrmIO7n6c8iYqxYNamGSyau9sKP4aC2uZBZ
NV+R4kTeod3dTPpGBB/mu3GpT8pTW4eKxhY2JlGBMo+gr6w8hB//t+0wB9xLVP1JahiiHmzPIz05
bvVWyBYw5DDZqhOs1AUH/NUkAIzL1ZbhScryDwtyezS2OVAVY4Kr1dEPu0dr9rNN1/vP+PoYGXSO
heP3QeOLDYnSkOG7yBDWGW4eRPzpmiZC3GntgP/prEw3vwDu2fa2Sngx5XYgrF6tKz8nV+PVEeuj
xlZO7QwJmCyon8Nj3IPYxNefLW0GLEj7bQjaRMa1fJU6fr03f4iZqmT8xCxxa1HymefAcqURKp7R
V9eg46QmD+8uIjJdNxbhRUlAbvItFn2zAcLTKvE1E43bS/B6YQ2LTEXcRdVr8T+FVlkCq9KHNjZB
1ncUXEAbGZFpo/1ukh2z9nddly+8nr6XY5Jl5MlqHUoW1UbiKaOITvX0/1r4aM1yLx1eRqEaTqNL
l608++BB4TagZ4EQ+ln352gQQHQO8p0IjLLivhZTfvfv/cDv645hg7pkyDCPvSERFp5Qt4HmMVX/
MUQPNpR/LA0eomW9GE8/BbI3yFOfB20Xo1AncekMrj0M/ETTe9cqhJNuAL5qBqNMQe26cFh+aZMD
SCRRZiTz5QPZbIyny3ChI+ZxpfpDZghyLTLzCMIAMawD8KqgUnRNC5SHbdYs3UmQmMDAbgdHSfGz
F98+/sW54dEu3iomtjdybxAPylBemzWPo58XnySiiMwY4y701HiGBj1yjyDuGALZPHcknLK0YtXO
makD43hB5J9vllWGpTi9cxeD/HVpDXRhjhW+ENmT3SsKK7iSxm5XclHUYwWx3fV8TULCqfdRMhgP
kY4wmF6aBbsBJ3jZNelfmz5dmwyuxErUgY1coIt5shEV83qV9qm5duYT7XNLuVrw3NVMmYocGM1C
idzK16+YBwGncspZCWaz3g0IV8w/QcO9c0+IJHAnY3GeWvIS/9PpfE4jv+l48lAvtyuBnFyixryo
HQzPcWUT05Z4AmMkY1PL6Zh4iRwNyJ5CWKvjhwpFdsw80NfgJ7cMwAHlREfui01FsppJdGVbhxD2
VkzS1w696nOLTsONb+Z5xJQcngQo0gNsXmBwTVJaXyG/pLsstVOkRzapSYe/BUjYRNgeb0/RuWlW
zVU7s309S4ptPm6XA6FCP+srX8oj23DD5PVLpkjQm4PsNOm0y8eUZ/Mxn9lYJPpopkMlC+tTzfFr
eBzLdxLDacNJVPc+sJysMphXU67wAqDaXyV7U0GlnlZITyB/mlxhBJQpujMjXsWgEm7dYDzkk2Dn
3pEgjRfscpbZ6XsVVtYySSCqUMBku0WsNkVADngpDKxyMuLIeLiE0dEVqVYOR+/04uil+wENI1bY
4kc1T7JObAlmcKfTdxnzEY1Hsy0qbdj5oLE6IHA6sYJMJ1wRzee0gGVx8IDV7/N5LUsm2L8HWz5b
t8cJj6ZMbRhKdprfkcMf2CF6IWtCJU2yimmKu7gUm8zfPNc+neiw0Gh6xaTZ3OKSH90iTQotxVEh
EK6qaDfmw+hZylgYKD6oxdot/7g/wwUHYngYFUS+QYCJKE8vWFnnxbdPHC7n2RkFGi83NKpwQfWx
ApFjuDipmZAGp2Fj2yfQdf9XLW45Fr1oJMi5IHv/xcVXjddnAhz07SccXjarM2903ep17J8UNhVH
XKjD9iTIjcrtOgfM9oLFMoKzVTD5r49P0HaxQpY1Qyk4J66LzbR7AYeJ7AZZRIxAw+L7rdCQHjlI
v6LP6a8ihM1oqaEOPNV/TkKSFPhCgzlukFHnrmKrTYyBPGlUNM+RRLPw18mv6fTMMDVSKgtWi9T2
39t3510e8rY88LYiV+st+fM9Kt+REVQLH2u4uGdoTaGZM5SdeImB8HLI91Jm63y/JBNviOQ8tPdG
n80LJ0v9Jh781LwINvSXMhdg4X44aJ9Lslv6ul2g8+J5fUaR8MTp0H/tVltEmDChMaLD49xoseEH
y1h7z1zS1RAwGlgA3lCa0ADif/B62s7MgmLvvhClnlQU5AuRZ3uslf8EPIHOfJldUvLSadn7NYn6
JzyxRU4MhQchKGw8UOG9G1J/jIan5Dlk1k7cvsA4oeRH+eqUahNBGC60Hm09qL6QIG21YtH4XKSZ
HHjQYqFb4zjW8ZuGYN/SddQ+bCSVmWrBFNs2xOpjDguZPuWz6oXaItdcL/QnL+FudbgRTu2huHHw
qFvO6PTy6/3kpHvJZyNj4D53VbHyZyOMVG6bkDxFXeKv2HekmNxcbte9qlwa2pTMSqAA+iX9fAyL
tyAYRgvSxDvMtCuSlQg9KnI/AgAr9cM+GnbRVzURLcHtwAQksLz5TKZm3+kJBquWYUqQBeviIeCn
3NmHXCpih+M07djgqPsZY9qGP2V33z5AajagDNWVE3omJV4K/5sOZ4jCtP0VBB7KjjI3RNrfTe6I
oR1sp62sfHxjxv9CIDxFyG0fv95Y3BaqRGS8R45SpaPoIXSv1JP9wV3u2GRts9WmeDRaDqSogEGi
ibXqjwX+X6/+IVh0T5jiz5mS8e1VDdWNZ4yvWmvn5WDUk0wwrr/9aOYC82DmHs2pwrc+ZiZjR5Ix
4fLw8GTcGgZgfP7vAySj6qOJaRUpOwbDZe/99y/NgcQiEfvntpl6MFQXamUOj/U4lYcXc96YplyM
GoAruHZxSFw0KE15meQzMFvMhl73tzzMNsycjkuehF2Pz8u6EBHlPqgGoAB+BiS3HcClNAQSC+aw
gfVy0Twglvr4vnC/EUgyqSbkpuIsBH/1yh3AZbyynPRAP9BdFPkz85VKnsI9GfCKDBSDNm71QbH7
Nxw3vBDuZbZpPvzWBNsQ0FnXBCk3v6w9rrMnr7szIcX1+LnhgCwHf5eB0aCe5NP0N1PocgBLfEb0
wfs1hNYPLnJufFRXewdY1TzSh5/t9z7faSnQdUF2isnsQ0OuBZimSXdPxlQooN3EDzTBseODC+8h
NBUZEJ4Pgun8D+yR+zXiGFvQlUH6A8f2lWFz3GUN4KT2RSut3GWXJzwdzDgmPUMoKWkH4hGTYdht
RuVcjdxq1hRZHiYI3lfGCMLzDSiEYhNCpaVIKNTAiymIFy92dVqZbOjI8blZyO3DFiP1/C6/osQ5
FAxbGdmKqm4nwuB7q7t6SgnQt9NfRlZ1CzWIGbldlaamf4vAM8boyx66JG1ETHWGY0THYBVjWRTv
riFeQvG8YHTqSji00k/1Ei2w5GYpzHtk53C9yM0OGSJfylsg6/i2zLNmw6OnHjyUtw8zQaSpl1Mj
Jrw9PBzWmd4VF1OmBqqxYFIojdFFjBed51I/RLIxuGEEVr6aCRjWyjSv2lX9eu2W2c6+tG5nTLpD
uzxyCql0xWTgP6coYAy7yJ59ctam+UFpNapf0UuthGokcLiiGVUnWVB8n02EqIIBUYUJf2u8BfxC
cDopZoBxi/pzsyAoqL3bgQn0uPH4wULUrT5PYkZQtHr672gEMkUoHx40cCyhjYjvWxWld2CLES6z
T0PpjKvm/B/YAq8Xgdzv5XxqTR9ypIGWxugW/24J3AbqQTZgsgdU7fjBL6aKt9DBCZYvtBNfw5RS
CEgCXVAqhWPmVN49FLhWBqd+WUu+7oA/a2wCNhT5YkyVod+NyBpD+aEf9MGsyRdkLKXXZrtBlc8I
5tzdVmfCQwhhTm4ogFh1C2sLVtABqfO8uu8J6bI0bgwsS7eI4Om9DUvVeTH/0f1AuWoIt8ca9O1t
nPXgFdefWhxcd521XftlzTEMUhX8B3X3vmpAClpx7Px98GPFhmIcja/tbqhDiQ2gyB18TmgkG+M4
IaKUOxyk1XoFr8KgERCfLhME1YHvIJv8auc8CCInYU2ppObTK0Tu5/Rp00qjr8lGXubTyq4EFpHF
GBdJM3kg/ABMr5FsmY5QUt/xvjQY5g79r+qYxMwjWRzECAxd+QzmFP2PMFkPmXEunV5SVYu3VTum
6ud6SuO8ou479L4rSoLDXJqRzHCfY5sZUoVtc4S+IOg6Zd+uwJ5E5hO/t+2UFrzunApIrurf9eyd
zoNU3NvsIjSj+vtXWmp8PzvScFnn6KNNFjNlds4UDKBOGMHk9SKaDmgENp6UuF8MA+XN1zlwxEfV
+IC0tny78zzjx2cbhvd3441bAcl2YQ19dnd85uExGNSqVhlO72kTTL5pGy7qGNkUojSaJver4Ikm
zSnbmKwMGLW+5Ab9srdevDlFzpICg1Se++XVbAVpQXMgD8NGrdm21RRgDtCwqXPSqKlneywCzJxZ
2UEZwgiqhpQxhP/W0BYJz6k34cWRtNHejRWrphfJetGqNEm8PEuCns+5hZWDGZlnPAPWM1KiDMuZ
nyXiz5/s1sDBcllc2vOsXOqbDQ27sOFKkDhagxTrTGsrUruojCxYqGwIuP8OHNO4+uR0vtAqJwuV
UlRcsy3u2ZsQRKpbofa8riwv+bTkwtT5FNr00xke6H9V1Mw8csRVU4epqt5wBskCiDIJ0BRrMiPv
NetXVhDTn2jThJNPwEN7ZmAZLk1fY+IDlCj8XXgprzYesHeDAIJNIh8aFoNScw9zFCQHtdaH4IOu
m6FwjFVhc1AyUR2kbLrY9NznIplorIn9hA/hWf2rqHHzLms09YPE/Y3v/bkavwjFrv59EWepesYy
NbbKjZMvNNzD8MbE9nMLWmvje0qtdoBId0bBRu9jlY8B+dpI4MfSbdsLslHLwngc9hrj2kFnr+rh
0rybQbQjAf2SB6ESaLBM/TJhi+Drwop+lRZSsGUPXRSUvKQU4CM1TkvRju7gu6Ro6KpyixT7EWHk
WWvhpEzzkWZiRM2+z3Lq8kvgEcVBfqKGgCjBsLaf5AvG6pQD4QgspJg5lMhZsRitDtOFxvxuXppF
K2tHwMerAYPgs+WBjNVpj6rCi/gOEC1OUoN7SZjUusyudREwgDBdMAq3Uri5SriAkU92zXHvQ6Lq
YT+8aaEKIspqal5itkqg1zyB5kvurFeiLdSN84kXLWrPXt0zpdfMCAr6d89lcCuE2i3aDi10mxGP
gwO4Ya5H76XOelbeG9wZA7u19o6e6yDuXjRrOEvCjo9tQUX/N1XcMCeBLKSZ+xG/eVQ8sA/lWAZy
8nj+Qsczq0YjGl5T890bjEZy07PP6gGYHBhwjcxKnBzPzo8qDi3VkpI+ffRMyfSfpp0oMY9oIvDg
N2hZcgubM1NGkX4qHg365V5tY1vXRlXqYPSE/rU02VQSd/FogY3P/br9pg6kUOUDV1ILe+2hdzVH
EtuGlhLAJyR6nmu1yQgSiCogKUu9Locn59LmPYqrKH/2nkTK9u2+PDLDmM7ZlbpJsD7Ypbz0m4/a
ptbh3gg5ZBBfUAE3rMXX42+UGWCWLwEVGcsa96OZmSOvm32d+yood0j9rCAKFl6Ttsww2eqn8qOt
+YKug/1Gp+HqnBZ8NdV2mThVaNdgjgEnUrzf5YuEZtbvJeObvCZ6SV7EMZlN0jL4VOrz3mBZBtIs
oRADB7LQDp01UbGKI6uMT0Q+ULh85ZOl8r5VEzlKKdNDcxcLVAE0fkNVnh6n+vWYDxo0L4kRsS9o
5cEpVWEYhtYuRwCWnACuRwa5W7WgE/IBEIfP1Ga/wymdrBmYw3i84GYvmQD5DtijtnAI2GkmwC+r
PC0DfcZgu15mC4CIf9fj7agMv0V1F6CZO7fhmnJWQwEbzxzs6n1gnD3DP0zQXvZhw/yAzHKXE3Ok
8Zr4q+g3zlbI4kjpm4AfZTIpxaiZnMzW0ZOcG3PVsDIFGP/Q4DsLB+16sgf4hL4r7IHiT455hnS7
rrHYiHyx4FcL7gP48xG8IU1cW4Jvd3WeKCSbA4EqvtOGvnTahBsqeSfZVZazUvdWM3mzUGdLQV6p
I9je+xFsskB0Dt39thi4Str2HAcCJDcHwNdpV6JDZjjNlT8Bq+mOVHWTMNeBEULumXBjzG7yCErf
YcMsoRdHPjZl15t6wwGyFjiFhkhOKdiiqV4Jck4cvWnHvLSS+yB8tFTzqNcU9aIP0WJo/y8WnNVx
Wb9AxAcXpGQ2FW0gj4P9RzgPsXoHosgRt1m421J+mCX7DVMB7cdqfigXaUab10NeCePJ7yDiRHdi
zkasnfuQp14FUnyEJB+P5pFA0E2xnEk+4JzCP8LZwmqIeeyWXoYBkC8bH6c6147fduSq15JyC0CD
WowlVOZdWrXyX2NG/Dj8Xi5FA5WGcai/Jld1jHmQCJoJ/k76JLOHGyyVUJHIAkQLH5DJrMWpAg/w
3CWTk08CWBjbZ/+dsPrEY44DuOBCFPaLR0OPo7AJlnWkszazcZxNyUt00ft6Td/BwIEmxPpMiJDD
bMlzSYxsLo0iWoHIkcZOT0x5slSX/Fl01L5T5DczBBKqqyj+XB6Rubx9qTQgev5JIhXyvZAtEO3W
56w3R3YfOvra9e0QQ3kql/AYPc+cpcRkkrt+RyRPGQUnYdrFt7NZN0mzeJMiEEYUp2Eo5B8L2kj1
uIlMAQE3sZqmFHonY3JFtc2G/VRpuCg8815oPMNrA5oqhcXYF4Ge+Qva8ssPr4pA4HjYf8gaUSI7
n3R+HEC9cJ3Od415I2xeM7+c+uls/md1N8Ogq78dOf88QafPTzAQcfuSRYoP8Tg3fWNBtw4+cros
+fkFhtMOq3DsTNXPUedfMidMvKdCg9AJrrMO93Q74PPdVwXjIoX/ZiJNy4rwBM9f2AAgLR9hcsx1
lldcG0Xc3KkiX8Au/7K7smGA7a4Y7vkuxDLqBZInO+kg4k7/0cMi8zdXGWcXQzsVBP9F+m4Ynlog
8VZTyxTuGxpcvDboCbmO/CljYQDwCt3gZHBzQxNe3svFC0S+uTCIYiG8WHGuCTQwbWNo01SCdaXy
Mp6soDp5HDRJGMW49otAEscMn3/f7vwo67JLlN7ChRwOmbGGSYIyHEXjLbrXEUE33sgY0RnrG8Ct
ZQ3vjcC0oHQlbRDkS3mErzgyBfGrRJl2i5vOOnj2Sp0vNBP1eKWv4kpho2e7RzctaUbQV0keaqse
CwaUBRQpXhsyKRpJ+vRNQS3gjuzpg/00CM7p8nGa4uEmCzKu8KtgX5CCIIsB2dnzi6Nl7CgEtz4u
Dm1f8oRWzVzSfUPaXTnLLaURkbGB23h8epGBzWiOQANw/dUsktcwj091h9JMU+jKm3cUFR4h3K3S
Qmi9KLWHPgKqzxu2JBG+VrYl97Fcr4B/mCTg9gPQPt2Fmgc1j2oiHzDKIFl7k1l0EZ7Jz/TyyvaC
8UCT6TOZmEtH1iGAETPqkGaQdXTmrfX7USXaJ2krkt6zsY1HaG0bjm75+VI3kKJTrIh++IykpH7u
rdcgWzYpQlIu7gmUUpxP2xIFaE058I1m5BfsR5+8xEoJTo1YyR+Ou0O+gbWtbwpY2ZWnOUqBD6Dw
mMCuElmcNfQ2L/EM8hkbhTUqqULRa4pZOD+X49YgG+Ft0Zfky7ryDuqQj6p6mqs1DtqbHQk08HU+
H5VUU0QV3oap4VGlJMkbfHZ3SR98u12aNHLUdBSmlnyaQlHMDxIZX/3xOXCNAP9k3/V6dOqPghQj
OrSIFFYX75FlSTlLzF+XHKtb3sGd0DlfPuCV4Sk1Q1r+r4bcXvCDpXLS8psSs9GZd3kQYjM3mnBw
bl7oLeogyTGKXmYc68SXz5lOfDLB3sfZIqpAus3kLbmsT0X4SU0X9qYhX4iOdarUbSbuohtND9qu
saD4++0P/e7bSoW9Q5CevVnHhaAUefulBplq1cVlq2rRsKMojcZc35juRwsxh3TpDHDFeTDQqSUT
y2/lviGe/5DNfeb2gVhmHKCFIa0/Vol6oPgz+RrF19pEh0FAalflENUQE2WX/hxDr9c+twiQRVDm
kIpNPwbqwPKcAZ33IPkip1uXEBiM1l6+xAd8wt6Ridu4+KL8kCCndoGKdPuuxtJstm1ukq07l2AO
P72iJZhmsYy0pzJZwzlBB5HRzf3GDT3tBIsUxq6ZX1Lg3L0+S8mkdJJvel9NazubiNBeMeTGhFMn
qLcj/5knfdJEPeJgqBHDGyDxUyPDk3eNT9tHHNPnzJ3jgPKj9f08S9IhSWgy0/oF4IC/2mo6A/ir
FCHNxSL6wtCroA2yduy1O4tWMQmetGnDcMYIXhi2FUtpwPgeDyrGJlvp7YNyUFR1kxjz6Nbq0yES
Lu+mvPYtInB+oJQEkE6wt0nE48iaQlnQMibNHd9xwk27+dfgvyCh+WrmDUHwNMyMfjWGbfECWO3w
646eaOwh8VTc7h+da1vCnouSMI9DrSk85/gMgjuOiYDdCNlY/SRQCCKUbio1m/G+YMW1OmIAjQwA
Q5uznpSZL37rx+mJGsAlwZPSNELUbz9pqzksbDhyaA2EALYyMVmiS71yHL2McMcHYNOQO14LcGLX
Z9/vguUGFAffyXbu3HdxwFZovhkU9R6N9YLeDgdkyzSyBxeglh5xXU4aqz0/xEDe5ioBOxrhgi3F
sAFJxUe0Lqx6gKcXBPZPFxJJfLQbeszYTfEF7NUZeA2RH6t+fwdYbK5HO0sbI0nOXeioRjc5gLUA
zVVq8A1tXB9UtAXQScOCZesmt6ZmmeJdOhpEViYydu843BhP8HgGk5+Zre/VAzKOEiWi/FvYYZtZ
alQ4ZwE+dCwIj5QJYBDKMhsgZ14UydTi8BXajM59+e4xTz0Eq+1AQiuqk6Q7pDN2p6BUtvP2Jx7Q
7tP+GX+PPGUo6h6NxES0Gnvh6l/fNze+G10zs2KEfp/THGUKM22P4adfav4CBJcNBYuT1HyW/g6O
BZjBF5QFCKDEhjXzVQHMsLl2E5m45dNofhLAACD2f6DCbzSyo8wpE6abX3f8ZZ2vZ+b2kbDTYfXQ
BNPISn13mWGmTz1VN8XBoR0LOOBdJ95T9Fhs5gzlEESVq12YYRp7xZ9OvECjqv2bpHsy+XZsp7hk
r5M/05Qi5lSipXRhTrCEbn4FQ/USVJyvzRrPhbX46KYwflF/GcWrXVZHA3+9MVEyH7Zh1moLyUfU
6VXz21+iwBD+cVjykuD+Zrdc1wxhUVRCEcLkQiAeiToOtLY81oAsFRrgP/5aY9tbDvEVAWhTHQZ9
lPhn6ggZvvTu49FS1LCbNTyFCJm/5YcykhEZ88cTn7oefsGhSUgmhhIbL9Dbbd0cVmn4tcixY0zP
Yc4IyaXvt+CCo8SMWw94k4hRQJJE8mIJiQKwE/ewaRqOQ1o8RPYyQpfdVc9aLTnqiHFlMVpjVyzf
On7kn0x2D87iq6261zVyyAW58Se9D8oqp4pSrWcGsFDfggfPtPEWFsEg4DROX8tSgjp1iVCywade
AK1WynPbHSeBo5dhVwR2CzLhEOk5IRlcX/QnjWKLJxblH05LsZ8Fa1SMkiZ+0+1sltj0FCwyX8/q
cdJn3rNZzSVcjQICV4OI/0VnmO08/NNVrRD2qHPPw7ob2z9moJKzVGP3jzty4zYpXvFfuwCEAi+X
I3RGlrwgLugXHwy5KQqDNt0zMp0fdmCS+uM3YOfqT9SNpgfb8m33uyaHKvZHoMWzK9kpBmoJVTAO
Z1hINJC4PRfuo8OQdzQjkm4OWPvxTrV9w4utoj1iVuNMmaOIymknociDlg0Z+D7rN/X8R1MEdOhY
ELo1xx37LsOjk42SmpPqrl9cFzZgQOL3iLXBJCzqVeHrwiYnLwBTJOy625CI4dgvDgd3qwZn6s1o
yzSqmvYGk/thV0yuK5dr8PSziceztVAxtbi78Kj/0weZm0Yz1Wmiww+kFbjeAXY0bV8GwPmKM1f0
962W5Tp3bkwdONz9O0guefMtgHfy/V+OKTrixFWn4QUXb8qCsf17Ajbii+ummXAA3ACR3hvzEtdB
sco3HjXTQQhSSxbBTKWauoqcNr1hZrE4rRPymqXf1l3BmoOSH2FX0D9O+Ceq1DX3IJe5RZcBPyut
PpTpGNwQbo14xkmta6KP7FHJHkNrq/pwH65uDLUlKUnu2B2flfHWCyvJZEjkWrIWn+hhkSeA+9ZP
gfOFbe8mDBXZIgHJ2c+lHlBz4YvXUrNfqoWO+1REdyZ4FBL5kfaXJoNN3lu9IAqgXBrrTIQDIWst
v3tHKTAodZMPKMSRN7I9Qj1BGyYt9UbTPed56hFBioWR0gcPTvCH/rUdv8GXNkfbfFf1YOuGCodH
eIBbHEDeYnR70GZ1nmIbiJ1GkEEQwQad3rl3jOi53LBAbuiM4jkP54fM6IyyU62DyjvCrsDU/UF5
ps9sYdVw3h1kHRW/g8AbJFTT4rxfi3HpW35uJ5PVkEjtPqKB3whKGz9CUFiEIzc9YYB1Gs9pOChq
+OU8aadCufY8JQTuh1roQ8Y0EJiR6v6WOlBqd50wEcaNeulOgQapw+IVUVZ6B/s79ZSD4mz8YNw+
UZdjeGOrB3MEOOzMuwg8/JKuoXdf6FNqZrwnaKhcSBqLN/fp5nUekNZFNW3hIaEEDjgP0kBCLw9f
i9mpQ7IeKNzE/duizfbe+/cpuWOedn53hHDccgT8hmLzTph8bM1SX8Gtnp/iK+J7auSzJIMwtaHp
4sIrE+suVuBGWM3uZhG2+OdqFPCjQ4bGqEewv8zBETfwpjI1HuI8HMeTxtTr4VBF5cUnguQIe+9h
rtfyLnJnK4BtoB77qdpv/lOjLUGzyx0WNqNntf2J0i2rBYdB8Na6TM+Bl+VMFJZFc3lXNwdwcbA1
MnX2tWp8qe0mpmaZ4peOaCaMAO4gIIvAbrDMMi6IF295Frxczykuk56kbsE4NH7NyUCwGMb+/37P
oF/4Opk1h1qDJqSPSysdXOS5oyCceKGa0zs0HmLBo7VjKzV6TgbMcib2cwgWSZn7ryKaZ5+9iZBg
tDdHmkfUBgeXHlndR2h39a6Jes4sLzdAB7KGJrSe5tN1Nw8TSwdUjmh+BJ+wBuzdRP7J2ZFUJ1Ot
y+yD9tDkQioPF5RZr72ZiLCTesSX21sW+YGc1nTxKWZnIj7rktiBZHDeU4DLzIpnuu6PCgGEq9GD
hmBbgwEAJzjE7JCwi8Xc1jlep+73TOf+WY536RFf4rea57Cq4ptaDB5ab3eP6ktK6P7yUJXjct2A
QmiT7N3rkcG8BBAocrhI0dxmVYIchh4ju1D2HigTnCRoJUebxx0BJg9JwemX04oSIhCTqWhgT8+i
CUG57nHyySxYOnIDfK7FXQeO1fLhyMMc/WUu0OqBmfwbcAfINqm+8INdrWgsNshx8XG1aRzUQZhJ
16A8B5rZJir4JVF6iwIOIP8TKXEYxJzAzMq3RENVNclDM17p0kMUwTBeVTSyG98BxyOolTwQo1Tq
d+uioOAUsB8dUJLFQPUDz4OvVNyUGgWJuGK84QXKZElQgaKXUQRvkDUPqm2kwQJ7c2VcXkbxBHbS
blF2k2QHlTzvLlu1O5aPPNECasUMF/4U5rIRDqn0NQ/Mnj2jagSu7c6ANlKzweGn8er3SsOrcE5n
DYe9GcJBnAfl1nS5ZQeUB/dxWxMO2V/8GWFlYGj5k8pQB2uwOaRTOl2GjmKnNFWN51pU6HdfLiNJ
3QM+f9oBYJxjBAtw78OcXQ0SrgJA6T3oTeKHKkhitX6H/0ytC7/k8od5OevExnDnjcQaG/PvbmI+
vjd1oXONIWmxEHdDNnnGYqPp2EyV5ZTAFyEQW00yFytnEUQn3hlVefoa3z3CIotWXltNnKzmLEkF
FVTrGWR8puMWhcBpSnSgCgcbh81sE1ddGRyf+KQdiKCwQb3mcsT4wdCLieNVMpUphF/i8fzoDIvb
9xUp/opmLuyXeH9kvIs3pRwHWgfCXERBhM6FAgS5qry9i1csmnNAddchpwWv2a+tRm7jG0ip96ej
Il388pAEOmrzBO78JCj/OFZ5nohaZSobPBii8HGi0sCAzB5Fyi/0YWfGsbrluQmQiw158QO5tTS8
minZ4rPwPNDUEbCd+5JDnJprL8YOVFBFxJsJBe5v1AbPS7zh7wdfBbmZeCnNm5j0KyFULfyzMNqD
nI1HGAFs0rhgyUPp64KvZL3QDJD7ojC0imRZ4NUR6HlS6R/hUtpWXRxTOxpdAN+NxR+q37dbgqlv
1DnSAxIe9XstU3O4Pdbv6Yu2u0bbpZeKU29difV4HQQ+FryO4Aeuqn5zRVOl0RIN6QBDqRPZQLyD
o8Hbdih9TOdC4PeuOZq3eIO94mdyZbKi/YXDgus7ufAAk16lg183x1Q92j9aDToNoCLHwQu5rjW1
3n8ZAy4xovz7rE1Aifs7KuIuXDmq7vGLg+Pl8bLIUGD1FXMc2NIBBUP3lEeKxy5ytaI6DXaepX4u
NsLfX4YbmIicDEGvcxBdJSniyprwVUdvZdL2GR+F7APLt8p2JKGHyKmS6UrQbu+U6YITcbeJj0M/
Po4DVlIfXX/wEqv6yCOg1nNP05L/vchKwu0EJaZ+QaHRPmuaF8+x7c8PVnTM068poGcmCaHE5XQE
2qEaFnfVR8XEewJ5JekxGM9Z2g9H5STBNna9qChv8N6GxlzGIDOmB3nZBxP0go6D3bnlL9yFs7N2
5ziqD7zK9tb4BlaWOhJgEj6r0J7J39F96Akw/KN9oVX6ZY1DcIg9f+lmtWfq2wNIjCDE4x12y2cs
WzU0Ik6XM1i8ZzH/JUDSw4Ia5L5El0eCEA9L93W/L2m8Y9aiQHdIaTCngMq5boQ0ggo6kw/mau1z
yO2W+8uyPkpE6B0m7msTG/nW3xowKPUHHpll6aKIvcujz1Dr7yfKGOWaEecslQd63YKtwv3aJm0/
dMDOQZPkEXSukPNlR7IHwaDDrfSdQb+E0qinEZmcXMwvu1XhnZdEVEVEQFANC0/ZKRyMQm/DUyHV
XFMHuU7QfHnZV9CqcrWwxK30dnKM8PO4Khc1n34V5DO/iq1OCS15vQiNKhG3z3d/wcikUztB4luB
plOZrWtAzsJWDq7WLBn+JKyI5zsJ2xnutodci2s4vYaaKx0oIbePxA9j8CBvFWZWnjBymUJu1SsF
cdSV9wRMrGMHvL+F3jgMCeMJkO1uFjIid5G0xpeTuuWxuC95tTTUpaA6hwtacJsIQ41TIpIw3xg8
q3qajKzHzV6S3ZCImMAZ1HmQhUXUH34Ui0ysW2/9qrED3rHpMAkfyjXIpoFKLuKr16tJVfNPxuj6
cJc068Qx2Ck6fGVFRk73nueJEzOf1PtJZyRf9i8GgC8etoCNwfFkpka6bJRLDAOKFDY3HXO6jrpE
ZMJ1hJwtH11+vPvwt1jxyodxWNQxaZJMzlAlaM9ruwtmM1u4BXtw6HHQxRf0mJwYqUxt5/YoHbBb
WWzFQP/UFnvnuUfkzFUUOKUOCNMjJUSmPwKkluTrrhOk/NeY60o0cyPUOgPva0y0Ivq3HfP5o30w
rPEdJFMmm1O35zPr8KyaUZJm7y5K7ntGVAtF9MMLCSIUq2DZxe2PQ/hmkjEfxQF/0xl+umTxUf0M
z/QHdZazjmU6r/vOp3QhwYrWPbIjRKbebxMifl5JXl3tFmiA38+bsYokwAKkEIyJJo97ZgPd9BXa
zREYyC2PVU3TtBhQS2qGXyEhadd4Mh4XAweWWbZBE3a7R5zwi63JIJukrswwFNxtYOwYa/CmMHdD
oAD6es8bPdCeNL1bArVbFldKAh0P+JiJztITOX75dtaAh+phJGNqCIl4xvctS0h8H5GA64QQgIGJ
6t7AwFSzxWngcqzyyHsf6NkfN2wNj2Fqfb7HLFZfr/daSnRXq5E/gre1puIFIbA2gXnbbiVF9AFN
+h7a2PTUWgQg6NwH2twZi0z3GSXEudK8KbMgW2XsnaD+m2JPto4oFYFo645+JNwWX8paaRKkK1GM
zb96LdPhrdQ15nZxwx3Is4Vl7PhL7/crcML+8GDhWbbUTNifA5Lz3hIDIMxI4CEoiCwPZ5cuFphH
KlXQK+dvUnGNQMU9qxYjwbZop2zKiKl4OwIMIMl6T8/kXLPnx5TQ+L/iq2ML+dt3YXtkDS0oba7F
wiwlHup0knWTVjKGb0hwMzHMHk1MlYM63V9yuADWv1+hkKKaazW0BhkWu65oKqtZ1dc7Vjh+5r4V
OXWqrnZRmw+KInk/T/5FD4Y9DwJysTcBQ7cPYFzu/4qwYhm0UlEkElnNT0vEEfICgd1V3HzrLV//
ytKPkZppi2LhicF8rLbkVA7tEnQFqym+ZhMV2uoNX9c2kqyDkskkHldSHalgpZJFLKRPQLP0FEEA
v8s3VRRitEXJKb9fWGBuKdgJZ0H+I0EMnqVsc6RU5UVaHeBTzG8DJWpCNrluc6q6FD6CACB6KWxu
MEZVb17I/0VJGDwfpfbbvUueKSTe2R3y6XrQxP0ITNdJLUf3M5BOZ0NOgyvAKdX0Wmqeid+QWw5t
Nt+9lmNm4+JQwbq9KwPDQSHz8FqLf1TLS9Xa0M7RHy+EPdIzG4BlwrGZyO16RYIj7CSFxXrQgOXG
IhXdqCjQu9g/rds4av1PDFDlLScxUxUhxN/XVBSKHYXW1OvpXBBqRSFT1Qy+Tkz2O+nlr0+89REZ
XNF/DyAKMAkjnGMEp64PLhpuc5tLWGh1yg/65XctuhnaMNUC5owgBSyf0SUyrR+GJNVtUsuXHmiX
ht+Zf3+6xDUdkdx4fy9PCMjSau6nzEkgYsiB6/MYdDXywh2jKnwXIDoGeMuXWtkHgRgJgSGJZ+e+
nIqwYMtmIT1DdS9t4Oiy+LtGKloUQ24VGsUgjG9S9dyy44pSHJP1ZusItJbiTBDdNuG3fQ9YI0Oh
yvm0llV+q70W71Ue2rn0earW3cAXiv6lFxzXKV7gVXI0K1FRFGjyAzSfbfD/IVO7/iqTQweZzpiL
E2Ovofb6L264+DeoEOC3UYizWHnBOE7Hlq4bGQeifbMBzLfQLjEMsF9LKn06cn8EewJgRfkPATlR
a3G9wI7zyHqN8kmj6577YC42rWf404u2NFyGDOVwjrJIrRbzjsI3T+BWiBp/j+YPCAtuq4gCgnLY
t4JOv7Q6+NjiIhAYrQdqboQ+hR7cxe61XBZLS4bl2HhMEk4r5L6q53xa0nk/ufe1DbEWsX2FXR3S
RRPoEbV/4hsq/vo3yNQYWGDCdDprwjuEQB880lCCHmBdbu+mKL5VwvQJukcVMMZRPsg/mTbHXw/z
n/zqeFmhNYt6rIwZ/Sl1feCRTcTt2nCE5IDXZzYN/SUpgiH7cRFoLqg4SsETJUhEiQTzftlsXUoe
IRASHdNFLvBRWY7LEiOFOcy9b/uQPz2FejTmuqc23z7/i94Eruz1E+gN36WyJgxkxMEpV5FSOS61
jT/rvinMKvkx+6vUIkJQHyt/nEl4dYChhyGaQCa5jokdispMhHO1OsjvlL3mmpoDLG8gbaCZO5uB
PZHNxzTTzJQeW+2h+3/5D64wzyo96ObVJfGwJoPAtfXGBwmB09m3icYcfJ1IuaBIFagehbU594V0
S77Ct2dJ49xW6Q1p4uYdA2mg8DGRT6kH5zGUqz20VMHwh3E+eqm6m/IsvLfQuM2jxgwRl5DNhj9C
XuREngxEG+m9l+YNPpbxOPY+kzkOCAgxdXCDdWTy50yffvEAjvNEuRZXP23XPmiy3fBFXh3iMzFa
vs6BLV8=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
