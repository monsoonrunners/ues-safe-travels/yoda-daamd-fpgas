// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Wed May 18 22:08:58 2022
// Host        : Xronos running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {d:/Documents/EEE4120F/YODA/Group 12 YODA Project Mean Working/Group
//               12 YODA Project Mean Working.gen/sources_1/ip/filter_min_blk/filter_min_blk_sim_netlist.v}
// Design      : filter_min_blk
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "filter_min_blk,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module filter_min_blk
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [5:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [5:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [5:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [5:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "6" *) 
  (* C_ADDRB_WIDTH = "6" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "1" *) 
  (* C_COUNT_36K_BRAM = "0" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     3.53845 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "filter_min_blk.mem" *) 
  (* C_INIT_FILE_NAME = "no_coe_file_loaded" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "0" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "64" *) 
  (* C_READ_DEPTH_B = "64" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "64" *) 
  (* C_WRITE_DEPTH_B = "64" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  filter_min_blk_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[5:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[5:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 19328)
`pragma protect data_block
dczcj7dqgMRXQ/wVplBEZVg5QwILelzL/G6YldnShwVP/h1uI5APw4bwlfMZ7VcLsyJJhFdDGSVH
d7NAcQyq97Ehp22ZRbBK3ymdVfd4+kJ2L8DEop3ht4vAZe8l70sPReuCjBQwgwGsU4V7Q+WEH5Mz
vvNhgl33WeiUhWHdSGJ0EKaPydZGzEgNSu5hgaKZGP8EupIbEutRyth6+yFm1QJ/pCjK5V5mU6pd
FLczkFBibdw/+2Pns+VD8jaWswg/+W30dgCfMJrVHM1CLGA+Qq8m//RqveQeJPp2ytQA2L04RJ58
/EZ6QbUadUm66nflUZiq0SKf0HzGkNRp4xETpv4H2aIpD0ZIfrwANoKAWVwZC0VOmn5cYP5rm5RU
yvRYU/+1LEaZibem/Ws62p8Q2xZrN+rsFjLvZztuhzIPrxqPqfebkRYyK3KSQn3885KuXg/E+AAj
yjhNtnjc2pXDNhYOBJCwOWKIxjk+obpGnSh6j9xmZ/9VpmqylEIs6Nb5ZsE6ODt1EgB/AixD4TxK
lojumXrKYagpK1iOHy+ECe6XqBcYgw6f48MxHbGQEDxjZIrchGStyVm9JsOdmkxFZJYphYs7xLeZ
g1oCLghYw6616Nh19weg1pn1um5CtDm40LsJvyqJ/edXOoq2Yiel9aGhCv67XXoKZZsUhEKAnmNm
O+vYZKpPZIP6dTtfopOoM0IQ3TtxbSPZtW8bo5UG9hi/JDOKwCLjyyPvWF7N3hOGrIQC2JjclKyv
0eqv13AfMAOJ7U8WngoLMlPj7TS4zwpUB7SFgaEbm9f1r5OokvUDo/bUP44qepVBHbzOHVNTIjHK
rmxxLfRQ3ugDVDhyZzrp5eSYbVUsgiZ3fJEIIXItsDVZUoZGIvTW3nIGnvIr6IUjQApCfmI6mCM5
t602ZNiEL+SjnrXiYcca6pT/h7NG5jozMoPFjbfzV6rnshcXN7fbGjWDorO4XdA9p6gaJPfHSDSR
lHuyb0zJC5Caoc62Ilta9VLGPm3q9uf645iRsaoYw7STaST8pQFJb/Zsi+Atzvbv7+0OGwYxb4Yn
dPleso3Er/jCRz8Tl9khWEUM6WxE20OLy9AWSqUNjHm0s2zCmfPUNlvGwZEwwHmaWb+jIppnKiYU
ABSaWe4smY6LXwdcHueAwIPe7lGmmi/j/JvPSF4W0kRm43mBOc8AYyZ3ayxbYH6uM++5lKP10eBC
31rHacThK9OEAb3Z6RXEJNfr/hQvmpEiUyEwR2Mmbo8kVtcmz5Ld5yuuGbPGSGQsJEFWgBmd65nL
2QFgAg8c2MvRJzK8LZNXEHpyq6pW5JekUQTdPDKFHy1gTLPZWbcT89TEy0Rpzfzcu/q2Y/XjEEX+
7pldU1Wab4F03C0sMSJuW6QOHcUDSPdo8a+xsfCaI5vIfgdPkbJ3J9yWd76XR1HfNGDt/ID0Xn00
I9H7/w1/O1pDv6eYcIc4Vy8uRMrv5LUaD5dCuER6iJJ8U+IxVrHBvPmeEKZp8y+2zuRCbhJ3PRLN
m8B6lp+VCBMq3a/hdc4fw+5eo011PhSYu6SPqlFpGdnxplsuH1CZSCUZThUhRhlek9YKQc0cWzTh
4Md+GaRlEuaqvgnxMouk3AGTYa7rqZjUCacxVuMRhQm+RnzepZLQ8RskfQ7N4bSPV4oGWCe23Z4N
Ft238zHa6BdlwpUSdVuX9Oy3jFnMuGHlUPilabXDMTW1Lik0RFaJQL9U9QUQW4p4uIe/WFbJG2FK
4jEJBGmvNBTY2SaRkP8JlCF3P3OKXQIaAGrombzsAGg+ndi7r8MCmK/0Cxm+GLnQjT/U/pWwNfAd
Iq1X2gHsTUUWcdY4algiLWulradoIYZVOlOpbV2M0oqaZJZCxPpPN+GJ92wJcFSfzYDLJ+KrExHh
/MlYxxEnqKHauH0kmSfAb4yuJQPc4y3LLqS0a0cejgll/cXkMSNmwVEHJT1e7eTT7kiHlDshWnIM
DWFWvjJcmmUWF9GLe7ch2CQOEWkE3HF52AtoOzGlYUOL+dISTFuzgcQIuasGQIsgsqDps02WaXBf
2VyD/zAZVDfVcZUJuqO5JAV4xjVxAYlslpbL87sY5bH1zaDqbLmZRzZiHk/JF814G1BEx0yNOHIM
fa9PD3G030SXfq4w60ZKbftAeU8mKdF4ZdM8hNHeiMCEgZH5C9DmGZ9tZ7nwsJyepFXlsV+hikmf
5O9tHLReoJogJ8mtmOB/YxGGCoDeUsg7zRm7QVhSKwYaWvQgvT9wiQe/wswCnYFgyTQWib7vKC2q
T+1azweyC22Jc6txD1I1oX9uH2xA4gHRQ99lefoyfuL/B3P7+h+souAc4kv9gnr+v8Rfwc0nPOhB
Q41sS8ImgaTXzwm7VJcBZ/MciZegmD3oDP1svCmEZbrN9DC6l/hChc/xI7QeO8qeQl6vatScqzg5
yt241qvDrqHQIcywDreXlAtn5VHgNX8YSqlNoe2oTdGEDYREcqc3lfVO+Um3cCsQUFj/30wb17cB
rvykJaC4WoHZtrKxuNZodzRsEUQ0K/xUO+Gou3tqwnsnTWy8fRiTZIEWmCNWDvoxb4qC6Z5mn65k
P8DtP/pra9s7pRP/2210i/FTy+1ONKTKxnG4hTzeeXgF6odeelbkJFB/vnjVemOa5wyZg12WnA6C
ab6wh2Fg6pvd2HrC+M5cIeiz+22jybP+ZOzFOkrG/eFLnXNv/X1u2wwj9bsiUkYvuv4KwOl0wLsC
TizgMcyA3rbwJmaUr4sQhSK9S6phT2PR2sdxzp+4y0eM60cpB6Ezzlil02Cw7tvJ1OY7DwNXbQeO
5RBfgTit03ORPl55jZKMwwlxd1aNjUFp5MMSZ2XAbpXIxcso2BhfsHTgkENiovFFgNu5iF1ihN+v
gU5nqAoqbrfKQG9Dhcs2Yztm/+tTYbjvl4Dyk2s0LghCNrye2BngCMecamGc+JkJ1SsMZsixogXE
JiGwlGSWgmmN2MzF8doXCGfGAuzBDWzteK00XAeglkj9nEvCTkgrx7RE/FdSqbXxrA6H8rogtkCt
q522VF8A8kUYVA0tLOF1tdweaupBXXMIceEbmrLoVMtpBqun1IeItFukL35KE3FJm77absOlI0Un
Z/8xxea24ytxgriSDI9s4FKYouDb3xw+NWmqEafpAtbgV8c4LgzbY+bKEwZOIlAvIYuCNO78oi6B
1rNibPaH5ezmVZKRW+bj2i4qLLyBuTdpuruNBNHQCnJJDv42wHdC2ZbLehJiIR7a/R654QtoGSeJ
AmFJFwfobxJr0W/pYVElJjVvizOn7mMOUU0hPvKpZIHqmvk09m+yxMSliHjHtjkm3unwIf0t07jv
+Y7rQT1eQ2hgkMOpWzxKB9+jGxNteqPxOQR4W8jyQXPJlq/dJCER9lKP4Uki3LlbT3icWx37FguG
3pPZ/7VcyJaiTxoeb7ubnXZbw+LgPrkQOwC+Q1IPNmWjvmmBhUEJAN5pe1gt0sUIUutYHu4bSYjK
UiO6ByVYhVamlrOvO9mWkoENw5Thu6nWgf7cUKPq2W+lett8NN35iw81ZVWmFg0XS6grFeyMs9DH
906Pkfmv22J/EwmicY2RWkVAMutX4HbaWmZOerCRZPVghfpJcd9kopf+qgNSqLhhk0TyQz/ovfvz
vuDp48LojUv/a1y4yUaUJezW/TEBJjBM2338KV4DkLEWgpSqx8JDPtOgV5zzPODaUiYRrAAe6DBj
Pad2QXkm8+f0P4rs3jT9qAIsRZWE3uLHH0rS+ZA7dVF2nFYGgv9Zoqywpbcxz2rhVsDEK2ZLfgUv
uLBvKZx5ksBfwpL4zddR8kmN5wwZ0fMD8DXlN/u6UV/JzmnIddjznK2xIAgG0AKK6C7iVkdNCfLk
iGLBYqimHyrRphfXGfRiJe3Pf6vtSHvXktX79WvcQ5CG8ocF1TMmAI6c1U4mL774Od1Axla74l3Q
KJXrLxnWtk958d6/dBX6MpMrnnyIZrkQUyQukj9BXUWzQJQnjqJFKeCpBWzQVomqGHOgTNsVekWB
/+MD2G5Gg24GrQzfs5JUv42tMuPff4RqUhUN4peXq8F595ekoU7gZ8OYMp8oZDMwX2qtuj9Hor2v
02dthQz4dt6ChmNtuTPDYPiE+mglOrAF5tybnJhw6agDnUBYIrd7lLwgKc3J0Z8XPIseknTR46oX
0fv0OVDiLXzsD6nkuB8z6KoHHhAk7w98SQcsiUAvZpZINRD6wTnV2KiVPoKX9gdpYTNpvneBcrB9
/BHt9lJMi8UFmsaRSgESE0XAWK4ZtxRAj8yUPWL6Lt7zmoFXfocpiIz1hL45nrgXMpW7cfUkRTAg
WsEeo6R66uFpdt0Yev5RtIF5t1iUhM2Sw/GFVCoju68CLCflFXO7Wb0RDi4iSBim+loAyX98ulG2
Hd7zyV7wS7QZ1RLJR1ru8aRtACEpAKxZ9SVeVm44FALoA3tZDdrEOUjx6c04FYrabuWOxORZjadE
kGC47kme4jf5m2TMgtknNn71IGnslrQQsJxcX+BDD8ZZ8rPntFUu20AJn5dSkEdsL4mo3N6RASY0
48gVg2Ypk2JqAQ37avWWXJLBXC/22WdjNlWPxl3FopE8Oln+C5GE+TjnwtzWrLWMg9+uT6WMAcAC
Y66/spfa70vxOob0bZuKC3nrWod6xrFQR8I36QUgqLUJ2qxWfZIcLqf7fdaNWiFSiqVSPDkW7lkW
hNs0Xkj0/t7AOAEpjTJFfm7PvG313Q8ypmNpo51NORZZbX2FRNT8R6iTD58AmfyH1okKBInskgKm
6Hjs/8OwU2CfX8qiSY6Hk7KXn2pv/A+J0/QlURqW3eS7GcG34SNXjSDjnNhbkx6IkLpHezZvJFbw
u4QUMPLul7jVRrQo9syqWiweNsqzibXSTefqiQ9Gz3mNGOzIVDPCKugLYlPuYKTFGvBGzFkg4w9R
BoYTMqgsXrmqv/vKBpYM9HPIiMyeWt/LfUuH45Eswzutl4Kzf0O2PObPBboX5JWDPFiFC7wqpw+Z
iK+WuNy89e9S4OPvpQpuyO+MAd7SQW8+Fh9XrOwwW6cWce5hWuYhgq3zSEf8wn1coqDwzkhVFNiX
Wm0QG6QvdrpBl9629uYM67+glu6b/+BYlNY4unMu1Ea4feGWeksbbBNp2fxupJ0gUm6Knd8vdD+6
wVib75q3DRGWZkn0Z2b1kJGcN5c9zxzUIIE5D8JNtKEHqH7A7Tja0A9ZO6kjf7PIOMlGY6ggleut
ybbI5gIPSKSYc6L+3RESN9w89MtEgHe98rNbOQIUM56aFU5gsPlTcRejhYCWOdAjRSQUopaPzxDE
sop/PDOZ8s9j3CbV3La1qR/at0NWL6edvDQf1sSkqt5qLs2sKoUnUGxoiRmOXpWl0cytBphaQjB+
9lJ0B9pDUQ9t92CzycdTfWHUFUi84niS9SgRYj2VCzGXLW2ZMOdpP5DFA13LAFczkeYv3HzPTAmK
oOk+Voj9iIhFmCOS+ALziAxoRpNwcT9Inx6VFFN7DySNYlVF4VDPSP3qHWvDIh7P6Fmr+Llmr4mm
QPHEgSKOBo0AVr5VtuvwJsdnMG/L/KbITO9uw10D93Su70q9paiHisHLNyU+1859kapLRp//oI7x
4ipu+LRR/euLAiGrQS7CSZP9QsbzF5IkAwbtyhYF7oNmk9/u4Hv22Nws7ZM8LuHMBWgkjHxCd0rp
721KVKiVL9V+6J/W1wPV2GnpNzKGLAci9nwczlEI+KH2v9jMhztbNV3h3zBYrl4lFN4F66SA5OKW
43rS6CXH1/yf8r9ifsU+3xi6AkNBu/jesbAXmQfBQV6OCr4g63RC7Mmov0x3PbvDn0vRhMPVauSW
qvSA1PG+rCZuFI2VDgTyKYBgzkaO/8swlIATEL3A0klRf91PmvjzPkywA2lLL/1y0C10a6egSmzF
wXE+EbO+1c2M2mKD6FeQGYG23Iuea7VQeqo+c78MSDqqGuLsxAbOHUJmScuCkPTseponrcLlz662
RbI1zmZp3+sY30fZq0/RbWX5VYlLNxoIi6lg2R/rxraQ6FgqP4tXhCCqjcGE1MSKGeCNIilb1bA8
nkLj0z0g/XTSgTO4Doa263ZTc1kkcVwTxNDSxjz3g2hDs0rzbQ8o9u0Z9TC2g61Xyb47U86Rb9+6
Sl4Bb/4Qk1sIKj8YVc4MbkkHngM1eU16zGs80A3WUny0MLJkRDtMnXswb7OD6V0X7zCxvybxyysP
AZzl/jjXGDTgUOwSSoH4pjuYhtiqWUnPdQiidVEVCAN2EdGhKRzb6Mr5B+/QOkG4VFfoQKBsouwx
n2Itg0dFmm77O1faRMG+GaDdE8HmCvFqANXOIvF/yL+88vQQ5L6NmA/n3CwM3tDs7GnL3hBNddQs
oAzklrEXWLAn24lit9OB09fpbMUsZQRRgmAzwMdExu71U5a62dSk5zsJO/AWJYzRkir1WuPa00ZZ
yljzVogXP+31+YrNESQxmCFRtuctUfENuLOYR8WNcoUhtPLxI7lJMLEXgKD56eF1ZKa+TzA7LH/j
rAiLkMn1TuOahW8U5z8hmv9QKmpjVFBDjJazFUWcVNMIH4iUjn4NsTe5BUSZWwvcVWj2zhnc6Lp8
K7/2b8RNLqE3fX0/VZz77s8O2AYjGBGf4HT5FbRK9MZ1G/M6IA/TVnvlkphZ+6JjesYwMBpIksEQ
E5K0N4O0WvELwxGW0aij2KmNAvC45IcbigUo+H29bxNaJuYPF7pirfjQJOgRD350rIReyAHyGm2F
VgbG2IZ9WyQorpZ4AVCoSXNirLpgoDj4Qz63Z5t8+4es122gH8CdxTwcOEKAw7Cmgqxk3tF1GzWg
Qb4ABMQgeg/+FqczzqrecveIfBifCL3aHiBFCaKUH9Pd06dVXzunSddnlkUoFDp5dCfFz+KOxy2j
KNZj7cd4V9eT1dquxBNmZm3ALGJRqptGwanLAkQoa75YAVms6hGiPXgJx7flQeg75m6OQlpQtOqb
4Xx4A29AtEkNuI6rnM6EviClMeBDCo8BRSAnnCTHbFCcYoDq6zmSvj+VIdj1PTJuwRzuAWjllDau
RTEcOYqBpLw66PVLKiyhs8rArf4bgVQ51cPaoFbHXNHhYe/vaNxG63bWqPB/bICIxzkzpz6xAF+3
6xq7khY/Qu9P1/ILofR0dg3bh3lLzWu9fasGyad2BeSz1q5/iV3jRJLtjJrnpJs3jrgGzO6rZVHb
CoTVGrg1G0cf7me2iJ7AS2euOKG96M8r4dmRjIhhYF3hgFf4vwuEJEneg+sS85PMsEFyC6WkPBI4
qEDA77x+VzDdax+Af4iM19f71jhLWoa6rSDQrsbJ6Rhdb6GzKd+nP3bIrNy/OT+AT9voC+NgN713
e4Y1a0gX+swpx766Cr0hKFxCfVMpvD2gsAomtsDrridBx6ATv2Pu/LtcvOIP25aC/Ug7dCZG/bFx
qEAOGgHPKhrqOoK4CUYB62b9Ex2Ab9Tfu1fgfTSWpYmdCAN5mEBwxpT5ziK9FadkpVHXJonxm4Dg
iqyPkrBipSMr78qpwJGgcr73sZK97fD3bBJCzd+WBXb2gvtTIdssSRPhzmip77Q3U3Azluy5UmKb
yy0VQSE5jaMkFBHDItmacY0MmdGyyNzkeAkiQcfIdT3xPuI6T84gP2Cma2CwLz9zDSMSv4jAq/r9
xM3INWnzznjr+eJNMZ8rE+j7ye+iNnS6agUslZM85d0fN4LcxlNa+TBh0jSqZ3W/RycPxw6pMiYx
IBpKqgBYFPq7DeMNPVKpzfVQ0qP4DHyU0W5gU6meXqDv8fNGyz8g0QXrtmntl45l0OfvQTZikLLm
KT1fom2pHyB08sS5RO1CvsEX7wUoG+GyA8mDt1Fo8v737Q4zcbxIWKiDRh4oucZ01s+swQo3aD0E
CUkjmqJ4UoJdugaP/Dk/Bnd214/k4npSxSjSFl+a+wX3j+ioDQ8akx3rVEY39RJKgFcxhXsx5XAG
iRsZ9r3YT3FxbW5ckA+0ao3tRUcJvu2tzfctnsK+vP8x2ZCW6yONiwwrc6ji1i+CWY3DfnqoPlOr
uWWiZoBm7HAGiYm5z6228rwc9lz5vYR1LRaJ2ApMqfWtJA239bIRzY367MI+LrmSuIXenW6je35n
tByFfSvOvmm0dqGUHLAgktBvMcIKA4/Hmj4O1y1mzpxX3hOQFJBq5S7V09PthTM2US/qSepy8CIm
qWCEqMLTqecrxI5MBmE6w91jVpA1RD8bp7qml+YFmLc+yAVBmAo9VmSfEH5Q1ZwTlwrILdF40XZO
H4JNbPDJKGrcR7NQOOm16Rdp0IOBH7vOnCQoGG0j86yWGi6L/H9awRTzWouZaBn6UM6IZwdTS0ep
Y7XJwFl3S4+IGxHp6jg4YOSUUVvsKwOWm/Gnu5YBMuhI2auAdg4Rc+RLFJRk68UL6dj2OXkE7Tdq
U80/q8wDI8L+lnpA6rKGzblFofLw7ik+OLUvgb+cgNP6qNcZeaa/9QVJK7JJv8xCsgbeUifqaYgC
xEhnevleUTxCPxwmxCg2+o+dGMCxioB7dF+4QrSp8DAi3MzgCrmgr1Q3Z2feSVCAA/0rH8EXGDZX
znAf7u2S359HzTxycgOsG5HC7BpXruB1xPQ3o0wno2gae3L0qnGbUTsE+P7RrzQ33OR0JbNxy9Ua
men0C5zd3Jv5I6SBpsaWPov0LcbFmLbew8cVHxd50sw72e3l8KgIwD+/fG/BC3rT0U/ONiwukLFQ
q+HUnhwRq9Nb+so4m7sdVKgmXAsySCJohF3WDRr9TAXBJnCGAXDdjDije1sAziNd64N11HT4wl8x
eufDZO8s3C6I+bC6pmo8vK+eJylWsdp/7jb4iX2Df8RlxRPHRR8ZroKhj6UrohEKbWdn2t+1o/wF
1P3YiwDDAjDmInsz7aAHVH3KAKiKmPIqoHQQvYXgWpHskKQGT7HfqhUcGchS/GV5m7DLv/38d98m
J9jjYj+1uUGi3IZD/eAiSk2qNdq/01DGnxACKe8pb84jY+yGlUqBmvv5rOaHnG7D5m7f13Q2n7ZT
poq64KmOD8tiSKgVzXY/CzAyQkWZoWvgNFaq4xkJ1YVUWuPfFoE3IL4XyO4U6rEiBCOJcksYs/Qp
qTZhh2Pf3KuIngr9dAQCEBbnPrgxZY8+PRmvMeTFBWz6Xm05DWSw+RHAqTxVV1g0RBYWIP+wM2yl
Jw5CxNHBnFEOop7t41qK8OxjeDudwbf82T+QgAXeDgO80l0o7fgRqpkUj5D/Wf38vYfyRzPqWnK8
/oZBndUFmnzBy9RM+gWMPwEJYwlZKGLhghBVV1F7a4lX4ObwYC+O16EOAkvLBV+VUSSm5TV5/dJy
F4JWGU5eaxaew7ZowIHYoKRY/6qsk9FpejBOaXysdZoPbUmNDFB0ojhzkRVjvdlQ28j2MlfpBjr7
iJOikBt0LrNj7e3Hn8B6tKY0uzIfxzAC1Dhlnn0JQEkHWWjIUGAwGN7Aaim/Hm+VzleI6bbZrz7H
N4rVsU/lb+2oPATKqqY4llIV0yPKvf7xj8lfWuyUW+P+HnOcq8pgtBak1/Jbphjr7Ymo13Too2Tg
uFiIJjOvRF9Pvmpde9XljJgZIRIBHn36WT7maPw+cCx6FZvrQ7gulr+XLw50R4slqNkfpI7ID5uH
U7BgtO+51AYgOUJflZ6L82ji4tbW4JnonyQHGQQOgqGhNSgB/pSXp9VMmLrybJC89f95skdZeFCz
aFXP/Z+B1DiIkvdrw5BEUyvVSSnEfoXHTJLJy5opP4wnn6nXYvvNhg4UqjY9DfFqFfCvHC0ab/Mk
h4YZHmQY+PqSpgxTtf4f5XsgcWSIXxuoWUA1Jo1/lVAQMmIIZs/qqFxintOhcMS9hYGcmyNS3YiZ
gI2a03dsSQ/z8munVi4P2+NZ+lFGbVWHN8AQKbmUCBGuCEfGFkU4rtWb80gP38eBsxSELOdgS4p0
10n1FIApgenT83fOT38+qFrfEk/Dy46uOnMLhYFz5L+mFfCV8YK96zpeOBSgL2FcIdBtXlJaEvoI
QzqLtQanOniheuezAW/gGC1ZUpBsvy7zLMdhysRn/zry1nA50OWfZRwNJQT4QBA1Syr0mcdc6rXf
uI5+f/GD5MzpKBK0VhQmH/0D3P/NtGliYSaI2gwwOWI+SVRvg2k8/pwlL3sYJXfpoiGgb6Q95TIS
nTzhmBwD5APoYyn8ZHQHT+uFjVOzg+AMPOdtEuEQxLkg+s16k8Ra38yCIFLvfRSHu/NzUXOsC/zD
/cxDGLV3mI8qUZQx8FVZZYoBOj8Nzjwa/IswNml4CtwT28lYgqUBKp3vJKzx4LGEKn8cJrmBrmLe
/zgzAreHsaxsr7EMAv9hW6i9g8Vio+WWOygfzplmE8J8jb+y4t40dnSTTjjMsb0+vfHAq7+V+Mvx
2Tcx0GD1HyOqwvv7mr6j8ZO0GJipoK9W0e+7Ub3cXiVdsR+YcFbpFTa8gins+9ZRkCKdNYPUwe06
2oGcbJeJXiIHzZ/CrABWGrwu7+5J6vGOHtk30L6upJHuN2f2AubxXN4dMadrX1K66lv2nXB0sorq
xGKTxdgLfdSZo/Znzb0QzKz9tHn7pm2fP9LrmI9teZ1IhClL4P3f3tvRg/VQxz/XNsdw9eVbokQ/
5wpewyfKq7wyLc6Z3gDJHapE2A8jGJj4h3FOO/IPu50X6PAc4TXVc4HCmNwlWc0nM4kCYSvs3ot7
wWxiJnF92BglqOiRq1QzAlrgxy1rMOBhbEbNrW3vVlr8ooWnYJH+TTwSJdJMAnB6mnzuWGBIGbTN
ix3tVI54Ut2HZlCgUfto2ONEncofV9X8z9bdFNSNUpDg4SePPazIx/0L96FHoRaNfxmwadxa8OzJ
eM5qp7nKsZgQd2lXTP2Krck0nJLQLo0y3XkANsBo3+5D++Fa1xko4jj/mog1o1Dm5UDg/BD6Jkc6
dObq4bVqkKff6kPDWYCn5ZToiM4IElPCdgtoNWJwAPYZpsRZi59s8rjsubGOJVe5Xs1rspN6oYov
NcGsqonvqJvnSlZ9EwKLg0taMu8xqJThMRIAniDVem6a7r7LDvZXMRcwMEWHR2lWsYMlhgJp+nHR
blnhhSOq9c96Kp0aJNKqfePY2CvJL4tehYiBztPca7UWpFWtkIacWwyidZopqZ2flzR4S3v2L/HT
cMG3bn8xjI/0r68EtplLPX8BEVldgTQ4Zl/cjR9vbGFd1shm5j+v+kmGuEBfQYc728w9XJKp2Mui
OtVIzH/In6INxE1wPlzqW52+PcXbQBrhT2uO3adH6v5lhjyssCVyLL9uT6jr++MezbIGQH7XXGUf
7WcBUiCOpn+6RLr5bPF8gGf6UFvHvMqA9YSfoGga+URfzbim2LDFwTn+ec4MTGTdM35i6qWhndj3
BpuzR94Qq1G+yw+aKgn/o8Ti7k8bd/oY+gzjogZzhNxUpvZktE4aaZg/MM1A9iqqZI/RfbcEbmOq
sV63uiGpY4vTt0ML7FshhghQq8+DGnKYJmFKKevY/0jaLjp5EACnoMfXXXixJfwxHHFxFh/bIvz6
tBhgNFlpkl2Njot0rBVD99Jv+d0neRitr2GY483b7aYR1lWyLaiuBUOO9pQ8uoQkVwFaNtPmVUcR
YFK+K081Uv5DeMX9uIIHxxDzFCzHZUkTJajeBP4YZmSEaVTTzNMf/pMpd3FtqUiCiXutacsCttkN
dXNGrXfGcUDN+kSTMdfGHFRgvBfqyoDJz7CEXoTyBgvlxhvvf6Hb36q6DgnD42gJ5qrz5Cac2ZnD
RdYQnHi0YWXhdstyVsMoPNNOC+8pATx9BWmsJ747Qe0E+gPcfvd49oCQl3zWxIMlYsy4WWvcG47L
A/IElpndnZ/3O8TeyUC7BMTiLWGukx0sJ4CO0eIed3ECHKxLlkP1CpHYugsIwZT1nifiHpTqRFk9
wDzX8xxZn8GTAOiDRWfwEoWchyHTzfuFcueH4zbsAPw0rsXDMC1lCiksj1dcTNuGwKeN6pq6458T
cs4t9mOTZeaQ7IhOocsaZyP6YxkVJn3Otaokv8A4ZTvNYigLfIeroEjKM1iHSSEuJ6LvkNvCvlpg
Tr4Si7z89I0+j+Uw6mfGfRdzzbFs2SnX61eMLq5Tiu3QkVws/FFKYfzbHGk+LU8+8zzRd86mIkkn
N1Dck4AOXEiU0nZkLiWM6SbUhCP3Bim3yD+8LYiuld8ziCxFwM04t4JF6ptCc9lMESbHZl985G6X
Mc/BvJGfzbJnJO0kH0Fvr6j/n2MwntK7kBfmXk4qkCwzrVhNyOhqR+SfFPVvGtMRVKrpvDzEr8YW
n72+jNbhYuMwaZnM0+Ur/wqKw+539T3ytEUGWoMyKQFkXxB7g9xbce+izzTrYxIDR/f2YILyl1bU
2gM6uHAjyXlN8AEeMzBLSbYAYfFPSMP5JIU2jSDS+VgRs1UHw0RoMZXm94VtvmodqIrZG1O5/3p0
dLjf94vWyXnuWVuJJ3NJ7lLn8/soLYC8AWmHjEzbsM0uyRq5wpQdaMHtxsaZmaUxYKCwDHT0R7CT
/qLFvaripcKXP/C10MAh3NoZP224dewrEgxkhJ/cye+YNfhUfN8Xu832f16xAIQwWOvkBM4NSOok
TWLbRTcpvF9X2H+2UxDO+k35dZzkVSqKSsfc1LBGCOKzoyS1WItPebJIqjb/HJxnet3+5Dvdpqwd
ArXNMNb6WMyPDZASDGIETueS1yNeEY2ftdkslwIZHSqXCy/upMvDqe3uFN646ijmaxqrFUxKWDIq
BJ2ecbzQvKqSUgPoGeJmY6dO5cjBBSSsmZDt+vtpbfYz+N4G3nvq0YOlQNp6nI4lMg+RkGoqjvRM
4fRxz1h1jSOKXLGo5DmHILQbYTmsEIjeG6YQ/AMyBEFnsaIbyfzoOE9WlnC+Bfjr4ADZGGVTfv1e
jelOF5byDusRYJ1Dw6BSs8l4WBPZcvOc83EkfxkHDvQmciqs9jGu2I54O/Y2FJ+dH10OKGLJVKAY
mB+Ap4O0o9C4jqN8OMOsQsq3oGrDw2c/Vgc11b7zs6Y0t4RIWQMvmdxbKI6IM9Qdgqw/D2oG20Ht
XbrX5tFkBQXq6u6KxZXQqW1sjULptZmsvw1uprVZRfi7Jbg+bmSAUbwee5K0tdRbgYS1Me9nHAEB
V9RCOKqhFrtKcKfRFlPSFzDGzQHTbP3TW2RdKtSu4UzcML6NtSTE15fnCEf/vgcpWzFgPdrGZMXo
uv3bgUN8Z9tdUBIilluAM1CbJBfdCsVJQDugxRkhkfaIVUkXZUZit07tLPXN10Boq4T9rqZ4XFRH
ymm0mVDBzH+PMA/r6KeXyTP7l/UzNR7ochn1u7h3i9dJaLFQVfpLZax4QPbtp212w7QMVm8wfTFw
zNUzdkBejqBCB2si8ERNxcPgdlGK9R9PqD3qnyAgCQy9EusjMEBb/LbvoKjNdSkW+vchkHlhVAcJ
3lLSW9/GDY7He25fzQ70l8fycukLkCsNehMfCbNvetN0JIo4XqHEbtyWsXs87pKarsVAVJs7KKlL
FkeVfVR0h1R4ljx3pCcLYVlt7XA05E9BNrcK0QmRln9gBUOjvNv4TyUHBdMhdwKNGGXeAeh0H8d8
HJrH8eb0XcRa4VvKWAAiSKIQMNwp87Noinvw3HaXZpvEXjRLaQ+K3d9Yv1GGKQ9UzT44nPy2AKYL
2FCy7rZHkHsVgiaNE8b60v/ufdO6KOjHbZaQZmR9m71MAf1SgmgfJOgFUEIDWO9k8nTL23/mCJuU
KsYKioHplIdcLvSy9NsnBFD8rCvwaulY1lFBDkQXUDmBFDeQ0weUDgr0pRNWb5HIun/zO3sZVXs5
ZPLMmjt6q5ieTtloqxywVVB0TfWuYv4an5vh3p4jHpsjE8LWDNmrABIcOAiYeJ8Pi2IjvtPq7v7r
VU2kRTewkIOteZDl6BNZbpUGbqqnzD3TudyHjZ3Dmn7zMnrkdIw1uUGGIMMAkk1dhWbTqO+G/2D7
EFmat8pfwiLMx/R3YL0fT6NKw0sDkYmD3MYspARum2UVDxIYo6ck8CeVVyexAJtf1rln14SrM7wF
P5KQQwd8XlFMEhpdz4iJ9wjllAIG3s5XKI5DhEoT8uKUKuwZg17ddZYdVyNXgTNylZCDp+TS90re
cxN0Sb7dEm8l48rkVgcIZXWtspKfj1YG1D+0dEfXMxGfU2ZdWusonlZYO7KSlWgknR+T0yQfoC7m
oGOhszYkAWBUM3EZf8qG/P3TgWSiBiQXfr0L+X73Kk83ot8CZem87oB7TGya0gwch9FN/j3lVaR3
kJyRczy2xTI1eNZuFtQOOK98N6DICIwWqsjWQjUfMjcXN/x49sKYIIVt9c8QifSo2Czz1u7ZzoYr
xWCfWMwjxu1ZQpZntoaklf/VHJq771t1+mFVfVFrhwTeWOkgLzk22++nB7sXKXnR1ODjIZ8WXXkl
emRW4ktBjKVB3kyMjs/+rZ+N9yXzSa6Z26h4fXi0qpj6/KVP2MYdi2kYszIFC6aX/yMZdvs16OpC
GM4zmMn6NVIyczunzIxZspYKFx9MDtJ7F3HyCR7Lybv22gdaNCOgrSgGQfudpqa98sAurlJfKaN9
Ykso/dqaMcB0iGQW84EtHXzRoXhqIAai/TyFIBNh+25GyjELpXdJ3onTCEpLZbZdK6QGtHyRnbzn
N8F0qNpIy+8ZhhhLJPuj8oy6v2xtkMoBhYZ2WGbzW6MAWldJ4D1aZaeahPoNAoZ0Zcd0NOaCFgmI
0qa2vkKxesn4TZbuTVlNdwmXK13j8KccGfzhe1+7cSkWAhbWrehCf+7tclGiEg/xw0S65UsmNHTf
GAEY87sqMef92q4TI0UdRVTDm/9ITM5byV3vrsZO9u5px2vt/3rCEEuhdVLqA/sKAYzJTZ1XkMcm
BZfyueq8wui8UF8fY8VQe4NiaHZh06qWChfCrRDkylxveX9H0bHdpcWNlkGpxNqSFppPRMdbEz/1
2EJ1PBBYgqH9e44xIBgL0Zzt6WwOhd5+IMRfoIdgKgxizdh+qCWtiOCFwJHnO/SJODe/4dvrbo1i
3iebhIiU8TB5jeikWR1LJ3s6iBwYK9uWe1kJEMHrNbZf9Y0JipQIFj2ssRTxZbU239M4O6IoLG4v
KkasYyXohGGRwolXYtlDmz1qFX6Q7f6AlZ8VaxkguKrZ9mMalD6DHtYpJ8acELx3WDxZJrO2F4ry
ODnMyCNVjcGJnYgHD9y6A8JZyrpMhxE7icYxoGMbDwPAm0hZXhBunwr4L0QLdkPdOvUlq5qUnF1V
y5ptHgzyCrADD+G1iEMlAzlP8NhfwtzePEmTIjWu0ESBeBO2uECmGlM+PyazmZ9wLPSzpPgDVSSJ
OZZz68Nt3vvXg7CJgvdxNlyHK4MBD1eQ1BgQIpMm0NAn8eL3r/zHh+HsffZ1MwvJJuy9jIsbHNDZ
manm7qpwPN+2H2iAyPEF5bxctKFY/zFsgOPQfS/MHdY1TnwZ1AfcKBPyOEdEuffBoqRc7/+penfk
g+G0zS6hc8hUSScaq3nbZZWprdsTvIg38OolqYov0/ZiWVe91NC6VjmPaVYhxG+3H+DB0rDvMsYV
GNX2hxLZNS7ct+ZZggsxa6wzRJMr/JRcLsbUqDo19GrWiy/sxppzbPYy1qkQYOxvMZ/vgnm4Aj3f
kALvExrrc9DkjsjUZEboZIok/B37hUKoSkXJXYAXlx63kzeb2tc8SjwMcztJN6MlL9eGO0/KDqaR
Rr8tNLe4l88C+LIIXMXH8xVwvYL/nCg3RI3v+zAZ11AsuyjufeSwZcsx7EJY1z1FqDSqFp6ZGaN/
mGodkghUn+pVoO0dBEhRdfgY11ljduGmx/VlZl+xcGrL7kb6gfdNVqO10SSW6+cwJcYCEa4bnLR1
Xtk56RtS4UgADGAXlk9aRiQXB83b+FmP9sOrpdosVrfXdP2QbdcMyJMeSDFvyr0WxFeRAID1HLsZ
48tQriXRA8uqnfbclq0a/bo/DfVpZp/Ad+ORqK4uAxN/VOGD2tUNA+xhy4fw52buRtvwp/9oPdTC
GhJwcPNL8hphBRlNM4jXrmFTIZPRNzWbGibW62tN/J9Lx48WaChBs5NkCtOsHws7UKoe+EiaxMcC
O4hppJErpYWds4qbGvt17N6uqG1JoIeslhgW4BuS0bEzyxCGTJPccdC8avlKE+ZtgyL9ewfmLAvZ
7KkNLA7VFcvG+UOpfv5kW0u9ANBaUfs5lQFEBUMTS9AFrM5tKNqilisbO+aV5lQ1W4j96M/PjVPZ
KDb20MrTEVmshoRNOy3JaiZKMkwIf65qW4HE2+x2sH7f572X4xC5Y1w3w9jpEQb8B+oLsgv00Cqc
IXMoMFR+okK4n+ZuF1fESUQ2cvxU1wXwTt29+Yq5c1SU4EpVWGwcRCevH+TuwtcEFuoAaUwoiA5N
f3/AWZUnLnrmfJhvkXhH9ue/LApO0XW0reXbDOWNKzxLE88fxkvGubHZzU7CihMmXR/O5sxOi93d
QIDKwBDLXx+vQoQkZAyzoLzLqzET3LW86O4KRAYyVNJ4mnLcZ4WunZw20FkmLG0+jUUtRDCWd0iW
po/6Y7mpp2AoNoC83DyPpkCMcxIlqoaaXZiaEDBkXfuGuSy7SKfnFKmf9HzGEAK7aRWkAe0klxHP
WNIqap7c1VnqkVA0/gKU83CsTX/Ix0at+nEJhNjfh1+HksT0WbphzwNNVcBVMMPAnIrWwg1MvaTL
uIX66FfSobH3kIjt59kGPvk5FYxOvm8wkZDwmYCdDlDQdiprC3X/zZAoZe9IuyzWu5yLwL7zhvEQ
y5zegoMHWPPeekSdKuixMqBJftoBrTvPcMxW1R1My78+aJx2Oap9ecSr8T5gYzO+8CsUQ36OOQtr
m6KqXRQAybvylBLBqBmH/ns1MMWd9sidU77WY/EaVbdHsjai8XPrtnU1UD2u1BRkA4AOlxX9Yqa2
ksCQUNGAz+AfG5o7awx7U0u+DpCa6xMR5NLnnS4Bk735orl1BN1NsBq4/3mChgC8vQpG57m3n36B
cZ/14ExzMhjZmjEI3Cj6A7ZHqOezbbVEt5/Z0xULWpJ6DL/71KqnZWa4SBCu5s0pNXsifnHAvHU3
wq+Y1G68LB+4INaddqD75DrA1sMAbK4MWQjK/FvFSnpvTdV93FZN/skXvSDof5ImJmgKiSAxe0kF
OPerEz93p7iIl6gw4UKamHbT5REOfS+j7LTYv2LjUd6nRQHq+sKPdANcD9WCz4EaF/MBP5j9X1wG
C/p2dKC2YfIZCNxLis5kHIUmvPDkI50OxMhawozKw9/p8xi0vcQdE+kNrOgTIdhot7BNSYnY2tfu
mzakMRgYnw2thPSbjbLUrt2dzmMKv0EMtf2QYw8/h1hmycq3fZcmu1FSOq9LLx/sX/st2RJow4gR
KPzY7KkANO+XbSE+hR6z9FdSADPNX2ZwxJ7Qel8Fy3Ml3dOli1jya9nzHnMru3zLP4TAh3ae6sKo
S8Y14Zm60Dq5TBZF6KpiJQ68dZd36kaZsHm8Hhy8c0Z1sRXJw0AaC50Hh8k7kIKH1YdS8EkCsMiD
Zr1czTlZMsgwZVJx29Wvn9vJuHDLMXupXKFL338fzkw7tUg4n4tPRaVJhviF+9WIvxbif0gQPuZ3
FwllmxZZknDugxuCwhad4Vnp3jQBw74sIhm0xpqWNlPoMSc80CxlvTtCzf6+O888Gg1lfdmOeE43
M+kd7/ayEnGvxj3TSeZRZCSFh+v5VbjNmSYxWrQqdmIyQOQDKrOEw7pSwc8fK6GZ6UXfTxIYK/Jj
PXHeK9v2QX6jv7mBWYpcvdXWXXlc0XhmN/kQAyhOBdSD8Pw4Y3uEiLPk+F0h4YDkhQsaIhIw7+sk
udLCN2lSAKxaw55vvIgScYCahophVSnZkF50+id1FjgE6/48ESKD20XKFwHbOwHJMRbYuPLPibIc
9xKg8b6CpvaEngc6dVFIWX8OwYg0DRS+7OyGD1AQGRJuAhe777WIk12tq7sNXxKnA9Nl5ThiXsA3
4IBR7ZBB+cB9mEM+p/TPBE/N5m49eJFiCHhoqV7+Cv8pnVPKvcLj/FqDfHGpoVAUNuocMybmQESm
F8+hfo7VMcg289qWJ5b6I8lslTKmvhYIYZpvXoscHf24OhNMcnWUISJTG4rqVEjyXJ6p1WAXMxkz
evaeCA/0bkC97Z1i7A/1+qLz7i6/Zs/UstcBXo7wYJZFuZMYgMjOFcDbaaW8wqJ7hK97SM6niVEd
gELBYsQ4+64KEwyBwSOR/mBuifof2DhDpLoIFB6Wnx62s1EIJxgJfujBrg24CXbdDftGTifS4Mwi
uoj3CNCts1xt4t4QohJXYKOgXBYKV3g4P0kR8geTDmJX/gqyH+4NVvpc/HFoUg4401qYOhG6V68o
JJFTvbxntmPS0viD6slewOO9aILOSmAARXPs5s/eDYNnCXWPUR/exqaMsqdHk/AoQFKkYTQ63M5l
vg9tDs9YdDLJY4le+4bOQgtGi/j+VuNh3L3cLs5OTz7x9QoDB/lbDZiUCUWKtSg/I+4rcuYlKFlF
uKs00yqzvNqz2JZDEE2rg1OTYtOdMw44enfnKxRb3IDDd4EM749sATNYV8lfOeIk8bC8/MGBdR+7
ZNB0pykBuAzjryYE3cZsmlvMm340ny3zPLP9EczFw/KNYqX+C+oYk8mCwlmsHIutrLTX4PFTW4Ns
Y75VZ2I39pscID8BxHIpJAoMZstZ09aXoJutOaE7RQyB/2BILZ68IqLeXmrjPS5UEYDmxwp74ucc
3Ew/T3hLyp4b4/hXVObNjqzEIsaOPERoxxj7tyJwV6wDXR9YXrAG5blOWqNgKqnvTE+94eldUXHg
KCgYQ6az32KiCIdU+TL+O1ZHjU1WvqV8CEpn1FtoAXja1XH3jKtZziUcQhdtj1dFsF+E71RvyRNl
lAydBRS7NE9A4rf2s8SZ1HPR9mMXz+O3BHdPl+dIc4lQ+eUyBDfFg0dDZkwxFxnBZnwUX2uzr/qW
RtifwF1fuI8BRw5Q9MkRhEwdEtl8L3cfuruh1QnZOx91Vqv5s3+YOun7Xtmwz9HOPe5jb8edqCRg
cxETIyrmfzuRMa7+nqQLFEiBW+OW3L+htudOxYWNHXDuGKj8Lgpn62I2cmLJftrTKMUqhOEgwnGc
R8CmeEupa8jnwJ9IPHqsjBiUuehIhSqiIxHA3WM2M2azJ81niWgXCkM4zfj+lx+A5Ly1efutl5Q9
GLhMml4K0Cs8uz2/u6I9dDRabd1p9A4nxpHZVjUtrsX8LlROCNHftn/YrvbgLZDVHVw4xYEnZkTQ
zCGk6rZzvM5Gval/85zzyPWcvTFuC0n/BXPRv6mgYt4JecFBlJbzj6zPzuaRHewiGM8pyaS4Hq5j
AkDWDJ4L8y2PFd9FaWboBX79ya6vgW3PKr+M9GFsQ3TAwDaFUIRDd+iUh2ATwBCQSgpAQndOYiHu
blefkBGu9Nh0DQjniEfeiTQ52umY9laP4DWbQrNFWx7n0vOGRPEeJ0WiXRFsC+X0Avtm9oCvM7Tn
FqaZ5F8Oh2vHbyzkqX0r4qfdLFB5sqPvAedUoajeACWB/xjkemGQK6ZSJJMaSyKcjhzGSyb1hqe1
cEta/v0ynrj89aEEU5C+ePDABuOyCW1+cY8kgNQQDbvp3IQKKvuT7GEYQB7yy9IjZo/fbe85jGF0
aiynhnxGXubvZ1iYSqEHq+y8BX4JDkQJfFp6Wj0sHSd5mXuC3ch/TgQyP2GxMOgB5ioybmdcL2SF
nVSx5jhJOT7++45fcQGvqpqLQgEh17lfLY0f4zvBdtMrTfcyHg2lrq3+UahgUoC4ae/J5dWkmVkX
h82FZd4nP7X04W2/odCMwoKBToNqLOTcIywEP3ar+RMw9JNd1d/7/mbxXpoH+SDzBmlpSOUHX2yx
f7n3Ol7UFvzbD/EW1wq+Kg2mFmwHf3o4z02gF6GPKLeXCoU1ZYxI/0qHe6Qp/enTMrt45KvXD3y3
zCoX9O8R4BUiDfRuJHRjlcGJrIcrJcvYJYt0T8w0Jrn2yBbeoh3cCRKNU6xki5mNLPuW0jfbGhUo
Iib1JaAK/yXQVAkyGVqFS7ewbY0Mi1cT9gc3dba+OSmpsYlNYauAgVzlLe+xzRWHwTF6/ml3X+HS
ymNwdHCpGQXfVEB3k3VF7zbn6aR4REb0y0Erwbunmhs9E8IowPm8nGWIMBekhM0BigyZTOdYgcma
leIufccmjysqd4n4qCTUN4ujF44FtmdXE8XpHjue49CN/6B67mLg0nltVBJqTYuUQ42pXDOki4cu
BFVH7AzpbQdb+rWAuDydV8fhqJuowWstuKDSWe32ZRLtQRSKWOEuKTFZfBbL4iLR+npWHUM+R2vj
5SJQblwLC3ilUw+zE6b4fj8Kxk8EK7fNv3UlX8DwrI3SF6cuxbpdWVYrsTc8uuYtVMlIRLdEXG7W
GOd316L5wVg7Tqp3Gfd7i9RWyPxh5dM9zW48rE//RBcnP6m/q1/GK9TSTUvxj9k1PSPu5hsoMevx
z3+2J4B9Niu20KjumgMXM4M7V1Sb0vB28NWIDY7END+rlL9e4tVClnXLLjbjtAUEVqb84tML8Uqt
CAOZNi5HaZ77VVheILAiGTW4Hl+eyd6bU3BtG0fNQ/xaEPHBwCIB66Q3AIY14LKJ8x3u2vmEiQNd
j/Pk6BfUyVJa08zwsJEDWRR8zdiTE5nV9BFTebe2dsP+oB/Bwcz+ZYDhUxU/usj++NMV7GZdNkVt
ns9l7yw3iBrIJDB+141ZratTyKgy5Ea8/KFR1bpi3YCeNBnJyFFhQun7JX4skwerdg3/sZam+P2J
I6jbMok55NInr/SGoQ4b1aR9V/GIVwuI2f3PB2DWpTjvW9o41SLxzgXd3iR1e59gD5+Odpo98xw4
AGllclDGH3WprXPout8kf79ULF+OFKthXOlK8i9MCiqEDfCRpBLrZRmCAwe9/kO9U0ufWTFvMhWk
POxfj4ui43ogotO8R/5g+skUrXXAIAfK7aBPCZF/D/bS/8IA8aleeaopT8xNflyy0s24hfV69j0f
Hnw7IkASaebT/si5CIkV9qYR1DWwRlDo6btAmWMKOYRSa7tyCcokiLsmoB1cR81AO6Flsqg8DC/Z
IOtZlynTrRJuvXidNqVyIoZ07ONop9QBRg2SDVtC44Grx9UxeEOWkdG5KiZ70Eqx0iTGSEOdzdTs
qY0bgZC9bc1gIhbLtbVLbifcIf+3hF5zGDEP2yYVGpTm07Ts9Q9dUPO6KW1KpQM6HJjXZCBpRL45
AXhgeBsNKLK31hB1pkvwaZ3oILM+vXMj6lWTLZSnu07S/M2zlGumYlN/25zOFb146U27Q/XCpRvk
sNEAcRe3FPUqCAu8eLaDLf6LWuHt/MlJ3EWY6Y96E9A35ViDykJhy3tuiAKeDtSpJqnd1RgOgN2Z
jj++ZyDXuk9YlLJjpESYKanpV22VQonBt0LY/HrOuayLdfp2F2asM0SumuYXxHS5D8bizllrSvw8
bsF+H4Tx9v2fBOuRmpeFVNxri+WWblnvmtICLssqcAJJSir93RfAE2t1VM7PlyeuYRILFqJsnrYU
mt2hWhF7shH5rHZJnKfY19G3qIC6e9wVlmn3Sl6QgKu504f50pow3332q1/BOHi5smiIRMMZU2yF
wEF6KTCAZ3MYFHN/HRZWslctCKq36ikKa1gwzMDW+3T8NW6gIx5slBTWcRiRSzThGb8PmkqeHb9D
kLt6boPdZy/grsd4v5yeXVNB+TLitV6wWgdZzUF7YvQK9tzloYGfWAOuDs3ztX3pEJHCSnOKaQn9
IHGHzqcez6EP4cN74khEEmhF0Pcud0alMGKgaZAac5SuhCfzg4R98nRefzKe65uBE+kXSzXUHsUn
LsX30+V+TapRp2u+u7JBPh9DXzjup5panpoIjMU/soLzn7scWl/ooSLyIWU+9TTtjcaEBc1AvdFc
YCCdZZDq3wTy/5eVwS18//AjWSHMgvKcimd5aF5fVgLcCm3ilay2U6JYDX/BWDiaokXHbT3xNaW3
vNk6Xfv1UChAz9tv5xdVuSJp1/iPUcwOG33TH3Ql4aaaOGG6wROqg0crR0OxnzfZTxL/uA+B2t9Z
3zcORU1cw+ZG4KH9vpqB/LmLgeYct9d+rrOl2xpBdces6JSPAA5uSFe8A3rlbVAlCUsf4vMRmU2f
S8Dt0CWE33Zx9zz6IUkKRRJbC7JYkWrp+roHA663QBF4fEyTXFP2y4eu2LLQSsQVF2z0c0dQt+2l
MiolroFpIpXzW/KFNelIQywqs8bC/JAh6Oq+MXZamMLLv6F2QfErf4OwdDrtu5wkz4mr/ptdwz4A
N3T8SxOpX6wunSF4PzvSYgbEx1RCdIpE2pAXakIPqgaJgmYdyItgZBrhRAlwYMKqQ9nJMTKUhho/
xhkRuzG9OT9dzH969oYmeU6SY84KLgsMsJYRSutqkSivzRpFBaSs2cmiO+ki8JfDVmWeylP+cELj
+YmhBG38sghEd6GJtv7PKlSvoDeb8Qb/jB3pYy5Rk/4gT0hAc/WInfAsG8HTyElb9afglIVFdvIf
93zFxj+XEqKZeMHOrEOZYJhbzuaRTytZfslEGdaIsd5jfZx0x+w0nJGUwkVaZ+RNVr1jsGwzXAGw
soz+tHaIXA3PMD1zHIyXSABYH0NBNi5c8DCs+NaIUiwygYE17CVMjImwzJQnxim3jg7WM2m6QAfM
BqYOezQ02dotli7czS3tzwtJYswMrfXGpAnAdIPaZiGvN87fdphJdQU8umaojR7Ppc6KjaEAgihB
9xQOU+jBHFHBThkyDJlVqlR6mgRn1FKToRBJHN5wjtYS+P9qbldo5SjLRpkSYSu4uKQGvoxrGJwQ
KO/7L55b6JcpxHSUZ/ZW5FUEgKoUMOa2nTW1LNlWMKjDKPANEIONLKUWshML1ngcM/sOXw6dHIjC
XfU+7W7V4W/ydADyOmXA092NFlF/zY3gR4lC+G0ia0AseD9BpnAcVIH9QfKXEYg2r69DiBMATCmY
hVFfem80S0qRbYZoB5f+94e7JKURHLdsbQHijOrfHakw9Asx85WSVrPTB8RDGLyXOOp2BfeEnPiF
zB3wmcGTcd34jPtorznnhoeHSSnnB+KgsURpSkv+4iVxo1aks6paXr3RO3b//ytXwDphxTVL0Os9
VJdpPY+k/Q/+Ppg2KAlbOxcTmVdYLtq6L86fG8xuHs33fbAoPyG35zfWv79nl1E+2Rliw6xAFihx
ctmJGsT5Uka9P0raWq1LI/UBIdUROKzvEMiArDyszmrRHBsV761FGVo9ZGDxLLT19dYvqk3q/L+K
ECCxgK60Yo9jNtMUY7c+hknYS7YlRnRA8UdGF/CLwgmOh8gR94Xw86VPN2W4xRZNj4rs03MkrhnO
1ka+/CpQ/qzQF9TICgtzb9Pi30gPv8XKaVn+XzbJ9gyqcZnUS6iDM95nZKiFZ33it5II2wMHbokl
OVRT6PeMvAeYssYZnYcnNTeAeTsW1EM/zFHOZaLQ2YLE+Uq1/yiHqzgC01y9xyKjwmu7pEDYQDt4
vinl605/exGAbrhx4PDPRuXk5K99eourXfGq+XYebRUyCsySslWJODAj4rUOsUAkckHEOgAJntgc
PPKcoC5AopAOwcLj3XcFXzTzbiKBqSAnz1xWT1Cc2J6VGsQTbj77PMoFsyo/EmeUelHpbfR6OeUg
9ghO/nrojxHon7Bhs38CqR6z+e4sRHqQWZJqbFfiVgr+RVG6SVlDVf9mpn+88/GqkzS7pqK6PKi7
NsgtJeZjy0bUqZ8q9xNkJ/yIvlQ1l8cDpyTBHlS//BKpZu9UraRldEdtpGqie1W5wb2os7mJzdwU
H6CBSEGKCA+GtGUTBsNJ491vizdhfzHuth0V1x5mNPmgf81FKXKRU+RH5LnkSb50cvikxxzC6mPd
K/IuErSMwqGH8ETc9VRgT2+mqo4M515U0yb+T6A/rmAhIfzJZwS/hvwdA95Pa4t4dxE6EAKrRDYQ
0kZBD+k0GArL4hC3RjDpGOh+Agd8MCGvMoVDHrg0PHQcKuNHzto2yALHxQd3yWqnw6kHRuKY3n4m
t+VCSJ6pTqssdz0WJnJ1dVVV+hfqzn4O9Ueid+m3vyIiv9++j6gwpT0WXV1ArAAz0g50wiMH+Bjl
NWdg9elJBBal9NrQ3ayFEN0nPrsba+3c/7ntta9Hb6JN1zMMNn8gYOasp1Z8acU+u7R2v9z8Kc/X
+Z8U457c4IwqEbmHD/+W6l9Pm7+clPlB+s7voWumrI+CvvwLc+Dp3RmpmYJD4zL1M7yvDi8/b3fu
oQXcjk1orkvgI5R34yUzc4WszhcF/OkVRK9JP13yh9v75MxsXJnrlQyA8mBEa4eQ95YyxSZ75D2R
B/xN8losFU2ttucfxkkyXPm4ecJSYuYKFTd7INa+hreAZ7TcN3sm2vgD0xIXqJWCuPpg7hfflnjV
dCKHIIF0Ij5JGQ1SvJrUzgT7gW/iwgKQDKsiBRQ04RE2ZfdtKxkyhoJMcosYNWK3VgZZGCe4jing
N6qBvsQSHJ1RzirlWXO7gYYfoP3pPIPn7ErYLvaT6+k+/9H12lwexU2ZUdLu93SYYwjPkbkG8PTT
Df/bOiwmOXrnCv1JfP9pi8Wc/16wx1ou5o11LOWXqycahkJ0y/930najPG04OBUoFksYtjVbhJF7
5nng9xf1YrTK348MrPwqwo2A/eJGKvVYVqrVNT+CUBorrwpv3UNwot9xKs/ArtAtsJ3Dg3X/uM7b
M3yX52I1EHZu7AIswwvyHA27en0i9AHkZgx/M9GuY/lF/jBcG9j+VVK3iY6I4iP/KbkMBMDMoYLR
kRN628GHA1ajaZRHKGPIoO3CE0cqn6XhZhoqMd/WBzsWCKU2FrEeac9wYW7yvmRyruaaAHycKLkq
zRLjPHQ2tnrGpSvDlQ2swNCJmx4c/v8XgS+niTHQw+FZVgIaH1ENe68JoIB4rdWA0Y+CWhSlyMMp
Df4+ErUFkMRXyt4akXV455LjbZr8yc1r1GkebHpqSLuSAEmS7FLrZQDC9TdthLp1nWSlEeRkALdN
USr5+0DCmQsPP9KSsIGG0R1BociOu66e/e2uSVTaMZbDiazYBd6pDDxW+KzzxS/xXd4QsmmaChgn
GvACVctlL4ry6EdXNSBpPug3Z6M0Uylt8JX91/QCQ+qe15aIn6EhNf0N9i2pdFkSX5MQOAnXYBlC
SZI9ARFNqRFbBVa2Dlbb0jlfOihEUpDrp0SH5TwKHfeTgR88qjTIOt8AAirwODdnHgVxh/uqZGZO
pkpIzy0f10dlOOO06ZTKpyefb+gtOgO4fPZ+zWiacMxji2YiPxZxCkfOQu1qFhMxJpmz9XQ2z9iZ
T1gDWuGTSsHALFkAl+k3q+V/RwKShNH+kVqxElqeJXcC/tgdZ6ef9utLE+x0QvFBBxV5mmwDtUSR
+O/7/NRfalXNqhkhsqJLAoAWWgM97jcakRI9/vBcBm4nBMXtlul013JTeI6ckv1c9ia2PNAcfRFM
5NHq7KjrRZm+yMuUlv+LNVPlGSwKbDpTYB70T8rHLA61okSPLBnxXV7fDUxTmJYWzfFYkeDbwbce
q338gevRP7U344YR+OZ1OTup6WyTsLaJq1wvmot5Q98kf5pzA7zrH+mTdoIMf3bGlal04Hp0UBbo
oSCV9WGl1ifhoehoXWWHH7PU0Qw0vAw+U5h0AJcg11yo2Num/DwPmGLtPbx4uyDd8+wuAcwi3gQy
GaSE1Vs=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
