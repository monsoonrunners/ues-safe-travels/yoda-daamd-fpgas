`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     UES Safe Travels
// Engineer:    Lefteri Vlahos (VLHLEF001)
//              Gavin Foster (FSTGAV002)
//              Brandon Ferreira (FRRBRA002)
// 
// Create Date: 17.05.2022 13:00:42
// Design Name: 
// Module Name: Deviation Filter
// Project Name: DAAMD FPGAS
// Target Devices: Nexys 4 DDR / Nexys A7
// Tool Versions: 
// Description: A module used to filter values from BRAM which fall outside of
// a specified deviation from a specified mean
// 
// Dependencies: N/A
//
// Revision:
// Revision 1.0.0 - Commented
// Additional Comments:
// Can be used with up to 32-bit values
//////////////////////////////////////////////////////////////////////////////////

module Filter(
    input clk, 
    input en,
    input [37:0] meanMax,
    input [37:0] meanMin,
    input [37:0] deviation,
    input [31:0] currentMin,
    input [31:0] currentMax,
    input [5:0] sliceCount,
    output reg dOut,
    output reg weaMin,
    output reg weaMax,
    output reg [5:0] rwAddr,
    output reg done
    );
    
    reg [1:0] readWait;
    reg writeWait;
    reg clkd;
    
    initial begin
        rwAddr <= 0;
        weaMin <= 0;
        weaMax <= 0;
        writeWait <= 0;
        readWait <= 2;
        clkd <= 0;
        dOut <= 0;
        done <= 0;
    end

    always @(posedge clk) begin
        if (en) begin
            case(done)
                0: begin
                    if (~writeWait && readWait==0) begin
                        if ((currentMax < (meanMax - deviation)) || (currentMax > (meanMax + deviation))) begin     // checks whether current maximum is outside acceptable range
                            weaMax = 1;
                        end
                        
                        if  ((currentMin < (meanMin - deviation)) || (currentMin > (meanMin + deviation))) begin    // checks whether current minimum is outside of accaptable range
                            weaMin = 1;
                        end

                        if (weaMin || weaMax) writeWait = 1;    // set write delay flag if module needs to write to memory
                        readWait = 2;                           // set 2 clock-cycle delay to allow module to read correct values accounting for BRAM read-write delay
                    end
                    else begin
                        readWait = readWait - 1;
                        if (readWait == 1 && writeWait) begin   // after one clock-cyle delay, pull weas low, and incrememnt memory address to allow full clock-cycle to read from BRAM
                            weaMin = 0;
                            weaMax = 0;
                            rwAddr = rwAddr + 1;
                        end
                        if (readWait == 0) begin                // end of delay between filtering values from BRAM
                            writeWait = 0;
                            if (rwAddr == sliceCount) done = 1; // if true, then all values have been filtered
                        end
                    end
                end
                1: begin
                    rwAddr <= 0;
                    weaMax <= 0;
                    weaMin <= 0;
                    readWait <= 0;
                end
                default:;
            endcase
        end 
        else begin
            rwAddr <= 0;
            weaMax <= 0;
            weaMin <= 0;
            readWait <= 2;
            writeWait <= 0;
            done <= 0;
        end
    end
endmodule
