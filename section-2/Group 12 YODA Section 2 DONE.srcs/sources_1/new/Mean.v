`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     UES Safe Travels
// Engineer:    Lefteri Vlahos (VLHLEF001)
//              Gavin Foster (FSTGAV002)
//              Brandon Ferreira (FRRBRA002)
// 
// Create Date: 17.05.2022 22:45:55
// Design Name: 
// Module Name: Mean
// Project Name: DAAMD FPGAS
// Target Devices: Nexys 4 DDR / Nexys A7
// Tool Versions: 
// Description: A module used to calculate the mean of 2 sets of numbers in BRAM
// 
// Dependencies: N/A
// 
// Revision 1.0.0 - Commented
// Additional Comments:
// Can be used with up to 32-bit values
//////////////////////////////////////////////////////////////////////////////////

module Mean(
    input clk,
    input en,
    input [5:0] sliceCount,             // used to keep track of how many values need to be processed
    input [31:0] maxData,               // data input for max value register
    input [31:0] minData,               // data input for min value register
    output reg [37:0] meanMax,          // output mean value for maximum register
    output reg [37:0] meanMin,          // output mean value for minimum register
    output reg [5:0] rAddr,             // read address used to read min and max values from registers
    output reg done                     // done indicator
    );
    
    reg clkd;                           // used to delay processing for 1 clock cycle to account for BRAM read delay
    reg [31:0] divIntermediateMax;      // used to avoid self-assigning the result of a divison
    reg [31:0] divIntermediateMin;      // used to avoid self-assigning the result of a divison

    // Initialisation -------------------------------------------------- //
    
    initial begin
        rAddr <= 0;
        clkd <= 0;
        meanMax <= 0;
        meanMin <= 0;
        done <= 0;
        divIntermediateMax <= 0;
        divIntermediateMin <= 0;
    end

    // Behavioural ----------------------------------------------------- //
    
    always @(posedge clk) begin
        if (en) begin
            if (clkd) begin
                case(done)
                    0: begin    // simple mean calculation
                        meanMax = meanMax + maxData;
                        meanMin = meanMin + minData;
    
                        if (rAddr == sliceCount) begin
                            divIntermediateMax = meanMax/sliceCount;
                            divIntermediateMin = meanMin/sliceCount;
                            meanMax = divIntermediateMax;
                            meanMin = divIntermediateMin;
                            done = 1;
                            rAddr = 0;  // if done, reset read address to 0 to allow other modules to interact with BRAM without issue
                        end
                        else rAddr = rAddr + 1;
                    end
                    
                    1:;
                    default:;
                endcase
            end
            clkd = 1;
        end
        else begin          // if module is not enabled, it resets itself to its initial state
            rAddr <= 0;
            clkd <= 0;
            meanMax <= 0;
            meanMin <= 0;
            done <= 0;
            divIntermediateMax <= 0;
            divIntermediateMin <= 0;
        end
    end
    
endmodule
