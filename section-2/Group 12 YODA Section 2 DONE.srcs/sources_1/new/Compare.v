`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:     UES Safe Travels
// Engineer:    Lefteri Vlahos (VHLLEF001)
//              Gavin Foster (FSTGAV002)
//              Brandon Ferreira (FRRBRA002)
// 
// Create Date: 12.05.2022 14:16:56
// Design Name:
// Module Name: Compare
// Project Name: DAAMD FPGAS
// Target Devices: Nexys 4 DDR / Nexys A7
// Tool Versions: 
// Description: Simple comparator module to slice audio into intervals and find
// absolute maxima and minima within each interval.
// 
// Dependencies: N/A
// 
// Revision:
// Revision 1.0.0 - Commented
// Additional Comments:
// Capable of processing up to 32-bit resolution signals
//////////////////////////////////////////////////////////////////////////////////

module Compare(
    input [31:0] audioIn,           // 32-bit audio input stream
    input clk,                      // clock signal
    input reset,                    // reset signal
    input [5:0] sliceSize,          // duration of the slices in 10 milliseconds
    output reg [5:0] wAddr,         // addr to read from audio sample BRAM
    output reg wea,                 // enables the max and min BRAM to write the current slice's max and min values
    output reg [31:0] maxOut,       // current slice max to write to max BRAM
    output reg [31:0] minOut,       // current slice min to write to min BRAM
    output reg done,                // used as en input on the Mean block; signals that the entire signal has been processed
    output reg [5:0] sliceCount     // number of slices the audio signal is divided into (calculated from slice size)
    );
    
    parameter SLICES = 64;          // maximum number of slices per audio recording
    integer count;                  // internal count variable to keep track of intervals
    integer incInterval;            // used to keep track of when new audio sample enters the system
    reg clkd;                       // used to delay module's execution by 1 clock cycle to account for BRAM read delay
    
    // Initialisation -------------------------------------------------- //
    
    initial begin
        maxOut <= 0;
        minOut <= 32'hffffffff;
        count <= 0;
        done <= 0;
        wea <= 0;
        incInterval <= 0;
        wAddr <= 0;
        clkd <= 0;
    end

    // ----------------------------------------------------------------- //
    
    // Behavioural ----------------------------------------------------- //
    
    always@(posedge clk) begin
        if (reset) begin    // if reset input is high, return to initial state
            done <= 0;
            clkd <= 0;
            wea <= 0;
            wAddr <= 0;
            count <= 0;
            maxOut <= 0;
            minOut <= 32'hffffffff;
        end
    
        if (wea) begin      // if wea is high, increment the write address, write the current values, and then reset them; this is done at the end of each slice
            wAddr = wAddr + 1;
            maxOut <= 0;
            minOut <= 32'hffffffff;
        end
        
        wea = 0;

        if (~clkd) begin    // this is executed only once when the compare module is activated
            sliceCount = 3072/(sliceSize*441);  // calculates the number of slices the audio signal should be split into
            incInterval = sliceSize * 1000000;  // used to delay the processing of samples according to the incoming sample rate (44.1kHz) of the audio signal
        end
        
        if (clkd) begin
            case(done)
                0: begin
                    if (wAddr >= sliceCount) begin      // checks to see if all slices have been processed; if so, then the module is done
                        done <= 1;
                    end
                    else begin
                        if (audioIn>maxOut) maxOut = audioIn;
                        if (audioIn<minOut) minOut = audioIn;
                        
                        count = count + 1;
                        
                        if (count == incInterval) begin     // detects when one audio interval has finished in order to write the current maximum and minimum
                            count = 0;
                            wea = 1;
                        end
                    end
                end
                
                1:begin 
                    wAddr <= 0;     // reset write address to zero to allow other modules to communicate withe same BRAM modules
                    wea <= 0;       // reset write enable to allow other modules to write to the same BRAM modules
                    maxOut <= 0;
                    minOut <= 0;
                end
                default:;
            endcase  
        end
        
        clkd = 1; // used to delay execution by 1 clock cycle in order to account for BRAM read latency
    end
    
endmodule
