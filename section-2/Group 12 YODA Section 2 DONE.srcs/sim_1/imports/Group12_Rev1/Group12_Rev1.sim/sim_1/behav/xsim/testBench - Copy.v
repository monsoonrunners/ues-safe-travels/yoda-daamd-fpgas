`timescale 1ns / 1ps

module testBench;
    reg [0:0] clk;
    reg [0:0] reset;
    reg [5:0] sliceSize;
    wire wea;
    wire [31:0] minOut;
    wire [31:0] maxOut;
    wire [31:0] doutaMax;
    wire [31:0] doutaMin;
    wire [5:0] addr;
    wire [31:0] audioIn;
    wire compDone;
    wire [5:0] numSlices;
    wire [37:0] meanMin;
    wire [37:0] meanMax;
    wire meanDone;
    wire [5:0] addr2;
    reg [31:0] deviation;
    wire [31:0] filterOutMax;
    wire [31:0] filterOutMin;
    wire filterWeaMax;
    wire filterWeaMin;
    wire [5:0] filterRAddr;
    wire [5:0] filterWAddrMax;
    wire [5:0] filterWAddrMin;
    wire [31:0] filterMaxBlkDOut;
    wire [31:0] filterMinBlkDOut;
    wire filterDone;
    
    
    reg [16:0] addra;
    parameter clkRate = 5;
    parameter clkRate_bram = 22676;

    Compare compare_ut (
        .clk (clk),
        .reset (reset),
        .sliceSize (sliceSize),
        .audioIn (audioIn),
        .wea (wea),
        .maxOut (maxOut),
        .minOut (minOut),
        .addr (addr),
        .numSlices (numSlices),
        .done(compDone)
    );
    
    Mean mean_ut (
        .clk (clk),
        .en (compDone),
        .numSlices (numSlices),
        .maxData (doutaMax),
        .minData (doutaMin),
        .meanMax (meanMax),
        .meanMin (meanMin),
        .addr (addr2),
        .done (meanDone)
    );
    
    Filter filter_ut (
        .clk (clk), 
        .en (meanDone),
        .meanMax (meanMax),
        .meanMin (meanMin),
        .deviation (deviation),
        .currentMin (doutaMin),
        .currentMax (doutaMax),
        .numSlices (numSlices),
        .filterOutMax (filterOutMax),
        .filterOutMin (filterOutMin),
        .weaMax(filterWeaMax),
        .weaMin(filterWeaMin),
        .rAddr(filterRAddr),
        .wAddrMax(filterWAddrMax),
        .wAddrMin(filterWAddrMin),
        .done(filterDone)
    );
    
    
    filter_max_blk filter_max_ut (
        .clka (clk),
        .wea (filterWeaMax),
        .addra (filterWAddrMax),
        .dina (filterOutMax),
        .douta(filterMaxBlkOut)
    );
    
    filter_max_blk filter_min_ut (
        .clka (clk),
        .wea (filterWeaMin),
        .addra (filterWAddrMin),
        .dina (filterOutMin),
        .douta(filterMinBlkOut)
    );
    
    max_blk max_ut (
        .clka (clk),
        .wea (wea),
        .addra (addr | addr2 | filterRAddr),
        .dina (maxOut),
        .douta(doutaMax)
    );
    
    min_blk min_ut (
        .clka (clk),
        .wea (wea),
        .addra (addr | addr2 | filterRAddr),
        .dina (minOut),
        .douta(doutaMin)
    );
    

    
    
    audio_blk_ram audio_ut (
        .clka (clk),
        .wea (0),
        .addra (addra),
        .dina (1'bz),
        .douta(audioIn)
    );
    
    initial begin
        addra <= 0;
        sliceSize <= 1;
        clk <= 0;
        reset <= 0;
        deviation <= 'd4000;
    end
    
    always #clkRate begin
        clk = ~clk;
    end
    
    always #clkRate_bram begin
        addra = addra + 1;
        if (addra >= 1024) addra = 0;
    end
    
endmodule