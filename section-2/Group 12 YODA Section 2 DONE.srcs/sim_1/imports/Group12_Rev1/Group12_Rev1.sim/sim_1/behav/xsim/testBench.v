`timescale 1ns / 1ps

module testBench;
    // Common
    reg clk;
    reg [16:0] addra;
    parameter clkRate = 5;
    parameter clkRate_bram = 22676;
    
    
    // Compare Module -------------------------------------------------- //
    // Inputs
    reg reset;
    reg [5:0] sliceSize;
    wire [31:0] audioIn;
    // Outputs
    wire compWea;
    wire [5:0] compWAddr;
    wire [31:0] minOut;
    wire [31:0] maxOut;
    wire [5:0] sliceCount;
    wire compDone; // used as EN input for Mean Module
    // ----------------------------------------------------------------- //


    // Mean Module ----------------------------------------------------- //
    // Inputs
    wire [31:0] doutaMax;
    wire [31:0] doutaMin;
    // Outputs
    wire [5:0] meanRAddr;
    wire [37:0] meanMin;
    wire [37:0] meanMax;
    wire meanDone; // used as EN input for Filter Module
    // ----------------------------------------------------------------- //
    
    
    // Filter Module --------------------------------------------------- //
    // Inputs
    reg [31:0] deviation;
    wire [31:0] filterOut;
    // Outputs
    wire filterWeaMax;
    wire filterWeaMin;
    wire [5:0] filterRWAddr;
    wire [31:0] filterMaxBlkDOut;
    wire [31:0] filterMinBlkDOut;
    wire filterDone;
    // ----------------------------------------------------------------- //
    

    // Audio Signal BRAM
    audio_blk_ram audio_ut (
        .clka (clk),
        .wea (0),
        .addra (addra),
        .dina (1'bz),
        .douta(audioIn)
    );

    // Compare Module
    Compare compare_ut (
        .clk (clk),
        .reset (reset),
        .sliceSize (sliceSize),
        .audioIn (audioIn),
        .wea (compWea),
        .maxOut (maxOut),
        .minOut (minOut),
        .wAddr (compWAddr),
        .sliceCount (sliceCount),
        .done(compDone)
    );
    
    // Mean Module
    Mean mean_ut (
        .clk (clk),
        .en (compDone),
        .sliceCount (sliceCount),
        .maxData (doutaMax),
        .minData (doutaMin),
        .meanMax (meanMax),
        .meanMin (meanMin),
        .rAddr (meanRAddr),
        .done (meanDone)
    );
    
    // Filter Module
    Filter filter_ut (
        .clk (clk), 
        .en (meanDone),
        .meanMax (meanMax),
        .meanMin (meanMin),
        .deviation (deviation),
        .currentMin (doutaMin),
        .currentMax (doutaMax),
        .sliceCount (sliceCount),
        .dOut (filterOut),
        .weaMax(filterWeaMax),
        .weaMin(filterWeaMin),
        .rwAddr(filterRWAddr),
        .done(filterDone)
    );
    
    // Interval-Wise Maximum BRAM
    max_blk max_ut (
        .clka (clk),
        .wea (compWea || filterWeaMax),
        .addra (compWAddr | meanRAddr | filterRWAddr),  // bitwise OR allows multiple modules to address the BRAM using the same input port; only one adress is non-zero at a time
        .dina (maxOut | filterOut),
        .douta(doutaMax)
    );
    
    // Interval-Wise Minimum BRAM
    min_blk min_ut (
        .clka (clk),
        .wea (compWea || filterWeaMin),
        .addra (compWAddr | meanRAddr | filterRWAddr), // bitwise OR allows multiple modules to address the BRAM using the same input port; only one adress is non-zero at a time
        .dina (minOut | filterOut),
        .douta(doutaMin)
    );
    
    // Signal Initialisation
    initial begin
        addra <= 0;
        sliceSize <= 1;         // set interval length to 10ms; this will result in 6 intervals using the test signal
        clk <= 0;
        reset <= 0;
        deviation <= 'd4000;    // this is set arbitrarily for testing
    end
    
    // Define Accelerator Clock Rate (100MHz)
    always #clkRate begin
        clk = ~clk;
    end
    
    // Define Audio Input Clock Rate (44.1kHz)
    always #clkRate_bram begin
        addra = addra + 1;
        if (addra >= 1024) addra = 0;
    end
    
endmodule
