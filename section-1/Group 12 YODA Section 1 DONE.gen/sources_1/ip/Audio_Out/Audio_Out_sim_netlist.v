// Copyright 1986-2021 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2021.2 (win64) Build 3367213 Tue Oct 19 02:48:09 MDT 2021
// Date        : Wed May 18 23:23:44 2022
// Host        : LAPTOP-5RLV6P1B running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim {c:/Users/beati/Documents/Uct/4th
//               Year/HPESLAB/EEE4120F_Project/Previous-Code/Group12_Rev1/Group12_Rev1.gen/sources_1/ip/Audio_Out/Audio_Out_sim_netlist.v}
// Design      : Audio_Out
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "Audio_Out,blk_mem_gen_v8_4_5,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "blk_mem_gen_v8_4_5,Vivado 2021.2" *) 
(* NotValidForBitStream *)
module Audio_Out
   (clka,
    wea,
    addra,
    dina,
    douta);
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME BRAM_PORTA, MEM_SIZE 8192, MEM_WIDTH 32, MEM_ECC NONE, MASTER_TYPE OTHER, READ_LATENCY 1" *) input clka;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA WE" *) input [0:0]wea;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA ADDR" *) input [9:0]addra;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DIN" *) input [31:0]dina;
  (* x_interface_info = "xilinx.com:interface:bram:1.0 BRAM_PORTA DOUT" *) output [31:0]douta;

  wire [9:0]addra;
  wire clka;
  wire [31:0]dina;
  wire [31:0]douta;
  wire [0:0]wea;
  wire NLW_U0_dbiterr_UNCONNECTED;
  wire NLW_U0_rsta_busy_UNCONNECTED;
  wire NLW_U0_rstb_busy_UNCONNECTED;
  wire NLW_U0_s_axi_arready_UNCONNECTED;
  wire NLW_U0_s_axi_awready_UNCONNECTED;
  wire NLW_U0_s_axi_bvalid_UNCONNECTED;
  wire NLW_U0_s_axi_dbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_rlast_UNCONNECTED;
  wire NLW_U0_s_axi_rvalid_UNCONNECTED;
  wire NLW_U0_s_axi_sbiterr_UNCONNECTED;
  wire NLW_U0_s_axi_wready_UNCONNECTED;
  wire NLW_U0_sbiterr_UNCONNECTED;
  wire [31:0]NLW_U0_doutb_UNCONNECTED;
  wire [9:0]NLW_U0_rdaddrecc_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_bid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_bresp_UNCONNECTED;
  wire [9:0]NLW_U0_s_axi_rdaddrecc_UNCONNECTED;
  wire [31:0]NLW_U0_s_axi_rdata_UNCONNECTED;
  wire [3:0]NLW_U0_s_axi_rid_UNCONNECTED;
  wire [1:0]NLW_U0_s_axi_rresp_UNCONNECTED;

  (* C_ADDRA_WIDTH = "10" *) 
  (* C_ADDRB_WIDTH = "10" *) 
  (* C_ALGORITHM = "1" *) 
  (* C_AXI_ID_WIDTH = "4" *) 
  (* C_AXI_SLAVE_TYPE = "0" *) 
  (* C_AXI_TYPE = "1" *) 
  (* C_BYTE_SIZE = "9" *) 
  (* C_COMMON_CLK = "0" *) 
  (* C_COUNT_18K_BRAM = "0" *) 
  (* C_COUNT_36K_BRAM = "1" *) 
  (* C_CTRL_ECC_ALGO = "NONE" *) 
  (* C_DEFAULT_DATA = "0" *) 
  (* C_DISABLE_WARN_BHV_COLL = "0" *) 
  (* C_DISABLE_WARN_BHV_RANGE = "0" *) 
  (* C_ELABORATION_DIR = "./" *) 
  (* C_ENABLE_32BIT_ADDRESS = "0" *) 
  (* C_EN_DEEPSLEEP_PIN = "0" *) 
  (* C_EN_ECC_PIPE = "0" *) 
  (* C_EN_RDADDRA_CHG = "0" *) 
  (* C_EN_RDADDRB_CHG = "0" *) 
  (* C_EN_SAFETY_CKT = "0" *) 
  (* C_EN_SHUTDOWN_PIN = "0" *) 
  (* C_EN_SLEEP_PIN = "0" *) 
  (* C_EST_POWER_SUMMARY = "Estimated Power for IP     :     2.95215 mW" *) 
  (* C_FAMILY = "artix7" *) 
  (* C_HAS_AXI_ID = "0" *) 
  (* C_HAS_ENA = "0" *) 
  (* C_HAS_ENB = "0" *) 
  (* C_HAS_INJECTERR = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MEM_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_A = "0" *) 
  (* C_HAS_MUX_OUTPUT_REGS_B = "0" *) 
  (* C_HAS_REGCEA = "0" *) 
  (* C_HAS_REGCEB = "0" *) 
  (* C_HAS_RSTA = "0" *) 
  (* C_HAS_RSTB = "0" *) 
  (* C_HAS_SOFTECC_INPUT_REGS_A = "0" *) 
  (* C_HAS_SOFTECC_OUTPUT_REGS_B = "0" *) 
  (* C_INITA_VAL = "0" *) 
  (* C_INITB_VAL = "0" *) 
  (* C_INIT_FILE = "Audio_Out.mem" *) 
  (* C_INIT_FILE_NAME = "Audio_Out.mif" *) 
  (* C_INTERFACE_TYPE = "0" *) 
  (* C_LOAD_INIT_FILE = "1" *) 
  (* C_MEM_TYPE = "0" *) 
  (* C_MUX_PIPELINE_STAGES = "0" *) 
  (* C_PRIM_TYPE = "1" *) 
  (* C_READ_DEPTH_A = "1024" *) 
  (* C_READ_DEPTH_B = "1024" *) 
  (* C_READ_LATENCY_A = "1" *) 
  (* C_READ_LATENCY_B = "1" *) 
  (* C_READ_WIDTH_A = "32" *) 
  (* C_READ_WIDTH_B = "32" *) 
  (* C_RSTRAM_A = "0" *) 
  (* C_RSTRAM_B = "0" *) 
  (* C_RST_PRIORITY_A = "CE" *) 
  (* C_RST_PRIORITY_B = "CE" *) 
  (* C_SIM_COLLISION_CHECK = "ALL" *) 
  (* C_USE_BRAM_BLOCK = "0" *) 
  (* C_USE_BYTE_WEA = "0" *) 
  (* C_USE_BYTE_WEB = "0" *) 
  (* C_USE_DEFAULT_DATA = "0" *) 
  (* C_USE_ECC = "0" *) 
  (* C_USE_SOFTECC = "0" *) 
  (* C_USE_URAM = "0" *) 
  (* C_WEA_WIDTH = "1" *) 
  (* C_WEB_WIDTH = "1" *) 
  (* C_WRITE_DEPTH_A = "1024" *) 
  (* C_WRITE_DEPTH_B = "1024" *) 
  (* C_WRITE_MODE_A = "WRITE_FIRST" *) 
  (* C_WRITE_MODE_B = "WRITE_FIRST" *) 
  (* C_WRITE_WIDTH_A = "32" *) 
  (* C_WRITE_WIDTH_B = "32" *) 
  (* C_XDEVICEFAMILY = "artix7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  (* is_du_within_envelope = "true" *) 
  Audio_Out_blk_mem_gen_v8_4_5 U0
       (.addra(addra),
        .addrb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clka(clka),
        .clkb(1'b0),
        .dbiterr(NLW_U0_dbiterr_UNCONNECTED),
        .deepsleep(1'b0),
        .dina(dina),
        .dinb({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(douta),
        .doutb(NLW_U0_doutb_UNCONNECTED[31:0]),
        .eccpipece(1'b0),
        .ena(1'b0),
        .enb(1'b0),
        .injectdbiterr(1'b0),
        .injectsbiterr(1'b0),
        .rdaddrecc(NLW_U0_rdaddrecc_UNCONNECTED[9:0]),
        .regcea(1'b0),
        .regceb(1'b0),
        .rsta(1'b0),
        .rsta_busy(NLW_U0_rsta_busy_UNCONNECTED),
        .rstb(1'b0),
        .rstb_busy(NLW_U0_rstb_busy_UNCONNECTED),
        .s_aclk(1'b0),
        .s_aresetn(1'b0),
        .s_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arburst({1'b0,1'b0}),
        .s_axi_arid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_arready(NLW_U0_s_axi_arready_UNCONNECTED),
        .s_axi_arsize({1'b0,1'b0,1'b0}),
        .s_axi_arvalid(1'b0),
        .s_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awburst({1'b0,1'b0}),
        .s_axi_awid({1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awlen({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_awready(NLW_U0_s_axi_awready_UNCONNECTED),
        .s_axi_awsize({1'b0,1'b0,1'b0}),
        .s_axi_awvalid(1'b0),
        .s_axi_bid(NLW_U0_s_axi_bid_UNCONNECTED[3:0]),
        .s_axi_bready(1'b0),
        .s_axi_bresp(NLW_U0_s_axi_bresp_UNCONNECTED[1:0]),
        .s_axi_bvalid(NLW_U0_s_axi_bvalid_UNCONNECTED),
        .s_axi_dbiterr(NLW_U0_s_axi_dbiterr_UNCONNECTED),
        .s_axi_injectdbiterr(1'b0),
        .s_axi_injectsbiterr(1'b0),
        .s_axi_rdaddrecc(NLW_U0_s_axi_rdaddrecc_UNCONNECTED[9:0]),
        .s_axi_rdata(NLW_U0_s_axi_rdata_UNCONNECTED[31:0]),
        .s_axi_rid(NLW_U0_s_axi_rid_UNCONNECTED[3:0]),
        .s_axi_rlast(NLW_U0_s_axi_rlast_UNCONNECTED),
        .s_axi_rready(1'b0),
        .s_axi_rresp(NLW_U0_s_axi_rresp_UNCONNECTED[1:0]),
        .s_axi_rvalid(NLW_U0_s_axi_rvalid_UNCONNECTED),
        .s_axi_sbiterr(NLW_U0_s_axi_sbiterr_UNCONNECTED),
        .s_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s_axi_wlast(1'b0),
        .s_axi_wready(NLW_U0_s_axi_wready_UNCONNECTED),
        .s_axi_wstrb(1'b0),
        .s_axi_wvalid(1'b0),
        .sbiterr(NLW_U0_sbiterr_UNCONNECTED),
        .shutdown(1'b0),
        .sleep(1'b0),
        .wea(wea),
        .web(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2021.2"
`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
oESHD2Q5NORrmTVTCApB+YFZJwjA1ezq7U6VZh96by+ofPCvSFp06AIoCLvB4BhPvxfob6kIkBpR
xVCOLM7HsDk7nO1JVWiYIJ6okoWTA8hAlPj3sdGuMwRlZNSBKn/c6F+CW5Jl37TEGotkhycSB3Bg
B/uu1THUZwIG87RPahE=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
RovEhaqHrFqzjckk+DIWG8LQeqg2Y/nACQDyXKKtSav7YHlgpKmgHZnsxwwNpqrqVRGyjTecSQ+e
6Mr/Pi9au3AgJVPL6VOgwNVE0yj2LpA4LPyWzxLN3+DiSDmsaCBNCBlVQi2MRKUabou8nLaXldbL
+7pv4pYhQdcyjDzuC2dx3HmzADqstdEiyXeU3ktJ29CDLDmGwDWdmsrl90s4YQSfBV2nj4/Vut3L
p/8dzphf1htPaNMujMxxgp3z4JzUEDJJokDL+gNutEEHiaWpI3URIA5v22vJu+NPD+eEraSioHfL
DPKAajZTwK5FHnonu4O2D0co8GWqWW5cUqZz9A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
jBQ6Th9yy7jtKQD1h235YLT6qO6XiBaBKGJrV1Z8H9M9ePJ9R/fA8E1okt4LyBvoWjR7tmCbIg7A
0/vuKOogkLtDE/BtTlp4z1iurO8rQrAcdZy/e+7GATawyJxFY7kZhnXASu9zB8TiOBELSlapkpxe
WuAzXLde9FBMBkq4RSc=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eucSNV2Zbm4zYc2tIGRlGmlVM8+WHY1NHe9drZdgDhGPOHz8PTqHapfnZ1kWuTLtPBLSMvcXNScn
UTvpULofBV6qD7WHLPg7UJcjpZVDL69lk88chgqrlc/RqaJXKNVv+Ubku53ZLU20uZK71bNymjSM
855RVWw5lvTHTCNC2MYIS94Fmrzuq8i0+tFh5qBKkHK2BC+fD7xVyyfuh4mZR2yr/hRs/emoI79E
IKoJnLiglVp6RXTsXFzZW4pIthbjWSuZlOQvoYkS2RMj8a0r9lyariphRQunoudc0bLO4Phk578c
40gusaaS/MI7idMT7k1Di96kvu5mHi23loRcZQ==

`pragma protect key_keyowner="Real Intent", key_keyname="RI-RSA-KEY-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
E/syLaRG2Ss/xTTkuAkOKXzm53+rCptYO2DkVukWhvlLmEB2daHCPrXt4gKeuG+0hIGWedSwCiLJ
7KNtEAiTumJ/j+3p7s3oXN9ftCSRolXoACsCclEAmwYjVM0ubCXUx6JNFOGt0yDl2Jsd5+W10mSJ
bYEKvRKi7koXM/eYJqbhTrtsrHDwRJEY0JVUPh8EOkLLqaIKbnjb6ENEY6qZOamp5PaWsSS30gJM
N6fB8D1AmGKnFbfY+d5TexS55Z92aYcAHNX2XwHsKnm45az1vHeZ0rTEU/oONIaSZfikRni1iDBg
x2GOue6sLiwxTEHaVkTJsOVR4mx0VsfFxavwRg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2021_01", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
dSHHpkQiOEzzKs4D71WVyDXLpkKuR9h9h3pBLtnCq2bXiwE/eQHmk5HeQb+qREg0Yv193OukqaQz
RZyuF5GQcqOpqFHMxO62HQ2pdjdpMT5CC7gHvmgiw9qBkJJrXpihIHER4X7OF2iNUfeqxJ8eiSz3
C0V20NlIwKG7Mxg8MVj++xmb32KMUqL7ptikkym20vVdhecVMNvpPoXp8uvaGT7991enWP9HGKUC
9kLY2DEYwRGE71UJJLGWo4n49R50ExFRj91xWnYfvp7uJsMNwnBp5l3GTZiMELX2RkRVSPOHr7l1
n2p5Vq7Uee2drny1IxZ/4c0hYY6y3QWSEqpESw==

`pragma protect key_keyowner="Metrics Technologies Inc.", key_keyname="DSim", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
HUtfqZ9dh5oZTOAt9a0ebo+wQbzg3izFQ0kVqZN81S4cBjQEF53WUiVlTKBDVjvLNUby4Se9WZjj
j86TQzuGJxLPDTohmbytErsg5JrlXHbHGwR4zGNGTbBs12X7PkxtS8wVCp+7b1rX6pOGOPqm6FoG
g6rZY/bTzVfGYF2CAOhjJUqUOXEAKnZRehspRyiBI28/ZZPSAUD/abKprW8PWCxMx2zPWztZz4No
R96jgvHezNzB1Ta8W7uRBFTMp+XVSToxTp2jzSXJZ0V5xJl+gdVjAMmf6+te2vqrK2wDWdMxk3Sf
iyLI4d0s25vCybcY2fZWacq5iO9pSlSaOQWgCA==

`pragma protect key_keyowner="Atrenta", key_keyname="ATR-SG-RSA-1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=384)
`pragma protect key_block
vYYu2Kvhv3RZi0pFbjRTQ/BBwfilCrGpkMls+Dz6HBGTZvSaC/anWgymoDS0XnoSENGG3Pz3EBF0
19OqLbyna95IHFe2bA7f8RgU9SEUffZ8eXGigfOjAWpZCN07Q77RkhGUKal7okWe3Q6xHtZy83l2
kW8ma3kOYL7GzQjtpbP3lINHLMqpGEo0dzbOHiJ5r6W5U6DsILGsoLQOXcw+MwrevvNRB0KkSklj
QnL8K2AK8PIsJGM6F8dj5KwRYhSBYNb1opuVpiJWlbHgADoeM+dhiRxBLmnaDE8PWs1ReY6uMzzH
SvvO6UEyxQtvS/Smm/uogr1eUFedUaBHPMEXnYlTAv/SKrh942GeknsqfrjGkZxWTN2NEnvpRUwT
fS0pyd/Err0s94b0srmcTYyxZfJGRUct2T8MCphZFaScAlhn655pxW9RaHMfcvDJUHpW8Qa+KhRt
9CWYScPIH6YNDByLQbhKL5BTpAYMNYPF2W7vM2ZzDob2NB7m6GGeKRr3

`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="CDS_RSA_KEY_VER_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
QSNmIeTT4pBji+CTjknWXN6sH9Wff8+t8KF+AC3fIoIw08jtLtShcB9ZGeEKG02RGCO4lNIUf5YB
2TVYk6EJ5XyCav12qDhc60n56UVrnpfo7drorY0NmOypuxECgO43h6SDWp9W7px3r4CJnQ4+X2Mj
943GdP30WfL5kbWHZJC1Dz9cBIqRa1EbNXvvAqBvRPS2+aXBXAPOC4rNVZGeIUspn/33IW3yJLSp
Jm5GIct87ZuSoz8+DXhUvsTj4hq8lgirVhfz1qhHm8SfODcE91FGUPw3vbpGWXsBX73t2zxFC1Hz
/6m4YqQJVxd+H5iGE4kbHxHyHnH7FIerqc8Phw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
UhfxKxECbuHK/o9ZExa2zP/MIPmFXuDNZwgpiawuBmPeRI1nJsYB7vzbBGMPKny4yIHLT8mHrQRc
fs05atkjIAbLea4+WNoCdCeg7/0PzuodM1ol3it6BHQ6Yzq4mnZbzlk8Xtwmk8ACAbzOr2SYxYWX
ueuUlimUSRusIe4+NiPvzbfHMAOVPjdmSY7zaSyeJuhdAR+fUGeHy5B23Xe2X6cDPeJ75IqcBeul
ox3dTXi3L8r/s1bTKX3FhxRyPZuh/xCWuEajsF2fEYdwWHKtLX6IQniLBJ5ZnVSS8D7IYPsvV4t0
9rWJqto5O1n3rAM44OvKvc9pOYXJupuv7g3gWg==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
fmo66vhS7nigYtLDMjdj7hgUnDG/fnO+cIaY/3qHrcwT7u/paj5enLuWHovegu9O9WRq3pPNnjuN
6vZRpuCgz5p4VAV7dVg9fuzg99BAjThp1Q/+HIPfdQ2LM14ZpTh4FXxthHGkTyS5PJArvZ3/UMpW
zwfdYd5+k2/emJ4/nuqoJHQG8k+O5EjSprLTvNZ/wrE1cT/fW/Lu2pxI4msHqVVYAXz7sJ13cQ+C
7tKxCV8vTyf0rpStdE+kZXg+jrc7vFKuPJO0U9axMsC0nXyeYx2jzfAHptGWKvfQaPg/Eo9mgLyN
qSJfFS6aIycuxNmg7L82WK401aWhnUn7GNrudg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 27616)
`pragma protect data_block
D7xF9hBGNiVpmpM5LPuuVKxS2YBcORwJFi8CBXCBFgcQJ9NUhE6FcmfPYJ5J9hLFMk3+ywWlwt1g
f5mQdHW5T1oVSD8XY5qmOhhD8+c3Ccv035HiJ4DGD2idwtlqlTRPu3ze+hV0VUanT8fRrphG4pfp
LN8ZDIxNL+jCu/uxxKk4rcsLdjwX4/yM0osNsR+BX47+J97wRZC69+U+vdPdOrRjvZCa+MxtNruH
EiyMeXvvZFREds/pUj7cfy/AKKH3yHvB6r5ewlQSaY1fcb5LA9N7VkYwqlnuedtU3hvKZ5O2adTI
BPqoJR5pd/CsMk6PLtMqYFQAqaY4fEA1iim9bpODrGpMqdSltqVKRG7r3yGy/nSfRUpHlRRHLMmZ
i+CGpw8ubqgG4N6ILINNby6s6QLSp7xjzhetFQDfcltYiwaJddr8x/+lHZAENgdJSGBqGeaJN2iS
+iG8vpC8C9cg5okWfrjy71OQhX7vhdLOkQVqaFKplgbC/N2vox+rjRfqk5jXcYbqoVOTAWUvCtWu
YcNPS8Bcar1lwBzvoMvYZsgFVm6WKkxxhQTPRoLrAfncBMZ933MYHzXiXx2jBZyDt4hwMWIEaN+D
YIYT3yUyoPfMLHseVdfLP8fgIFyhbzZ+caqTcdjOSipSApLMjGuCA6pw6wuyxuXkw8JUyFx7FoHM
aViizlFmPCaf9PdjgaxC1mXAuYDeZ+/Y0JK8ax1iLdVcAXjT7oZOc99UyKsnIQ6kQZzPpUwJYgD7
w4iJCusr+7a2Eb3a1NN6NZUcJqYDg46tfe4Utopv9EAao5qGkN+V2qa65Tmaj4LB/pwaQikioi9M
Giw4K/DMpdRxK12leqY92ZWcjijw3LxZDn+VZQCRUKdDOVYqrdKgWZh/Ojrwzql2rQm94/2fel+w
IuqscEsSkbrl3/XH27e96EXh+BfbXyocIMr/Xfe58n3LxgXVKNBdMuajVOU7n6MBFzsLbRcSztTE
Ivs1/lykNUxiCwONvAAGqFbb2/GaaqzVK1y0q1Urgdq22bwTpQMS10/Oz9iUKRs2eUU/NFvH8aUX
FIoT/krYiMAz+fq9b9T1/lCgtByX81cEiTEr/EZ79kbZIQ68/zyNkFP2Oo4Dg/BEYKAcLYiP9lv0
j5utEVMBl7wizs2irXTUeuWfJq+2ZYhUEZgskG8DznsShGDqFX8cbPGPZeHzflowRFCgPdBjM7LM
ScuKSycYioDDO+Q6cSpCLtthc2aqNnXTKs9L9VsMBWjp4oYYe71xoRsjde1OLHQZmlL194pBsVy9
kF7+T09Cd6N7dI9rBtsGBibprRZcHZocJzEXcdKUCHjDF5vJpScAGPXxCmx4cXb8Pbc5S5XeHCXP
QBDIuYKBVTSHHUJyJ5QDZHFsq+9rW7STswkiA5s5kXTPc0iRcuq02Mv78xslm5bunjhOoaNof5N6
AkNnf2QI0KlM3T7ma3NnVvxP+IZmur7qmlQzNZoGzyaQDB+YZKZpBKvboBKquXW3leNkdEtiQMZe
5JILxSLM/typ379Ogoca7N26tRLfplBpKKe0253aNw08Pgic+pjmZYRrnoSjiNxz7bOT1Zl9NfHn
KxbgMk+U8zYqtSYNX2djMC31Y8lRdKEyidQDm2vQDeGEoOf+2nJiH5Mk2UcMECTeLC3YNSl6vlxQ
LDLXpLaa/UfRQfZGv6SsGxQh0aGppZgvc5jJIobk3yGpoZCji/M6t2S9R9i5fgOW2Id915sx+R0w
1Irc/apmdRzoieABlEh1iNveAJpI2TTSlJXT7Sihrx1mOKZU/Gl4+NVaKclhecv0P3o8Yy4O0oFR
4OmQv+k8xlcbIYeWrUnRT5XhwH6GVQ0HM5VlDGhjpOvlYwH8IJBE7CgDVhQ+VKkTq6mqNGEeKgS1
tloE7wY93nWLzqqKZJE0BY1cId5xkuKxy0k8O6HFkHEDxU0yafRo6dNq/GQ2POHwqtQqVd50Wv8i
ivanUVGioKAVE+oUXUtZN0TFYs1rwYZSfEkBD5ABj4h6QLGwzvpQ+CR5DAePgoXRae6MyvfUL720
g8c5NgODQKjVGzMN5BzBMygjWC8sN2vL560EuhyMaZqvzvn25jIxoIWvOSeTdEWQ5mU5ve0/rtba
DpSXvQxTSqnOl6oUOac9akymJ+KzAAGdOAtgCTip6w8vuOPmROmMmHjAzguAb2hJ0NHvniKaLS7Z
3Hes9jMsyuKCUInefqLSZKP84hHQGidEmaBXzZZvkVxG8ENQEaIKll/4XchNfwh+86f1om9YQJSx
YXPLqHylEFlrSGF0OAeace8t0C2O3pTHy1428g4ej591oGWhg/i1sVFlz3MlkgDgnmz7G4W1w1aw
V/LWD3mVI6JXMFFLYcXrggfKbhoRh2HiMx7E9j1i8gCtK9tII+gqGxiAD/+WbcLLZqs1kdqA1bp5
Kmb03bG0JWpCcs1Aq16h9sM0ACJhItYMPiXgYHCCQK2ldJy/xubKEr06fOIIAKB24q7wAWev5uEQ
DkuMqXGxelhtHJvXZLkUAa5hqjKOU6qfQovNEe6DE75Ct0B1AjJuVwpFo2bldqdoKPoHKANbdWp7
zPWz9wHdqoNCG3cHg3FOwH6ycEZsOyLmWXXnrIZqc/39XZ7LZBCB8CNGqElOsVMkrSOKOL2rpjWD
mZRXw+X9gB5LHnkQwpgx047IGw0LXjtM82Lm6KErL94Hkhjn+D8T7dPYZzmnP+a7P6SHY5JYG+C1
8y3Fc8tRyg2Wp7oPljrzJrc9EHD4BfW4YcXirqZxB3ZXwXbnDSkdEoQqqN6cugJOT5M+b95rlTFA
iv5VCgdHqvx4Uzsc1Xxwf9ZQ4n+It5LCIxjPXL0B46oRe2wwjT55dLoG5SCJd/+tBiYCG94bgZzD
FQx383Z6t12FdRNM7Y5jnOnHuNPII44j57XaSQbejk+EOjkDxAqJIl19VX9EdqmGTY3ww/KVM26L
9aQKwg1fYmSyXdJKRmd0uYaNvdHFrkB6ES3bJBgu8J9dRsMi0Nukiee7O1h/ymsIVgxQBULHMOqR
6lXU/amqNQBdmxMMJ7/xJgX06Xkdl4Ujr6ldfdDgfXw8syon7nPw4Wtxkxj87K6Ev8frjm/w3H2c
LbinVnP7a/u5hY0DPl0G3OWAwlgfVbuJyjhFK0TD/KZW5ypbj7Bl0oXqk+lKE3W2+SiZMpGAA3/t
RO1NgMonGSpbyt0JjTyiQxF+Ur17b2Jr9Ly0re975IXhj3Lx7F4riCDYmhbcekYRczaiqLW+lGXj
P0g5E7jp8rjMdeK3VbC/z5Nj1OxJ22TK1eeMAMhLEinGllrxMjjOQBTY6vW3q1+2Y0IrifxnogHW
5XxomC5YMj933NOjWxC6DN2ucu6OyQRZNRIUwulEZUxqCOR8Ai8l7fekMX0WN/YNCIQwUwLxPHiP
UY7RZuv26mwqQ6+6upwOmgIz4rr+zGQhmPSD80t+a+t1ZNSDYo9yTKhDslILK86TTzuOMevWkrnv
gpgWQomT95eNpt9D0//4Qe6RKGIzx6ClzoC2LDEDu3r3zMc2fr3lPO4rWZKYmS8MT0NVvPWuJZDl
pDXZcDjLc51qiNOUoFd6V/9U1iTtM2DJImkmakC5YLoWDFsEeNqqHP2eK25U+hzFev5+QjUqH4go
VQL//Mk3gbuDvDSG+POs7uM70JNhKDxJCNjYuC7Xe+dNWKbJsyNh2thXU1aWcxA+UW/TzQSK/s3O
MCdmMoPa0fprIotAXmqeUXve6wNUqjyfx7hm78JfJK0J4rKCkNOhtPAFXu/V8AbvhB6+GRf7FQJ2
YBY/HNN5xXy2rH0Th35u4uspMBffHK7DQi6eIH0R0Ntg6kHLbl1j1n4Lu/9r05Ij+ZiLIK2/SSgl
XxhpoqVSN3zKTVWViv5s1OQ6ffDFfijB2BtbCaC6aZHbDGYbPp62Z49p/AYbJI4uRAWvYu5CXBW7
fEbvzh+7/P1ec77wdig3bTfFUfYXvCXaFC9YxgL/0ZIW4Z7MNYWGs7fQ2VtUHauHyd89Rm6Y1d2C
utC3F67tjYFTrtYunpzoaoUbXf9TNxlO1RmQRWPftOIgSisLvS44R1WGrsu662Kzj9FXkyPl5kiv
uHtODOhOvtZF2uUVpmg4/UoNSoH1fcHrtZRE8vpJXY1czfnSP8a6SmbQ4+fjM9fFE0K9AkXfwgK6
GPMUmC5LfXU/Ji6/qFsc0LxHaH7tU9Z2DMpyKjw7U2ELfv+fa7VX2ecJPaFHZNPuiiPC0VNUDw10
BxUtzy9S9HzhHokJPLuvF2kSFPkYmOsMh6+OrtPPSE+s+ay5YrIfOfzO8t01jghqPmo20BC1VHa/
wgF8BwWFMDrae7PbhsB9vD/pDd3W4IqjFtEefYKPLVI7BrTM24gR0JTHz7ew1bjd1Vf8Xvghafq2
WAbbhS4Gl//8Y9ng76VVcnmMnZy/GqTsSVIXzxm4ObpxjGdwfnJBv9piQS7SW22yD5ZEzzxYyn7c
YEU66enyHs38j0sVBStsjToWagH0S8BgrnmBGPSDb6RBErl0go90x6IwBLguQfCv/zvcdZvSTFnw
vLUcHvm6Z8AxTXuCQpMH1V/aFInxhIjUw/H//JCeNFQAKlA/VGUAg+YPQVv10xruIlZ4O4ha/HwW
O3/ZcPn2SA8AFCKYIQ1HgCXyY1KaRLTKHv7agoEOwf77gkxuA5RiO4Eu7AaBGdQ7BtpihtP8UoB7
JFfkCw3N0xQLQFcmWXid8jD1phMjhm6Y6u74K3BbbWz1792oSCm2Fdt9/sGR3xFrdSH1ZmuAsa39
kkKEd5umLp+t9tAXfWWZHbC+DfHP7P5xoJHU0fW3V6fqIo3HCi9pCNEtzEwDyzT1D4h9j7UsXMlQ
KleOFoYC4/UMdzdl7T1lE+Q22eltQXlywA4oUCFaw9j/5eindQRa0Wfyy6vRwsKFsIhkW1guy9qA
2rqg4BZiIApbkl/5U/Wxv+qE14iegja19xBSE+4ZvUQ1n6FV/OnKSXR/Tiiv8oOQ178VZ59pc7Z4
vrU/mDJOnJezeJcrNV27I8LPjsC4wEFDtIjYhhk8AGBwj1YFrMsXvIKmE/mmXYGBS3RFrdm+T+z1
suX7xbepZKl2K8aajxrKBuWpGTwp0Vq6wE532TTJFCIth2YWJ2XwV2aS/uv3ZZjum2zhx7lNirLG
JxzcmxhikkPVR3NX/LqQ9bQJBZ1lSeoA2Gr/krApW1JKrUAz9Fmz7lThAyS3Rb6jgw73M+KATqBu
gMVwRtL0U+J4UzXEOWC6gIACFQyw5hUN6d8lNTNaAh6SQfCgvbE2ft30j1Fiy0geg4L/l9PFZfk+
0lR9tOvkcrvuzez+YaCF6E/r0XPyexDzmr/pifybJBQ+DaKwKUDRL22XthlC8nQglkH6TGa8EKty
G/Mi+K/NKyZTVkxTR8PidAYbXcXNl0vQs6SjXGMdOMUmA5jx5uAV/OtzDBM79nUjsVt7C4kOyHR5
Xpy/tAERCkkTATW9QVwx7yZxXbswzXUM3+v22/izp59AU48573Kwszch0sfSYevj7kqz1MveHLKq
U4s2bsbG4hgT8zBq9yqV9rqGR8PdrrlssxgGQhk0Twsadd0EecGh+iyD7B5lH7FQWvpeBhkU++o6
/AH+N63FOQB+eSURbNRILMHlqwe/jYEx05aHf3O/ZJZyCzHZBmfvcquXYqINf7x+0Sglsi+08mcN
z/BzTxjoXMHaZb3ET43c/h0Kf+FyrluEpsP3GtOEsb5DWQrngh1LYw2xeX8BE7L5yVr+rpmgHR7W
vvg4Qxg854AlPqm5+Xd3DZXlXU7ZBlEA3Hdpy8sx5f/Sdv6MjztQhFhDN9BRwuwpOmQ0gVYOx+HQ
wsPR+GcVPLfJ7IQ1X+OCQ43zP1jfad7czIkZupJIqj2ug1fkHGryJgB29BTZ9Igdn+YAUd1oIb3S
tnSzvBXbxIYrUb0chC8VHqjtBHvxyXCdXKkCTC1d+DeX/Xhk7snIaJbF9pExeI1/Cnm0I0WPhVtC
RIv/Oy8jEcyaD0po9OFnSfxdwd9l5DQkR/DzvLIyrUd/HOSYCctXXLIe458Bo9HViSVm9mdDLebv
q7bFUohrWr6NqhI/L2ryCXFU0F/55T8KgqkjZTzZI58UDrZ2fN1wlK0+pdRi9bf1HO8BfEKGtBym
CV0CjB0282wC9UAPjNOCkYE4UDfVCS643b+w5vOvyYf5OXM+c7M3Ep3+145QAWNmNUvLHKAPslsG
GKPxoBlJ7BQLRSnZSUrPbioRscfnsRDLTEmdxDSt1IwE0MjM2qgrt3aEFGWmHAJuuc9sp/QMVsZl
fdhIILLIExT5teec7x5KcWGLIE65FqW3wYOhjz9+k3r9UygvmUoP/6ECRd6MNott7r4JTzRrMLXd
Iomvl1FTLlQykA/ZqPg4AY256jhaYrt49EBigJjmMebbYVzoR2gLk0S3rM1/MRJ3pI748gTKJQFP
Ltp1+xuOyPZGRPzfmPbwkSMbuje5go35fkofFsaUiOMNEscCL/YoUUsOuO+17P436gJ4Ucd8ErhG
FU2y0XbHP+1bisNOtPoTWcCbwfsJO3JuS+Gujysyk11A7IxRLoXCV5tPL3Q8Iostbg8bV4Czcev6
tu77bRBWSDlDgsBFEzWNKjDS5CnrGLrJxvnCISWsYjcIUblOToc8NlfA/BKKWcTukD48rosQ1CNw
slUoVy8kRJDnNXjewxLeU/BJG8yPhUQTXCN6ofl2mKTaJ1A/yo7NLz0ONQ/Az69mCLiUfmGpe0jU
haI0cAwkyedlWpQ9EreKcOYe7JgLtSfiDJRJi6Brt2O5U433Qn4t58YlYbzoSjSMLxvTfJ3y2XqS
hbiOPMFM63pNI1JwsR6cAlN2V4OznkpQ9uDMb1gq52UDFVZsmf2JUL1UqVqq9PiFlSpaKOCcGJ3e
2U6Sn/SzYS4m9X2NaRxJRg4sXCI/4uAvklrmb/2/ekGynBN8eEcszg9tqesEWz0tK4Q5vRz8RhYS
6y8SkgKY69j4teRKwfs7/2XjVv1vc3XRRMLbxnoj4SncfRiVMGKJhtMiGzlbUP5tbspnqSZDAlHn
DJVen146GAmoSuHiUbwXHhjj8tXOVXmgwLgmRVmKyyeqq4BEoPH4yvDN+s4SaC21lk248NPjapls
itkcDjbIYm1RhVvetWO7kRJ83te5teuYLn9Vf9z6RbVFWEDLekKyLDMvnwR0oB2GVUyZyLXkAtcO
Y7qLEck1LPGQ7WCWneWbYzlTLtWt3JAfC5+qxQikcDE1g0TkXAtmjGT2GoS4vVpU8xu7IsJYIGCg
ntD0Z0ypZYggml0uYYG+WM4zlheCuuQCTuzhuq36OpFPYP56c9axO+zcvCBiCBFWwYMbnph8YQPL
1ev6QiCQQkk5b07tX35SH1e7cfsbds8+QQBAJPLwesDb5EZBmyD880QwI+IXuCLfr+p2uoazDkBn
eTfqEcGnhsptr8W6RaoKCXtCsVBSdZStCtNrRLiJIkdR7VWg5/BkbCNWUXyLH522AEQ+dnz35w1I
4EwOqhU77Ttm4mEoNbWyqczJLRcXxHOfLb4/uPcZAz9LbcHOFPKOqMYtEc1jHj9e6Spj9mtkYW++
hVEMZBQ3ylUsRnZsx37h5VGRUIJvkaDX/3CTU/LVLOvqrDDDvyiZlisEIYnS1xeV91kWzzUrZo4a
hp9cKR9PZaEPcsDo3Q9ircU/Tl9i6RApzX/onoa4suYwdFp4He+n5Bo0wEN8cPyERQiguhAWuImS
/shoyUcYDMsq4Bm6OgZ0ym4haTqIVlS8S3FFlJcJJLO4PoQpxWRwUDN9tSuXHrRwGG3GZSVuo6Um
U6UckJnKTseFWi3zkzD0Qg13Ac+p5085U+70IO67jOSGAhpCxS2oGP/9D3DZGTjXOetjWrPx/oAE
5KH7/2lREx8zeoCt2EuqskS6WSSBEr114BLbWsmniQQ9zpYtIzkipJs4t8+jkZJR9yIbD/A6yLnD
u5RKYVIUDrboCxu6Axp1kJdL6RWiisZYCnOi7BygjllJlNbsKR4O+wr1INecYWRx6B10BG2k3qoR
HDEm9uMPjPtjRUgAZ+i0PzvWuZIfDXLeUE7bMaDmT3K4j3imxbLCTJe5EYyVedkV3GcS82Vt44YT
J8eibkMVH0NtksdnF5uPM2bMvfT9scia36UpTTgKo47lnbk3dYjjiQrdaOH4uURqbpbbAe4wTsSR
DEAyx5/4cF2Vjl1obZN3FPADf6Pqb82dT85MhbRCAA9vw36CaDnptMn2jA3DhWg08R1FqGidQbQ6
Twu95zQ2UjbMGjlm3O9F7o3Smt2kJWqhfKFhFoSU1djLT9pu1RHMN8InqMLwTNSUyxVG+nisF08w
ImNXO+VhSIZ9zdu0PY/Cx5XCjZeARU0lfvjKQ5WP3hSXkLLdJ0KzDZ2bOnQ3WNRs88vCc9VXc1JQ
HIXEaDeuLLibFAhRLurBgDfxyXOXhj0qBGH6ioE47M1EcyOwmTxA6bbnW/d/MIGr4UkEIvltmqlZ
a6Y/v7d3r7F4ca3jyiwO5umJHWhr3cu0Yhqc1ndClWkbnHPwA6Dkuq/fZXojCUP6au0ulH6bFzpv
aX8YyczLETvXrQ1T8Lzwxl7B06YJkgFYkzi+SexqmJBM/mmKINe7LkXkEGbdwfOhttt4yCgb6ppj
vxgji0JPSULkB7Rgb4hn8il++C3mNkOlD89hJcE1/H4xQSW3jV1GrJd7sIBzN3PnaNkpfFJMUNk6
78C8RshCafdH7qeUK1Q1qqBmwUWfHiW39ETUyr8bejEBQTwYdEHEoSEOGTzG1QeT1cD4zbFv6t8l
nnN0yLDV3U4/ERxHASL+Z5kwb/iv1/009PK9JYu/A3r0ezQN0kqjCa8bEV20uh/zuKzyg7fo/QbN
nTp85nEEjjycktYsAXJcL6XwuZnR4Hkgv8W8f0JI8a2ugJOj7/brSnXAcTvMCsOixQiO5I9N8jeG
ZacmlGH2pMH2a94bBN32QCjp3CV5VnAU1mHnlUUe1mc7FfMUiImMcwPw0u/a/rX9WOaqor2RhO4a
PyeXDW4Fi6kc6R1Q/LZyS6jFF4/qyoexqcUiL1+zpgl2aRCN1HufPoDH4goNdAfZqFS6NO/SJBNv
hSEWE+KYGtQeATCbLaxPk8TEjDmcugN3F1MvJjefxPYRpF4xsY4YO7Dgv6bJ0Cg3NJxpHwRPNOmb
DqV4syk5b3Xh+dXfKpBNWuKAZTeyt8TQh2YJNgeDGk0873mCz1uvcT5/m1Wmm789fNshd7c5m+Kj
U5sXvXrJnMsvqPJ/a5TTNExCEjhPkMqIikitggRlHfrNjFMDzymxhIq6+Bkf8tdHPpts9YHTTp6o
e8SXgrrfz/NIHZ6Z85BvxVAcbgKjmenOBDX5jWKVhAyJ+/Vu2HAIMhVVVmeUURkCFQgJp/3TVrzg
8GGWNJK0pPVbylyGnJNkaQr/ymNxldtzQvMCnz5M5wn+DPRYUPFPjFKfbGHfOyjshq4EQcdzmBkR
IDrpaA8lMkIG6IAi1Ss4XeP38ggjjxReAC/GApKi/i28vAvfBIye8virQG62bLPqFm58f9o9u7HN
LprvzHUlLbleIgqKnpFx1VDTXJtOnWmpVYgzxLQ0/AkrGbKXJw763kaDijvPadTqWmFMuVtOh1jE
yQZvoy4Jo0xjtzTUfcMAbWlepHrs9Xxv/9Lq9Eh8aUY4piar5JgmpVfNnLPLLwI9HoqEaGplrVJv
P6PfGGxz14UVAwVTj5pL4lNLkb5dSgH5xmoWKLj68+LmIhTs5DIgIX6fexTu9PXNt7J8EMqZVnxH
BOagHXm7At0eXfv9/FmDOTe8xd3/lUeQ6fFchUxjxDypYNkjGq4SlQ7nvHhFiQOpB0SeuAacsL3/
i7kFofy5AMX4aAFxorsu56lpCHtM5TeUy7DdN7E8yx601NTKT8fFDDOMGkRRES5fwsm72/1lG4Pc
Uqmamjdd1UdSPBXnupm2JGH1DCHObvZbTQjPanMTEUnNdnOGi63VZIAr63MQ2Wkt4TrISOeZusUj
ra9u/JSgPEd/mjFQElNf3aS2xQUtshA0GvRw4R2kwBn220TqscSzTbKwInwyZYYEjSgiL27JUF8T
qPkuIqg3B1hPLOhKda1JD+SzgkZ98ph8/6/UY/Kkzb+ADgofBDv1WIp1YsDB0ZhHHF7l4ZUfuVnp
uFiUAJ+twlLUionLxwpiFB4n64XN3nvPkst3ilyo/ozcK1XpwjLwUH45Zz0ziCHLSz8xpm6aqJDK
KnDl0B+KWUVlAgC5AEX82kSvLB25s150h/thny4qqZPSW/2KGz3ZCSF7zuujM/bLNWPTu2r19CN/
vNz3HiYeEwP17PBIxhIBJTKIbPxrKzDvM+5xZM18w7OcYkdl33/jfUj/K1YNrLyeRVPtVt1H7gWx
3vKrIZfdl6QJUgmhRYrZuzqRWQ8+mcegfkNoacNXbSfcfZMp9ErnajFZAmkIX9iUdY2W7liSY4rr
nm15Z+3lyZC3o4SzVQvpNgSf2xdwAGMctMyjf2P4yAr6dRQkoi010uptnLQV8VHVHmRefxGCn67Z
spa8b1cGG4GE+sJgceMuKX21n8q4i2fDHJHGsWLF8PO+ZZunI1Fdy7Q+0t5SuNOaaA5CZGOJmNE0
2ilL8futICyPu4DWpVwG1Vn32UndW9su6hM7s2gpQCBnvA9BKMJnG7a1e3qIZT6WS0XTx2XfKYpl
CYk5BxeQpC1u6AALGlki1pQEfDnsrBBEDpEnNpsuSYA+ruh3xvFU0iHDx5MYZdc7I7N3YXZZoMGn
iu4NC4P2lGAxgcxPXEkSA31msqwmT9GNjX9vzQwYw05GHwsl7dBtouKaETkghHpKWhSbEwF26FVz
r2dF2mg9Q5kKlqp+RswAgBbe92LA4pu3UIAuDy/W950aCUtSrFA4ExyazOHEoIcmP+b2r07q5k6m
93Vn5OIgoEQYyWJncqmPqjvIHmpfmtaS+TNtNtAxW7ZbjNIBVOKEGYXIYB9tL0cD3Jm0xKBbG6IM
4aZyjN5ZHwb2LhwnXWCSFRvYW6/JBsPODrEyag/fK5WrzEAWbIieSrkB3gw3oYGPZ7aTn+hzV3jb
g47jUjiTFzHzI4RWSZJYs1URTN84jnc/cBWX039PPxZ+PBMqJmMX7GOezsQ5g9kBdbjTzk11Srii
tFbH5L7GNYB+ifPj2ZXMTmCTz6mrlHhLJEJs9KtR/qdCUc+fYxOfPQgsQasxaywd4OksPvGwG0gS
1WvLTew+GDqIZ+4OjxfxJkCzRdrfyai5TSBuM+oajEewZecW6klwFNg2NoKD3jWzU/D0MwLpfyUF
/q6681B3npyXGoVcL/TMReQTn54UeFfNiuB7LzEc6tdmu7W6hQvli57nUXS1vwI9HVAVC3f1AlOW
lMaTY0SVBNgf2AHxvwafmTFWm+BXI9HdmUtg6cgTo8ncUWkJdsHkpfC3huf2E0DyNfTIea+nh7b8
mqOGLP/a+BvVq0i3y2Zb1EkjUcUAuBijvWp6KwQ5j715eNxxVuvoMN811gW0hKZ0MO9sK0c+o7tF
S6LKm20mD7q5n0cSxDOhLlmZFK80L7aqtYnEs1BWjpQ2LRkuNc0L87+80hFqrBez8zbF8I1Nuv/7
tVgaufjkHeSad6hprJQnfomhtONO5iLg3Ibgmy6G/BXbh1KXyq1SGmafWWVqIqX0tK6WH22VcSa4
D612RJbbAZnzmN+OT4mo7iGyugPoSh62UhmnSFyevT4I9XLwgjLA199zCVqvPMth9qXjFHbzVGd9
szG1WZSan8gn8fLvoI1llzThUSqkRQ8ApGegrgcaKGIGh3xthb6TwxTB0wFfCRk1u4vX4F3B1q0k
TqBSVJdYdDixxwD9lyxPY0t+Qc5f9WcCpIQ8qdagul16xPaHy3pJdGYXbyt/OpmxiEUxLzOFejy9
i/dVVdErrREpDJK1t9+utV+Y08yQdHTvdot2dcBKSkXndKrY6JKB6LsUWbcDxLvQGNIV3sG+qmog
w9VP4s/KwLU5ldNKWfWQ3PVONCW0SBUuuh4sP0dw4EbQf+uNcdhIQ/AF8R6c06CZ/nYDj/kQOkyP
XnH84KfanK391OY2tYyuupBZFmLC5L0+m3JBVDi38vBwwAJRCQhD69whh3cXzL+ul4pbbU1btTOJ
FqhTPjktl4QeYEkvGP/b9mrbWegO+XrZ56A46DUK4iCFPvP6RSKoiXy/WmBiiCxl1xSfnoSFtTIt
GXo82YGmwdFthGWblDZgXthvtwWNRwpKvVvWPYHUbqBZ0k2pxT6u0QVxLp/DzIRj9zZTWKB98n98
Ixi0JbJm/xXIOw1/xMgS/BQzHLb8/ohrVtnZ4GXTpnoTY5YefHBJd2QCAh+GswABGPFs8Cy4lExd
qsFOI7Cyb9Ip/C6vnYNNFgRl/AWpIixfGzhywYExSDqSI0IFWuYO1iChTO6r4B5absTmRt7GbUeu
6DqfX75GexwrYYFJsdHDN3ZrjK57dWfRQbtGh495ne5h4nmUfEGiGvjWIm5ceropAQraZ2BRwv5a
yuLo2TjAe255+rsiTGcnwJqSWJ6j6GnXu4DPVWk9C+F5sSEO4Gq6PFVeF28TB3TiRY6APNtx7dqg
U34aWnnpew78dmN1HTK6S9vQUvcoteX+F6wsa+TF+GoI1eJ9WW6Ndrr5sNXh6ZXdXWVXsUNDAEaR
J1qexkSo7ZyLV5exmJhHTRDAsbA4ly4lyJiW3ZUiVsb9bwvCTzj+QvjOjeVXNSXMMBeLEs0BiUYC
MeZuSRL1Qno/bRs8Cv89SDu3PPTq5PIH2aKJLjGQUbYACB5X7g+fFI2ofs8NdWYuqivhWVqq3N9q
A67I9J9MMRsBrt0/jM6m02ivNuPbRORItT2F9t9n5nUN/eIVIgH5tCKpRoqJ+23Mo0u1GcX7A1iU
AEPZ1MUgMuyvx55zCUoqXqCP31D2tVn5mDXyqXb2XYRVdAOwAIYQzigj/SyChYI0zvV1T6ashvWj
RuCJN5+Pdbf1Kr5i19yQF1b5lxIK/OHQxttgD7nOufu2Jy0KA6VCoVmTZlN1tOis9/S1WDeAWeKN
TWJ8pWp+VtCd6riT6wZ6GvhFLnqbPJ+eAKpDJUUJYzHwUKiiErhE7Sx+1SQQVtZDYEPv1sBbFyDe
+FwKIQX7vwjVMrqCRisDuY3tZuxZSPRkrXcZDaSBcXsRIe8C3Km9euV9XyKzbJugfxsVgxIRFWOj
3gr0IF4tYpv1mnjZNw3TfIFnoNNqL/iywTO6D8XMo4Aomg8Teau885w2jMaLnddIL2vgoeZdJkWI
Z5243m+ZAxAKddOdWAWAV6WqMjUOKquwROC05Hj4P02ETuwp9D+Hi3p1bkIIXAsV98rD/d8iOs00
o+oBjo5H4Cu+XNCzMHrWjrQ3Qqi3hLKb3curVvpO/d0t9bbV/5kZWzkDOMDvv7EKahZrBFzJXi/y
83R51qpp6DDuGiZiyQfIe4a2GkuxVnEjSYwt28JZk9WWlyK062aIG7Z9RJfQsQmiPyBIvHKAzQ6e
6k1jhqJDloEX4QuSm8uTYN7E1I5V6Q8KWKa98KZetJrHV8uUKQQh0YlEMaGAbo5fJ+E+6Yq6lOHp
UePff/v1LIpZ5iPPird7QV0fZnzo1qbeSR65i+3LAVL0BKNMX3Zw24XPpH+AZF9x0+IiSL9U3NZ4
KzARbExomi+qlGqNcRFWD2Fa2+LBx1JFwFo/Z/2poTqk2GlmtoBGqwTpcTrCKgaYIXJl8ZdjR+/q
RC2vwD4hbga4SNIw/MmO9lqd7BKCPbnlFr+fXMnEbcv+U0BveUiSY3xO+RZTGP1F1bFcGqHLXhJS
aqKHiwyR4UHu21AzebesU8SjaVdzOy7ARvTX7oIHxKNUgJR7WkZKn/EWRyYBQ5GDvCOFNW3Mv9Yp
zMiOLjuCdQMV1+0YsTy84TH1cgGWI0ShJ9Y2c06C5CM+Jse33+J7jHaeJWEVhDXXM3xRiD6W0J0b
BQYcfinfPowVhT1R++HhDcWFzVqPFMHozjwdEkUHuR3/bqZFaM01NJg+ZRZWEZy3MxQIskRJ3won
Q6diVttT2Ge6RC2KsWfmmRkvyT0jxDbHCpWkfBD6GnlJvBjgNORWrhzqoGfyJr2R5EuN8yJvxNMn
CnN61prtS+3feKsbyMXn4lsP1+Q2NlrTw/8RHan0eDXgJ4s6psaHcunh2uDPIfDhy4Cb5bWSsZwf
v78+HqhsC0FCXgiCuWctcpjJiNHdRlsRKyor2l+PW/zY9vdd8Ir9wC7N7nI02qdWAKX8QqEoF2ZU
3HKUevR3+jDSk06knLZldOLB+NcAKYdstz1RFtnar7Pzuxg1UePOtNQYpfASR8TE46pOVTFJHsjG
5AgABSDZKSyPIn+o66Mbb5vh+MFJvUM50pVcARTit6YVnXLVKNMcmxXOFecO/b44/y7lxLsdOtNB
SnlPuvNAZn+RQwbaiBb8ffVCjEUB6bUZ7MQtdPRNYMz/ohTNCZTeDd7LEyhSbLQXyqulgyVieQNp
f0TWcVz/5IgLIp2JSml3h3bHVQEfVPnPn7xIwpn9zs3wcwimFK3TsqwBch5BinKvTQcq/VuhYVv+
ockdKohWH/xQGrGphFP/HJGAwfyqRyJtq9VE25LKs12ZdnOyCj+NOo/Xj9B1Ad6N2dACXpeqPOWx
v4uLnuaYf0Drpu1EU420YblLQH5k3rrJ4DTc/v6MwWiRmeobrj3PQVfixnUZKkChW0t/HGbucq4/
a1WQB/ulz+SR4RvaAp5ny7PHpOAFu5v+u3frylEPfLH887D+JHd0oVCehgQKGfmbWjCK5VQNB7wr
bsh09jBM9VJbnf2SADgX4O7ULw23T7R8WBIYF/QaKwJNUVtPunGhsK7aC5wbgAEOWQAQZ6SIp+tn
V1/JMKXYbBVAoReIL4+v30DACfotPLWiRNaZ4iBM+jUkOGM8PNvq2QxnvdDF6YVgEYxs7WJedzxg
Xe8FSdwJ8KN/qIwQRQVf6RjfJa7FMUI3JMg8d9Xp87tWllzCXEVfo3BkWFnzxxKAemtpJ9zGxuZn
LC//wWSwwdcLkXcIj7mWMatJ6b7DHQhbQmNfsFSBMEHkgMKdFuCJTVyFvUYvTxpwz02R6jubmBWV
Sc9pEP/vASIQbXUb4bLX0sIshq3nbGaWkSYjeOYXFp9y4bznxHR5fZIra+5VHoKqNGG8XQV23sHB
8C5dsxDQQu9yVFweQOcN+K2ZDxvyMbwD8W9wJcrdkJlulDub9EjVV9w/8hrt3zhB3q5DvLDtAkrZ
63AhE/+Sc3JgE/uUvqykMoMQ/UAkG8Smg8TwPT90oMk0TP1hVVnwxXuS8frGU/Mm3kUE4vJJ74y/
HZ/+slngaGotncDnXkD10XcgZFsj3VBsMfx3DkqqGzFbRamYdwXgUGa1tGcldj2Dfo7kIEo0WcoR
ej+rnZokNMqim4s8wvaOsRPPZgiv4GJhpI9FkKvfoU+s1iI9LlrMn5nrGXhIX4bIxKreMLP+wCEb
NhVzJo0fWQqusrkRmUeMkEO6MiYb946ZVobKGMn6XJcn6nHz2iuTIwkuuML39s42FAiqzCe/EQ1B
6cAYF4vR5jbuBOSY7kSUhYeZBpAkNgD4qh7EYMz3x4yd/zrjSpXrs60E2KDKd5eNrkC3h9FP+aCA
vN0WZOJsmR73caiFCWsOfYihAOsWLzGEBzYgtc5LtQI1RlKZPljdD6mf9xIRCkuuwE/aUfv548Da
gVyAeey+YfGmrG59ea6nxGfoTK+18e8AJRPQA56kZVTlHSEy2e2d01q3x2Gz+BXZqAR1jOdqlx0k
2SHMH19JGxY/wR9+4ZWVdv879B7shU59EJdtybX+au0zaaju/L2P+MS+BzwwGyxvsYTKk3/M/Xe7
bT82n/8gaKhnNzPZblpCpRO9JfGd0g8lrS8ke4L/FS4dtwEBXNMy62MS3z3h74xBYf3inGCAmdYn
oWrm8xr/BJNLKlOJH6lEt/8tdpy5ZE6stBSAqiof0+y4yj4wjNtV0bfwN4+Ul7j7U/PPE9gP02iR
LDs+6fhmPdQDvahTF4JrKyjJhPnmyMBPXlpP+mxBsmHQ3Nx/fW8nQmzcyJMLQTo0vJxY2/88AuBw
8BmpFSXHxAiuKxPeKnlFwKQzLfZ32niYd/mJsfR800xcd+up/woCbFkQGQDKdZxFGqhV6Y3Ipt9/
urj3SW+JZVmH8egQdiFActCoeXfFWHkx4elCJjp7XxjbyWLhKUP5I4wQG/jDyhDxC7d4GD1yP+q+
cThBcYTebKiBopkxu51ysYFrT5K/zpujoFGHUC+Tqq5a/2OtXx8Hxuyeouselr0F17jLE+BR3kNk
90DY0G2/iDaBRGaCZba6tcmKm+6Y1ISiHaDOJzpCptDxLsfvWV6Zpb+zEmPqYgcI9YcGP9P78ef8
dCFgJQ7AnVxC4dUMT7FXBF2DMxYQ+qLVyujQAReH65u0egO4aSZvja0yMoGtBwu/vJj7G6wz1rpk
eGP7n7ijj98Kx/dlCMiRb9PS/MUa93dIILxXGSYedUUMl7SyaWPsW3aNSHGRPtit/b84/Mqctfwe
V7I8CtBV1R4E3MF9eocg5Cs73ZEjXQA6ltm+m2la5tAR0Rl0wcTNVDArQ8VjqffZWGgZmZYCp7i9
v/sfTqKc2nY1j6Y9WuM8nPJHgaxYKENjGN4eqzBe3rsD0ANWfP1x7NBgzY7eHCSAW/rC+C9Fi53W
itG1eIhKGxuyvz32lIvJRB+RFmlBWT2VIOfbsF3wJR8bQjCVaP77BFj6pSIERk3ydrX7VDlOfo62
S8+PIO/iS47gvysUiXrmF86SvbBOwTxYOq5Fkk/x3PGftRrGf3TYjN4NRew0MHVXQ++G8Bkejn5M
TUAKZbzG4FFIbnAjJP/61Fpdqp/lZ5tdFLuwfW6eYHdAJPU1FCnVON7XR20j2z+xg5dAMjWwz7Xe
6WFbmeykxnoWgkaxps54pmSqHdDRC7z9RUGSC0QcjF8jxpMTA5+456+MSn26Emati9jW1UW605W/
tVDzGtFWT9s87oYHspTT4is+I9esmBFe0DemkHPOCmxieHgR1QCZGnZFvxvOVfF8/90rRD8WpOtq
SJ6sKCIfG7pMXQ5zM3ZuaCK3jlDAt7ZabRNAC7eOd6tma42Q8PT+S87qKRFuZ+YsWrB0j/0w8Oh/
FgKARmtTRoANHuw2qcf7YdGOwCzOHlctscyvRYqvqDGLGe6Fxnd41dNrth8b2VMM/BXFBSRg6Hdz
79OHkK8wwsufjpeoa1rsjD3V2nBbPEUJ9z2Uq4HoY8q4O/dYRvq4KfZC7sI9kg69SpbvbUQz5Nrl
nCOiz8ODXxT3vQFLAX9KCn5Vdr3oAAJh21AZodNkiApT8gZ06tkAdnpRTPd0u37dfOTXLAV5tnDp
tK68EcO8k3Y5/U9IbnUzAMzBXO1acOKGMoVNEFfArJCWx1OYh8okHB2neHDNNmdAdwxHaxexDlhh
kFFbHgHeWTNZ96eNRG6UchSm0Lniel/V6V6MNnmIrQgv+HsmVH0sPyaql660OaO0dSJo/eonxQPU
HUELcNLTLylw9WgLMcSY9u/57PyZyEYBT+Hr+1wcKekhbqPmJbBn1/HWeLEXshAThzFnaWrwxQPs
sAlBUXVIaavhdp56smSzv4qDDiY69SjtlwiPywO/IlD3UG03pCZnxIgrDDKAD4q+JXItaWsG+uPW
JnXqBORensoRcEAkJiAoFAlYeQ1UTiQAVeS0yfeZQ5dY+T2G+snj08lyb9F4Rl1mWdgwPkLvObgL
9epW15RJXFFkNPg1B1kCOBhlDLmRlJGSMXxFkMtzxrC5aC+/h7Zq5/0aDYyuX+M7M/DTHC9OZiiN
WPcG3OGX8wtSI4q7ilgIm9LSDdL5puKpb9U/+4CJcJnTLUmjMqRV+Gby5u5MmyN3IqGboIBcj9cX
pyE5Uea4pRGtVXBkO5YYEUM8Mj3SaXUHeeOcI/HCTXzPcHZ5oZcWc0jv2Lu0+OtrPImhGYm7a1he
wUKSsvDtecHV5s7EoeFla3uIkHJfSAzhQ72US3G96+zHV83Gafkf/QFTA5Aka2Zz9qBd1dChnyhU
W2CP+8RVNJ7hdA0vR+OmgUTR4aNaQ37nkhEMfs7sEMvOmAFqaCkzaRsONyuTWLf6E9C4eP/iKKjk
z7xwwI2YRsUbod4IGDfT/MNmKqjmqJGpT9pof9RVCb6WLPvSGONhble2LTzGmVTVAq8OpohqneYj
CaJuv4buUvRajR3elrBpPARp1GmfhVXpxa6PkPrGE79Z9Nr12J754wgxXKtBIOWcinqwttHbcFxa
3/Dhg7uwk8hj7QZ1Ry+aMlF6xiYt2fkV/qiVE3D1tDy6/gZp5qrwZz50DZFXROeKo0a4tVB8E+7M
iTQ1TnPwarxphHzLr8Ln7sRAf5i4DzskGkR8ClJPdtq8rF8Hp/7oiL3ktM15Yy7UdnmgRpqN6BTx
xpuEo4UJfP7tt8Jpa+J99erQ8MlseRo31X78nrxgEkQcj58F1UXWEZZHl/CpaZ7B/98+9Az+Tp4O
N6t3/POTiDiAfunZth7ndZ3sqjcXKr7B4B9qPLQ9EOONWTVD5vWOX7+fUrD/B7rOJygz5XnTeq6N
lfn2yAyybebo04/sZAC2aOdwP/OpFyDhcZqDEttOG/GomBAJZfA7YbXa8GJcS/4RLRpuTybWRLKE
GA+PxB5RetYrCxQAiq7/TUHZo8DSyzORq6hiE98QChESbxIfDxJ6Z/4ivfByjt6Pt3ZiNv6z7JUs
ohbNysN/HiHpsYAP1edGRSgEBgw7qqYckQtVFhld4gN+aZwlYvCXM1/k1oKj/f2FC/fvVKVDdBJ/
Ndx9XM38iLEfXt1OyFbh6ibayGm/OwOekPCr1rLgAlYToW893y1uOBt/X+gD0zOXB01F063qlPT7
kkvKQ8v2SY8ONPeCNRe7sxBd/y1AP9t9UB74bjHoPrZVLJdzZcuyzJ3UaCt4u3LHArL4J3leVrMt
bhrjRo45K38/diUlasFVG6Bm3SMKNKODEy4ObaNaR7JqWlNEzHPKBz05VKrmlLBfntpTAAQ9YBPH
sbrcSNOlCvqx2xH7tx44cei/stGejl9CWIJtFMYjg6PtNg9kok2AZRLeIMZGqycNnDbj9QiTyAQG
pgaZ2J/XFTBtrb4i2PSSzqd7dGu7e+AMM2so61dI85uDfLe3y1XjkKHhqLvcdC+/btG1StBAHyJG
o8qjIu2h9rvPgOWdluXeqaIbDMokWcnYtwGrxERhlACnRafpGecZqvz2jwIpHbR8W64XzWvtCGti
CKntrveW9fruhUsWvZPJ90jg/xiGkhIKtbAqh3suN9JcSwuMjS61nWG9VTLSGTnSYDRnPbjM5GWx
J7raZPmxtAdy1P5Jberv5zhUZ9znvDhxferAMezeqM24Lfeq+R0dRlAS1EkZnlNQzd0pkmdlZqZg
YlEXxkiger3WiZr7QCboJoZbzlWNNMtcadE8+sNBQQjWgTuwKOEo/eep7W1/nT3wlCpxmkp6A1eb
/+Q/C5Lm0kO2PztjS82mN+OrOpMq5mFhWMSy/j5c/iREkY/CKJDDdmcfrs5W44+G9J+x+qY3rLFq
0PR/+18wfXu0+rWYub5zwSILqxlVDGR9fkJ+p7AIJKxP/iHh3IQvr9/jRdOJxKFKflYF2jKDEGtE
2BZzexfZLJA4ytcUNhu0LLXpUHIaMc2wAGdWpr97k2ZbALG8MA9CL6pfBGEacvjSYFDlxoyWpU8n
oDayh1aPKXdHW3+bEBoBh3z+jsbF+3s8drJmsn7yGoo4Xkl83qIigXXCs1F8SuHfe1xYWuI/1mfT
urEu/wrARyHdHHD6mKApJTfnMmZ0Kc8Wu/TmjNY9DRzv62/OaO5lVL8Onnx3h5pNN22eD/2jmH65
0A8OMKrOc58AqrYlogzNgb2WnNJwiWGROcegnhnaDQgvdKpY6HmdqO0Xe1H7h84yFY+LSm2i87RS
zMnu1ucAIm7sNtasfx3nX2Qb+o4WZbvUjj7heDH8J8Gc+ws/TEcHqIh9hE0oTTfuMjGtSV/jv1tw
lrigLgvvRIvd/jrkGRD1/ani7/ze+BaS4n0skuAVs7QivmPaHrv45l9Lp3uCt9loJL4n9/qrHOgR
P41b3c/l+3yq6sMpViRjVQV2Lfa9RM4WU2j5DiZ0o7FIj4aL4vcB67ZCCRaaQoLAJW2RaGVclbR0
Oj10RLZfMbaoqyJCQ1ipN2zBmv4UKH93THfbm3/j0NkN1d3/12+JHr/yrsJtLgk/QCQ5wrcyiQux
THyc3oRofkfNNeZKYOlLFis7JMfSqVa2GQD8K99A8GIJ944UxYsnjxdMsQ4MHRTQktXN7pq2gzUw
4w738vXPyypelpfYMq9WKHMbpLjjqcw0eTV16tOnkndrlbiF6gEhezAQNJR+kN3xrB0JrpO511WR
g4N0WkMXiQ+FzCGSZK0XGOB+JkgkOs4zhliscTlAcjMtEAQkj3ldVtZb/Z6EkSzz3HGqldldt2Tk
px8LhEgqAJjhoD+W9FB0ZMVGSClcGzFpKygKlSTh5/kVctjkm66KTF0uUWqB/x1VaGzvl4wnqJAs
1mmAk/KReekVZ/EDVUKRzQCqBfvcxztW+TOXDxJp9DwQNeEbw3mr+6dBMW9EP7qh6Hn5lVPnj+P1
Pu2W6P+LjpT2tV7NpsR7ZYTA+ZmAHHPIubrA1jDmKMngLWQrQw85VQOvkoYi28dCVLEL1MBEQ+C7
ZUDZcE0m9Nu3p7UP/oNG/szczT4i13L14b5q24la3XDD1dp9V2qljVnl4Ej2pnttDydQBmMLi746
OEdiwX/YNcbtwK4YnAQF8o3/jogUDmK7iVAW4tASj5v+tKpe/YvCN65xCToFudSLPmFV7eUFRChC
hpQho8cQ4FL2x3EnjMn5oxsJHTP/WlQrVeiesdlsy39CK2k1gwmRNZF73PIwyk4dNLnRgb3T0S8+
E+LHLZaRjSWukx4/x2DiONqH0pIW5PHR7sFX4PPyABvic382sHF4P4lnDf7sX6Q4GsX6nOCO71wG
zSAsi0reFv7tnFRs9mdbBKjp3DRUcTSSGHRE/K58aoCSG1R7SIPa1CDgHbLY2ONrFkQb4Di1HK4c
5OS/bebk08cpIZrwEnQl4EyUes4OcxWm/wlQidmMZ2y4mkJgMECx1gyHQwtS/BNauxNZxf9OdIVc
aXlS5OTNdTpqXExqtf3FX0O43ncK/KUYWdXGQel73D5rg3Anmh7tayiE+h11pVVzNkyXTWHCZR88
Q5gHnJoe57TOwKkqn69oWH46G49cRe5a4L5+ZJtj2JdlbyxYmRu0CZTiVe64ldhLJz0k1RWEvVho
kX41guDAO8oB4llF9A73MvLegDarvDazmGQguUQGj8Epsb2wMSM/eJt0j7whvITBgisIzDZPehC9
o2rcMTqdTPXpJhzfO5JL2CdTGsdV3vBsn/Deobg+wwiPDuwbfiIpK+sdUVjdOpJ2z+5RuihadcKC
9VKBwKAwCMCogPfVjxBnShK8XPQFb9a4dNZmTFsbtOFmua6Oa8hgBiZt6rWMANfdHYvcZEM3wT40
EzvsR36zubU38eiu049G3obtRjTwt0VHfQ1A9V+JTsudgEj3V3SXOhi8tGatun+T91MsVukiQRwo
rQJV3foUOaAS+tel/zZadhQsCwd31M0Ol79b+Rj2s2q8qt4AOEebNGK3ggXOvNJcDIJt84K5ovcl
XLbJasB6/lzDPTnN1+KlscOJcNkpWCzdjznaDoX7LLGBb4IkAPVoBRqBcHzAwatLhlMrNzU4X1EE
fQazY8u5bxHZLKloDiqnTJGFNR3ankio5Ciq2WsYXfnl31Fy8JOGixYb4bflLXPC7GqKpLKKxQjC
xlkxKwhAq3le89cs4dKocOk1rWtKoVuf2WLH5UGTftH/RvuidAS7AzKEgczyiwgOHknsx5dttfCk
AI2TmvAZ2L4U6iWeLtVlwSGguoKM7JoWZxhV1cLufutRj575qGmiMMOEue1xJyT+4eBW+A2arlUk
fk6sWkMSlZHnnmvLrs7DIuZQM4S3q20yxweCi9T9vLNxU7Jz8dBSQ9tv+YLjB5JpANdxFbQE0GnV
9nUOpmR/QMAqIOwlMoGrmWD+aAxh9qgxW42EqIn4MBUPuHDgUb2dZcJNL0GFYACQQjxBHZPhr71G
W1SxgZgi0xTr4tIs4kHnWr+cYc5teFFrEqOwchJMSvWPG8OOQTZAw8MR5Ud31Cm19R8OyA6F1IyI
WeGSoBCTREiVv8ZuJbjqRWOb6aY5f+LjTLyn0cGV+T+u7fkxtx6NmH7Lc+KesTVG27MdqJn1ToYS
cIZG0wUvCrrq62EMnFuP2ZcZREvBYDejvPBRXUP5WF//87boSAjcUaUlwiinpJL0I+OWVOStDhDV
dVrfIn0SVDdPMztu0DfOLG47fqtgD/4toHWV13c+J0jXXG7qvNIN+vzRaF6Ks6waavPSY/yhFBzd
bHD5lh8TAd1crQxnmhkjSI5yaK75417eK7l10H0QjTyqlerSRQRx/7O2fW2w4DrSMnFlGgO2TvYp
od5cMf86xp6sNCcyiENT0X7a7C1CqChdUsnj8/ENFSc4OrKCtmNm9poZrC6/JOcCSmCy/yse1XQt
uNSxDRKV48r+ZJLxrGq6HmrPTxIyzwnXWz2vkqxDtnNBgJa6GvFuAuJvi/5tc04eCq1JX33RdP1z
rHKdu2tq00mdsr7s5D8//f/0sGeBmfXhBbkAfjoPvrba6PvAWS2ClPxhq+J3hf3r4/dC3VZPJfNn
HVEqZFemR9PxAJOTbHzt8ZEEuxdFpseOh3iwLN7vsR/fudmXueTokeYqKszMaXT5GSrQ+iaA0xMM
d/N/Ki5aWQ2vyhhmidD3PLkHLe4NXRCJP7CLxnqnUOgnwoelY1pz5h4YOcsdBj99Dhki9MR9B9TZ
PAi0XKp/DkhCUjfxehpKQldAvdXSGPcoLVJ9YimpPlKCC958uGlAp+aE+AWpBF5DlzQ+uHaoslDs
9+DwdYp5Zu+ujN2AKl4CsCGcvo6fx2MBO8/t1Z/1/A5r6A1G/e6l/czOyYqq3z8rDCVc3fnytdyv
LbcVImbt1taaXwJ0DVtoXy11BC3WRlnsvc0U7IBt1Gi+6nCxybX5crmeIo6kLV3Cl6ytWUwSYNYw
NNtasX3lKfC8Q1BsNQH6kdaa68JDNN/YAwW9rWBCcRgr8p0HD6h2YP5MNG8NroKcjMyBgnvcJA6u
PrG/YDIYJXdH/6W28P/lCo3JzBXXUqloSREPsKXQoMqcQ/JmDhQ5Cqi8mqhD7ecscWM4dX25WPIe
nH0Wbjfdzl9uDvswLtQxB+Wixna+FsvBVUqnKCyyv5Hz7QzYKCtiyWK+tmNLPZ9zjsrDAlu3uKoB
/c1jzendEKpGqWarF8jDTaDedYDiUUEgPxPxwoMHP2nbBZUhkGcd9XVV2Q6OHN4tv7/8pkwPVnAP
oLIVzTEVLc0gDqpa1TJGpn4Bv3nHwz7LuaH94mYDGjrwHGX84BAux+0UTVtjGJU3edFWm7Nyw3CK
MLgP/VWsVOp4m8OzdViZ/HSawF3dYnBbWzvUlFinc7n7wMIqoJnphms8KtBf+uGqyC4MFeL/Qsei
XuJvxXmUIjtJ5vfAsB/DWHjBNLuUk5jVRbw7bZXD7u1HvV/uLL/OL5f2y0I37Qe4kKhWrWdWB5Jn
oCu7OIwbgm62eQb9g81MpsH0YqL7BzrD6v4ub/L9Y5k1kcCfnrVEJrP6I1xk4i0+PV/28LmQxX7D
fZgVpfHiDu/mNVSoYlnwdwHaV6wPZ3LCwZe4PvReT8UUpLcHhoF6T4uN5HGlNENrU6PiFmg81uEe
41jP0thaoAq7FI7UCayMat9DuFkrTd0F0GHk5zXhZsxu9/67Z793yuK+OJbf/UV6mTpGllVTyPcM
BMOEDAcgIOKwKHR/ZvqRTZ+pSgcmj9DkObYVAc2dYpEdS7bru9TrxCcSgAMMjbn1dyzeNkoY/lUd
9OJcmEIqpBuQ2fPMpI9WEQEWHFcpqLrm0ve6poFrk73igJI39uJaOwJQ46iiQjbpZifYGkobq3ls
kkXrK4Kyjr2miDS9Wd8OqRWIHHGEFnJtuyzjXB6xk6t8OPCkY9aM1+CRnUBVATYX8zsrIsz1yGph
uvsdWMx8zFL29sHPyfIH5k7rVnHgEc3Zt7cWUMpuTctiFersJpr+qeRGtVq+c9rrSnfvXBh0GJla
Za7sczm4r+KwQ1geXo3+tuqb1VKaWLjYx4HuW7uGJ+NP82oDXkqv4L2Dp7mz6Dp0YOhSL5Plk3w/
ytZojgm1UDT97rDi59yz6LeKlGdojSIZsSUeWh7kzIcy9oyMgtRv6+WwZk5gOW3wQ3G8/AO2qWGu
Lv8S1cgXW3cuReKQkOD9KfdWJGuMnoogHCEF3bPZwPqwBilf0auB9cBLNGVuZuH9vooBc27NUJzS
msmAOrH98r6PB7SlWi0imkmqgVN7mX1cYDkwBorGuGqDTPKwaY/23MyrAOBoOzBq8lykh/i8PJE0
oj5SlW9LKZjvxmVVufN/ttbUjBNtVM5kzStUfl8bfEmivZVI7qbNVZwPtNqcBmINB6xUJo0cnHZd
5oP+HXMi8LTYpkxu0HO9oQRfluzfcfYFu/ZhaZ1Cf+RVIFSK6yBq1rt+Z2HddKbVd4K9Uuc9UE7X
c6V0VHglD8CUKxxQjGF9SrjLqktH4/VhyHej16+Io3U0OtakOJR3fLx5z0iXd06ml/wD5QDgiS5R
yXxxrG541R8BSaT5L0ylCp6S0sd/aO/qs3yZiy5TlH1ur6UneYYRtZVr7PWuFwsC9wro4MKG/xnG
b+q7NxnShdj2Mpluz81+We0QvvmZKLYVDQGZVwUDA4z0iBGOQrlWkDslUaUvR0M1cCiV3XWMO4uj
DnWgnmL6GAJJrMaV6sa6I4AITHPdWmPOqxdHh2+zlO6IaALyB88Q2DKJWb8E48OqcM7fHwLZJG1E
EdWFEwovJLe1k/MWz8uG+XBEki88/meucIEHoimmDgCTf6SisaFkt4olwpx/kTHRH4S/vHoxeU9C
SMYYIHtn3Wajb5EZX84wHto4tczGwG915GA/LgnqrVrXw7FZoC6s7uJ01NPp0Yj0etXiu0za+jtu
hg9iLUlYq+3nXfG6jFp90N/BykfZShCL2UTAWDNY4oeZ+wmZmLhlzhp0ZU5QR7rlGGy13n/mwixi
YPe93BsKZvBeMzO8eXR1N7C50gNv8VtiIBNaNUPyzYttiq55KH4BMqkVCATNhMJ6vSwHdP643ZOh
PpDdBHk33RR6aqRTdNUicUCB1254tIK+5z1aWIhTEMKjWLVSEE/VLsujSbvYAMfa547P5KbGOQlH
HrRgFXx2ds/iMHl7Rq1B+KKlqGOCPWV6eUXbTijfCK6HpqWpGs/xQtGyRAFjZRyEfhzBRa7/cLso
rfjyhYGK9CFHhJQVcCP+PJSkqC5B/chYaUO5ZQ+p53SKD8VQb+f/A51/Pa+ZifaeBzgzlrdzVmFQ
7SsJl12kLTmo1oNftrBTvqnWOzvn1nKLucgZU6ldyhWNcTIF2w+fxhzHKQxb56r0WUms7WGQd1zS
WhPkJoCOOFA5DsPDP75x8qD4x3fC1gzJjKdJmAppuptu19laklq5C+NXhwEOc2PEMMI3ntUHxd/O
77Zzc1MCADcmXgG6EDUbYNk69suI0k5ZUDaSZBwBg3v/nvPcARS64+tXmyVm2BY0f+hLDzMN6Ibw
H9f0nUlnpGsLRnKJx9GiLXQ4RNN4OA1QEjSh6yIzSPnMMTNSEVmL5o3OFifEv0FE+9rRrYzp7ain
9OkT3Yl8tCT69EEuRlVLQrktmHWU/7Nhg831nT9bbdOA6MHFMBIDpX/bSfaOaIiM9LP2BFSiWPgD
rBfgcc/LjWji6ZRPqEQzFmomNEf9up+qO/xXfyGCv6F365YzNt04O9xqrIP3n6oETUXLumhnblbY
kE3qwQSbTHvCyhP81QDHgyByvOdsnEtBql+W5v7h64vyp3bC4Xnk+2WTcWJ0+A539CU2Ua+M5zA2
pN7fewxrrs+V47r1e3fFSlROcPt6L4v3FXcIKexoTczehd2L/ngo70FWI2rl5YvgWUuGVGSfHUXn
Fs2OZhJHB6tf1Zq8mHFuYT2nlhyhZzLBKfpEROZXJjvfx3QZ0AB7SOQSxabNIMhNJdVj+4Ta8KPw
UoKuxyDq3lV58mofIJ1eDB/YLliV5wLPhZx6UxyUUOlcJpkKafZeOu4B+qRPS6AtSvtUavYCXlra
6bafHItgUGqu30BQ35A0CcwSh0TIJVMZ77aR5/TQn1RLaLui3B/rPSt9FM3h+fJsANpYnQODS+Tl
Lp5rT5Na9YnJHhtQn47tsLvxU8RBtvL1IfhXJRILQneQfH39gUlxej514AvaI0kDMj9s5Fm5O/SH
8LdxniyQUgCqjBtfDzl/mqzMndgz3v0jZr2tvSc96li1Np+sUgV2xQTJQ3RpHwg81LhYw7AaITZz
1ajL/Bz2t4PrZ39UHNYo/Zb5LeifB3RRf3OY+0X9JXV0eCu/sBl3bFFexpCDTD/5FEHkiCXcSqXm
Dh9/DqWuDZAzZHGfDtCQegw6UBIgGDX4aYoSz6Os3DoSn/LXCBjjNqd+HbsWy0xCQWe49ITFAo+S
wVgcSdVyZfwYsAjT7n15KWbuVgLUWLkwt587dtmYyk4cqYlsr1FW5cWgC89fyTnDlwwL5am+GKiN
6dieoP/ru3fSGcOXttEIIZYbmWRVwdvvU2iBTREMWxWJk6+irhN6FDpN8USRKYKN1jctuysbf+PC
F0fm6lzAmYRHmXcnkdNPGD1tXRgS5XHqirU50537wC5fM1ROMd7dY9RqV8u+4vP81b6Xq+W43M2E
QyUBefG0Dp4rLsoHTdjFLq50K7ZFy0g/VLg5yMLsIoWWpQACG9e5CP4u5UDk3psR6c5xDGKzj7G7
GiQqlzgqMHT36pJWyYhb5foiNWlnEwjJmTJW604QzpF64YnN9pCx1PACL7OP/HgSt8/wkiMiekVm
SoV73rgSuhHRlYkH7Zec2Y3EhgjISMcYMgO2A1CKEtvrxVZbEL5YLqGXgyg1Pa1xfDB11LXJ1Zdt
QGaicmJnx7n+bKDjibOXhKVizR4YTi62bTYhGkfTIoABJ9q8IRrForPkfxR/qgvjz8jLN8+9T+oq
O+g88r488kQ6KMBle29razXoGSl4vqb/SZ+0i4LhiN6uKE7q3XRz48XPa/tLdy8GwaBjRbekSTmV
gB65eDy6e/3+c5OeUCez9iVyDcjYgdVFGnls0KkVs0iN5N/sfhOSIrEbr9hg1YIgtsc4ivzSOiIn
V5JZ/KfGk6HVhmTH7NSUZpXL2qspX/BFyi7L2EVC5O+DRrcnS+Ve/wy1NtTisoXuTux7fq+N1OP+
W4i/gwJdANfZrCol9PxHTReh2lCHRliVodqL5pLGoxQhs2ZOHolLO3RUq/R0P5oH7MGeg2gJ6YYp
2mi7M18KV8z+tBSTyr7Bze6rpvwcPnuiPCvysMKngCDvRewdF498XNGeDqqy0DhpuQ8RxpqwtK2r
7BXTn7DwMcRXpxyPZMm+bE8cd7adzbRxnkua9lN/9QOB06/PeTt5VBkrmWaPeBDNFF8gW0u04Zub
sOB9fsxJkdhusjNVV71LahOiziQkbBbdytolOO/pXVbRJzcFxvC68ZTKTi1d7HOfMrCUk15P2Ci1
eYMBrG/t2kaiABwqUChatm3snUNNdB+gqrXXFf+AneAJkC6HgIuXFqJvhUagr3Weyphvkg+k7pZM
MJ87M3S61reKXRvpImjNfO4zJjDC4uvsvmkgSOUSod10xk418SSB3j9opj/oMR6R3L2NJDFes4YF
ec/42PiSgGeZvrvEU/q0Vn2QbWzVA23U/gXTLssPbkySLs0YwmrKtnXf3rzIef3kMO2+RzXWNnAk
SaE+5XyQ47hRiSZ00/8nJ/pO3CqvEdyO6C2ImlJ5iwOrfScaNZwVUVX+gvuzpno2cEkVO0+VZgex
d/5mY0IPMv25bGlDgPei8JLiIHbwQ7M0C3+8jqxcTcSyiStOyAQYSr/6D4vIejgCmgGyGLTckcCA
R2Hs1br3CgUBXq4ICVPCLSCLWGCelqHt3O3nacHYtyvqawzccXJvJCn8bsF5ufvqvnrHZbjJp3Xr
ewCJkaEoG1bbJByb+zscAiNf12Z15329GZORSed7bwxMEb4fFpdSzCZVazMWVSl04aDb0ObrNLVG
IjHRk9k2CycLAoZ1M0FxTEKc54VsSD6tW8HFDifjAoSF8YRL6ekTXLq6fTh8Kg+1Oty3oWYcJ8Mq
9ExRol22ftiSpnEOmUwBa6fTPr45UCwCfLi4ekSuCK0UgmxnI+mzrzbsSq5TA4rGLcmJdykShI58
DVuTrdowEfXFPTu14wG8gg8kzxEyZZ5a0BY4eAJgX7xcUdDtl14k4Hoqdi733V71q6ZlMNOqpjKi
Lsagkw4+Lj4g8qo4SIQ05M+WBHVu90kvsQX/r1QHrH5YBRwvLmcpkw1h64+2X8SWpDptUkwu83w0
npODhACsyQmbVpWcxT0J0jWojDEDI0zDNuwIfCvLzgBLKDZ8pa/Ir/jbSl4L8sYUjW5yOrjWvu2O
TQh088fJj3HISSwVf1OxF7Y1s1hcSnfRg0pj5sLctCjPDSiQ7lXlcZq+DLIHDfoaSacpTWGPGXz3
gIOiFIQl20OTKBFS9TITysEaRuZmMpAPGt/cPeASQnwnNLA8VLP/0iab3Ite+dNYQQsQ75P/xo1P
Tr0zRlFs9pIBM6Sh/DCly7fXia3EBwme5DoIr6uUanF261o42qlcCOPrhe4dbc7ndY4X2FPLFTF2
1XRSwxoydKpJhvfVeaDVRLy/3kgC6x0MtxmLMMRuPIIGChmVCbVjVLHKX2kAd4IdUWHtMnypMiKO
qOy+bDPdQQcx5452HB7MZeq+20GzyiNkh+Meo5bFf14ozQT3tUSTLyrVreoBahqifCuZqyJ6WZfX
A2esHOHuh1UHPVGgwY9uFTOpZNig2hDxaZ4Mk10uOH+CzZYAzEyZJQPje8F0aHxiaBRJGwqI1uD8
BBhxf3z2XyG6QZl3JZIWo+bi7Sj7lBdb7qB+h1p0axwnLbGpoQCT+JYRvm09OHoxuM188jtrSRZe
GdPaIYIM1NcvyaQrgZAN2NiDBSVo1oAUOvLpNSAFRVvU+gofYQ6H8zWM6pICRKYZD6IvslsFe8tk
rWH++yew7toHfxhxScwU/tPBlHWCS9IKyC2SxhA+fesJFl3GifvBkjVUj2jG26Nk66U/3Wh87Fq0
8kJYAIbBhPTwnl4TBrEoJOdDcSyTeV0mgBK+STh8cYr7NMrxO++roqX9fZvR/aSHNR+UCjoyjfwm
eYTYoO4pvUPA1v3Kxc2U0JYcUdYmhBsOOknLw6jhN+1WvVG9QPoVLZXO9BNNM/EyH6EHH63MLEBH
3UAH/TTwuRmDzSh6TH0bX4PmALl4cxt2Bw40/5UB61brlwXA8Obe1xqe11+SoAoLdKf2FMqNW/Wc
zAlmpmwGoHPu0Lcgs9B/FOj0pMKp5Ftye4eGMMUunRz4GG005Js8ZUNXCRh/Z3mAzRfIjOl8kX8L
+1etq/pxYz0n1lmLi3xbbS0UAX3bKZR8ei4c1EOQ6mCXWmCHZm/D+X25bVWNYddJvXH+OIbFoyob
Dmuho1k14pC7lV83y0IAu3b+w0gL6bhWnveLIy9voye1ELcn/x1/pHOHXACYnPf+2vgGKLnJ/5Nq
Y4oBbcf59Q5zTPGIGAo1gD72ZaJWXBg5hpHoKIy4wEYgczYqFg1Gf6EnR+zq1prMS9m5qTTMKSG1
8O3xXeKfvYp3uPDnaqbWS7bD17qtCQ6zqgv3hPEis4YXYpIOmz3U2j75uc+wMFXFSLgZI4x41dOh
7GUZzhHWv5unb5W6HnoGD202Mv4L6gT0LlTZrAITLz6DUtIfxh10QL6I7d/1wgBw8k9hmDJAFJW6
w4haiJGDbrxSyoEsXtLeHukQpdYnMES62FlXeIqJwxF6B2vfrU/0HncxDVKoVMV+OeHx/1xInR/1
v52bFyrGKgMj5f39XWiUadBGwHOBsaT+pELMGWF+jEkSFPVtBOEvnCIbAy4jktSicaxNYYYftUsu
PDJBZK4kDmZHxWHZkHPbQJBm+7dd2XBHjRhxSXhCZrvPXFyQgHFjyCJp4+s1qopVGwOZcvYUY1ex
UEjMOaqPXhnOQiS0fX9++wSWUPzYIVKMoWw9shjNzMB7boVd6MiEdIJ5l4FyCeMd4hDLbiYX4z7G
wS7Jvm/D40jEeEr5QTKspz3kP3MhoeV5QNrFcj8y8+pcHevrDL4pypBWHRG5DcQ837/7cYi6+Ff4
rpwFp0SMD97mMC90qQ8ZYJP+Rup/QhkyR7NISSYF95tSVwO/uNx4S/v9rVaHpJbGK82OcCd6fYoe
gzBXuj/fOvACPvAk1TMitmFU7xRPtun+sCOmGR8d5oj1tCX5UNKMZcBCqLSykn0/mwiuGR8U6CAG
6XPSaI8deeFWybJpvehdTMVFaOo8CxxGxHWph+y7quO78kWfX26hwvfhOP3gVuHSpcWhHN+pXD82
3MUZYUanb+oabBcCJzWY28bsXRuilMgM3gMCvtS8088vOtbHhAlbvsFYfauCCfRaIUP0WwRLiBYF
AdySbkoioQZb2BFwLv3kkTTwQjN/OGhV4m23ugzp0dFCXVkDrDHpvoSt1ejKzAecuMTtRCnIDHxY
8qsIyOtOaeJM6QJ1IsLPhLkxW8jUMsoJzN0qu7pGToW2y3DYJPBZQJwwwS81Hlly7soxFT1XLSr1
C3WqOD/oPI67/cbeXKeIszCmDyGdICNwlMECukbbgvdBa7tIS6cCXq8rXN5ZzHgwR6vXHmrQSmht
ebu3JEa/pTLDz8VZ4eRw5es0mfJgxgBCt7fKuM4Ayy+KRjL4wEDfS253Psdhs2Eex2JHeXO0TJbX
4rUFL1WN17FzA+AK7IIGOk2TXxyNAwHKAy/xw+mfcPumEsY+259XYVvwNIXWCXFbSqHh3sLb9Nkw
ssKj/ohBrfJeSdfwxCucKzL1TSJlWJLIUSsMALRp0WBwiYuN/AiopLCR5cTy8UHKhjbvyr0/W8jf
612iQx84NcQz10ins3XQLEbrDE0054uN72M7rNilO0Xam2GwCUeTHnKE4j3fVuNQ68d9Gn8czU7C
G42a1XfKU28F14Kdy27OLQVqKSivnjY64w8f9OTW1eJ64VRty2Qai2MLs7Z6oB+pZRnFnFbRvrr8
ZH43SEVPksg+5jMXe9DzBuLiOD/dcS7YNlvAT5L4o0pn/BVdJeKiuHgxJaH5et8xQVtKQmafyyHG
TKJSBqJWCmi2PEuG03OD/cfFpbLZS6QGS1lbz7D5bLQpd9na7aNIqKZwPwIExy23lhnI8RfRoNyu
xnowQAEl6n8k55fp2XSKeH2ITkyrt8VQPkTX0olfdmCYA15NgsePeidSgX9DGHSAUkYs+F1lxL4f
RxzLeanUT0mFGnkiP8l+P42waviY+/ZHX67ngaanvcMfbsYm9mYv93cOef6G9huurm57SqEx9QOg
+lZ75jU3janKWLNadKHrZ7t5a3NyFXnDTD8fEIiaLUSVmgu7v0CpFRnPjwKLo9eatQJWtgFgbxQT
/fCXLWabr5uj0/h7fpeSV4twQursCJODXVK42buaNb6DFiK+Dad5lEUGadY6UsEmhBDQYTUB3nD/
kqZZjYstrU2Dv+tUgKI8ou+m3fnPW6iovm2FjwXRgjpOBTH+TNcfnWSagG7so/n4i+Lm9U29UPeJ
qsNZQ1anyllMyzKjxbK2/Ilmm1AUnw4TmtFfB5xcqD/SFeHrxEBhCnpaisLhjRNubVOlMS9d22Sk
d9cK3DkMfVFrUmdNlPMnR2GdWFAH9HpLiLN2Z8gzCwSxys2Hl5lXRZFiiwpoWMb6eAhhEkCjcWad
2rM5s/HqbEhiITH6gT4ZRrN8BA+oRmG+oMzdTkSYwUiCyDTrU57HdaVLI7+1t+hCM8VGlu/hRRiG
I6zr5xohjL2A0X6zQ1rtv34jnASTDtKdnEL6YR7Zo9ftv7q0xju6HfwiMhrd9xoIkgq+Mq8lnA9h
hn3OJbXhtuT/SEtMYBZgsKsF0fLfb+kPxq/etfvpt3D5si/8/LB7ttNVfzQqkGV9khkN5PYELvc9
k6IGJMqUaRM0gGWuPkPfi15C1nP2QwjtCYV+0rC76MhpeVbCsr9zXxF8otKyXJsONCpZm2pjJxtn
RFoeyWNzJ/IcxliqiW6swil6jj0tKGGbE5TWLiRDqGAFyqgn8WrIlC1rg3bgsg7fqk2zPZWgvdCT
bGdDYUqQdSo7HZwxQsvtt8tuJFU0E2o0Z8qUQs/N/rD78RI1R+6fm7Ann3qvG9d1EReZi/5Kfa0A
craDcjlGYqouNm4PD2iH0EQX0ZMNP6Rb/Yv98tMKnOeUA4nzTMvuBcqoi7S/Bd2Q+bdznBL4mfau
B6B25AKVdgZdpArh1nYQGap7iRXTqZVtgEI4gjIUlqti7LmYgU+MNV7cuS+dO2YApbK4Hx0WuFV3
A1GgWCnnSIYyHOuSpjGftZB7NtOhQaUfk5BCTwY8c5I9ynfhj3f/SZGi/yNFrrE1NB4lkETfN7Tf
4tmNRY+k94B2l3MALIWdA9LeAKzGh0Nl3zCJwh5WCyFJnlIogGeOAfadGihpDngt6gtKKKAwV+wN
m99mC9UkEwbStQ/icKKQR700D2j4bo+zZip5brWsBaEriFyqGtE1EnmBQK7jWaeXAeUABMpU+RlY
0xOXZ3zkGDpKW+0v9I+fxshbca0ffqi0YUpQepd2DlwnK7lXZlpjqB5O2V1knSBR3+4KiWSmDEcS
eyUS0TXXzgvPlP2RDEoFYEWtXYhNq8z4VDDkeldqX5ZZXWsX8SlNCYC0nl//lhX72l5yUTTMJUY1
1jOREmjPjmHr/q2pXwoUphk9JRE0YRbJ7a7faT0Cul8qZCisTMrDgf38AlTdK02KEfj8VRllD4db
xj4Sj0fqEn+TadYU45PSXgFvfQggwtBnsfNPoL/haABjZhG6/HSBb+LIJtPBGHaevTgYioFSshBF
iLmskfj5PVUgIYP2PhAQhozhwRc2xcfhUgwhLFMmyXhTW8zEwnZAX7SneeAnZPSoQRDVOTyaEEgF
kocGMnU3KV8AiL+XwFXUdzli+tPEEVgx31c2Gzi2QJJOIBamWCfQRj1HpWLkKirIPIM6196RooIB
NxfwKj9VxjKwBejVz8bt+YxbL7x8+p2gOIxH8rHBPrOxF6tp/b4RGgz/1C5SSvcTruKSQbkcl2db
7N2QpEnqAV8BwmFfFHJCdkXKHdRnAV09+tCBqJ2k2wXxy9DOiJruFXtwwU4ACBU9LJo2b7y55sOz
RWuDPVf5E0iF4lJuLYmSxdJxHJ8e9+2suamZZIUL75OrGvumDYLfS9s6SNBoqV1yMgfVmyfPLk8G
apAKX5c1pDM9GIYq3lfKYFy5Tr8JnxyVRxFCWG6I+uNxfv5wcA8hdy49Mivt9fwo9evHjsf1uEKp
Pt17lUpbfyC+9S4QrZDInhjdAOeclo2xsFnfDD4Cwo5v6ppV0W107LfzKkHL1tvvXqtNoBG9s4wy
zUXPN2ustUv2HjG8kNyjD3MpZLlkeiqgVMmIMUcq8nEEZEFg/iApxWsOt6RFx+/XPCugKMYoGPwT
c4THemsVQGVmavHyhG6PniSxcA0w0z8GQ/7H/xZb6RhfhSavrw9juidWDCJOoq7G+u8FHhY7bj/K
n4OoqW+azID+OfgE3QMzhyISyu/PnmcVk6VpOqLtJtPhgfFkPYg2sny3TkF9SFbebFzkpbnG3aR+
2pJnEWjxPrh37nCN0ppQWzNhgVbxuDv0DiAhbjAAoX6UjC48O02xc9WyAXXI91qkWS1vQsd9MCwf
yPKIaH8YcykKlsqpWJWCNCIIPVRTiGJJh92mc8AWzWRa2rpuwywWE8aCAwhmknEqjzjCcHYKFUNJ
JyVT37esddhhzbDLPDjK7XDCB2YZ6DjYCtc/Qw6/aYydUWKLPkUW9VZUESTL9bvPV0r7qb4cityY
mmmQOw41Kr8ed2vVl+jKha4YOmXLPVG8+Z/t3Xy2b81au4psfnMMI99jjzsjy5bQZufIH7HPNpMG
9/e8Bh9zxeKZGL4OF1GptL1sFylt75L/94h9f4ASkki6GcnJQKkrE+NhDWjN4Z3eG5Md79IBmmDd
vMih8uC51ZfUSzM5IHbOvpeFsmRhxFVXAhfmiUqCPxDPcP3Cet+1OI0rINX+YKQJ+gTCIeG2soSL
W1gsFfDxEh6aspd+cVA7vPNk3I6xKcrjdY0uw7+xYo905515pO3mOFsK3Jgcd0FSjIfTodLg8kIO
DDmty7FeDFsHacRv4lUazaKRew16yvi3lQ+6RmoIjHmxxUssu2w4W0aKuMRY+jvp24IugVA/w9jq
v4GxOvwY9KS02fECJwdoiaLVMMs9gk5vnvFPiKuzVziLdkYCUP+vkFjeX0MNM7FZXuE0aDn4kMq/
QKTKk11RCn7VBfNcNW19NVuQNhLHlqptiPtkKS1sUFsNh79RXL1Di9jJIKJJzxb7Np+bD6XB/r2V
EGvcNlyj5Zd0M5F/Ozamcuh3FtkJeMhRmRc7h5HcgJ02Ty+nTLdiQIg5wkisw20HjTijAjPrR7oS
eONyq17hyQ6+3GhszjVwqvCe23f/tRIs6Gwd3djTyWI8lVCrnDUp6m7N/QwISwNXwfcjSWJCN2Hq
gnvTEBP6XhGJMTLGVvAdUbaZ/5iho2KIitYTvb09XHegh6ak7GYm55zIGz+tUi4dmjOlWuSEPJkM
qpDGqBZuynIX8hRv2RODt47tNVsJ6XB0bTVDBBykhrXGJhlMp/cy5pn4E/9fX2hl2F6Dbf797RUn
e0r2aUbhDGWnoW7mqs7xnIOsjGrmunvFotSId7KSgV5XwQTkXI8moS8zZZnpMCKYl1WPwSH9l0cB
huq92fHR4jys6TRlU/4ZwFBht45hUvSdVHLrXp1qQi09paaisL2CxJXoHAIDntEWplhSmjGyyzrr
ZPP9yois1xl8+cHC30HUZqRPyK4ojUwgnO6IwtWvYrFnK9VmbCMw37AxML5OslvqjIDKYs48l+mq
MROALjOezZYljyxjeZP3Vh/0kwYtoE0BGWuK5IL0vshhqfvLa91sAXCLvEK/bTuEV6FRgWesrBd4
GDoYx3KhAb4TW80U5psmk38Oqi/7Evf495AHEHXBdXjMjnBR1GmX01XNzWzqFL1uILN+mLu07xdS
CjWL03sJrII/UD68BnQs8CFcT9XBn51F5Q5vhd5xkviYKkxG4V5B+Pmv6f7Y2GUu/cGNO04ZGhGZ
3o3gmxOHZwCHuRmS5vA0NIwHJjr4gCG1dtpqWW8lbn7BRo+dWjwmfw8Wkk30h4INEMR7ifEaNxgQ
KPdBJnIMx2wilZiQ1cwuFHDwcb8QcJTnBpb208O4vLFhxJ6PXnz/Y/AdYDS8pC6Ei1o2bH2Pikk5
JC0soLEKifDWsBUZLUGAFcqS7ny3v48znKW/KI8Ry1JKp5iUTlVOC6qXqmqIiplwU7l8/YJJ+3SK
HqN/wWQ/0h3ScDWS4bzDRbTECDrzwrNzviMRbMTJoJSpF5UM4k9/4ofI/R5SsudC/TYAO3WWd0QI
55ZzXEON11nG3bxOpSwVtIX4U+ioy/B0cDFyzkxuso0caR5sS3NeupFrTfdTMN2O9o8JnFNPhC9e
tHYL5qhDT6wpF1BRtK+bo5u/i5BRvaOXBeW9/vFAARrDaxFNowstEDSpi7baN7+eSEbx8TZViT03
xadpKarsjjbLFK4mpVBOWT/IkznDaPQ8veCIGcMJJJzTCaiZb7T3A+Q2ZIX+SK0gVNCBejPdlHjc
RJ/qrOoeGdJuNCJFZ3fG1Tf7toXJIzKlYffQgb+dfSIoQxGiSd6vfw64V46zV8mFyCjdfCEWvd5Y
KTLhRs7Wjg+WbMgfUVLzM3/CmkTKYQ3coBd1NqJJwnqphoy6kzrqC5cBBHpxBcLWHFJpHMSBPWIL
uKWMsknpEFweazZs4p1LhrmFQg/bcCRkyojyUMSuI2JhlsIRoE9/9buXbMg6F7dPzLpJzh/dZa9x
erx/4VO31s1NESNYAPQyjQPwGeoMFLgz5d5v4qdjnKx9UXFuRp6JUO8gMHjGrhwKnuENxgA0Xsrn
rEk5nJ7P+fXiYNL3E3/94akS90CLaAUK8CwfjIcMKQPeC8GFyYCgIAQS2WOznq5JmcZCdrePW49A
OjFihYiupjdaHnHbhbVIe/YoZpHL2hY8Momx9z+XSRhvd0EcwAybWxfOexTQlZdrrd0PfVbz5v3M
l/nnshiRsQ6VBAYqg0XjUuax8Z+Gk8od0A1PYtqnrIfy0AV0Rv2iF2rwWTJouJXqWrBwIM8KDArQ
ETD369hyTH9bqUeuF3iQHPh/I+nOkaavYukZ0cmd4y2L8+zOyY/9HwSPUsm8C754bx4y8RUH5IEQ
JtIgBSRg6zRLJlQIsjvS44zGFqSpCEKkG8LcATU5DmYZqIcm4i8iC6z+qiF6ZIf+Ix2xUyIDbqWN
r04QZh+3K00cZuozzSIswwI6U0Qx46NguIJGjX124Luw8NBvQ1KShnEiKvbWKWWdkFEqdl0QAeqQ
Q8UazyONmS0DXPnymaP+mf9F7UvePouWC7Z0goPtRo+o66hiy5nP8z4og1KASxuGAs1Xbj0YTmLu
a7DVNbZbcWNs7HXquiftpRD7D+I+RzjYzz5lpg==
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
