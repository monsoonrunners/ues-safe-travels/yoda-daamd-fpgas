`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Cape Town
// Engineer: Brandon Ferreira (FRRBRA002)
//           Gavin Foster (FSTGAV002)
//           Lefteri Vlahos (VHLLEF001)
// 
// Create Date: 12.05.2022 14:16:56
// Design Name: 
// Module Name: Compare
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module Compare(
    input [31:0] AudioIn,
    input clk,
    input reset,
    output reg [31:0] MaxOut,
    output reg [31:0] MinOut
    );
    
    //////////////////////////////////////////////////////////////////////////////
    // Compare Module Initial Conditions
    //////////////////////////////////////////////////////////////////////////////
    reg clkd;
    initial
    begin
        MaxOut = 'h00000000;
        MinOut = 'hffffffff;
        clkd = 0;
    end
    
    //////////////////////////////////////////////////////////////////////////////
    // Operational Section
    //////////////////////////////////////////////////////////////////////////////
    
    always@(posedge clk) begin
        
        if (clkd == 1)
        begin
            if (reset == 1)
            begin
                MaxOut <= 0;
                MinOut <= 'hffffffff;
            end
            
            else
            begin
                if (AudioIn>MaxOut) MaxOut = AudioIn;
                else if (AudioIn<MinOut) MinOut = AudioIn;
            end
        end
        clkd = 1;
    end
    
endmodule

