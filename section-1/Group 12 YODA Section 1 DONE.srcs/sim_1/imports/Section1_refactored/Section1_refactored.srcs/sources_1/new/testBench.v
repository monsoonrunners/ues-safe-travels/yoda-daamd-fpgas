`timescale 1ns / 1ps
module testBench;
     // Set up wires, parameters and registers
    reg [0:0] clk;
    reg [0:0] reset;
    wire [31:0] MinOut;
    wire [31:0] MaxOut;
    wire [31:0] AudioIn;
    
    reg [16:0] addra;
    parameter clkRate = 5; // FPGA clock rate for one half cycle, so clock speed of 100MHz
    parameter clkRate_bram = 22676; //Clock speed to simulate 44.1 kHz audio input
    
    // Initialise modules
    Compare compare_ut(
        .AudioIn (AudioIn),
        .clk (clk),
        .reset (reset),
        .MaxOut (MaxOut),
        .MinOut (MinOut)
    );
    
    Audio_Out audio_ut (
        .clka (clk),
        .wea (0),
        .addra (addra),
        .dina (1'bz), //set to high-impedence as we will never be inputting data
        .douta(AudioIn)
    );
    // Initial Values
    initial begin
        addra <= 0;
        clk <= 0;
        reset <= 0;
    end
    // Clock rate for FPGA
    always #clkRate begin
        clk = ~clk;
    end
    // Clock rate to increment audio input
    always #clkRate_bram begin
        addra = addra + 1;
        if (addra >= 1024) addra = 0;
    end
    
endmodule