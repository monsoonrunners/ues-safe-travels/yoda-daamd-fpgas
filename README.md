# EEE4120F DAAMD FPGAS
Digital Accelerator for Audio Mean and Deviation Filtering Per Group of Audio Samples

### Members
- VLHLEF001
- FSTGAV002
- FRRBRA002

## Project Description
The code in this repository implements a digital accelerator for audio filtering in Verilog which is targeted at the Nexys A7 FPGA board.
The code given includes project files for Xilinx Vivado, as well as two Python golden measures - one for section 1, and one for section 2.

### Section 1
Section 1 implements a simple min/max compare module which finds the absolute maximum and minimum an input audio signal.

### Section 2
Section 2 implements 3 modules.
The first module is a compare module whose purpose is to divide an audio signal into several intervals, and storing each of the interval-wise maxima and minima into Block Read-Only Memory (BRAM).
Next, a mean module calculates separate means for the maxima and minima and this is then used as a centre point in the filter module.
Finally, the filter module which takes in a constant deviation and is used to filter out all the maxima and minima which are > (mean + deviation) and < (mean - deviation).
