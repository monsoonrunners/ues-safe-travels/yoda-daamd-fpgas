#==================================================================
# Start Of Code
#==================================================================
# Import Stage
#==================================================================
import numpy
import time
#==================================================================
# Constants
#==================================================================
SLICE_COUNT = 6			# Slice Amount
FILTER_CONSTANT = 10		# Filter Constant

MIN_MEAN = 0			
MAX_MEAN = 0			

MAX = '0x00000000'	# Initialize the Min Value 
MAX_INT = int(MAX,16)		

MIN = '0xFFFFFFFF'	# Initialize the Max Value
MIN_INT = int(MIN,16)		
		
MAX_BUFFER = []	 # Contains Maximum values for each time slice
MIN_BUFFER = []	 # Contains Minimum values for each time slice
#==================================================================
# Functions
#==================================================================

#==================================================================
# DataIn will read a data file into an array for later processing
#==================================================================

def DataIn(DataFile):
	DataItem = open(DataFile,"r")
	Data = DataItem.read().splitlines()
	DataItem.close()
	return Data
#==================================================================
# MeanCalc will take in an array and calculate the mean for the 
# data items
#==================================================================

def MeanCalc(DataIn):
	Mean = 0

	for i in DataIn:
		Mean = Mean + i
	Mean = Mean/(len(DataIn))

	return Mean
#==================================================================
# Filter will first take a value in a data array and subtract its 
# mean value from it. If the value falls outside of a set constant 
# range, it will be removed. This process is done for both a 
# Maximum buffer and Minimum buffer.
#==================================================================

def Filter(DataMin, MinMean, DataMax, MaxMean, FilterConst):
	Output_Min = []
	Output_Max = []

	for Min in DataMin:
		sum = Min - MinMean
		if(abs(sum) < FilterConst):
			Output_Min.append(0)
		else:
			Output_Min.append(Min)

	for Max in DataMax:
                sum = Max - MaxMean
                if(abs(sum) > FilterConst):
                        Output_Max.append(0)
                else:
                        Output_Max.append(Max)

	return Output_Min, Output_Max
#==================================================================
# Golden Code
#==================================================================
# Data Prep: 
# Will read data into an array and convert the values into Hex
#==================================================================
myData = DataIn("myfile.txt")

for i in range(0,len(myData),1):
	tempData = int(myData[i],16)
	myData[i] = hex(tempData)
#==================================================================

start = time.time()  # Starts a timer for measuring execution time

# Splits data based on amount of time slices
dataSlices = numpy.array_split(myData,SLICE_COUNT)
#==================================================================
# Data Process: 
# Performs a Min-Max compare for all data items and time slices
# Fills Max and Min data buffers based on time slices
# Performs a MeanCalc for the Max and Min buffers
# Filters out values using Filter function
#==================================================================
for slice in dataSlices:
	for num in slice:
		Int_num = int(num,16)

		if (MAX_INT<Int_num):
			MAX_INT = Int_num
		if (MIN_INT>Int_num):
			MIN_INT = Int_num

	MAX_BUFFER.append(MAX_INT)
	MIN_BUFFER.append(MIN_INT)

	MAX_INT = int(MAX,16)
	MIN_INT = int(MIN,16)

MIN_MEAN = MeanCalc(MIN_BUFFER)
MAX_MEAN = MeanCalc(MAX_BUFFER)

MinOut, MaxOut = Filter(MIN_BUFFER, MIN_MEAN, MAX_BUFFER, 
			MAX_MEAN, FILTER_CONSTANT)
#==================================================================
# Ends timer function for measuring execution time

end = time.time() 
#==================================================================
# Prints out results
#==================================================================

print("==========================================================")
print("MAX BUFFER VALUES ARE: ",MAX_BUFFER)
print("MIN BUFFER VALUES ARE: ",MIN_BUFFER)
print("==========================================================")
print("MAX BUFFER FILTER VALUES ARE: ",MaxOut)
print("MIN BUFFER FILTER VALUES ARE: ",MinOut)
print("==========================================================")
print("EXECUTION TIME IS: ",end-start)
print("==========================================================")

#==================================================================
# End Code
#==================================================================

