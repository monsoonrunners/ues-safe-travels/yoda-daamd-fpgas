#==================================================================
# Start Of Code
#==================================================================
# Import Stage
#==================================================================
import numpy
import time
#==================================================================
# Constants
#==================================================================
MAX = '0x00000000'		# Initialize Max value
MAX_INT = int(MAX,16)

MIN = '0xFFFFFFFF'		# Initialize Min value
MIN_INT = int(MIN,16)
#==================================================================
# Functions: DataIn Reads a data file, i.e) Input data
#==================================================================
def DataIn(DataFile):
        DataItem = open(DataFile,"r")
        Data = DataItem.read().splitlines()
        DataItem.close()
        return Data
#==================================================================
# MAIN
#==================================================================
myData = DataIn("myfile.txt") # Reads input data into an array

#==================================================================
# Converts data to Hex for processing
#==================================================================

for i in range(0,len(myData),1):
        tempData = int(myData[i],16)
        myData[i] = hex(tempData)
#==================================================================

start = time.time()	# Start Timer for measuring execution time

for num in myData:
	Int_num = int(num,16)

	if (MAX_INT<Int_num):
		MAX_INT = Int_num
	if (MIN_INT>Int_num):
		MIN_INT = Int_num


end = time.time()	# End Timer for measuring execution time

#==================================================================
# Prints out results
#==================================================================

print("==========================================================")
print("MAX VALUE IS: ",MAX_INT)
print("MIN VALUE IS: ",MIN_INT)
print("==========================================================")
print("EXECUTION TIME IS: ",end-start)
print("==========================================================")

#==================================================================
# End
#==================================================================
